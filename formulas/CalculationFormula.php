<?php

namespace app\formulas;

use app\models\Calculation;
use app\models\Region;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class CalculationFormula
 * @package app\formulas
 */
class CalculationFormula extends Component
{
    /**
     * @var Calculation
     */
    public $model;

    /**
     * @return string
     */
    public function getZak1()
    {
        return $this->model->object_name;
    }

    /**
     * @return string
     */
    public function getZak2()
    {
        return $this->model->customer_name;
    }

    /**
     * @return string
     */
    public function getZak3()
    {
        return $this->model->certificate_of_consent;
    }

    /**
     * @return string
     */
    public function getZak4()
    {
        return $this->model->object_address;
    }

    /**
     * @return string
     */
    public function getZak5()
    {
        return $this->model->type_oil;
    }

    /**
     * @return string
     */
    public function getZak6()
    {
        return $this->model->calorific_value;
    }

    /**
     * @return string
     */
    public function getZak7()
    {
        return $this->model->year_start_consume;
    }

    /**
     * @return string
     */
    public function getZak8()
    {
        $objectType = $this->model->object_type;
        $objectType = ArrayHelper::getValue(Calculation::objectTypeLabels(), $objectType);

        return $objectType;
    }


    /**
     * @return string
     */
    public function getK1()
    {
        $region = $this->model->region_id;
        $region = Region::findOne($region);

        if ($region == null) {
            return null;
        }

        return $region->name;
    }

    /**
     * @return string
     */
    public function getK2()
    {
        return $this->model->temperature_average;
    }

    /**
     * @return int
     */
    public function getK3()
    {
        return $this->model->temperature_calculated;
    }

    /**
     * @return string
     */
    public function getK4()
    {
        return $this->model->winder_speed;
    }

    /**
     * @return string
     */
    public function getK5()
    {
        return $this->model->duration_heating;
    }

    /**
     * @return string
     */
    public function getK6()
    {
        return $this->model->c_alfa;
    }
}