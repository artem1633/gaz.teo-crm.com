<?php

namespace app\formulas;

use app\models\Equipment;
use app\models\ObjectsTech;
use app\models\ObjectsTechType;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class TehFormula
 * @package app\formulas
 */
class TehFormula extends Component
{
    /**
     * @var ObjectsTech
     */
    public $model;

    public function getTeh1()
    {
        return $this->model->objects->name;
    }

    public function getTeh2()
    {
        return $this->model->objects->count_object;
    }

    public function getTeh3()
    {
        $purposeSystem = $this->model->purpose_system;
        $purposeSystem = ArrayHelper::getValue(ObjectsTechType::getTypes(), $purposeSystem);

        return $purposeSystem;
    }

    public function getTeh4()
    {
        return $this->model->consumption_thermal_units;
    }

    public function getTeh5()
    {
        return $this->model->consumption_equivalent_fuel;
    }

    public function getTeh6()
    {
        return $this->model->day_per_week;
    }

    public function getTeh7()
    {
        return $this->model->norm_document;
    }

    public function getTeh8()
    {
        return $this->model->amount;
    }

    public function getTehQhd()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }

        if($equipment == null){
            return 0;
        }

        return $equipment->formula->getO11() * $equipment->formula->getO3() * $equipment->formula->getO4();
    }

    public function getTehQh()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }
        if($equipment == null){
            return 0;
        }  

        return round($this->getTehQhd(), 2) * $this->model->objects->calculation->formula->getZak6() * pow(10, -6) * $o2;
        // return round($this->getTehQhd(), 2) * $this->model->objects->calculation->formula->getZak6() * pow(10, -6);
        // return $equipment->formula->getO11() * $this->model->objects->calculation->formula->getZak6() * pow(10, -6) * $o2;
    }

    public function getTehQh1()
    {
        return $this->getTehQh() * 1163;
    }

    public function getTehQn()
    {
        return ($this->getTeh8() * $this->getTeh4() * pow(10, 3)) / 1000000;
    }

    public function getTehGn()
    {
        try {
            $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }
//            return ($this->getTeh8() / 1000 / $this->model->objects->calculation->formula->getZak6() * pow(10, 3)) / $o2;
            return (round($this->getTehQn(), 2) / $this->model->objects->calculation->formula->getZak6() * pow(10, 3)) / $o2;
        } catch(\Exception $e) {
            return 0;
        }
    }

    public function getTehGut()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }
//        return ($this->getTeh8() / 1000 / 7000 * pow(10, 3)) / $o2;
        return ($this->getTehQn() / 7000 * pow(10, 3)) / $o2;
    }
} 