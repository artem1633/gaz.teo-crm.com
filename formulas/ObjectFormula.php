<?php

namespace app\formulas;

use Yii;
use app\models\Objects;
use yii\base\Component;

/**
 * Class ObjectFormula
 * @package app\formulas
 */
class ObjectFormula extends Component
{
    /**
     * @var Objects
     */
    public $model;

    public function getO1()
    {
        return $this->model->name;
    }

    public function getO2()
    {
        return $this->model->count_object;
    }

    public function getO3()
    {
        return 0;
    }

    public function getO4()
    {
        return $this->model->count_object;
    }

    public function getO5()
    {
        return 0; // TODO: Не правильный счет в ТЗ
    }

    public function getO6()
    {
        return $this->model->power_v;
    }

    public function getO7()
    {
        return $this->getO6() / 1164;
    }

    public function getO8()
    {
        return $this->getO6() * $this->getO4() * $this->getO7() * $this->getO4();
    }

    public function getO9()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->calculation_id, 'number_group' => $this->model->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }

        return 1000000 / 70000 / $o2;
    }

    public function getO10()
    {
        return $this->model->calculation->object_type;
    }

    public function getO11()
    {
        try {
            return $this->getO7() / $this->model->calculation->formula->getZak6() * 1000000 / $this->getO2();
        } catch (\Exception $e){
            Yii::warning($e->getMessage(), 'Exception while O11 counting');
            return 0;
        }
    }
}