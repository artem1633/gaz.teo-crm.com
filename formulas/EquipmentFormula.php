<?php

namespace app\formulas;

use Yii;
use app\models\Equipment;
use yii\base\Model;
use app\models\Objects;
use app\models\ObjectsOv;
use app\models\ObjectsGvs;
use app\models\ObjectsTech;

class EquipmentFormula extends Model
{
    /**
     * @var Equipment
     */
    public $model;

    public function getO1()
    {
        return $this->model->name_equipment;
    }

    /**
     * @return float
     */
    public function getO2()
    {
        if($this->model->KPD == null){
            return 1;
        }

        return $this->model->KPD / 100;
    }

    public function getO3()
    {
        if($this->model->coefficient_unambiguity == null){
            return 1;
        }

        return $this->model->coefficient_unambiguity;
    }

    public function getO4()
    {
        if($this->model->count_equipment == null){
            return 1;
        }

        return $this->model->count_equipment;
    }

    public function getO5()
    {
        // var_dump($this->getObQhMax1());
        // var_dump($this->getO3());
        // var_dump($this->getO4());
        // var_dump($this->getO6());
        // exit;

        $object = Objects::find()->where(['calculation_id' => $this->model->calculation_id, 'number_group' => $this->model->number_group])->one();

        if($object == null){
            return -1;
        }

        try {
            if($object->type_object == Objects::TYPE_TECH)
            {
                $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                // return round(round($object->formula->getTehQh(), 4) / ($object->formula->getTeh6() / 7 * 24 * 365 / $object->formula->getTehQn()), 2);
                // return round($object->formula->getTehQh(), 4).' / ('.$object->formula->getTeh6().' / 7 * 24 * 365 / '.$object->formula->getTehQn().')';

                // return $this->getObQh().' / ('.$object->formula->getTeh6().' / 7 * 24 * 365 * '.$this->getObQhmax().')';

                $o5 = round($this->getObQh() / ($object->formula->getTeh6() / 7 * 24 * 365 * $this->getObQhmax()), 2);

                Yii::warning($this->getObQh()." / "."(".$object->formula->getTeh6().' / 7 * 24 * 365 * '.$this->getObQhmax().') = '.$o5, '05 calculation in Tech objects');

                return $o5;
            } else {

                $o5 = round($this->getObQhMax1() / floatval($this->getO3()) / floatval($this->getO4()) / floatval($this->getO6()), 2);

                Yii::warning($this->getObQhMax1()." / ".floatval($this->getO3())." / ".floatval($this->getO4())." / ".floatval($this->getO6())." = ".$o5, 'O5 calculation in OV and GVS objects');

                return $o5;
            }
        } catch (\Exception $e)
        {
            return 0;
        }
    }

    public function getO6()
    {
        $object = Objects::find()->where(['calculation_id' => $this->model->calculation_id, 'number_group' => $this->model->number_group])->one();

        if($object == null){
            return -1;
        }

        if($object->type_object == Objects::TYPE_TECH){
            $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

            return $object->formula->getTehQh1() / $this->getO3() / $this->getO4();
        }

        return $this->model->power_v;
    }

    public function getO7()
    {
        // return $this->getO6() / 1164;
        return round($this->getO6() / 1163, 5);
    }

    public function getO8()
    {
        // return $this->getO6() * $this->getO4() * $this->model->pover_g * $this->getO4();
        // return $this->getO6() * $this->getO4() * $this->getO3() * $this->model->pover_g * $this->getO4() * $this->getO3();
        return round($this->getO3() * $this->getO6() * $this->getO4(), 2);
    }

    public function getO8_1()
    {
        return round($this->getO7() * $this->getO4() * $this->getO3(), 5);
    }

    public function getO9()
    {
        return round(1000000 / 7000 / $this->getO2(), 1);
    }

    // public function getO10()
    // {
        // return $this->model-> 
    // } 

    public function getO11()
    {
        try {
            $object = Objects::find()->where(['calculation_id' => $this->model->calculation_id, 'number_group' => $this->model->number_group])->one();

            if($object == null){
                return -1;
            }

            if($object->type_object == Objects::TYPE_TECH){
                // var_dump($this->model);
                // exit;
                return $this->model->consumption_e;
            }

            return round($this->getO7(), 5) / $this->model->calculation->formula->getZak6() * 1000000 / $this->getO2();
        } catch (\Exception $e){
            return 0;
        }
    }

    public function getO12()
    {
        try {
            return $this->getO11() * $this->getO3() * $this->getO4();
        } catch (\Exception $e){
            return 0;
        }
    }

    public function getObQhmax()
    {
        $eq = $this->model;

        $objects = Objects::find()->where(['calculation_id' => $eq->calculation_id, 'number_group' => $eq->number_group])->all();

        if(count($objects) == 0){
            return 0;
        }

        $sum = 0;

        foreach($objects as $object)
        {
            if($object->type_object == Objects::TYPE_OV)
            {
                $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getOvQomax();
                $sum += $object->formula->getOvQvmax();
            } else if($object->type_object == Objects::TYPE_GVS)
            {
                $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getGvsQhm();
            } else if($object->type_object == Objects::TYPE_TECH)
            {
                $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getTehQh();
            }
        }

        return $sum;
    }

    public function getObQhmax1()
    {
        $eq = $this->model;

        $objects = Objects::find()->where(['calculation_id' => $eq->calculation_id, 'number_group' => $eq->number_group])->all();

        if(count($objects) == 0){
            return 0;
        }

        $sum = 0;

        foreach($objects as $object)
        {
            if($object->type_object == Objects::TYPE_OV)
            {
                $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getOvQomax1();
                $sum += $object->formula->getOvQvmax1();
            } else if($object->type_object == Objects::TYPE_GVS)
            {
                $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getGvsQhm1();
            } else if($object->type_object == Objects::TYPE_TECH)
            {
                $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getTehQh1();
            }
        }

        return $sum;
    }

    public function getObQh()
    {
        $eq = $this->model;

        $objects = Objects::find()->where(['calculation_id' => $eq->calculation_id, 'number_group' => $eq->number_group])->all();

        if(count($objects) == 0){
            return 0;
        }

        $sum = 0;

        foreach($objects as $object)
        {
            if($object->type_object == Objects::TYPE_OV)
            {
                $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getQo();
                $sum += $object->formula->getQv();
            } else if($object->type_object == Objects::TYPE_GVS)
            {
                $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getGvsQh();
            } else if($object->type_object == Objects::TYPE_TECH)
            {
                $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getTehQn();
            }
        }

        return $sum;
    }

    public function getObGut()
    {
        $eq = $this->model;

        $objects = Objects::find()->where(['calculation_id' => $eq->calculation_id, 'number_group' => $eq->number_group])->all();

        if(count($objects) == 0){
            return 0;
        }

        $sum = 0;

        foreach($objects as $object)
        {
            if($object->type_object == Objects::TYPE_OV)
            {
                $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getOvGuto();
                $sum += $object->formula->getOvGutv();
            } else if($object->type_object == Objects::TYPE_GVS)
            {
                $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getGvsGut();
            } else if($object->type_object == Objects::TYPE_TECH)
            {
                $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                $sum += $object->formula->getTehGut();
            }
        }

        return round($sum / 1000, 5);
    }
}