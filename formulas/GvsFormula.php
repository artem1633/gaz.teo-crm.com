<?php

namespace app\formulas;

use app\models\Equipment;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\ObjectsGvs;
use app\models\ObjectsGvsType;

class GvsFormula extends Component
{
    /**
     * @var ObjectsGvs
     */
    public $model;

    public function getGvs1()
    {
        return $this->model->objects->name;
    }

    public function getGvs2()
    {
        return $this->model->objects->count_object;
    }

    public function getGvs3()
    {
        $purposeSystem = $this->model->purpose_system;
        $purposeSystem = ArrayHelper::getValue(ObjectsGvsType::getTypes(), $purposeSystem);

        return $purposeSystem;
    }

    public function getGvs4()
    {
        return $this->model->water_consumption_rate;
    }

    public function getGvs5()
    {
        return $this->model->count_work_day;
    }

    public function getGvs6()
    {
        return $this->model->work_per_day;
    }

    public function getGvs7()
    {
        return $this->model->calculation_regarding_quantity;
    }

    public function getGvs8()
    {
        return $this->model->count_consumers;
    }

    public function getGvs9()
    {
        return $this->model->heat_loads_known;
    }

    public function getGvs10()
    {
        return $this->model->heating_load;
    }

    public function getGvs11()
    {
        return $this->model->ventilation_load;
    }

    public function getGvs12()
    {
        return $this->getGvs10() + $this->getGvs11();
    }


    public function getGvsQhm($enableCount = true)
    {
        if($this->getGvs9() == 1){
            $qhm = $this->getGvs12();
        } else {
            $qhm = ($this->getGvs4() * $this->getGvs8() * (55 - 5) * pow(10, -6)) / $this->getGvs6();
        }

        if($enableCount){
            return $qhm * $this->getGvs2();
        }

        return $qhm;
    }

    public function getGvsQhm1($enableCount = true)
    {
        return $this->getGvsQhm($enableCount) * 1163;
    }

    public function getGvsQhc()
    {
        return $this->getGvsQhm() * 1 * (55 - 15)/(55-5);
    }

    public function getGvsQhc1()
    {
        return $this->getGvsQhc() * 1163;
    }

    public function getGvsNO()
    {
        return $this->model->objects->calculation->formula->getK5() / 7 * $this->getGvs5() * $this->getGvs6();
    }

    public function getGvsNs()
    {
        return (365 - $this->model->objects->calculation->formula->getK5()) / 7 * $this->getGvs5() * $this->getGvs6();
    }

    public function getGvsQh()
    {
        return $this->getGvsQhm() * round($this->getGvsNO()) + $this->getGvsQhc() * round($this->getGvsNs());
    }

    public function getGvsGnch()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }

        return ($this->getGvsQhm() / $this->model->objects->calculation->formula->getZak6() * pow(10, 6)) / $o2;
    }

    public function getGvsGn()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }

        return (round($this->getGvsQh(), 3) / $this->model->objects->calculation->formula->getZak6() * pow(10, 3)) / $o2;
    }

    public function getGvsGut()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }

        return ($this->getGvsQh() / 7000 * pow(10, 3)) / $o2;
    }
}