<?php

namespace app\formulas;

use Yii;
use app\models\Equipment;
use app\models\ObjectsOv;
use app\models\ObjectsOvType;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class OvFormula
 * @package app\formulas
 */
class OvFormula extends Component
{
    /**
     * @var ObjectsOv
     */
    public $model;

    public function getOv1()
    {
        return $this->model->objects->name;
    }

    public function getOv2()
    {
        return $this->model->objects->count_object;
    }

    public function getOv3()
    {
        $types = ObjectsOvType::getTypes();
        $type = ArrayHelper::getValue($types, $this->model->building_type);

        return $type;
    }

    public function getOv4()
    {
        return $this->model->building_volume;
    }

    public function getOv5()
    {
        return $this->model->specific_thermal_characteristic_heating;
    }

    public function getOv6()
    {
        return $this->model->specific_thermal_characteristic_ventilation;
    }

    public function getOv7()
    {
        return $this->model->building_height;
    }

    public function getOv8()
    {
        return $this->model->internal_temperature;
    }

    public function getOv9()
    {
        return $this->model->heat_loads_known;
    }

    public function getOv10()
    {
        return $this->model->heating_load;
    }

    public function getOv11()
    {
        return $this->model->ventilation_load;
    }

    public function getOv12()
    {
        return $this->getOv10() + $this->getOv11();
    }

    public function getOvKip()
    {
        $k3 = $this->model->objects->calculation->formula->getK3();
        $k4 = $this->model->objects->calculation->formula->getK4();
        return 0.01 * sqrt(2 * 9.8 * $this->getOv7() * (1 - (273 + $k3) / (273 + $this->getOv8())) + pow($k4, 2));
    }

    public function getOvQomax($enableCount = true)
    {
        $ovQomax = null;
        if ($this->getOv9() == 1) {
            $ovQomax = $this->getOv10();
        } else {
            $k6 = $this->model->objects->calculation->formula->getK6();
            $k3 = $this->model->objects->calculation->formula->getK3();

            $kip = round($this->getOvKip(), 3);

            $ovQomax = $k6 * $this->getOv4() * $this->getOv5() * ($this->getOv8() - $k3) * (1 + $kip) * pow(10, -6);

            Yii::warning("{$k6} * ".$this->getOv4()." * ".$this->getOv5()." * (".$this->getOv8()." - ".$k3.") * (1 + ".$kip.") * 0.000001 = {$ovQomax}", 'ovQomax counting');
        }

        if($enableCount){
            return $ovQomax * $this->getOv2();
        }

        return $ovQomax;
    }

    public function getOvQomax1($enableCount = true)
    {
        return $this->getOvQomax($enableCount) * 1163;
    }

    public function getQo()
    {
        try {
            $k3 = $this->model->objects->calculation->formula->getK3();
            $k2 = $this->model->objects->calculation->formula->getK2();
            $k5 = $this->model->objects->calculation->formula->getK5();

            // $result = (round($this->getOvQomax(), 3) * 24 * ($this->getOv8() - $k2) * $k5) / ($this->getOv8() - $k3);
            $result = (round($this->getOvQomax(), 4) * 24 * ($this->getOv8() - $k2) * $k5) / ($this->getOv8() - $k3);

            $ovQomax = round($this->getOvQomax(), 4);

            Yii::warning("({$ovQomax} * 24 * ({$this->getOv8()} - {$k2}) * {$k5}) / ({$this->getOv8()} - {$k3}) = {$result}", 'OvQo calc');

            return $result;
        } catch(\Exception $e){
            Yii::warning($e->getMessage(), 'Exception while ovQo counting');
            return 0;
        }
    }

    public function getOvQvmax($enableCount = true)
    {
        $ovQvmax = null;
        if ($this->getOv9() == 1) {
            $ovQvmax = $this->getOv11();
        } else {
            $k6 = $this->model->objects->calculation->formula->getK6();
            $k3 = $this->model->objects->calculation->formula->getK3();

            $ovKip = round($this->getOvKip(), 2);

            // $ovQvmax = $k6 * $this->getOv4() * $this->getOv6() * ($this->getOv8() - $k3) * (1 + $ovKip) * 0.000001;
            $ovQvmax = $k6 * $this->getOv4() * $this->getOv6() * ($this->getOv8() - $k3) * 0.000001;

            // $ovQvmax = floor($ovQvmax*pow(10,4))/pow(10,4);
            $ovQvmax = round($ovQvmax, 4);
        }

        if($enableCount){
            return $ovQvmax * $this->getOv2();
        }

        return $ovQvmax;
    }

    public function getOvQvmax1($enableCount = true)
    {
        return $this->getOvQvmax($enableCount) * 1163;
    }

    public function getQv()
    {
        try{
            $k3 = $this->model->objects->calculation->formula->getK3();
            $k2 = $this->model->objects->calculation->formula->getK2();
            $k5 = $this->model->objects->calculation->formula->getK5();

            return ($this->getOvQvmax() * 24 * ($this->getOv8() - $k2) * $k5) / ($this->getOv8() - $k3);
        } catch (\Exception $e){
            return 0;
        }
    }

    public function getOvGncho()
    {
        try{
            $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }
            return round($this->getOvQomax(), 4) / $this->model->objects->calculation->formula->getZak6() * pow(10, 6) / $o2;
        } catch(\Exception $e){
            return 0;
        }
    }

    public function getOvGnchv()
    {
        try {
            $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }
            return $this->getOvQvmax() / $this->model->objects->calculation->formula->getZak6() * pow(10, 6) / $o2;
        } catch (\Exception $e){
            return 0;
        }
    }

    public function getOvGnch()
    {
        return $this->getOvGncho() + $this->getOvGnchv();
    }

    public function getOvGno()
    {
        try {
            $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }
            return $this->getQo() / $this->model->objects->calculation->formula->getZak6() * pow(10, 3) / $o2;
        } catch (\Exception $e){
            return 0;
        }
    }

    public function getOvGnv()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }
        return $this->getQv() / $this->model->objects->calculation->formula->getZak6() * pow(10, 3) / $o2;
    }

    public function getOvGn()
    {
        return $this->getOvGno() + $this->getOvGnv();
    }

    public function getOvGuto()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }
        return $this->getQo() / 7000 * pow(10, 3) / $o2;
    }

    public function getOvGutv()
    {
        $equipment = Equipment::find()->where(['calculation_id' => $this->model->objects->calculation_id, 'number_group' => $this->model->objects->number_group])->one();
        if($equipment){
            $o2 = $equipment->formula->getO2();
        } else {
            $o2 = 1;
        }
        return $this->getQv() / 7000 * pow(10, 3) / $o2;
    }

    public function getOvGut()
    {
        return $this->getOvGuto() + $this->getOvGutv();
    }


}