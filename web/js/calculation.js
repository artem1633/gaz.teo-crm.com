window.OvObjectCalculation = null;
window.OvObjectType = null;



function fillClimat() {
    let idCity = $('#calculation-city_id').val();
    if (idCity) {
        $.get("get-climat", {id: idCity})
            .done(function (data) {
                if (data.data.error) {
                    console.log('Город не найден!!!');
                } else {

                    // var durationHeating = 0;
                    // durationHeating += data.data['duration_op1'];
                    // durationHeating += data.data['duration_op2'];
                    // durationHeating += data.data['duration_op3'];
                    // durationHeating += data.data['duration_op4'];
                    // durationHeating += data.data['duration_op5'];
                    // durationHeating += data.data['duration_op6'];
                    // durationHeating += data.data['duration_op7'];
                    // durationHeating += data.data['duration_op8'];
                    // durationHeating += data.data['duration_op9'];
                    // durationHeating += data.data['duration_op10'];
                    // durationHeating += data.data['duration_op11'];
                    // durationHeating += data.data['duration_op12'];

                    var winderSpeed = data.data['winder_speed'];

                    if(winderSpeed == null || winderSpeed < 1){
                        winderSpeed = 1;
                    }


                    $('#calculation-temperature_average').val(data.data['temperature_average']);
                    $('#calculation-temperature_calculated').val(data.data['temperature_calculated']);
                    $('#calculation-winder_speed').val(winderSpeed);
                    $('#calculation-temp_average1').val(data.data['temp_average1']);
                    $('#calculation-temp_average2').val(data.data['temp_average2']);
                    $('#calculation-temp_average3').val(data.data['temp_average3']);
                    $('#calculation-temp_average4').val(data.data['temp_average4']);
                    $('#calculation-temp_average5').val(data.data['temp_average5']);
                    $('#calculation-temp_average6').val(data.data['temp_average6']);
                    $('#calculation-temp_average7').val(data.data['temp_average7']);
                    $('#calculation-temp_average8').val(data.data['temp_average8']);
                    $('#calculation-temp_average9').val(data.data['temp_average9']);
                    $('#calculation-temp_average10').val(data.data['temp_average10']);
                    $('#calculation-temp_average11').val(data.data['temp_average11']);
                    $('#calculation-temp_average12').val(data.data['temp_average12']);
                    $('#calculation-duration_heating').val(data.data['heating_period']);
                }
            });
    } else {
        alert('Не выбран город');
    }

}

function buildingTypeChange() {
    let idType = $('#objectsovform-building_type').val();
    if (idType) {
        $.get("get-ov-building-type", {id: idType})
            .done(function (data) {
            	console.log(data,  'Loaded data of the type');
                if (data.data.error || data.error) {
                    console.log('не найден!!!');
                } else {
                    var buildingVolume = $('#objectsovform-building_volume').val();

                    var object = null;


                    var objectHeating = 0;
                    var objectVentilation = 0;

                    if(data.object.type === 0){
                        
                        if(data.object.external_temperature <= -30){
                            if(buildingVolume > 500 && buildingVolume <= 2000){
                                objectHeating = 0.37;
                            } else if(buildingVolume > 2000 && buildingVolume <= 5000){
                                objectHeating = 0.28;
                            } else if(buildingVolume > 5000 && buildingVolume <= 10000){
                                objectHeating = 0.24;
                            } else if(buildingVolume > 10000 && buildingVolume <= 15000){
                                objectHeating = 0.21;
                            } else if(buildingVolume > 15000 && buildingVolume <= 25000){
                                objectHeating = 0.20;
                            } else if(buildingVolume > 25000){
                                objectHeating = 0.19;
                            }
                        }

                        if(data.object.external_temperature >= -20){
                            if(buildingVolume > 500 && buildingVolume <= 2000){
                                objectHeating = 0.45;
                            } else if(buildingVolume > 2000 && buildingVolume <= 5000){
                                objectHeating = 0.38;
                            } else if(buildingVolume > 5000 && buildingVolume <= 10000){
                                objectHeating = 0.29;
                            } else if(buildingVolume > 10000 && buildingVolume <= 15000){
                                objectHeating = 0.25;
                            } else if(buildingVolume > 15000 && buildingVolume <= 25000){
                                objectHeating = 0.23;
                            } else if(buildingVolume > 25000){
                                objectHeating = 0.22;
                            }
                        }

                        if(data.object.external_temperature > -30 && data.object.external_temperature < -20){
                            if(buildingVolume > 500 && buildingVolume <= 2000){
                                objectHeating = 0.41;
                            } else if(buildingVolume > 2000 && buildingVolume <= 5000){
                                objectHeating = 0.3;
                            } else if(buildingVolume > 5000 && buildingVolume <= 10000){
                                objectHeating = 0.27;
                            } else if(buildingVolume > 10000 && buildingVolume <= 15000){
                                objectHeating = 0.23;
                            } else if(buildingVolume > 15000 && buildingVolume <= 25000){
                                objectHeating = 0.21;
                            } else if(buildingVolume > 25000){
                                objectHeating = 0.2;
                            }
                        }

                    } else if(data.object.type === 1){

                         if(buildingVolume > 1 && buildingVolume <= 100){
                             objectHeating = 0.74;
                         } else if(buildingVolume > 100 && buildingVolume <= 200){
                             objectHeating = 0.66;
                         } else if(buildingVolume > 200 && buildingVolume <= 300){
                             objectHeating = 0.62;
                         } else if(buildingVolume > 300 && buildingVolume <= 400){
                             objectHeating = 0.6;
                         } else if(buildingVolume > 400 && buildingVolume <= 500){
                             objectHeating = 0.58;
                         } else if(buildingVolume > 500 && buildingVolume <= 600){
                             objectHeating = 0.56;
                         } else if(buildingVolume > 600 && buildingVolume <= 700){
                             objectHeating = 0.54;
                         } else if(buildingVolume > 700 && buildingVolume <= 800){
                             objectHeating = 0.53;
                         } else if(buildingVolume > 800 && buildingVolume <= 900){
                             objectHeating = 0.52;
                         } else if(buildingVolume > 900 && buildingVolume <= 1000){
                             objectHeating = 0.51;
                         } else if(buildingVolume > 1000 && buildingVolume <= 1100 ){
                             objectHeating = 0.5;
                         } else if(buildingVolume > 1100 && buildingVolume <= 1200 ){
                             objectHeating = 0.49;
                         } else if(buildingVolume > 1200 && buildingVolume <= 1300 ){
                             objectHeating = 0.48;
                         } else if(buildingVolume > 1300 && buildingVolume <= 1400 ){
                             objectHeating = 0.47;
                         } else if(buildingVolume > 1400 && buildingVolume <= 1500 ){
                             objectHeating = 0.47;
                         } else if(buildingVolume > 1500 && buildingVolume <= 1700 ){
                             objectHeating = 0.46;
                         } else if(buildingVolume > 1700 && buildingVolume <= 2000 ){
                             objectHeating = 0.45;
                         } else if(buildingVolume > 2000 && buildingVolume <= 2500 ){
                             objectHeating = 0.44;
                         } else if(buildingVolume > 2500 && buildingVolume <= 3000 ){
                             objectHeating = 0.43;
                         } else if(buildingVolume > 3000 && buildingVolume <= 3500 ){
                             objectHeating = 0.42;
                         } else if(buildingVolume > 3500 && buildingVolume <= 4000 ){
                             objectHeating = 0.40;
                         } else if(buildingVolume > 4000 && buildingVolume <= 4500 ){
                             objectHeating = 0.39;
                         } else if(buildingVolume > 4500 && buildingVolume <= 5000 ){
                             objectHeating = 0.38;
                         } else if(buildingVolume > 5000 && buildingVolume <= 6000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 6000 && buildingVolume <= 7000 ){
                             objectHeating = 0.36;
                         } else if(buildingVolume > 7000 && buildingVolume <= 8000 ){
                             objectHeating = 0.35;
                         } else if(buildingVolume > 8000 && buildingVolume <= 9000 ){
                             objectHeating = 0.34;
                         } else if(buildingVolume > 9000 && buildingVolume <= 10000 ){
                             objectHeating = 0.33;
                         } else if(buildingVolume > 10000 && buildingVolume <= 11000 ){
                             objectHeating = 0.32;
                         } else if(buildingVolume > 11000 && buildingVolume <= 12000 ){
                             objectHeating = 0.31;
                         } else if(buildingVolume > 12000 && buildingVolume <= 13000 ){
                             objectHeating = 0.3;
                         } else if(buildingVolume > 13000 && buildingVolume <= 14000 ){
                             objectHeating = 0.3;
                         } else if(buildingVolume > 14000 && buildingVolume <= 15000 ){
                             objectHeating = 0.29;
                         } else if(buildingVolume > 15000 && buildingVolume <= 20000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 20000 && buildingVolume <= 25000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 25000 && buildingVolume <= 30000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 30000 && buildingVolume <= 35000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 35000 && buildingVolume <= 40000 ){
                             objectHeating = 0.27;
                         } else if(buildingVolume > 40000 && buildingVolume <= 45000 ){
                             objectHeating = 0.27;
                         } else if(buildingVolume > 45000 && buildingVolume <= 50000 ){
                             objectHeating = 0.26;
                         } else if(buildingVolume > 50000){ 
                             objectHeating = 1.6 / Math.pow(buildingVolume, 1/6);
                             objectHeating = parseFloat(objectHeating.toFixed(2));
                         }

                    } else if(data.object.type === 2){

                         if(buildingVolume > 1 && buildingVolume <= 100){
                             objectHeating = 0.92;
                         } else if(buildingVolume > 100 && buildingVolume <= 200){
                             objectHeating = 0.82;
                         } else if(buildingVolume > 200 && buildingVolume <= 300){
                             objectHeating = 0.78;
                         } else if(buildingVolume > 300 && buildingVolume <= 400){
                             objectHeating = 0.74;
                         } else if(buildingVolume > 400 && buildingVolume <= 500){
                             objectHeating = 0.71;
                         } else if(buildingVolume > 500 && buildingVolume <= 600){
                             objectHeating = 0.69;
                         } else if(buildingVolume > 600 && buildingVolume <= 700){
                             objectHeating = 0.68;
                         } else if(buildingVolume > 700 && buildingVolume <= 800){
                             objectHeating = 0.67;
                         } else if(buildingVolume > 800 && buildingVolume <= 900){
                             objectHeating = 0.66;
                         } else if(buildingVolume > 900 && buildingVolume <= 1000){
                             objectHeating = 0.65;
                         } else if(buildingVolume > 1000 && buildingVolume <= 1100 ){
                             objectHeating = 0.62;
                         } else if(buildingVolume > 1100 && buildingVolume <= 1200 ){
                             objectHeating = 0.6;
                         } else if(buildingVolume > 1200 && buildingVolume <= 1300 ){
                             objectHeating = 0.59;
                         } else if(buildingVolume > 1300 && buildingVolume <= 1400 ){
                             objectHeating = 0.58;
                         } else if(buildingVolume > 1400 && buildingVolume <= 1500 ){
                             objectHeating = 0.57;
                         } else if(buildingVolume > 1500 && buildingVolume <= 1700 ){
                             objectHeating = 0.55;
                         } else if(buildingVolume > 1700 && buildingVolume <= 2000 ){
                             objectHeating = 0.53;
                         } else if(buildingVolume > 2000 && buildingVolume <= 2500 ){
                             objectHeating = 0.52;
                         } else if(buildingVolume > 2500 && buildingVolume <= 3000 ){
                             objectHeating = 0.5;
                         } else if(buildingVolume > 3000 && buildingVolume <= 3500 ){
                             objectHeating = 0.48;
                         } else if(buildingVolume > 3500 && buildingVolume <= 4000 ){
                             objectHeating = 0.47;
                         } else if(buildingVolume > 4000 && buildingVolume <= 4500 ){
                             objectHeating = 0.46;
                         } else if(buildingVolume > 4500 && buildingVolume <= 5000 ){
                             objectHeating = 0.45;
                         } else if(buildingVolume > 5000 && buildingVolume <= 6000 ){
                             objectHeating = 0.43;
                         } else if(buildingVolume > 6000 && buildingVolume <= 7000 ){
                             objectHeating = 0.42;
                         } else if(buildingVolume > 7000 && buildingVolume <= 8000 ){
                             objectHeating = 0.41;
                         } else if(buildingVolume > 8000 && buildingVolume <= 9000 ){
                             objectHeating = 0.4;
                         } else if(buildingVolume > 9000 && buildingVolume <= 10000 ){
                             objectHeating = 0.39;
                         } else if(buildingVolume > 10000 && buildingVolume <= 11000 ){
                             objectHeating = 0.38;
                         } else if(buildingVolume > 11000 && buildingVolume <= 12000 ){
                             objectHeating = 0.38;
                         } else if(buildingVolume > 12000 && buildingVolume <= 13000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 13000 && buildingVolume <= 14000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 14000 && buildingVolume <= 15000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 15000 && buildingVolume <= 20000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 20000 && buildingVolume <= 25000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 25000 && buildingVolume <= 30000 ){
                             objectHeating = 0.36;
                         } else if(buildingVolume > 30000 && buildingVolume <= 35000 ){
                             objectHeating = 0.35;
                         } else if(buildingVolume > 35000 && buildingVolume <= 40000 ){
                             objectHeating = 0.35;
                         } else if(buildingVolume > 40000 && buildingVolume <= 45000 ){
                             objectHeating = 0.34;
                         } else if(buildingVolume > 45000 && buildingVolume <= 50000 ){
                             objectHeating = 0.34;
                         } else if(buildingVolume > 50000){ 
                             objectHeating = 1.3 / Math.pow(buildingVolume, 1/8);
                             objectHeating = parseFloat(objectHeating.toFixed(2));
                         }

                    } else if(data.object.type === 3) {

                    	// objectHeating = 1.6 / Math.sqrt(buildingVolume);
                    	// alert(123);
                    	objectHeating = 1.6 / Math.pow(buildingVolume, 1/6);
                        objectHeating = objectHeating.toFixed(2);

                    } else if(data.object.type === 4) {

                    	// objectHeating = 1.3 / Math.sqrt(buildingVolume);
                    	objectHeating = 1.3 / Math.pow(buildingVolume, 1/8);
                        objectHeating = objectHeating.toFixed(2);

                    } else {
                        $.each(data.data, function(index, obj){
                            if(object == null){
                                if((data.data.length - 1) <= index){ // Последний
                                    if(buildingVolume >= obj.building_volume){
                                        object = data.data[index++];
                                        objectHeating = object.heating;
                                        objectVentilation = object.ventilation;
                                    }
                                } else {
                                    if(buildingVolume <= obj.building_volume){
                                        object = data.data[index];
                                        objectHeating = object.heating;
                                        objectVentilation = object.ventilation;
                                    }
                                }
                            }
                        });
                    }

                    window.OvObjectCalculation = data.data;
                    window.OvObjectType = data.object;

                    $('#objectsovform-specific_thermal_characteristic_heating').val(objectHeating);
                    $('#objectsovform-specific_thermal_characteristic_ventilation').val(objectVentilation);
                }
            });
    } else {
        alert('Не выбран город');
    }
}

function systemGvsTypeChange() {
    let idType = $('#objectsgvsform-purpose_system').val();
    if (idType) {
        $.get("get-gvs-purpose-system", {id: idType})
            .done(function (data) {
                if (data.data.error) {
                    console.log('не найден!!!');
                } else {
                    $('#objectsgvsform-water_consumption_rate').val(data.data['water_consumption_rate']);
                    $('#objectsgvsform-calculation_regarding_quantity').val(data.data['calculation_regarding_quantity']);
                }
            });
    } else {
        alert('Не выбран город');
    }
}

function systemTechTypeChange() {
    let idType = $('#objectstechform-purpose_system').val();
    if(idType != 16){
        if (idType) {
            $.get("get-tech-purpose-system", {id: idType})
                .done(function (data) {
                    if (data.data.error) {
                        console.log('не найден!!!');
                    } else {

                        if(data.data['is_random'] == 1){
                            $('#destination-system-wrapper').slideDown();
                        } else {
                            $('#destination-system-wrapper').slideUp();
                        }

                        $('#objectstechform-consumption_thermal_units').val(data.data['consumption_thermal_units']);
                        $('#objectstechform-consumption_equivalent_fuel').val(data.data['consumption_equivalent_fuel']);
                        $('#objectstechform-day_per_week').val(data.data['day_per_week']);
                        $('#objectstechform-norm_document').val(data.data['norm_document']);
                        $('#label-objectstechform-amount').html(data.data['amount_name']);
                        $('#objectstechform-amount_name').val(data.data['amount_name']);

                        $('label[for="objectstechform-consumption_thermal_units"]').text('Расход тепловых единиц [тыс.ккал], '+data.data['one_name']);
                    }

                });
        } else {
            // alert('Не выбран город');
        }
    }
}
function ovPowerChange() {
    let powerO = $('#objectsovform-heating_load').val();
    let powerV = $('#objectsovform-ventilation_load').val();
        $('#ov_all').html(parseInt(powerO)+parseInt(powerV))
}

function gvsPowerChange() {
    let powerO = $('#objectsgvsform-heating_load').val();
    let powerV = $('#objectsgvsform-ventilation_load').val();
    $('#gvs_all').html(parseInt(powerO)+parseInt(powerV))
}

function ovLoadsKnowChange() {
    let LoadsKnowValue = $('#objectsovform-heat_loads_known').is(':checked');

    if (LoadsKnowValue) {
        $('#objectsovform-heating_load').prop('disabled', false);
        $('#objectsovform-ventilation_load').prop('disabled', false);


    } else {
        $('#objectsovform-heating_load').prop('disabled', true);
        $('#objectsovform-ventilation_load').prop('disabled', true);
    }
}

function gvsLoadsKnowChange() {
    let LoadsKnowValue = $('#objectsgvsform-heat_loads_known').is(':checked');

    if (LoadsKnowValue) {
        $('#objectsgvsform-heating_load').prop('disabled', false);
        $('#objectsgvsform-ventilation_load').prop('disabled', false);


    } else {
        $('#objectsgvsform-heating_load').prop('disabled', true);
        $('#objectsgvsform-ventilation_load').prop('disabled', true);
    }
}

function group(group) {
    var PosId = $('#crud-datatable-object').yiiGridView('getSelectedRows');
    if (PosId == "") {
        alert("Нет отмеченных записей!");
    } else {
        $.ajax({
            type: 'POST',
            url: 'group-object',
            data: {pks: PosId, num_group: group},
        }).done(function (data) {
            console.log(data);
            $.pjax.reload({container: '#crud-datatable-object-pjax', async: false});
            $('#crud-datatable-object-pjax').removeClass('kv-grid-loading');
            $.pjax.reload({container: '#crud-datatable-equipment-pjax', async: false});
            $('#crud-datatable-equipment-pjax').removeClass('kv-grid-loading');
        });
    }
}