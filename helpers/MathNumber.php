<?php

namespace app\helpers;

class MathNumber {

	public static function handleOne($number)
	{
		if($number < 0){
			return "({$number})";
		} else {
			return $number;
		}
	}

	public static function round($number)
	{
		if($number < 1){

			$str = explode('.', strval($number));

			// var_dump($number);
			// exit;

			if(isset($str[1])){
				$decimals = $str[1];

				$ziro = true;
				$counter = 1;
				foreach (str_split($decimals) as $n) {
					if($n == '0'){

					} else {
						$ziro = false;

							// var_dump($decimals[$counter++]);
							// exit;

						// echo "b: {$counter} ";

						if(isset(str_split($decimals)[$counter+1])){
							$nextNum = str_split($decimals)[$counter];
							// var_dump($decimals[$counter]);
							// exit;
							// echo "a: {$counter}";
							// exit;
							$doubleNextNum = str_split($decimals)[$counter+1];

							// echo $doubleNextNum.' ';


				 			if($doubleNextNum >= 5){
								$nextNum++;
							}

						} else {
							$nextNum = '';
						}

						if($nextNum == '0'){
							$nextNum = '';
						}

						$newDecimals = '';

						if(($counter - 2) > 0){
							$newDecimals = implode('', array_slice(str_split($decimals), 0, $counter)) . $nextNum;
						} else {
							return round(floatval($str[0].'.'.$decimals), 2);
						}


						return $str[0].'.'.$newDecimals;
					}

					$counter++;
				}
			}

			return $number;
		} else {
			return round($number, 2);
		}
	}
}