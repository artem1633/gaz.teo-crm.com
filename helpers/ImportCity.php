<?php

namespace app\helpers;

use app\models\City;
use app\models\Region;

/**
 * Class ImportCity
 * @package app\helpers
 * Статический класс, который импортирует данные из Excel
 *
 */
class ImportCity
{
    /**
     * Импорт
     */
    public static function import()
    {
        $import = new ImportCity();
        $file1 = __DIR__ . '/../web/uploads/climat1.xlsx';
        if (!file_exists($file1)) {
            return false;
        }
        $import->importMainData($file1);

        $file2 = __DIR__ . '/../web/uploads/climat2.xlsx';
        if (!file_exists($file2)) {
            return false;
        }
        $import->importMountsData($file2);


    }

    private function importMountsData($file)
    {
        $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);;

        $worksheet = $objPHPExcel->getSheet(0);

        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $currentRegion = 0;
        for ($row = 5; $row <= $highestRow; ++$row) {
            $cell = $worksheet->getCellByColumnAndRow(2, $row);
            $val = $cell->getValue();
            if (!$val) {
                //Регион
                $cell = $worksheet->getCellByColumnAndRow(1, $row);
                $val = $cell->getValue();
                $region = Region::find()->where(['name' => str_replace('*', '', trim(strval($val)))])->one();
                if (!$region) {
                    $region = new Region();
                    $region->name = str_replace('*', '', trim(strval($val)));
                    $region->save();
                    $currentRegion = $region->id;
                }
            } else {
                //Город
                $cell = $worksheet->getCellByColumnAndRow(1, $row);
                $val = $cell->getValue();
                $city = City::find()->where(['name' => str_replace('*', '', trim(strval($val)))])->one();
                if (!$city) {
                    $city = new City();
                    $city->temperature_average = 0;
                    $city->temperature_calculated = 0;
                    $city->winder_speed = 0;
                } else {
                    $currentRegion = $city->region_id;
                }
                $city->name = str_replace('*', '', trim(strval($val)));
                $val = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $city->temp_average1 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $city->temp_average2 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $city->temp_average3 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                $city->temp_average4 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                $city->temp_average5 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                $city->temp_average6 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $city->temp_average7 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                $city->temp_average8 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                $city->temp_average9 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                $city->temp_average10 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                $city->temp_average11 = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                $city->temp_average12 = floatval($val);
                $city->region_id = $currentRegion;
                $city->save();
            }
        }
    }

    private function importMainData($file)
    {
        $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);;

        $worksheet = $objPHPExcel->getSheet(0);

        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $currentRegion = 0;
        for ($row = 7; $row <= $highestRow; ++$row) {
            $cell = $worksheet->getCellByColumnAndRow(2, $row);
            $val = $cell->getValue();
            if (!$val) {
                //Регион
                $cell = $worksheet->getCellByColumnAndRow(1, $row);
                $val = $cell->getValue();
                $region = Region::find()->where(['name' => str_replace('*', '', trim(strval($val)))])->one();
                if (!$region) {
                    $region = new Region();
                    $region->name = str_replace('*', '', trim(strval($val)));
                    $region->save();
                    $currentRegion = $region->id;
                } else {
                    $currentRegion = $region->id;
                }
            } else {
                //Город
                $cell = $worksheet->getCellByColumnAndRow(1, $row);
                $val = $cell->getValue();
                $city = City::find()->where(['name' => str_replace('*', '', trim(strval($val)))])->one();
                if (!$city) {
                    $city = new City();
                }
                $city->name = str_replace('*', '', trim(strval($val)));
                $val = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                $city->temperature_average = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                $city->temperature_calculated = floatval($val);
                $val = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                $city->winder_speed = floatval($val);
                $city->heating_period = 0;
                $city->region_id = $currentRegion;
                $city->save();
            }
        }
    }
}