<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObjectsTechType;

/**
 * ObjectsTechTypeSearch represents the model behind the search form about `\app\models\ObjectsTechType`.
 */
class ObjectsTechTypeSearch extends ObjectsTechType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['id', 'day_per_week'], 'integer'],
            [['consumption_thermal_units', 'consumption_equivalent_fuel'], 'number'],
            [['norm_document', 'amount_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObjectsTechType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'consumption_thermal_units' => $this->consumption_thermal_units,
            'consumption_equivalent_fuel' => $this->consumption_equivalent_fuel,
            'day_per_week' => $this->day_per_week,
        ]);

        $query->andFilterWhere(['like', 'norm_document', $this->norm_document])
            ->andFilterWhere(['like', 'amount_name', $this->amount_name])
            ->andFilterWhere(['like', 'name', $this->amount_name]);

        return $dataProvider;
    }
}
