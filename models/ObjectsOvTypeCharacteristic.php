<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objects_ov_type_characteristic".
 *
 * @property int $id
 * @property int $objects_ov_type_id Тип
 * @property double $building_volume Строительный объем здания
 * @property double $heating Удельная тепловая характеристика отопления
 * @property double $ventilation Удельная тепловая характеристика вентиляции
 *
 * @property ObjectsOvType $objectsOvType
 */
class ObjectsOvTypeCharacteristic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_ov_type_characteristic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['objects_ov_type_id'], 'integer'],
            [['building_volume', 'heating', 'ventilation'], 'number'],
            [['objects_ov_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ObjectsOvType::className(), 'targetAttribute' => ['objects_ov_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'objects_ov_type_id' => 'Тип',
            'building_volume' => 'Строительный объем здания',
            'heating' => 'Удельная тепловая характеристика отопления',
            'ventilation' => 'Удельная тепловая характеристика вентиляции',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectsOvType()
    {
        return $this->hasOne(ObjectsOvType::className(), ['id' => 'objects_ov_type_id']);
    }
}
