<?php

namespace app\models;

use app\formulas\GvsFormula;
use Yii;

/**
 * This is the model class for table "objects_gvs".
 *
 * @property int $id
 * @property int $objects_id Объект
 * @property int $purpose_system Назначение системы
 * @property string $water_consumption_rate Сут. норма затрат воды (коэф.альфа)
 * @property int $count_work_day Количество рабочих дней ГВС, в неделю
 * @property string $work_per_day Время работы в сутки, ч
 * @property string $calculation_regarding_quantity Расчет относительно количества
 * @property int $count_consumers Количество (потребителей)
 * @property int $heat_loads_known Известны проектные тепловые нагрузки
 * @property string $heating_load Нагрузка отопления q, Гкал/час
 * @property string $ventilation_load Нагрузка вентиляции q, Гкал/час
 *
 * @property GvsFormula $formula
 *
 * @property Objects $objects
 */
class ObjectsGvs extends \yii\db\ActiveRecord
{
    /**
     * @var GvsFormula
     */
    private $_formula;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_gvs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['objects_id', 'purpose_system', 'count_work_day', 'count_consumers', 'heat_loads_known'], 'integer'],
            [['water_consumption_rate', 'work_per_day', 'heating_load', 'ventilation_load'], 'number'],
            [['calculation_regarding_quantity'], 'string', 'max' => 255],
            [['objects_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['objects_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'objects_id' => 'Объект',
            'purpose_system' => 'Назначение системы',
            'water_consumption_rate' => 'Сут. норма затрат воды (коэф.альфа)',
            'count_work_day' => 'Количество рабочих дней ГВС, в неделю  (от 1 до 7)',
            'work_per_day' => 'Время работы в сутки, ч  Для проточных водонагревателей 1-3 часа, котельная - 24',
            'calculation_regarding_quantity' => 'Расчет относительно количества',
            'count_consumers' => 'Количество (потребителей) (от 1 до 99999)',
            'heat_loads_known' => 'Известны проектные тепловые нагрузки',
            'heating_load' => 'Нагрузка потребления воды q, Гкал/час  (0 или от 0,005 до 2)',
            'ventilation_load' => 'Нагрузка циркуляции q, Гкал/час  (0 или от 0,005 до 2)',
        ];
    }

    /**
     * @return GvsFormula
     */
    public function getFormula()
    {
        if($this->_formula == null){
            $this->_formula = new GvsFormula(['model' => $this]);
        }

        return $this->_formula;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);
    }
}
