<?php

namespace app\models;

use app\formulas\OvFormula;
use Yii;

/**
 * This is the model class for table "objects_ov".
 *
 * @property int $id
 * @property int $objects_id Объект
 * @property int $building_type Тип здания
 * @property string $building_volume Строительный объем здания, V, м3
 * @property string $specific_thermal_characteristic_heating Удельная тепловая характеристика отопления
 * @property string $specific_thermal_characteristic_ventilation Удельная тепловая характеристика вентиляции
 * @property string $specific_thermal_characteristic
 * @property string $building_height Свободная высота здания, L, м
 * @property float $internal_temperature Внутренняя температура, tj, oC
 * @property int $heat_loads_known Известны проектные тепловые нагрузки
 * @property string $heating_load Нагрузка отопления q, Гкал/час
 * @property string $ventilation_load Нагрузка вентиляции q, Гкал/час
 *
 * @property OvFormula $formula
 *
 * @property Objects $objects
 */
class ObjectsOv extends \yii\db\ActiveRecord
{
    /**
     * @var OvFormula
     */
    private $_formula;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_ov';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['objects_id', 'building_type', 'heat_loads_known'], 'integer'],
            [['building_volume', 'specific_thermal_characteristic_heating', 'specific_thermal_characteristic_ventilation', 'building_height', 'internal_temperature', 'heating_load', 'ventilation_load'], 'number'],
            ['heat_loads_known','safe'],
            [['specific_thermal_characteristic'], 'string', 'max' => 255],
            [['objects_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['objects_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'objects_id' => 'Объект',
            'building_type' => 'Тип здания',
            'building_volume' => 'Строительный объем здания, V, м3',
            'specific_thermal_characteristic_heating' => 'Удельная тепловая характеристика отопления',
            'specific_thermal_characteristic_ventilation' => 'Удельная тепловая характеристика вентиляции',
            'specific_thermal_characteristic' => 'Specific Thermal Characteristic',
            'building_height' => 'Свободная высота здания, L, м',
            'internal_temperature' => 'Внутренняя температура, tj, oC',
            'heat_loads_known' => 'Известны проектные тепловые нагрузки',
            'heating_load' => 'Нагрузка отопления q, Гкал/час',
            'ventilation_load' => 'Нагрузка вентиляции q, Гкал/час',
        ];
    }

    /**
     * @return OvFormula
     */
    public function getFormula()
    {
        if($this->_formula == null){
            $this->_formula = new OvFormula(['model' => $this]);
        }

        return $this->_formula;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public  function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);
    }
}
