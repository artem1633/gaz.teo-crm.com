<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObjectsOvType;

/**
 * ObjectsOvTypeSearch represents the model behind the search form about `\app\models\ObjectsOvType`.
 */
class ObjectsOvTypeSearch extends ObjectsOvType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['id'], 'integer'],
            [['specific_thermal_characteristic_heating', 'specific_thermal_characteristic_ventilation'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObjectsOvType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

            'specific_thermal_characteristic_heating' => $this->specific_thermal_characteristic_heating,
            'specific_thermal_characteristic_ventilation' => $this->specific_thermal_characteristic_ventilation,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
