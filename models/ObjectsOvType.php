<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "objects_ov_type".
 *
 * @property int $id
 * @property string name Наименование
 * @property string $specific_thermal_characteristic_heating Удельная тепловая характеристика отопления
 * @property string $specific_thermal_characteristic_ventilation Удельная тепловая характеристика вентиляции
 * @property integer $type Тип
 * @property double $external_temperature Наружная температура воздуха
 *
 */
class ObjectsOvType extends ActiveRecord
{
    const TYPE_BEFORE_1930 = 0;
    const TYPE_LIVE_BEFORE_1958 = 1;
    const TYPE_LIVE_AFTER_1958 = 2;
    const TYPE_OTHER_BEFORE_1958 = 3;
    const TYPE_OTHER_AFTER_1958 = 4;

    /**
     * @var array
     */
    public $charecteristics;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_ov_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','specific_thermal_characteristic_heating','specific_thermal_characteristic_ventilation'], 'required'],
            [['name'], 'string'],
            [['specific_thermal_characteristic_heating', 'specific_thermal_characteristic_ventilation', 'external_temperature'], 'number'],
            [['type'], 'integer'],

            [['charecteristics'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип',
            'external_temperature' => 'Наружная температура воздуха',
            'specific_thermal_characteristic_heating' => 'Удельная тепловая характеристика отопления',
            'specific_thermal_characteristic_ventilation' => 'Удельная тепловая характеристика вентиляции',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->charecteristics != null){
            ObjectsOvTypeCharacteristic::deleteAll(['objects_ov_type_id' => $this->id]);
            foreach ($this->charecteristics as $charecteristic) {
                $ovTypeCharacteristic = new ObjectsOvTypeCharacteristic([
                    'objects_ov_type_id' => $this->id,
                    'building_volume' => $charecteristic['building_volume'],
                    'heating' => $charecteristic['heating'],
                    'ventilation' => $charecteristic['ventilation'],
                ]);
                $ovTypeCharacteristic->save(false);
            }
        }
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_BEFORE_1930 => 'До 1930 года',
            self::TYPE_LIVE_BEFORE_1958 => 'Жилые до 1958 года',
            self::TYPE_LIVE_AFTER_1958 => 'Жилые после 1958 года',
            self::TYPE_OTHER_BEFORE_1958 => 'Прочие до 1958 года',
            self::TYPE_OTHER_AFTER_1958 => 'Прочие после 1958 года',
        ];
    }

    public static function getTypes()
    {
        return ArrayHelper::map(static::find()->orderBy('name asc')->all(),'id','name');
    }
}
