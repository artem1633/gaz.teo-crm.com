<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property int $region_id
 * @property string $name Название
 * @property string $temperature_average Средняя температура наружного воздуха за отопительный период, tc ,oC
 * @property string $temperature_calculated Расчетная температура наружного воздуха для отопления, to,oC
 * @property string $winder_speed Расчетная скорость ветра, w, м/с
 * @property string $heating_period Продолжительность отопительного периода, no, сут
 * @property string $c_alfa Поправочный коэффициент a-учитывающий отличие расчетной температуры нар. воздуха для проектирования отопления tO от -30 ,oC
 * @property string $temp_average1 Средняя температура Январь
 * @property string $temp_average2 Средняя температура Февраль
 * @property string $temp_average3 Средняя температура Март
 * @property string $temp_average4 Средняя температура Апрель
 * @property string $temp_average5 Средняя температура Май
 * @property string $temp_average6 Средняя температура Июнь
 * @property string $temp_average7 Средняя температура Июль
 * @property string $temp_average8 Средняя температура Август
 * @property string $temp_average9 Средняя температура Сентябрь
 * @property string $temp_average10 Средняя температура Октябрь
 * @property string $temp_average11 Средняя температура Ноябрь
 * @property string $temp_average12 Средняя температура Декабрь
 * @property int $duration_op1 Продолжительность ОП Январь
 * @property int $duration_op2 Продолжительность ОП Февраль
 * @property int $duration_op3 Продолжительность ОП Март
 * @property int $duration_op4 Продолжительность ОП Апрель
 * @property int $duration_op5 Продолжительность ОП Май
 * @property int $duration_op6 Продолжительность ОП Июнь
 * @property int $duration_op7 Продолжительность ОП Июль
 * @property int $duration_op8 Продолжительность ОП Август
 * @property int $duration_op9 Продолжительность ОП Сентябрь
 * @property int $duration_op10 Продолжительность ОП Октябрь
 * @property int $duration_op11 Продолжительность ОП Ноябрь
 * @property int $duration_op12 Продолжительность ОП Декабрь
 *
 * @property Region $region
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'duration_op1', 'duration_op2', 'duration_op3', 'duration_op4', 'duration_op5', 'duration_op6', 'duration_op7', 'duration_op8', 'duration_op9', 'duration_op10', 'duration_op11', 'duration_op12'], 'integer'],
            [['temperature_average', 'temperature_calculated', 'winder_speed'], 'required'],
            [['temperature_average', 'temperature_calculated', 'winder_speed', 'heating_period', 'c_alfa', 'temp_average1', 'temp_average2', 'temp_average3', 'temp_average4', 'temp_average5', 'temp_average6', 'temp_average7', 'temp_average8', 'temp_average9', 'temp_average10', 'temp_average11', 'temp_average12'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'region_id' => 'Регион',
            'name' => 'Название',
            'temperature_average' => 'Средняя температура наружного воздуха за отопительный период, tc ,oC',
            'temperature_calculated' => 'Расчетная температура наружного воздуха для отопления, to,oC',
            'winder_speed' => 'Расчетная скорость ветра, w, м/с',
            'heating_period' => 'Продолжительность отопительного периода, no, сут',
            'c_alfa' => 'Поправочный коэффициент a-учитывающий отличие расчетной температуры нар. воздуха для проектирования отопления tO от -30 ,oC',
            'temp_average1' => 'Январь',
            'temp_average2' => 'Февраль',
            'temp_average3' => 'Март',
            'temp_average4' => 'Апрель',
            'temp_average5' => 'Май',
            'temp_average6' => 'Июнь',
            'temp_average7' => 'Июль',
            'temp_average8' => 'Август',
            'temp_average9' => 'Сентябрь',
            'temp_average10' => 'Октябрь',
            'temp_average11' => 'Ноябрь',
            'temp_average12' => 'Декабрь',
            'duration_op1' => 'Январь',
            'duration_op2' => 'Февраль',
            'duration_op3' => 'Март',
            'duration_op4' => 'Апрель',
            'duration_op5' => 'Май',
            'duration_op6' => 'Июнь',
            'duration_op7' => 'Июль',
            'duration_op8' => 'Август',
            'duration_op9' => 'Сентябрь',
            'duration_op10' => 'Октябрь',
            'duration_op11' => 'Ноябрь',
            'duration_op12' => 'Декабрь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public static function getCitiesWithRegion()
    {
        $cities = Yii::$app->db->createCommand('SELECT  city.id as id, CONCAT(city.name,", ",region.name) as fullname FROM `city` INNER JOIN `region`  ON `city`.`region_id` = `region`.`id` order by city.name');
        return ArrayHelper::map($cities->queryAll(),'id','fullname');
    }

    public function beforeSave($insert)
    {
        $r = parent::beforeSave($insert); // TODO: Change the autogenerated stuby
        $this->heating_period = $this->duration_op1+$this->duration_op2+$this->duration_op3+$this->duration_op4+$this->duration_op5+$this->duration_op6;
        $this->heating_period +=$this->duration_op7+$this->duration_op8+$this->duration_op9+$this->duration_op10+$this->duration_op11+$this->duration_op12;
        return $r;
    }
}
