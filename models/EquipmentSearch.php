<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Equipment;

/**
 * EquipmentSearch represents the model behind the search form about `app\models\Equipment`.
 */
class EquipmentSearch extends Equipment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'calculation_id', 'number_group', 'type_object', 'count_equipment'], 'integer'],
            [['name_equipment', 'status'], 'safe'],
            [['KPD', 'coefficient_unambiguity', 'coefficient_load', 'power_v', 'pover_g', 'specific_consumption', 'consumption_e', 'consumption_o'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Equipment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'calculation_id' => $this->calculation_id,
            'number_group' => $this->number_group,
            'type_object' => $this->type_object,
            'KPD' => $this->KPD,
            'coefficient_unambiguity' => $this->coefficient_unambiguity,
            'count_equipment' => $this->count_equipment,
            'coefficient_load' => $this->coefficient_load,
            'power_v' => $this->power_v,
            'pover_g' => $this->pover_g,
            'specific_consumption' => $this->specific_consumption,
            'consumption_e' => $this->consumption_e,
            'consumption_o' => $this->consumption_o,
        ]);

        $query->andFilterWhere(['like', 'name_equipment', $this->name_equipment])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
