<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "objects_tech_type".
 *
 * @property int $id
 * @property string name Наименование
 * @property string $consumption_thermal_units Расход тепловых единиц
 * @property string $consumption_equivalent_fuel Расход условного топлива
 * @property int $day_per_week Расход условного топлива
 * @property string $norm_document Комментарий, ссылка на норматив
 * @property string $amount_name
 * @property string $one_name Наименование одной единицы
 */
class ObjectsTechType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_tech_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['consumption_thermal_units', 'consumption_equivalent_fuel'], 'number'],
            [['day_per_week', 'is_random'], 'integer'],
            [['norm_document', 'amount_name', 'one_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'consumption_thermal_units' => 'Расход тепловых единиц',
            'consumption_equivalent_fuel' => 'Расход условного топлива',
            'day_per_week' => 'Дней в неделю',
            'norm_document' => 'Комментарий, ссылка на норматив',
            'amount_name' => 'Количество (название)',
            'is_random' => 'Произвольный вариант',
            'one_name' => 'Наименование одной единицы'
        ];
    }

    public static function getTypes()
    {
        return ArrayHelper::map(static::find()->orderBy('name asc')->all(),'id','name');
    }
}
