<?php

namespace app\models;

use app\formulas\TehFormula;
use Yii;

/**
 * This is the model class for table "objects_tech".
 *
 * @property int $id
 * @property int $objects_id Объект
 * @property int $purpose_system Назначение системы
 * @property string $consumption_thermal_units Расход тепловых единиц
 * @property string $consumption_equivalent_fuel Расход условного топлива
 * @property int $day_per_week Расход условного топлива
 * @property string $norm_document Комментарий, ссылка на норматив
 * @property string $amount_name
 * @property string $amount
 * @property float $thermal_1_7
 *
 * @property TehFormula $formula
 *
 * @property Objects $objects
 */
class ObjectsTech extends \yii\db\ActiveRecord
{
    /**
     * @var TehFormula
     */
    private $_formula;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_tech';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consumption_thermal_units', 'consumption_equivalent_fuel', 'amount'], 'required'],
            [['objects_id', 'purpose_system', 'day_per_week'], 'integer'],
            [['consumption_thermal_units', 'consumption_equivalent_fuel', 'amount', 'thermal_1_7'], 'number'],
            [['norm_document', 'amount_name'], 'string', 'max' => 255],
            [['objects_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['objects_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'objects_id' => 'Объект',
            'purpose_system' => 'Выбор системы',
            'destination_system' => 'Назначение системы',
            'consumption_thermal_units' => 'Расход тепловых единиц',
            'consumption_equivalent_fuel' => 'Расход условного топлива',
            'day_per_week' => 'Количество рабочих дней в неделю:',
            'norm_document' => 'Комментарий, ссылка на норматив',
            'amount_name' => 'Amount Name',
            'thermal_1_7' => '1,7 расхода тепла',
            'amount' => 'Количество',
        ];
    }

    /**
     * @return TehFormula
     */
    public function getFormula()
    {
        if($this->_formula == null){
            $this->_formula = new TehFormula(['model' => $this]);
        }

        return $this->_formula;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);
    }
}
