<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "objects_gvs_type".
 *
 * @property int $id
 * @property string name Наименование
 * @property string $water_consumption_rate Сут. норма затрат воды (коэф.альфа)
 * @property string $calculation_regarding_quantity Расчет относительно количества
 */
class ObjectsGvsType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_gvs_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','water_consumption_rate', 'calculation_regarding_quantity'], 'required'],
            [['water_consumption_rate'], 'number'],
            [['calculation_regarding_quantity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'water_consumption_rate' => 'Сут. норма затрат воды (коэф.альфа)',
            'calculation_regarding_quantity' => 'Расчет относительно количества',
        ];
    }

    public static function getTypes()
    {
        return ArrayHelper::map(static::find()->orderBy('name asc')->all(),'id','name');
    }
}
