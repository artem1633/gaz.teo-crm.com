<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\City;

/**
 * CitySearch represents the model behind the search form about `\app\models\City`.
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','region_id'], 'integer'],
            [['name', 'duration_op1', 'duration_op2', 'duration_op3', 'duration_op4', 'duration_op5', 'duration_op6', 'duration_op7', 'duration_op8', 'duration_op9', 'duration_op10', 'duration_op11', 'duration_op12'], 'safe'],
            [['temperature_average', 'temperature_calculated', 'winder_speed', 'heating_period', 'c_alfa', 'temp_average1', 'temp_average2', 'temp_average3', 'temp_average4', 'temp_average5', 'temp_average6', 'temp_average7', 'temp_average8', 'temp_average9', 'temp_average10', 'temp_average11', 'temp_average12'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'temperature_average' => $this->temperature_average,
            'temperature_calculated' => $this->temperature_calculated,
            'winder_speed' => $this->winder_speed,
            'heating_period' => $this->heating_period,
            'c_alfa' => $this->c_alfa,
            'temp_average1' => $this->temp_average1,
            'temp_average2' => $this->temp_average2,
            'temp_average3' => $this->temp_average3,
            'temp_average4' => $this->temp_average4,
            'temp_average5' => $this->temp_average5,
            'temp_average6' => $this->temp_average6,
            'temp_average7' => $this->temp_average7,
            'temp_average8' => $this->temp_average8,
            'temp_average9' => $this->temp_average9,
            'temp_average10' => $this->temp_average10,
            'temp_average11' => $this->temp_average11,
            'temp_average12' => $this->temp_average12,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'duration_op1', $this->duration_op1])
            ->andFilterWhere(['like', 'duration_op2', $this->duration_op2])
            ->andFilterWhere(['like', 'duration_op3', $this->duration_op3])
            ->andFilterWhere(['like', 'duration_op4', $this->duration_op4])
            ->andFilterWhere(['like', 'duration_op5', $this->duration_op5])
            ->andFilterWhere(['like', 'duration_op6', $this->duration_op6])
            ->andFilterWhere(['like', 'duration_op7', $this->duration_op7])
            ->andFilterWhere(['like', 'duration_op8', $this->duration_op8])
            ->andFilterWhere(['like', 'duration_op9', $this->duration_op9])
            ->andFilterWhere(['like', 'duration_op10', $this->duration_op10])
            ->andFilterWhere(['like', 'duration_op11', $this->duration_op11])
            ->andFilterWhere(['like', 'duration_op12', $this->duration_op12]);

        return $dataProvider;
    }
}
