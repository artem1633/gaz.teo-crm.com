<?php

namespace app\models;

use app\formulas\CalculationFormula;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "calculation".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $object_name Название объекта
 * @property string $object_address Адрес объекта
 * @property string $customer_name Заказчик (собственник)
 * @property string $certificate_of_consent Данные о справке-согласии
 * @property string $calorific_value Теплотворная способность газа
 * @property string $temperature_average Средняя температура наружного воздуха за отопительный период, tc ,oC
 * @property string $temperature_calculated Расчетная температура наружного воздуха для отопления, to,oC
 * @property string $winder_speed Расчетная скорость ветра, w, м/с
 * @property string $heating_period Продолжительность отопительного периода, no, сут
 * @property string $c_alfa Поправочный коэффициент a-учитывающий отличие расчетной температуры нар. воздуха для проектирования отопления tO от -30 ,oC
 * @property string $temp_average1 Январь
 * @property string $temp_average2 Февраль
 * @property string $temp_average3 Март
 * @property string $temp_average4 Апрель
 * @property string $temp_average5 Май
 * @property string $temp_average6 Июнь
 * @property string $temp_average7 Июль
 * @property string $temp_average8 Август
 * @property string $temp_average9 Сентябрь
 * @property string $temp_average10 Октябрь
 * @property string $temp_average11 Ноябрь
 * @property string $temp_average12 Декабрь
 * @property int $duration_op1 Январь
 * @property int $duration_op2 Февраль
 * @property int $duration_op3 Март
 * @property int $duration_op4 Апрель
 * @property int $duration_op5 Май
 * @property int $duration_op6 Июнь
 * @property int $duration_op7 Июль
 * @property int $duration_op8 Август
 * @property int $duration_op9 Сентябрь
 * @property int $duration_op10 Октябрь
 * @property int $duration_op11 Ноябрь
 * @property int $duration_op12 Декабрь
 * @property string $employer_name Исполнитель
 * @property string $created_at Дата и время создания
 * @property string $year_start_consume
 * @property integer $object_type
 * @property integer $city_id
 * @property integer $region_id
 * @property float $duration_heating Продолжительность отопительного периода
 *
 * @property CalculationFormula $formula
 *
 * @property User $user
 * @property Equipment[] $equipments
 * @property Objects[] $objects
 */
class Calculation extends \yii\db\ActiveRecord
{
    const OBJECT_TYPE_BUILDING = 0;
    const OBJECT_TYPE_RECONSTRUCTION = 1;
    const OBJECT_TYPE_MODERNIZATION = 2;
    const OBJECT_TYPE_UPDATE_EQUIPMENT = 3;
    const OBJECT_TYPE_UPDATE_BUILDING = 4;

    /**
     * @var CalculationFormula
     */
    private $_formula;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calculation';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

    }

    public function rules()
    {
        return [
            [[], 'required'],
            [['user_id', 'duration_op1', 'duration_op2', 'duration_op3', 'duration_op4', 'duration_op5', 'duration_op6', 'duration_op7', 'duration_op8', 'duration_op9', 'duration_op10', 'duration_op11', 'duration_op12', 'object_type', 'city_id', 'region_id', 'year_start_consume'], 'integer'],
            [['calorific_value', 'temperature_average', 'temperature_calculated', 'winder_speed', 'heating_period', 'c_alfa', 'temp_average1', 'temp_average2', 'temp_average3', 'temp_average4', 'temp_average5', 'temp_average6', 'temp_average7', 'temp_average8', 'temp_average9', 'temp_average10', 'temp_average11', 'temp_average12', 'duration_heating'], 'number'],
            [['temperature_average', 'temperature_calculated', 'winder_speed', 'calorific_value', 'c_alfa', 'duration_heating', 'temp_average1', 'temp_average2', 'temp_average3', 'temp_average4', 'temp_average5', 'temp_average6', 'temp_average7', 'temp_average8', 'temp_average9', 'temp_average10', 'temp_average11', 'temp_average12',
            'duration_op1', 'duration_op2', 'duration_op3', 'duration_op4', 'duration_op5', 'duration_op6', 'duration_op7', 'duration_op8', 'duration_op9', 'duration_op10', 'duration_op11', 'duration_op12'], 'required'],
            [['object_name', 'employer_name', 'object_address', 'customer_name', 'certificate_of_consent', 'employer_organization', 'employer_position', 'type_oil'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'object_name' => 'Название объекта',
            'object_address' => 'Адрес объекта',
            'customer_name' => 'Заказчик (собственник)',
            'certificate_of_consent' => 'Данные о справке-согласии',
            'calorific_value' => 'Теплотворная способность газа, ккал/н.м3',
            'temperature_average' => 'Средняя температура наружного воздуха за отопительный период, tc ,oC',
            'temperature_calculated' => 'Расчетная температура наружного воздуха для отопления, to,oC',
            'winder_speed' => 'Расчетная скорость ветра, w, м/с',
            'heating_period' => 'Продолжительность отопительного периода, no, сут',
            'c_alfa' => 'Поправочный коэффициент a-учитывающий отличие расчетной температуры нар. воздуха для проектирования отопления tO от -30 ,oC',
            'temp_average1' => 'Январь',
            'temp_average2' => 'Февраль',
            'temp_average3' => 'Март',
            'temp_average4' => 'Апрель',
            'temp_average5' => 'Май',
            'temp_average6' => 'Июнь',
            'temp_average7' => 'Июль',
            'temp_average8' => 'Август',
            'temp_average9' => 'Сентябрь',
            'temp_average10' => 'Октябрь',
            'temp_average11' => 'Ноябрь',
            'temp_average12' => 'Декабрь',
            'duration_op1' => 'Январь',
            'duration_op2' => 'Февраль',
            'duration_op3' => 'Март',
            'duration_op4' => 'Апрель',
            'duration_op5' => 'Май',
            'duration_op6' => 'Июнь',
            'duration_op7' => 'Июль',
            'duration_op8' => 'Август',
            'duration_op9' => 'Сентябрь',
            'duration_op10' => 'Октябрь',
            'duration_op11' => 'Ноябрь',
            'duration_op12' => 'Декабрь',
            'employer_name' => 'ФИО',
            'employer_organization' => 'Организация',
            'employer_position' => 'Должность',
            'created_at' => 'Дата',
            'type_oil' => 'Вид топлива',
            'duration_heating' => 'Продолжительность отопительного периода',
            'year_start_consume' => 'Год начала потребления',
            'object_type' => 'Тип объекта',
            'city_id' => 'Город',
            'region_id' => 'Регион',
        ];
    }

    /**
     * @return array
     */
    public static function objectTypeLabels()
    {
        return [
            self::OBJECT_TYPE_BUILDING => 'Новое строительство',
            self::OBJECT_TYPE_RECONSTRUCTION => 'Реконструкция',
            self::OBJECT_TYPE_MODERNIZATION => 'Модернизация',
            self::OBJECT_TYPE_UPDATE_EQUIPMENT => 'Дооборудование',
            self::OBJECT_TYPE_UPDATE_BUILDING => 'Достройка'
        ];
    }

    /**
     * @return bool
     */
    public function isValidEquipments()
    {
        $equipments = Equipment::find()->where(['calculation_id' => $this->id])->all();
        foreach ($equipments as $eq) {
            $object = Objects::find()->where(['calculation_id' => $eq->calculation_id, 'number_group' => $eq->number_group])->one();

            $eq->coefficient_load = $eq->formula->getO5();

            if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
                if($eq->power_v == 0 || $eq->power_v == null || $eq->coefficient_load > 1){
                    return false;
                }
            } else {
                if($eq->consumption_e == 0 || $eq->consumption_e == null || $eq->coefficient_load > 1){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @return CalculationFormula
     */
    public function getFormula()
    {
        if($this->_formula == null){
            $this->_formula = new CalculationFormula(['model' => $this]);
        }

        return $this->_formula;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipments()
    {
        return $this->hasMany(Equipment::className(), ['calculation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Objects::className(), ['calculation_id' => 'id']);
    }
}
