<?php

namespace app\models\forms;

use app\models\Objects;
use app\models\ObjectsOv;
use Yii;
use yii\base\Model;


class ObjectsOvForm extends Model
{
    public $building_type;
    public $building_volume;
    public $specific_thermal_characteristic_heating;
    public $specific_thermal_characteristic_ventilation;
    public $specific_thermal_characteristic;
    public $building_height;
    public $internal_temperature;
    public $heat_loads_known;
    public $heating_load;
    public $ventilation_load;
    public $name;
    public $count_object;
    public $calculation_id;
    public $type_object;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_ov';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'internal_temperature'], 'required'],
            [['specific_thermal_characteristic_heating', 'specific_thermal_characteristic_ventilation', 'building_height', 'building_volume', 'building_type'], 'required', 'when' => function(){
                return $this->heat_loads_known != 1;
            }],
            [['count_object'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['heat_loads_known'], 'integer'],
            [['building_volume', 'specific_thermal_characteristic_heating', 'specific_thermal_characteristic_ventilation', 'building_height', 'internal_temperature', 'heating_load', 'ventilation_load'], 'number'],
            ['heat_loads_known', 'safe'],
            [['specific_thermal_characteristic'], 'string', 'max' => 255],

            ['heat_loads_known', function(){
                if($this->heat_loads_known == 1){
                    if($this->heating_load == 0 && $this->ventilation_load == 0){
                        $this->addError('heat_loads_known', 'Заполните пожалуйста выделенные поля');
                        $this->addError('heating_load', 'Это поле обязательно для заполнения');
                        $this->addError('ventilation_load', 'Это поле обязательно для заполнения');
                        return false;
                    }
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название системы',
            'count_object' => 'Количество',
            'building_type' => 'Тип здания',
            'building_volume' => 'Строительный объем здания, V, м3',
            'specific_thermal_characteristic_heating' => 'Удельная тепловая характеристика отопления',
            'specific_thermal_characteristic_ventilation' => 'Удельная тепловая характеристика вентиляции',
            'specific_thermal_characteristic' => 'Удельная тепловая характеристика',
            'building_height' => 'Свободная высота здания, L, м',
            'internal_temperature' => 'Внутренняя температура, tj, oC',
            'heat_loads_known' => 'Известны проектные тепловые нагрузки',
            'heating_load' => 'Нагрузка отопления q, Гкал/час',
            'ventilation_load' => 'Нагрузка вентиляции q, Гкал/час' ,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);

    }

    public function loadData($id)
    {
        $objects = Objects::find()->where(['id'=>$id])->asArray()->one();
        $this->load($objects,'');
        $this->load(ObjectsOv::find()->where(['objects_id'=>(int)$objects['id']])->asArray()->one(),'');
    }

    public function save($id = null)
    {
        if ($id) {
            $objects = Objects::findOne($id);
            $objectsOv = ObjectsOv::findOne(['objects_id' => $objects->id]);
        } else {
            $objects = new Objects();
            $objects->calculation_id = $this->calculation_id;
            $objects->type_object = $this->type_object;
            $objectsOv = new ObjectsOv();
        }


        $objects->count_object = $this->count_object;
        $objects->name = $this->name;
        $objects->save();

        $objectsOv->objects_id = $objects->id;
        $objectsOv->building_height = $this->building_height;
        $objectsOv->building_type = $this->building_type;
        $objectsOv->building_volume = $this->building_volume;
        $objectsOv->heat_loads_known = $this->heat_loads_known;
        $objectsOv->heating_load = $this->heating_load;
        $objectsOv->ventilation_load = $this->ventilation_load;
        $objectsOv->internal_temperature = $this->internal_temperature;
        $objectsOv->specific_thermal_characteristic = $this->specific_thermal_characteristic;
        $objectsOv->specific_thermal_characteristic_heating = $this->specific_thermal_characteristic_heating;
        $objectsOv->specific_thermal_characteristic_ventilation = $this->specific_thermal_characteristic_ventilation;
        $objectsOv->save();

    }
}