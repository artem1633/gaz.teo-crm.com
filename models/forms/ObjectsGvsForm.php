<?php

namespace app\models\forms;

use app\models\Objects;
use app\models\ObjectsGvs;
use app\models\ObjectsOv;
use Yii;
use yii\base\Model;


class ObjectsGvsForm extends Model
{
    public $name;
    public $count_object;
    public $calculation_id;
    public $type_object;

    public $purpose_system;
    public $water_consumption_rate;
    public $count_work_day;
    public $work_per_day;
    public $calculation_regarding_quantity;
    public $count_consumers;
    public $heat_loads_known;
    public $heating_load;
    public $ventilation_load;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_ov';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {

        return [
            [['name', 'count_object', 'count_work_day',
                'work_per_day', 'heat_loads_known'], 'required'],
            [['water_consumption_rate', 'calculation_regarding_quantity', 'count_consumers', 'purpose_system'], 'required', 'when' => function(){
                return $this->heat_loads_known != 1;
            }],
            [['name'], 'string', 'max' => 255],
            [['heat_loads_known','count_object', 'purpose_system', 'count_consumers', 'heat_loads_known'], 'integer'],
            [['water_consumption_rate', 'heating_load', 'ventilation_load'], 'number'],
            [['calculation_regarding_quantity'], 'string', 'max' => 255],
            ['heat_loads_known', 'safe'],

            ['count_work_day', 'number', 'min' => 1, 'max' => 7],
            ['work_per_day', 'number', 'min' => 1, 'max' => 24],

            ['heat_loads_known', function(){
                if($this->heat_loads_known == 1){
                    if($this->heating_load == 0 && $this->ventilation_load == 0){
                        $this->addError('heat_loads_known', 'Заполните пожалуйста выделенные поля');
                        // $this->addError('heating_load', 'Это поле обязательно для заполнения');
                        // $this->addError('ventilation_load', 'Это поле обязательно для заполнения');
                        return false;
                    }
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название системы',
            'count_object' => 'Количество',
            'purpose_system' => 'Назначение системы',
            'water_consumption_rate' => 'Сут. норма затрат воды',
            'count_work_day' => 'Количество рабочих дней ГВС, в неделю (от 1 до 7)',
            'work_per_day' => 'Время работы в сутки, ч (от 1 до 24)',
            'calculation_regarding_quantity' => 'Расчет относительно количества',
            'count_consumers' => 'Количество (потребителей)',
            'heat_loads_known' => 'Известны проектные тепловые нагрузки',
            'heating_load' => 'Нагрузка потребления воды q, Гкал/час',
            'ventilation_load' => 'Нагрузка циркуляции q, Гкал/час',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);

    }

    public function loadData($id)
    {
        $objects = Objects::find()->where(['id'=>$id])->asArray()->one();
        $this->load($objects,'');
        $this->load(ObjectsGvs::find()->where(['objects_id'=>(int)$objects['id']])->asArray()->one(),'');
    }

    public function save($id = null)
    {
        if ($id) {
            $objects = Objects::findOne($id);
            $objectsOv = ObjectsGvs::findOne(['objects_id' => $objects->id]);
        } else {
            $objects = new Objects();
            $objects->calculation_id = $this->calculation_id;
            $objects->type_object = $this->type_object;
            $objectsOv = new ObjectsGvs();
        }

        $objects->count_object = $this->count_object;
        $objects->name = $this->name;
        $objects->save();

        $objectsOv->objects_id = $objects->id;
        $objectsOv->purpose_system = $this->purpose_system;
        $objectsOv->water_consumption_rate = $this->water_consumption_rate;
        $objectsOv->count_work_day = $this->count_work_day;
        $objectsOv->work_per_day = $this->work_per_day;
        $objectsOv->calculation_regarding_quantity = $this->calculation_regarding_quantity;
        $objectsOv->count_consumers = $this->count_consumers;
        $objectsOv->heat_loads_known = $this->heat_loads_known;
        $objectsOv->heating_load = $this->heating_load;
        $objectsOv->ventilation_load = $this->ventilation_load;
        $objectsOv->save();

    }
}