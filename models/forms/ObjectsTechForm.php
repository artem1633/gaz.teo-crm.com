<?php

namespace app\models\forms;

use app\models\Objects;
use app\models\ObjectsOv;
use app\models\ObjectsTech;
use Yii;
use yii\base\Model;


class ObjectsTechForm extends Model
{
    public $purpose_system;
    public $destination_system;
    public $consumption_thermal_units;
    public $consumption_equivalent_fuel;
    public $day_per_week;
    public $norm_document;
    public $amount_name;
    public $amount;
    public $thermal_1_7;

    public $name;
    public $count_object;
    public $calculation_id;
    public $type_object;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects_ov';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'count_object', 'purpose_system', 'consumption_thermal_units', 'consumption_equivalent_fuel',  'day_per_week', 'norm_document', 'amount'], 'required'],
            [['count_object'], 'integer'],
            [['name', 'destination_system'], 'string', 'max' => 255],
            [['purpose_system'], 'integer'],
            [['consumption_thermal_units', 'consumption_equivalent_fuel', 'amount', 'thermal_1_7'], 'number'],
            [['norm_document', 'amount_name'], 'string', 'max' => 255],

            ['day_per_week', 'integer', 'min' => 1, 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название системы',
            'count_object' => 'Количество',
            'purpose_system' => 'Выбор системы',
            'destination_system' => 'Назначение системы',
            'consumption_thermal_units' => 'Расход тепловых единиц [тыс.ккал]',
            'consumption_equivalent_fuel' => 'Расход условного топлива [кг]',
            'day_per_week' => 'Количество рабочих дней в неделю (от 1 до 7)',
            'norm_document' => 'Комментарий, ссылка на норматив',
            'amount_name' => 'Amount Name',
            'amount' => 'Количество',
            'thermal_1_7' => '1,7 расхода тепла',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);

    }

    public function loadData($id)
    {
        $objects = Objects::find()->where(['id' => $id])->asArray()->one();
        $this->load($objects, '');
        $this->load(ObjectsTech::find()->where(['objects_id' => (int)$objects['id']])->asArray()->one(), '');
    }

    public function save($id = null)
    {
        if ($id) {
            $objects = Objects::findOne($id);
            $objectsOv = ObjectsTech::findOne(['objects_id' => $objects->id]);
        } else {
            $objects = new Objects();
            $objects->calculation_id = $this->calculation_id;
            $objects->type_object = $this->type_object;
            $objectsOv = new ObjectsTech();
        }


        // $objects->count_object = $this->count_object;
        $objects->count_object = 1;
        $objects->name = $this->name;
        $objects->save();

        $objectsOv->objects_id = $objects->id;
        $objectsOv->purpose_system = $this->purpose_system;
        $objectsOv->destination_system = $this->destination_system;
        $objectsOv->consumption_thermal_units = $this->consumption_thermal_units;
        $objectsOv->consumption_equivalent_fuel = $this->consumption_equivalent_fuel;
        $objectsOv->day_per_week = $this->day_per_week;
        $objectsOv->norm_document = $this->norm_document;
        $objectsOv->amount_name = $this->amount_name;
        $objectsOv->amount = $this->amount;
        $objectsOv->thermal_1_7 = $this->thermal_1_7;
        $objectsOv->save();

    }
}