<?php

namespace app\models;

use app\formulas\ObjectFormula;
use Yii;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property int $calculation_id Расчет
 * @property string $name Название системы
 * @property int $count_object Количество
 * @property int $status Статус
 * @property int $type_object Тип объекта
 * @property int $number_group Группа
 *
 * @property ObjectFormula $formula
 *
 * @property Calculation $calculation
 * @property ObjectsGvs[] $objectsGvs
 * @property ObjectsOv[] $objectsOvs
 * @property ObjectsTech[] $objectsTeches
 */
class Objects extends \yii\db\ActiveRecord
{
    private $_formula;

    CONST TYPE_OV = 1;
    CONST TYPE_GVS = 2;
    CONST TYPE_TECH = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calculation_id', 'count_object', 'status', 'type_object', 'number_group'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['calculation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calculation::className(), 'targetAttribute' => ['calculation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'calculation_id' => 'Расчет',
            'name' => 'Название системы',
            'count_object' => 'Количество',
            'status' => 'Статус',
            'type_object' => 'Тип объекта',
            'number_group' => 'Группа',
            'power_g' =>'Мощность',
            'power_v' =>'Мощность, Вт'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete()
    {
        $groupObjectsCount = Objects::find()->where(['calculation_id' => $this->calculation_id, 'number_group' => $this->number_group])->count();

        if($groupObjectsCount == 0){
            Equipment::deleteAll(['calculation_id' => $this->calculation_id, 'number_group' => $this->number_group]);
        }

        parent::afterDelete();
    }

    /**
     * @return ObjectFormula
     */
    public function getFormula()
    {
        if($this->_formula == null){
            $this->_formula = new ObjectFormula(['model' => $this]);
        }

        return $this->_formula;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalculation()
    {
        return $this->hasOne(Calculation::className(), ['id' => 'calculation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectsGvs()
    {
        return $this->hasMany(ObjectsGvs::className(), ['objects_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectsOvs()
    {
        return $this->hasMany(ObjectsOv::className(), ['objects_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectsTeches()
    {
        return $this->hasMany(ObjectsTech::className(), ['objects_id' => 'id']);
    }

    public static function getTypesObjects()
    {
        return [
            static::TYPE_OV => 'Отопление и вентиляция',
            static::TYPE_GVS => 'Горячее водоснабжение',
            static::TYPE_TECH => 'Технология',
        ];
    }
}
