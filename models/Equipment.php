<?php

namespace app\models;

use app\formulas\EquipmentFormula;
use Yii;

/**
 * This is the model class for table "equipment".
 *
 * @property int $id
 * @property int $calculation_id Расчет
 * @property int $number_group Группа
 * @property int $type_object Тип объекта
 * @property string $name_equipment Наименование оборудования
 * @property string $KPD КПД в %
 * @property string $coefficient_unambiguity Коэффициент однозначности
 * @property int $count_equipment Количество, ед
 * @property string $coefficient_load Коэффициент загрузки
 * @property string $power_v Мощность, кВт
 * @property string $pover_g Мощность, Гкал/ч
 * @property float $pover_common Мощность общая
 * @property string $specific_consumption Удельный расход, Гкал/ч
 * @property string $consumption_e Расход ед., м3/ч
 * @property string $consumption_o Расход об., м3/ч
 * @property int $status Статус
 *
 *
 * @property EquipmentFormula $formula
 *
 * @property Calculation $calculation
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * @var EquipmentFormula
     */
    private $_formula;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calculation_id', 'number_group', 'type_object', 'count_equipment', 'status'], 'integer'],
            [['KPD', 'power_v', 'pover_g', 'pover_common', 'specific_consumption', 'consumption_e', 'consumption_o'], 'number'],
            [['name_equipment'], 'string', 'max' => 255],
            ['coefficient_unambiguity', 'number', 'min' => 0, 'max' => 1],
            ['coefficient_load', 'number', 'min' => 0, 'max' => 1],
            [['calculation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calculation::className(), 'targetAttribute' => ['calculation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'calculation_id' => 'Расчет',
            'number_group' => 'Группа',
            'type_object' => 'Тип объекта',
            'name_equipment' => 'Наименование оборудования',
            'KPD' => 'КПД в %',
            'coefficient_unambiguity' => 'Коэффициент одновременности',
            'count_equipment' => 'Количество, ед',
            'coefficient_load' => 'Коэффициент загрузки',
            'power_v' => 'Мощность, кВт',
            'pover_g' => 'Мощность, Гкал/ч',
            'pover_common' => 'Мощность общая',
            'specific_consumption' => 'Удельный расход, Гкал/ч',
            'consumption_e' => 'Расход ед., м3/ч',
            'consumption_o' => 'Расход об., м3/ч',
            'status' => 'Статус',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $sumOvQomax = 0;
            $sumOvQomax1 = 0;
            $sumOvQvmax = 0;
            $sumOvQvmax1 = 0;
            $sumOvQo = 0;
            $sumOvQv = 0;
            $sumOvGncho = 0;
            $sumOvGnchv = 0;
            $sumOvGno = 0;
            $sumOvGnv = 0;
            $sumOvGuto = 0;
            $sumOvGutv = 0;

            $sumGvsQhm = 0;
            $sumGvsQhm1 = 0;
            $sumGvsQh = 0;
            $sumGvsGnch = 0;
            $sumGvsGn = 0;
            $sumGvsGut = 0;

            $sumTehQh = 0;
            $sumTehQh1 = 0;
            $sumTehQn = 0;
            $sumTehQhd = 0;
            $sumTehGn = 0;
            $sumTehGut = 0;


            $objects = Objects::find()->where(['number_group' => $this->number_group, 'calculation_id' => $this->calculation_id])->all();

            $calculation = Calculation::findOne($this->calculation_id);

            foreach ($objects as $object)
            {
                if($object->type_object == Objects::TYPE_OV){
                    $object = ObjectsOv::findOne(['objects_id' => $object->id]);

                    if($object){
                        $sumOvQomax += $object->formula->getOvQomax();
                        $sumOvQomax1 += $object->formula->getOvQomax1();
                        $sumOvQvmax += $object->formula->getOvQvmax();
                        $sumOvQvmax1 += $object->formula->getOvQvmax1();
                        $sumOvQo += $object->formula->getQo();
                        $sumOvQv += $object->formula->getQv();
                        $sumOvGncho += $object->formula->getOvGncho();
                        $sumOvGnchv += $object->formula->getOvGnchv();
                        $sumOvGno += $object->formula->getOvGno();
                        $sumOvGnv += $object->formula->getOvGnv();
                        $sumOvGuto += $object->formula->getOvGuto();
                        $sumOvGutv += $object->formula->getOvGutv();
                    }

                } else if($object->type_object == Objects::TYPE_GVS){
                    $object = ObjectsGvs::findOne(['objects_id' => $object->id]);

                    if($object){
                        $sumGvsQhm += $object->formula->getGvsQhm();
                        $sumGvsQhm1 += $object->formula->getGvsQhm1();
                        $sumGvsQh += $object->formula->getGvsQh();
                        $sumGvsGnch += $object->formula->getGvsGnch();
                        $sumGvsGn += $object->formula->getGvsGn();
                        $sumGvsGut += $object->formula->getGvsGut();
                    }

                } else if($object->type_object == Objects::TYPE_TECH){
                    $object = ObjectsTech::findOne(['objects_id' => $object->id]);

                    if($object){
                        $sumTehQh += $object->formula->getTehQh();
                        $sumTehQh1 += $object->formula->getTehQh1();
                        $sumTehQn += $object->formula->getTehQn();
                        $sumTehQhd += $object->formula->getTehQhd();
                        $sumTehGn += $object->formula->getTehGn();
                        $sumTehGut += $object->formula->getTehGut();
                    }

                }
            }


            $allQhmax = $sumOvQomax + $sumOvQvmax + $sumGvsQhm + $sumTehQh;
            $allQhmax1 = $sumOvQomax1 + $sumOvQvmax1 + $sumGvsQhm1 + $sumTehQh1;
            $allQh = $sumOvQo + $sumOvQv + $sumGvsQh + $sumTehQn;
            $allQh1 = ($sumOvQo * 4.1868) + ($sumOvQv * 4.1868) + ($sumGvsQh * 4.1868) + ($sumTehQn * 4.1868);
            $allGnch = $sumOvGncho + $sumOvGnchv + $sumGvsGnch + $sumTehQhd;
            $allGn = $sumOvGno + $sumOvGnv + $sumGvsGn + $sumTehGn;
            $allGut = $sumOvGuto + $sumOvGutv + $sumGvsGut + $sumTehGut;

            if($this->KPD == null){
                $this->KPD = 100;
            }
            if($this->coefficient_unambiguity == null){
                $this->coefficient_unambiguity = 1;
            }
            if($this->count_equipment == null){
                $this->count_equipment = 1;
            }
            if($this->power_v == null){
                $this->power_v = 0;
            }
            if($this->pover_g == null){
                $this->pover_g = $this->power_v / 1164;
            }
            if($this->pover_common == null){
                $this->pover_common = $this->power_v * $this->count_equipment * $this->pover_g * $this->count_equipment;
            }
            if($this->specific_consumption == null){
                $this->specific_consumption = 100000 / 7000 / $this->KPD;
            }
            // if($this->consumption_e == null){
                // $this->consumption_e = $this->pover_g / $calculation->formula->getZak6() * 1000000 / $this->KPD;
            // }
            // if($this->consumption_o == null){
                // $this->consumption_o = $this->consumption_e * $this->coefficient_unambiguity * $this->count_equipment;
            // }

//            $allQhmax1

            if($this->coefficient_load == null && $this->power_v > 0){
                $this->coefficient_load = $allQhmax1 / $this->coefficient_unambiguity / $this->count_equipment / $this->power_v;
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return EquipmentFormula
     */
    public function getFormula()
    {
        if($this->_formula == null){
            $this->_formula = new EquipmentFormula(['model' => $this]);
        }

        return $this->_formula;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalculation()
    {
        return $this->hasOne(Calculation::className(), ['id' => 'calculation_id']);
    }
}
