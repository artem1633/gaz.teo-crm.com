<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%calculation}}`.
 */
class m191117_154109_add_year_start_consume_column_to_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('calculation', 'year_start_consume', $this->string()->comment('Год начала потребления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('calculation', 'year_start_consume');
    }
}
