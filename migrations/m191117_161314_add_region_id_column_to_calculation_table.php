<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%calculation}}`.
 */
class m191117_161314_add_region_id_column_to_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('calculation', 'region_id', $this->integer()->comment('Регион'));

        $this->createIndex(
            'idx-calculation-region_id',
            'calculation',
            'region_id'
        );

        $this->addForeignKey(
            'fk-calculation-region_id',
            'calculation',
            'region_id',
            'region',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-calculation-region_id',
            'calculation'
        );

        $this->dropIndex(
            'idx-calculation-region_id',
            'calculation'
        );

        $this->dropColumn('calculation', 'region_id');
    }
}
