<?php

use app\helpers\ImportCity;
use yii\db\Migration;

/**
 * Handles the creation of table '{{%city}}'.
 */
class m191107_123051_create_city_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer()->comment('Регион'),
            'name' => $this->string()->comment('Название'),
            'temperature_average' => $this->decimal(8, 2)->notNull()->comment('Средняянаружного воздуха за отопительный период, tc ,oC'),
            'temperature_calculated' => $this->decimal(8, 2)->notNull()->comment('Расчетнаянаружного воздуха для отопления, to,oC'),
            'winder_speed' => $this->decimal(8, 2)->notNull()->comment('Расчетная скорость ветра, w, м/с'),
            'heating_period' => $this->decimal(8, 2)->notNull()->defaultValue(0)->comment('Продолжительность отопительного периода, no, сут'),
            'c_alfa' => $this->decimal(8, 3)->notNull()->defaultValue(1)->comment('Поправочный коэффициент a-учитывающий отличие расчетной температуры нар. воздуха для проектирования отопления tO от -30 ,oC'),
            'temp_average1' => $this->decimal(8, 2)->notNull()->comment('Январь')->defaultValue(0),
            'temp_average2' => $this->decimal(8, 2)->notNull()->comment('Февраль')->defaultValue(0),
            'temp_average3' => $this->decimal(8, 2)->notNull()->comment('Март')->defaultValue(0),
            'temp_average4' => $this->decimal(8, 2)->notNull()->comment('Апрель')->defaultValue(0),
            'temp_average5' => $this->decimal(8, 2)->notNull()->comment('Май')->defaultValue(0),
            'temp_average6' => $this->decimal(8, 2)->notNull()->comment('Июнь')->defaultValue(0),
            'temp_average7' => $this->decimal(8, 2)->notNull()->comment('Июль')->defaultValue(0),
            'temp_average8' => $this->decimal(8, 2)->notNull()->comment('Август')->defaultValue(0),
            'temp_average9' => $this->decimal(8, 2)->notNull()->comment('Сентябрь')->defaultValue(0),
            'temp_average10' => $this->decimal(8, 2)->notNull()->comment('Октябрь')->defaultValue(0),
            'temp_average11' => $this->decimal(8, 2)->notNull()->comment('Ноябрь')->defaultValue(0),
            'temp_average12' => $this->decimal(8, 2)->notNull()->comment('Декабрь')->defaultValue(0),
            'duration_op1' => $this->tinyInteger(2)->defaultValue(31)->comment('Январь'),
            'duration_op2' => $this->tinyInteger(2)->defaultValue(28)->comment('Февраль'),
            'duration_op3' => $this->tinyInteger(2)->defaultValue(31)->comment('Март'),
            'duration_op4' => $this->tinyInteger(2)->defaultValue(30)->comment('Апрель'),
            'duration_op5' => $this->tinyInteger(2)->defaultValue(0)->comment('Май'),
            'duration_op6' => $this->tinyInteger(2)->defaultValue(0)->comment('Июнь'),
            'duration_op7' => $this->tinyInteger(2)->defaultValue(0)->comment('Июль'),
            'duration_op8' => $this->tinyInteger(2)->defaultValue(0)->comment('Август'),
            'duration_op9' => $this->tinyInteger(2)->defaultValue(0)->comment('Сентябрь'),
            'duration_op10' => $this->tinyInteger(2)->defaultValue(31)->comment('Октябрь'),
            'duration_op11' => $this->tinyInteger(2)->defaultValue(30)->comment('Ноябрь'),
            'duration_op12' => $this->tinyInteger(2)->defaultValue(31)->comment('Декабрь'),
        ]);
        $this->addForeignKey('fk-city-region_id','{{city}}','region_id','region','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-city-region_id','{{city}}');
        $this->dropTable('{{%city}}');
    }
}
