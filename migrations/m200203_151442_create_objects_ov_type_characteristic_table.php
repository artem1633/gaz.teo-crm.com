<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_ov_type_characteristic}}`.
 */
class m200203_151442_create_objects_ov_type_characteristic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_ov_type_characteristic}}', [
            'id' => $this->primaryKey(),
            'objects_ov_type_id' => $this->integer()->comment('Тип'),
            'building_volume' => $this->float()->comment('Строительный объем здания'),
            'heating' => $this->float()->comment('Удельная тепловая характеристика отопления'),
            'ventilation' => $this->float()->comment('Удельная тепловая характеристика вентиляции'),
        ]);

        $this->createIndex(
            'idx-objects_ov_type_characteristic-objects_ov_type_id',
            'objects_ov_type_characteristic',
            'objects_ov_type_id'
        );

        $this->addForeignKey(
            'fk-objects_ov_type_characteristic-objects_ov_type_id',
            'objects_ov_type_characteristic',
            'objects_ov_type_id',
            'objects_ov_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-objects_ov_type_characteristic-objects_ov_type_id',
            'objects_ov_type_characteristic'
        );

        $this->dropIndex(
            'idx-objects_ov_type_characteristic-objects_ov_type_id',
            'objects_ov_type_characteristic'
        );

        $this->dropTable('{{%objects_ov_type_characteristic}}');
    }
}
