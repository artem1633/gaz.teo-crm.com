<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%objects_tech_type}}`.
 */
class m200130_152456_add_one_name_column_to_objects_tech_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects_tech_type', 'one_name', $this->string()->comment('Наименование одной единицы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects_tech_type', 'one_name');
    }
}
