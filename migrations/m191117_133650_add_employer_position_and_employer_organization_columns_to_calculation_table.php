<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%calculation}}`.
 */
class m191117_133650_add_employer_position_and_employer_organization_columns_to_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('calculation','employer_organization', $this->string()->comment('Организация исполнителя'));
        $this->addColumn('calculation','employer_position', $this->string()->comment('Должность исполнителя'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('calculation','employer_organization');
        $this->dropColumn('calculation','employer_position');
    }
}
