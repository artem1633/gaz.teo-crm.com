<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%calculation}}`.
 */
class m191117_160325_add_city_id_column_to_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('calculation', 'city_id', $this->integer()->comment('Город'));

        $this->createIndex(
            'idx-calculation-city_id',
            'calculation',
            'city_id'
        );

        $this->addForeignKey(
            'fk-calculation-city_id',
            'calculation',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-calculation-city_id',
            'calculation'
        );

        $this->dropIndex(
            'idx-calculation-city_id',
            'calculation'
        );

        $this->dropColumn('calculation', 'city_id');
    }
}
