<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%objects_tech}}`.
 */
class m191213_192719_add_thermal_1_7_column_to_objects_tech_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects_tech', 'thermal_1_7', $this->float()->comment('1,7 расхода тепла'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects_tech', 'thermal_1_7');
    }
}
