<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_gvs}}`.
 */
class m191108_145004_create_objects_gvs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_gvs}}', [
            'id' => $this->primaryKey(),
            'objects_id' => $this->integer()->comment('Объект'),
            'purpose_system' =>$this->smallInteger(2)->comment('Назначение системы'),
            'water_consumption_rate' => $this->decimal(8,3)->comment('Сут. норма затрат воды (коэф.альфа)'),
            'count_work_day'=>$this->smallInteger(2)->comment('Количество рабочих дней ГВС, в неделю'),
            'work_per_day'=>$this->decimal(8,2)->comment('Время работы в сутки, ч'),
            'calculation_regarding_quantity'=>$this->string()->comment('Расчет относительно количества'),
            'count_consumers'=>$this->integer()->comment('Количество (потребителей)'),
            'heat_loads_known'=>$this->boolean()->comment('Известны проектные тепловые нагрузки'),
            'heating_load'=>$this->decimal(8,3)->comment('Нагрузка отопления q, Гкал/час'),
            'ventilation_load'=>$this->decimal(8,3)->comment('Нагрузка вентиляции q, Гкал/час'),

        ]);
        $this->addForeignKey('fk-objects_gvs-objects_id','{{%objects_gvs}}','objects_id','{{objects}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-objects_gvs-objects_id','{{%objects_gvs}}');
        $this->dropTable('{{%objects_gvs}}');
    }
}
