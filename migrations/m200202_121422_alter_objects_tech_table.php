<?php

use yii\db\Migration;

/**
 * Class m200202_121422_alter_objects_tech_table
 */
class m200202_121422_alter_objects_tech_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('objects_tech', 'consumption_thermal_units', $this->float());
        $this->alterColumn('objects_tech', 'consumption_equivalent_fuel', $this->float());
        $this->alterColumn('objects_tech', 'amount', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
