<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%calculation}}`.
 */
class m191117_135919_add_type_oil_column_to_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('calculation', 'type_oil', $this->string()->comment('Вид топлива'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('calculation', 'type_oil');
    }
}
