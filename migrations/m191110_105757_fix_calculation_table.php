<?php

use yii\db\Migration;

/**
 * Class m191110_105757_fix_calculation_table
 */
class m191110_105757_fix_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{calculation}}','certificate of consent','certificate_of_consent');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191110_105757_fix_calculation_table cannot be reverted.\n";

        return false;
    }
    */
}
