<?php

use yii\db\Migration;

/**
 * Class m200202_114807_alter_objects_ov_table
 */
class m200202_114807_alter_objects_ov_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('objects_ov', 'building_volume', $this->float());
        $this->alterColumn('objects_ov', 'heating_load', $this->float());
        $this->alterColumn('objects_ov', 'ventilation_load', $this->float());
        $this->alterColumn('objects_ov', 'internal_temperature', $this->float());
        $this->alterColumn('objects_ov', 'building_height', $this->float());
        $this->alterColumn('objects_ov', 'specific_thermal_characteristic_heating', $this->float());
        $this->alterColumn('objects_ov', 'specific_thermal_characteristic_ventilation', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
