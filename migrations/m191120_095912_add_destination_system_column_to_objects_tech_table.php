<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%objects_tech}}`.
 */
class m191120_095912_add_destination_system_column_to_objects_tech_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects_tech', 'destination_system', $this->string()->comment('Назначение системы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects_tech', 'destination_system');
    }
}
