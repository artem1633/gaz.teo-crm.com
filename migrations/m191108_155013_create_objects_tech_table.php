<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_tech}}`.
 */
class m191108_155013_create_objects_tech_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_tech}}', [
            'id' => $this->primaryKey(),
            'objects_id' => $this->integer()->comment('Объект'),
            'purpose_system' => $this->smallInteger(2)->comment('Назначение системы'),
            'consumption_thermal_units'=>$this->decimal(8,3)->comment('Расход тепловых единиц'),
            'consumption_equivalent_fuel'=>$this->decimal(8,3)->comment('Расход условного топлива'),
            'day_per_week' =>$this->smallInteger(1)->comment('Расход условного топлива'),
            'norm_document' =>$this->string()->comment('Комментарий, ссылка на норматив'),
            'amount_name' => $this->string()->comment(''),
            'amount' => $this->decimal(8,2)->comment(''),
        ]);
        $this->addForeignKey('fk-objects_tech-objects_id','{{%objects_tech}}','objects_id','{{objects}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-objects_tech-objects_id','{{%objects_tech}}');
        $this->dropTable('{{%objects_tech}}');
    }
}
