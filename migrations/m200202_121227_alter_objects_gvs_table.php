<?php

use yii\db\Migration;

/**
 * Class m200202_121227_alter_objects_gvs_table
 */
class m200202_121227_alter_objects_gvs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('objects_gvs', 'heating_load', $this->float());
        $this->alterColumn('objects_gvs', 'ventilation_load', $this->float());
        $this->alterColumn('objects_gvs', 'water_consumption_rate', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
