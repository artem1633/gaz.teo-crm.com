<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_ov_type}}`.
 */
class m191109_012947_create_objects_ov_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_ov_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'specific_thermal_characteristic_heating' => $this->decimal(8,3)->comment('Удельная тепловая характеристика отопления'),
            'specific_thermal_characteristic_ventilation' => $this->decimal(8,3)->comment('Удельная тепловая характеристика вентиляции'),
        ]);

        $this->insert('{{%objects_ov_type}}',['name' =>'Административные здания, конторы','specific_thermal_characteristic_heating' =>0.43,'specific_thermal_characteristic_ventilation' =>0.09]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Клубы','specific_thermal_characteristic_heating' =>0.37,'specific_thermal_characteristic_ventilation' =>0.25]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Кинотеатры','specific_thermal_characteristic_heating' =>0.36,'specific_thermal_characteristic_ventilation' =>0.43]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Театры','specific_thermal_characteristic_heating' =>0.29,'specific_thermal_characteristic_ventilation' =>0.41]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Магазины','specific_thermal_characteristic_heating' =>0.38,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Детские сады и ясли','specific_thermal_characteristic_heating' =>0.38,'specific_thermal_characteristic_ventilation' =>0.11]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Школы и высшие учебные заведения','specific_thermal_characteristic_heating' =>0.39,'specific_thermal_characteristic_ventilation' =>0.09]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Больницы','specific_thermal_characteristic_heating' =>0.4,'specific_thermal_characteristic_ventilation' =>0.29]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Бани','specific_thermal_characteristic_heating' =>0.28,'specific_thermal_characteristic_ventilation' =>1]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Прачечные','specific_thermal_characteristic_heating' =>0.38,'specific_thermal_characteristic_ventilation' =>0.8]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Предприятия общественного питания, столовые, фабрики-кухни','specific_thermal_characteristic_heating' =>0.35,'specific_thermal_characteristic_ventilation' =>0.7]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Лаборатории','specific_thermal_characteristic_heating' =>0.37,'specific_thermal_characteristic_ventilation' =>1]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Пожарные депо','specific_thermal_characteristic_heating' =>0.48,'specific_thermal_characteristic_ventilation' =>0.14]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Гаражи','specific_thermal_characteristic_heating' =>0.7,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Жилые здания постройка до 1958 г.','specific_thermal_characteristic_heating' =>0.62,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Жилые здания постройка после 1958 г.','specific_thermal_characteristic_heating' =>0.78,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Здания, построенные до 1930 г. to &gt; -20 °C','specific_thermal_characteristic_heating' =>0.45,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Здания, построенные до 1930 г.-20 °С &gt; to &gt;= -30 °С','specific_thermal_characteristic_heating' =>0.41,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Здания, построенные до 1930 г. to &lt; -30 °С','specific_thermal_characteristic_heating' =>0.37,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Прочие здания, построенные до 1958 г.','specific_thermal_characteristic_heating' =>0.66,'specific_thermal_characteristic_ventilation' =>0]);
        $this->insert('{{%objects_ov_type}}',['name' =>'Прочие здания, построенные после 1958 г.','specific_thermal_characteristic_heating' =>0.65,'specific_thermal_characteristic_ventilation' =>0]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%objects_ov_type}}');
    }
}
