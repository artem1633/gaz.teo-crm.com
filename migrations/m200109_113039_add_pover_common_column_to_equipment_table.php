<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%equipment}}`.
 */
class m200109_113039_add_pover_common_column_to_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('equipment', 'pover_common', $this->float()->comment('Общая мощность'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('equipment', 'pover_common');
    }
}
