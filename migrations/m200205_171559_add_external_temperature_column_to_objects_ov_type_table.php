<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%objects_ov_type}}`.
 */
class m200205_171559_add_external_temperature_column_to_objects_ov_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects_ov_type', 'external_temperature', $this->float()->comment('Наружная температура воздуха'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects_ov_type', 'external_temperature');
    }
}
