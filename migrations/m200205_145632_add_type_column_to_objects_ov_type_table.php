<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%objects_ov_type}}`.
 */
class m200205_145632_add_type_column_to_objects_ov_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects_ov_type', 'type', $this->integer()->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects_ov_type', 'type');
    }
}
