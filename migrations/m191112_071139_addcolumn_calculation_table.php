<?php

use yii\db\Migration;

/**
 * Class m191112_071139_addcolumn_calculation_table
 */
class m191112_071139_addcolumn_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{calculation}}','employer_name',$this->string()->comment('Исполнитель'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{calculation}}','employer_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191112_071139_addcolumn_calculation_table cannot be reverted.\n";

        return false;
    }
    */
}
