<?php

use yii\db\Migration;

/**
 * Class m191110_215516_addcolumn_object_table
 */
class m191110_215516_addcolumn_object_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{objects}}','power_v',$this->decimal(8,3)->comment('Мощность, кВт'));
        $this->addColumn('{{objects}}','power_g',$this->decimal(8,3)->comment('Мощность, Гкал'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{objects}}','power_v');
        $this->dropColumn('{{objects}}','power_g');
    }

}
