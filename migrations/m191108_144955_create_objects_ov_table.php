<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_ov}}`.
 */
class m191108_144955_create_objects_ov_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_ov}}', [
            'id' => $this->primaryKey(),
            'objects_id' => $this->integer()->comment('Объект'),
            'building_type' => $this->smallInteger(2)->comment('Тип здания'),
            'building_volume' => $this->decimal(8,3)->comment('Строительный объем здания, V, м3'),
            'specific_thermal_characteristic_heating' => $this->decimal(8,3)->comment('Удельная тепловая характеристика отопления'),
            'specific_thermal_characteristic_ventilation' => $this->decimal(8,3)->comment('Удельная тепловая характеристика вентиляции'),
            'specific_thermal_characteristic'=>$this->string()->comment(''),
            'building_height'=>$this->decimal(8,2)->comment('Свободная высота здания, L, м'),
            'internal_temperature'=>$this->decimal(8,2)->comment('Внутренняя температура, tj, oC'),
            'heat_loads_known'=>$this->boolean()->comment('Известны проектные тепловые нагрузки'),
            'heating_load'=>$this->decimal(8,3)->comment('Нагрузка отопления q, Гкал/час'),
            'ventilation_load'=>$this->decimal(8,3)->comment('Нагрузка вентиляции q, Гкал/час'),
        ]);
        $this->addForeignKey('fk-objects_ov-objects_id','{{%objects_ov}}','objects_id','{{objects}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-objects_ov-objects_id','{{%objects_ov}}');
        $this->dropTable('{{%objects_ov}}');
    }
}
