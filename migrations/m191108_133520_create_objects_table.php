<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%_objects}}`.
 */
class m191108_133520_create_objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects}}', [
            'id' => $this->primaryKey(),
            'calculation_id' => $this->integer()->comment('Расчет'),
            'name' => $this->string()->comment('Название системы'),
            'count_object' => $this->integer()->comment('Количество'),
            'status' =>$this->boolean()->comment('Статус'),
            'type_object'=>$this->smallInteger()->comment('Тип объекта'),
            'number_group'=>$this->smallInteger()->comment('Группа')

        ]);
        $this->addForeignKey('fk-objects-calculation_id','{{%objects}}','calculation_id','{{calculation}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-objects-calculation_id','{{%objects}}');
        $this->dropTable('{{%objects}}');
    }
}
