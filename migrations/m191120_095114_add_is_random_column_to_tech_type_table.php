<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tech_type}}`.
 */
class m191120_095114_add_is_random_column_to_tech_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects_tech_type', 'is_random', $this->boolean()->defaultValue(false)->comment('Произвольный вариант'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects_tech_type', 'is_random');
    }
}
