<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%calculation}}`.
 */
class m191117_125808_add_created_at_column_to_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('calculation', 'created_at', $this->dateTime()->comment('Дата и время создания'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('calculation', 'created_at');
    }
}
