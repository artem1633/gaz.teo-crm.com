<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_tech_type}}`.
 */
class m191109_012928_create_objects_tech_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_tech_type}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->comment('Наименование'),
            'consumption_thermal_units'=>$this->decimal(8,3)->comment('Расход тепловых единиц'),
            'consumption_equivalent_fuel'=>$this->decimal(8,3)->comment('Расход условного топлива'),
            'day_per_week' =>$this->smallInteger(1)->comment('Расход условного топлива'),
            'norm_document' =>$this->string()->comment('Комментарий, ссылка на норматив'),
            'amount_name' => $this->string()->comment('Количество (название)'),
        ]);
        $this->insert('{{%objects_tech_type}}',['name'=>'Квартира с газовой плитой','consumption_thermal_units'=>970,'consumption_equivalent_fuel'=>138.571,'day_per_week'=>16,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество жильцов']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Квартира с газовой плитой (при отсутствии горячего водоснабжения в квартире)','consumption_thermal_units'=>1430,'consumption_equivalent_fuel'=>204.286,'day_per_week'=>6,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество жильцов']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление завтрака','consumption_thermal_units'=>0.5,'consumption_equivalent_fuel'=>0.071,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество завтраков в год']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление обеда','consumption_thermal_units'=>1,'consumption_equivalent_fuel'=>0.143,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество обедов в год']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление ужина','consumption_thermal_units'=>0.5,'consumption_equivalent_fuel'=>0.071,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество ужинов в год']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление завтрака и обеда (в комплекте)','consumption_thermal_units'=>1.5,'consumption_equivalent_fuel'=>0.214,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество комплектов питания']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление обеда и ужина (в комплекте)','consumption_thermal_units'=>1.5,'consumption_equivalent_fuel'=>0.214,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество комплектов питания']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление завтрака, обеда и ужина (в комплекте)','consumption_thermal_units'=>2,'consumption_equivalent_fuel'=>0.286,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество комплектов питания']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Приготовление пищи в мед.учреждениях','consumption_thermal_units'=>760,'consumption_equivalent_fuel'=>108.571,'day_per_week'=>7,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Среднегодовое кол-во коек']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Выпечка формового хлеба','consumption_thermal_units'=>600,'consumption_equivalent_fuel'=>85.714,'day_per_week'=>6,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Годовое производство хлеба, тонн']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Выпечка хлеба подового, батонов, булок, сдобы','consumption_thermal_units'=>1300,'consumption_equivalent_fuel'=>185.714,'day_per_week'=>6,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Годовое производство хлеба, тонн']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Выпечка кондитерских изделий (тортов, пирожных и т.д.)','consumption_thermal_units'=>1850,'consumption_equivalent_fuel'=>264.286,'day_per_week'=>6,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Годовое производство изделий, тонн']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Применении газа для лабораторных нужд школ, вузов, техникумов','consumption_thermal_units'=>12,'consumption_equivalent_fuel'=>1.714,'day_per_week'=>5,'norm_document'=>'СП 42-101-2003(Приложение А)','amount_name'=>'Количество учащихся']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Обжиг керамического кирпича','consumption_thermal_units'=>980,'consumption_equivalent_fuel'=>140,'day_per_week'=>5,'norm_document'=>'По данным института по проектированию заводов строительных материалов "Росстромпроект" ВСНХ, г. Москваа','amount_name'=>'Производство кирпича тыс.шт/год']);
        $this->insert('{{%objects_tech_type}}',['name'=>'Выработка нормального пара','consumption_thermal_units'=>639.8,'consumption_equivalent_fuel'=>91.4,'day_per_week'=>6,'norm_document'=>'СТО ГАЗПРОМ РД 1.19-126-2004(таб.1.4).','amount_name'=>'Производство пара, тонн/год']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%objects_tech_type}}');
    }
}
