<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects_gvs_type}}`.
 */
class m191109_012936_create_objects_gvs_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objects_gvs_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'water_consumption_rate' => $this->decimal(8,3)->comment('Сут. норма затрат воды (коэф.альфа)'),
            'calculation_regarding_quantity'=>$this->string()->comment('Расчет относительно количества'),
        ]);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Жилые дома (квартирного типа) с душами','water_consumption_rate'=>85,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Жилые дома с сидячими ваннами, оборудованными душами','water_consumption_rate'=>85,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Жилые дома с ваннами длиной от 1500 до 1700 мм, оборудованными душами','water_consumption_rate'=>105,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Жилые дома с повышенными требованиями к их благоустройству','water_consumption_rate'=>115,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Общежития с общими душевыми','water_consumption_rate'=>50,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Общежития с душами при всех жилых комнатах','water_consumption_rate'=>60,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Общежития с общими кухнями и блоками душевых на этажах','water_consumption_rate'=>80,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Гостиницы, пансионаты и мотели с общими ваннами и душами','water_consumption_rate'=>70,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Гостиницы и пансионаты с душами во всех отдельных номерах','water_consumption_rate'=>140,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Гостиницы с ваннами в отдельных номерах','water_consumption_rate'=>100,'calculation_regarding_quantity'=>'жителей (3,5 на кв)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Больницы с общими ваннами и душевыми','water_consumption_rate'=>75,'calculation_regarding_quantity'=>'коек']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Больницы с санитарными узлами, приближенными к палатам','water_consumption_rate'=>90,'calculation_regarding_quantity'=>'коек']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Больницы инфекционные','water_consumption_rate'=>110,'calculation_regarding_quantity'=>'коек']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Санатории и дома отдыха с ваннами при всех жилых комнатах','water_consumption_rate'=>120,'calculation_regarding_quantity'=>'коек']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Санатории и дома отдыха с душами при всех жилых комнатах','water_consumption_rate'=>75,'calculation_regarding_quantity'=>'коек']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Поликлиники и амбулатории','water_consumption_rate'=>5.2,'calculation_regarding_quantity'=>'больных в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Детские ясли-сады (дневные) со столовыми на полуфабрикатах','water_consumption_rate'=>11.5,'calculation_regarding_quantity'=>'детей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Детские ясли-сады (дневные) со столовыми и прачечными','water_consumption_rate'=>25,'calculation_regarding_quantity'=>'детей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Детские ясли-сады (круглосуточные) со столовыми на полуфабрикатах','water_consumption_rate'=>21.4,'calculation_regarding_quantity'=>'детей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Детские ясли-сады (круглосуточные) со столовыми и прачечными','water_consumption_rate'=>28.5,'calculation_regarding_quantity'=>'детей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Пионерские лагеря со столовыми на полуфабрикатах','water_consumption_rate'=>40,'calculation_regarding_quantity'=>'детей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Пионерские лагеря со столовыми и прачечными','water_consumption_rate'=>30,'calculation_regarding_quantity'=>'детей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Прачечные механизированные','water_consumption_rate'=>25,'calculation_regarding_quantity'=>'килограмм сухого белья']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Прачечные немеханизированные','water_consumption_rate'=>15,'calculation_regarding_quantity'=>'килограмм сухого белья']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Административные здания','water_consumption_rate'=>5,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Учебные заведения с душевыми и буфетами','water_consumption_rate'=>6,'calculation_regarding_quantity'=>'учащихся и преподавателей в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Лаборатории высших и средних специальных учебных заведений','water_consumption_rate'=>112,'calculation_regarding_quantity'=>'	приборов в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Общеобразовательные школы с душевыми и столовыми','water_consumption_rate'=>3,'calculation_regarding_quantity'=>'учащихся и преподавателей в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Общеобразовательные школы с продленным днем, душевыми и столовыми','water_consumption_rate'=>3.4,'calculation_regarding_quantity'=>'учащихся и преподавателей в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Профессионально-технические училища с душевыми и столовыми','water_consumption_rate'=>8,'calculation_regarding_quantity'=>'учащихся и преподавателей в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Школы-интернаты с учебными помещениями и душевыми','water_consumption_rate'=>2.7,'calculation_regarding_quantity'=>'мест']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Школы-интернаты со спальными помещениями','water_consumption_rate'=>30,'calculation_regarding_quantity'=>'мест']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'НИИ и лаборатории химического профиля','water_consumption_rate'=>60,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'НИИ и лаборатории биологического профиля','water_consumption_rate'=>55,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'НИИ и лаборатории физического профиля','water_consumption_rate'=>15,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'НИИ и лаборатории естественных наук','water_consumption_rate'=>5,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Аптеки (торговый зал и подсобные помещения)','water_consumption_rate'=>5,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Аптеки (лаборатория приготовления лекарств)','water_consumption_rate'=>55,'calculation_regarding_quantity'=>'работающих']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Предприятия общественного питания с обеденным залом','water_consumption_rate'=>4,'calculation_regarding_quantity'=>'условных блюд']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Предприятия общественного питания продающие на дом','water_consumption_rate'=>5,'calculation_regarding_quantity'=>'условных блюд']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Мясные пищевые производства','water_consumption_rate'=>3100,'calculation_regarding_quantity'=>'тонн продукции']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Рыбные  пищевые производства','water_consumption_rate'=>700,'calculation_regarding_quantity'=>'тонн продукции']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Овощные  пищевые производства','water_consumption_rate'=>800,'calculation_regarding_quantity'=>'тонн продукции']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Кулинарные  пищевые производства','water_consumption_rate'=>1200,'calculation_regarding_quantity'=>'тонн продукции']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Магазины продовольственные','water_consumption_rate'=>65,'calculation_regarding_quantity'=>'работающих в смену (20 м2 торгового зала)']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Магазины промтоварные','water_consumption_rate'=>5,'calculation_regarding_quantity'=>'работающих в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Парикмахерские','water_consumption_rate'=>33,'calculation_regarding_quantity'=>'рабочих мест в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Кинотеатры','water_consumption_rate'=>1.5,'calculation_regarding_quantity'=>'	мест']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Клубы','water_consumption_rate'=>2.6,'calculation_regarding_quantity'=>'	мест']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Театры','water_consumption_rate'=>5,'calculation_regarding_quantity'=>'	мест']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Стадионы,спортзалы, бассейны для зрителей','water_consumption_rate'=>1,'calculation_regarding_quantity'=>'	мест']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Стадионы и спортзалы для физкультурников (с учетом приема душа)','water_consumption_rate'=>30,'calculation_regarding_quantity'=>'физкультурников']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Стадионы,спортзалы, бассейны для спортсменов','water_consumption_rate'=>60,'calculation_regarding_quantity'=>'спортсменов']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Бани для мытья в мыльной с тазами на скамьях и ополаскиванием в душе','water_consumption_rate'=>120,'calculation_regarding_quantity'=>'посетителей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Бани с приемом оздоровительных процедур и ополаскиванием в душе:','water_consumption_rate'=>190,'calculation_regarding_quantity'=>'посетителей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Бани - душевая кабина','water_consumption_rate'=>240,'calculation_regarding_quantity'=>'посетителей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Бани  - ванная кабина','water_consumption_rate'=>360,'calculation_regarding_quantity'=>'посетителей']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Душевые в бытовых помещениях промышленных предприятий','water_consumption_rate'=>230,'calculation_regarding_quantity'=>'душевых сеток в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Цехи с тепло-выделениями св. 84 кДж на 1 м3/ч','water_consumption_rate'=>24,'calculation_regarding_quantity'=>'человек в смену']);
        $this->insert('{{%objects_gvs_type}}',['name'=>'Остальные цехи','water_consumption_rate'=>11,'calculation_regarding_quantity'=>'человек в смену']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%objects_gvs_type}}');
    }
}
