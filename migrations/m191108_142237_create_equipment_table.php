<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%equipment}}`.
 */
class m191108_142237_create_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%equipment}}', [
            'id' => $this->primaryKey(),
            'calculation_id' => $this->integer()->comment('Расчет'),
            'number_group'=>$this->smallInteger()->comment('Группа'),
            'type_object'=>$this->smallInteger()->comment('Тип объекта'),
            'name_equipment'=>$this->string()->comment('Наименование оборудования'),
            'KPD'=>$this->decimal(8,2)->comment('КПД в %'),
            'coefficient_unambiguity'=>$this->decimal(8,2)->comment('Коэффициент однозначности'),
            'count_equipment'=>$this->integer()->comment('Количество, ед'),
            'coefficient_load'=>$this->decimal(8,2)->comment('Коэффициент загрузки'),
            'power_v'=>$this->decimal(8,2)->comment('Мощность, кВт'),
            'pover_g'=>$this->decimal(12,6)->comment('Мощность, Гкал/ч'),
            'specific_consumption'=>$this->decimal(8,2)->comment('Удельный расход, Гкал/ч'),
            'consumption_e'=>$this->decimal(8,2)->comment('Расход ед., м3/ч'),
            'consumption_o'=>$this->decimal(8,2)->comment('Расход об., м3/ч'),
            'status' =>$this->boolean()->comment('Статус'),
        ]);
        $this->addForeignKey('fk-equipment-calculation_id','{{%equipment}}','calculation_id','{{calculation}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-equipment-calculation_id','{{%equipment}}');
        $this->dropTable('{{%equipment}}');
    }
}
