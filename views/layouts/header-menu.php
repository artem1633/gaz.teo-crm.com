<?php

use app\models\User;
use yii\helpers\Url;

$is_admin = Yii::$app->user->getIdentity()->isSuperAdmin() ;
?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],'visible'=>$is_admin],
                    ['label' => 'Расчеты', 'icon' => 'fa  fa-users', 'url' => ['/calculation']],
                    ['label' => 'Регионы', 'icon' => 'fa fa-globe', 'url' => ['/region'],'visible'=>$is_admin],
                    ['label' => 'Города', 'icon' => 'fa fa-trello', 'url' => ['/city'],'visible'=>$is_admin],
                    ['label' => 'Тип здания ОВ', 'icon' => 'fa fa-tasks', 'url' => ['/ov-type'],'visible'=>$is_admin],
                    ['label' => 'Тип системы ГВС', 'icon' => 'fa fa-tasks', 'url' => ['/gvs-type'],'visible'=>$is_admin],
                    ['label' => 'Тип системы Техн', 'icon' => 'fa fa-tasks', 'url' => ['/tech-type'],'visible'=>$is_admin]
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
