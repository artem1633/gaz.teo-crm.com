<?php

use app\models\Objects;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Calculation */

?>
<div class="calculation-create">
    <?php
    if ($type_object == Objects::TYPE_OV) {
        echo $this->render('_form-object-ov', [
            'model' => $model,
            'isNewRecord' => true,
        ]);
    }
    if ($type_object == Objects::TYPE_GVS) {
        echo $this->render('_form-object-gvs', [
            'model' => $model,
            'isNewRecord' => true,
        ]);
    }
    if ($type_object == Objects::TYPE_TECH) {
        echo $this->render('_form-object-tech', [
            'model' => $model,
            'isNewRecord' => true,
        ]);
    }
    ?>
</div>
