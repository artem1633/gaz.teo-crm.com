<?php

use app\models\City;
use app\models\Objects;
use app\models\User;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Calculation */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel app\models\ObjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->registerJsFile('/js/calculation.js');
CrudAsset::register($this);
?>


<div class="calculation-form">
    <?php $form = ActiveForm::begin([
        'fieldConfig' => ['options' => ['class' => 'form-group-sm']],
    ]); ?>



    <div class="panel panel-success ">
        <div class="panel-heading">
            <h4 class="panel-title">Исполнитель</h4>
        </div>
        <div class="panel-body">
            <?= $form->field($model, 'employer_organization', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'employer_position', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'employer_name', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title">Заказчик</h4>
        </div>
        <div class="panel-body">
            <?= $form->field($model, 'object_name', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'object_address', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'customer_name', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'certificate_of_consent', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'calorific_value', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'type_oil', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'year_start_consume', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'object_type', ['template' => "<div class='row'> <div class='col-md-3' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-9'>{input}</div></div>\n{error}",
            ])->dropDownList(\app\models\Calculation::objectTypeLabels()) ?>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">Климат</h4>
                <div class="sm-flex-box">
                    <div class="pull-right-md-only md-margin-top margin-sm-5 col-xs-12 col-sm-1 col-md-1" >
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-xs-sm btn-block-sm btn-primary']) ?>
                    </div>
                        <div class="btn btn-xs-sm md-margin-top btn-default btn-block-sm margin-sm-5 pull-right-md-only"  id="fill-snip"
                         onclick="fillClimat()">
                            Скопировать данные из СП
                            <span class="hidden-sm">131.13330-2018</span>
                        </div>
                    <div class="pull-right-md-only md-margin-top margin-sm-5 col-xs-6 col-sm-3 col-md-3" >
                        <?php

                        if($model->isNewRecord){
                            $cityList = [];
                        } else {
                            $city = City::findOne($model->city_id);
                            $cityList = $city != null ? [$city->id => $city->name] : [];
                        }

                        sort($cityList);

                        ?>
                        <?= $form->field($model, 'city_id')->dropDownList($cityList)->label(false) ?>
                        <?php

    //                    echo $form->field($model, 'city_id')->widget(Select2::class, [
    //                        'data' => City::getCitiesWithRegion(),
    //                        'options' => [
    //                            'placeholder' => 'Город ...',
    //                        ],
    //                    ])->label(false)

                        ?>
                        <?php
    //                    echo Select2::widget([
    //                        'name' => 'fill-city',
    //                        'data' => City::getCitiesWithRegion(),
    //                        'options' => [
    //                            'placeholder' => 'Город ...',
    //                        ],
    //                    ]);
                        ?>
                    </div>
                    <div class="pull-right-md-only md-margin-top margin-sm-5 col-xs-6 col-sm-3 col-md-3" >
                        <?= $form->field($model, 'region_id')->widget(Select2::class, [
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Region::find()->orderBy('name asc')->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => 'Регион ...',
                            ],
                            'pluginEvents' => [
                                "change" => "function() { $.get('/city/get-city-by-region?regionId='+$(this).val(), function(response){
                                    html = '';
                                   for(var i = 0; i < response.items.length; i++){
                                        // var option = new Option(response.items[i].name, response.items[i].id);
                                        var option = '<option value=\"'+response.items[i].id+'\">'+response.items[i].name+'</option>';
                                        
                                        
                                        html = html + option;
                                        
                                   }
                                   
                                   $('#calculation-city_id').html(html);
                                }); }",
                            ],
                        ])->label(false) ?>
                        <?php
                        //                    echo Select2::widget([
                        //                        'name' => 'fill-city',
                        //                        'data' => City::getCitiesWithRegion(),
                        //                        'options' => [
                        //                            'placeholder' => 'Город ...',
                        //                        ],
                        //                    ]);
                        ?>
                    </div>
                    <div class="visible-sm-xs" style="height: 37px;">
                    </div>
                </div>

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'temperature_average',
                            ['template' => "<div class='row'> <div class='col-md-8' style='margin-top:-6px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'temperature_calculated',
                            ['template' => "<div class='row'> <div class='col-md-8' style='margin-top:-6px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'winder_speed',
                            ['template' => "<div class='row'> <div class='col-md-8' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'c_alfa',
                            ['template' => "<div class='row'> <div class='col-md-8' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'duration_heating',
                            ['template' => "<div class='row'> <div class='col-md-8' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <h5><strong>Средняя температура</strong></h5>
                <hr style="margin: 0px 0px 5px 0px">
                <div class="row">
                    <div class="col-md-2"><?= $form->field($model, 'temp_average1',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average2',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average3',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average4',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average5',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average6',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">

                    <div class="col-md-2"><?= $form->field($model, 'temp_average7',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average8',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average9',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average10',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average11',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'temp_average12',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                </div>
                <h5><strong>Продолжительность отопительного периода</strong></h5>
                <hr style="margin: 0px 0px 5px 0px">
                <div class="row">
                    <div class="col-md-2"><?= $form->field($model, 'duration_op1',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op2',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op3',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op4',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op5',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op6',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><?= $form->field($model, 'duration_op7',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op8',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op9',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op10',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op11',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                    <div class="col-md-2"><?= $form->field($model, 'duration_op12',
                            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                            ])->textInput(['maxlength' => true]) ?></div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <?php Pjax::begin(['id' => 'global-pjax-container']) ?>
        <?php if (!$model->isNewRecord) : ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4 class="panel-title">Объекты (потребители тепловой энергии)</h4>
                </div>
                <div class="panel-body">
                    <div id="ajaxCrudDatatableObjects">
                        <?= GridView::widget([
                            'id' => 'crud-datatable-object',
                            'dataProvider' => $dataProviderObjects,
//                            'filterModel' => $searchModelObjects,
                            'pjax' => true,
                            'columns' => require(__DIR__ . '/_columns-objects.php'),
                            'panelBeforeTemplate' => Html::a('Добавить ОВ <i class="fa fa-plus"></i>', ['create-object', 'calculation_id' => $model->id, 'type_object' => Objects::TYPE_OV],
                                    ['role' => 'modal-remote', 'title' => 'Добавить объект', 'class' => 'btn btn-block-sm btn-success']) . '&nbsp;' .
                                Html::a('Добавить ГВС <i class="fa fa-plus"></i>', ['create-object', 'calculation_id' => $model->id, 'type_object' => Objects::TYPE_GVS],
                                    ['role' => 'modal-remote', 'title' => 'Добавить объект', 'class' => 'btn btn-block-sm btn-success']) . '&nbsp;' .
                                Html::a('Добавить Техн <i class="fa fa-plus"></i>', ['create-object', 'calculation_id' => $model->id, 'type_object' => Objects::TYPE_TECH],
                                    ['role' => 'modal-remote', 'title' => 'Добавить объект', 'class' => 'btn btn-block-sm btn-success']) . '&nbsp;',
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'panel' => [
                                'headingOptions' => ['style' => 'display: none;'],
                                'after' => BulkButtonWidget::widget([
                                        'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить выбранные',
                                            ["bulk-delete-object"],
                                            [
                                                "class" => "btn btn-danger btn-xs",
                                                'role' => 'modal-remote-bulk',
                                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                                'data-request-method' => 'post',
                                                'data-confirm-title' => 'Вы уверены?',
                                                'data-confirm-message' => 'Вы действительно хотите удалить данный элемент?'
                                            ]),
                                    ]) .
                                    '<div class="clearfix"></div>',
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!$model->isNewRecord) : ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4 class="panel-title">Подбор оборудования</h4>
                </div>
                <div class="panel-body">
                    <div id="ajaxCrudDatatableObjects">
                        <?= GridView::widget([
                            'id' => 'crud-datatable-equipment',
                            'dataProvider' => $dataProviderEquipments,
//                            'filterModel' => $searchModelEquipments,
                            'pjax' => true,
                            'columns' => require(__DIR__ . '/_columns-equipments.php'),
                            'panelBeforeTemplate' => false,
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'panel' => [
                                'headingOptions' => ['style' => 'display: none;'],
                                'after' => '',
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if($model->isNewRecord == false): ?>
            <?php if($model->isValidEquipments() && count($dataProviderEquipments->models) > 0): ?>
                <?=  Html::a('Скачать расчет', ['generate', 'id' => $model->id], [
                                'class' => 'btn btn-info btn-block', 'title'=>'Скачать отчет', 'data-pjax' => '0'
                            ])."&nbsp;" ?>
            <?php else: ?>
                <?=  Html::a('Скачать расчет', '#', [
                    'class' => 'btn btn-default btn-block', 'title'=>'Скачать отчет', 'data-pjax' => '0'
                ])."&nbsp;" ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php Pjax::end() ?>

    </div>

    <?php Modal::begin([
        "id" => "ajaxCrudModal",
        'options' => ['style' => 'overflow-x: auto;'],
        "footer" => "",// always need it for jquery plugin
    ]) ?>
    <?php Modal::end(); ?>
    <style>
        .modal-content {
            width: 800px;
        }
    </style>

    <?php

$script = <<< JS

$('#calculation-temperature_calculated').change(function(){
     var temperatureTable = {
        '0': '2.05',
        '-5': '1.67',
        '-10': '1.45',
        '-15': '1.29',
        '-20': '1.17',
        '-25': '1.08',
        '-30': '1',
        '-35': '0.95',
        '-40': '0.9',
        '-45': '0.85',
        '-50': '0.82',
        '-56': '0.8'
    };

    var keys = Object.keys(temperatureTable);
    
    var value = $(this).val();
    
    console.log(keys);
    
    
    
    var i = 0;
    
    if(keys.includes(String(value))){
        var result = temperatureTable[value];   
    } else {
        for(var key in temperatureTable){
            var temperature = parseInt(key);
            var a = parseFloat(temperatureTable[key]);
            
            if(value > temperature){
                i++;
                continue;
            }
            
            var nextI = i + 1;
            var nextTemperature = parseInt(keys[nextI]);
            
            // console.log(temperature+' '+nextTemperature);
            
            if(value <= nextTemperature){
                i++;
                continue;
            }
            
            console.log('Your value is between '+temperature+' and '+nextTemperature);
            
            var nextA = parseFloat(temperatureTable[nextTemperature]);
            
            var step = (a - nextA) / 5;
            
            var diff = Math.abs(nextTemperature - value);
            
            // console.log(diff);
            
            // console.log(step);
            
            var result = nextA + (step * diff);
            
            i++;
        }        
    }
    

    
    console.log('result is: '+result);
    
    $('#calculation-c_alfa').val(result);
});

   


JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>