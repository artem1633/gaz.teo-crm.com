<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Calculation */
?>
<div class="calculation-update">

    <?= $this->render('_form', [
        'model' => $model,
        'searchModelObjects' => $searchModelObjects,
        'dataProviderObjects' => $dataProviderObjects,
        'searchModelEquipments' => $searchModelEquipments,
        'dataProviderEquipments' => $dataProviderEquipments,
    ]) ?>

</div>
