<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Calculation */
?>
<div class="calculation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'object_name',
            'object_address',
            'customer_name',
            'certificate_of_consent',
            'calorific_value',
            'temperature_average',
            'temperature_calculated',
            'winder_speed',
            'heating_period',
            'c_alfa',
            'temp_average1',
            'temp_average2',
            'temp_average3',
            'temp_average4',
            'temp_average5',
            'temp_average6',
            'temp_average7',
            'temp_average8',
            'temp_average9',
            'temp_average10',
            'temp_average11',
            'temp_average12',
            'duration_op1',
            'duration_op2',
            'duration_op3',
            'duration_op4',
            'duration_op5',
            'duration_op6',
            'duration_op7',
            'duration_op8',
            'duration_op9',
            'duration_op10',
            'duration_op11',
            'duration_op12',
        ],
    ]) ?>

</div>
