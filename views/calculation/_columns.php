<?php

use app\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Equipment;
use app\models\Objects;
use app\models\ObjectsTech;
use app\helpers\MathNumber;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
         [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'id',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format' => ['date', 'php:d M Y H:i'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'employer_name',
        'label' => 'Исполнитель',
        'value' => function($model){
            $arr = [$model->employer_organization, $model->employer_name, $model->employer_position];

            return implode(',', $arr);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'object_name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Мощность',
        'width' => '8%',
        'format' => 'html',
        'value' => function($data){
            // $objects = \app\models\Objects::find()->where(['calculation_id' => $model->id])->sum('power_v');

            // return $objects;

            $equipments = Equipment::find()->where(['calculation_id' => $data->id])->all();

            $output = [];

            foreach ($equipments as $model) {
                $object = Objects::find()->where(['calculation_id' => $model->calculation_id, 'number_group' => $model->number_group])->one();
                $objectTech = ObjectsTech::find()->where(['objects_id' => $object->id])->one();
                $model->power_v = $model->formula->getO6();
                $model->pover_g = $model->formula->getO6() / 1163;
                $model->pover_common = $model->power_v * $model->count_equipment;

                $powerV = MathNumber::round($model->power_v);

                $output[] = "{$object->count_object} x {$powerV} кВт";
            }

            return implode(',<br>', $output);
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Оборудование',
        'value' => function($model){
            $equipment = \yii\helpers\ArrayHelper::getColumn(\app\models\Equipment::find()->where(['calculation_id' => $model->id])->all(), 'name_equipment');

            return implode(', ', $equipment);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => User::getUsersName(),
        'visible' => Yii::$app->user->getIdentity()->isSuperAdmin(),
        'value' => function($model) {
            return isset(User::getUsersName()[$model->user_id]) ? User::getUsersName()[$model->user_id] : 'Не определен';
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'object_address',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'certificate_of_consent',
//    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'calorific_value',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temperature_average',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temperature_calculated',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'winder_speed',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'heating_period',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'c_alfa',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average1',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average2',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average3',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average4',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average5',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average6',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average7',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average8',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average9',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average10',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average11',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'temp_average12',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op1',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op2',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op3',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op4',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op5',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op6',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op7',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op8',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op9',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op10',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op11',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'duration_op12',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{generate} {update}{delete}',
        'buttons' => [
            'generate' => function ($url, $model) {
                return Html::a('<i class="fa fa-file-excel-o text-success" style="font-size: 16px;"></i>', $url, [
                        'title'=>'Скачать отчет', 'data-pjax' => '0'
                    ])."&nbsp;";
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'title'=>'Изменить',
                ])."&nbsp;";
            }
        ],
    ],

];   