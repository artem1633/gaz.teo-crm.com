<?php

use app\models\ObjectsOvType;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Objects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objects-form">

    <?php $form = ActiveForm::begin([
        'fieldConfig' => ['options' => ['class' => 'form-group-sm']],
     ]); ?>

    <?= $form->field($model, 'name', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count_object', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'building_type', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->dropDownList(ObjectsOvType::getTypes(),['onChange'=>'buildingTypeChange()', 'prompt' => 'Выберите тип']) ?>

    <?= $form->field($model, 'building_volume', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'specific_thermal_characteristic_heating', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'specific_thermal_characteristic_ventilation', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'building_height', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'internal_temperature', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'heat_loads_known')->checkbox(['onChange'=>'ovLoadsKnowChange()']) ?>

    <?= $form->field($model, 'heating_load', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true,'disabled'=> $model->heat_loads_known ? false : true,'onChange'=>'ovPowerChange()']) ?>

    <?= $form->field($model, 'ventilation_load', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true,'disabled'=> $model->heat_loads_known ? false : true,'onChange'=>'ovPowerChange()']) ?>

    <div class="row">
        <div class="col-md-7">
            <label>
                Общая нагрузка ОВ q, Гкал/час
            </label>
        </div>
        <div class="col-md-5" id="ov_all">

        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS

$('#objectsovform-building_volume').change(function(){

    var buildingVolume = $(this).val();

    if(window.OvObjectCalculation != null){

        var object = null;

        var objectHeating = 0;
        var objectVentilation = 0;

        if(window.OvObjectType.type === 0){
                        
                        if(window.OvObjectType.external_temperature <= -30){
                            if(buildingVolume > 500 && buildingVolume <= 2000){
                                objectHeating = 0.37;
                            } else if(buildingVolume > 2000 && buildingVolume <= 5000){
                                objectHeating = 0.28;
                            } else if(buildingVolume > 5000 && buildingVolume <= 10000){
                                objectHeating = 0.24;
                            } else if(buildingVolume > 10000 && buildingVolume <= 15000){
                                objectHeating = 0.21;
                            } else if(buildingVolume > 15000 && buildingVolume <= 25000){
                                objectHeating = 0.20;
                            } else if(buildingVolume > 25000){
                                objectHeating = 0.19;
                            }
                        }

                        if(window.OvObjectType.external_temperature >= -20){
                            if(buildingVolume > 500 && buildingVolume <= 2000){
                                objectHeating = 0.45;
                            } else if(buildingVolume > 2000 && buildingVolume <= 5000){
                                objectHeating = 0.38;
                            } else if(buildingVolume > 5000 && buildingVolume <= 10000){
                                objectHeating = 0.29;
                            } else if(buildingVolume > 10000 && buildingVolume <= 15000){
                                objectHeating = 0.25;
                            } else if(buildingVolume > 15000 && buildingVolume <= 25000){
                                objectHeating = 0.23;
                            } else if(buildingVolume > 25000){
                                objectHeating = 0.22;
                            }
                        }

                        if(window.OvObjectType.external_temperature > -30 && window.OvObjectType.external_temperature < -20){
                            if(buildingVolume > 500 && buildingVolume <= 2000){
                                objectHeating = 0.41;
                            } else if(buildingVolume > 2000 && buildingVolume <= 5000){
                                objectHeating = 0.3;
                            } else if(buildingVolume > 5000 && buildingVolume <= 10000){
                                objectHeating = 0.27;
                            } else if(buildingVolume > 10000 && buildingVolume <= 15000){
                                objectHeating = 0.23;
                            } else if(buildingVolume > 15000 && buildingVolume <= 25000){
                                objectHeating = 0.21;
                            } else if(buildingVolume > 25000){
                                objectHeating = 0.2;
                            }
                        }

                    } else if(window.OvObjectType.type === 1){

                         if(buildingVolume > 1 && buildingVolume <= 100){
                             objectHeating = 0.74;
                         } else if(buildingVolume > 100 && buildingVolume <= 200){
                             objectHeating = 0.66;
                         } else if(buildingVolume > 200 && buildingVolume <= 300){
                             objectHeating = 0.62;
                         } else if(buildingVolume > 300 && buildingVolume <= 400){
                             objectHeating = 0.6;
                         } else if(buildingVolume > 400 && buildingVolume <= 500){
                             objectHeating = 0.58;
                         } else if(buildingVolume > 500 && buildingVolume <= 600){
                             objectHeating = 0.56;
                         } else if(buildingVolume > 600 && buildingVolume <= 700){
                             objectHeating = 0.54;
                         } else if(buildingVolume > 700 && buildingVolume <= 800){
                             objectHeating = 0.53;
                         } else if(buildingVolume > 800 && buildingVolume <= 900){
                             objectHeating = 0.52;
                         } else if(buildingVolume > 900 && buildingVolume <= 1000){
                             objectHeating = 0.51;
                         } else if(buildingVolume > 1000 && buildingVolume <= 1100 ){
                             objectHeating = 0.5;
                         } else if(buildingVolume > 1100 && buildingVolume <= 1200 ){
                             objectHeating = 0.49;
                         } else if(buildingVolume > 1200 && buildingVolume <= 1300 ){
                             objectHeating = 0.48;
                         } else if(buildingVolume > 1300 && buildingVolume <= 1400 ){
                             objectHeating = 0.47;
                         } else if(buildingVolume > 1400 && buildingVolume <= 1500 ){
                             objectHeating = 0.47;
                         } else if(buildingVolume > 1500 && buildingVolume <= 1700 ){
                             objectHeating = 0.46;
                         } else if(buildingVolume > 1700 && buildingVolume <= 2000 ){
                             objectHeating = 0.45;
                         } else if(buildingVolume > 2000 && buildingVolume <= 2500 ){
                             objectHeating = 0.44;
                         } else if(buildingVolume > 2500 && buildingVolume <= 3000 ){
                             objectHeating = 0.43;
                         } else if(buildingVolume > 3000 && buildingVolume <= 3500 ){
                             objectHeating = 0.42;
                         } else if(buildingVolume > 3500 && buildingVolume <= 4000 ){
                             objectHeating = 0.40;
                         } else if(buildingVolume > 4000 && buildingVolume <= 4500 ){
                             objectHeating = 0.39;
                         } else if(buildingVolume > 4500 && buildingVolume <= 5000 ){
                             objectHeating = 0.38;
                         } else if(buildingVolume > 5000 && buildingVolume <= 6000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 6000 && buildingVolume <= 7000 ){
                             objectHeating = 0.36;
                         } else if(buildingVolume > 7000 && buildingVolume <= 8000 ){
                             objectHeating = 0.35;
                         } else if(buildingVolume > 8000 && buildingVolume <= 9000 ){
                             objectHeating = 0.34;
                         } else if(buildingVolume > 9000 && buildingVolume <= 10000 ){
                             objectHeating = 0.33;
                         } else if(buildingVolume > 10000 && buildingVolume <= 11000 ){
                             objectHeating = 0.32;
                         } else if(buildingVolume > 11000 && buildingVolume <= 12000 ){
                             objectHeating = 0.31;
                         } else if(buildingVolume > 12000 && buildingVolume <= 13000 ){
                             objectHeating = 0.3;
                         } else if(buildingVolume > 13000 && buildingVolume <= 14000 ){
                             objectHeating = 0.3;
                         } else if(buildingVolume > 14000 && buildingVolume <= 15000 ){
                             objectHeating = 0.29;
                         } else if(buildingVolume > 15000 && buildingVolume <= 20000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 20000 && buildingVolume <= 25000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 25000 && buildingVolume <= 30000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 30000 && buildingVolume <= 35000 ){
                             objectHeating = 0.28;
                         } else if(buildingVolume > 35000 && buildingVolume <= 40000 ){
                             objectHeating = 0.27;
                         } else if(buildingVolume > 40000 && buildingVolume <= 45000 ){
                             objectHeating = 0.27;
                         } else if(buildingVolume > 45000 && buildingVolume <= 50000 ){
                             objectHeating = 0.26;
                         } else if(buildingVolume > 50000){ 
                             objectHeating = 1.6 / Math.pow(buildingVolume, 1/6);
                             objectHeating = parseFloat(objectHeating.toFixed(2));
                         }

                    } else if(window.OvObjectType.type === 2){

                         if(buildingVolume > 1 && buildingVolume <= 100){
                             objectHeating = 0.92;
                         } else if(buildingVolume > 100 && buildingVolume <= 200){
                             objectHeating = 0.82;
                         } else if(buildingVolume > 200 && buildingVolume <= 300){
                             objectHeating = 0.78;
                         } else if(buildingVolume > 300 && buildingVolume <= 400){
                             objectHeating = 0.74;
                         } else if(buildingVolume > 400 && buildingVolume <= 500){
                             objectHeating = 0.71;
                         } else if(buildingVolume > 500 && buildingVolume <= 600){
                             objectHeating = 0.69;
                         } else if(buildingVolume > 600 && buildingVolume <= 700){
                             objectHeating = 0.68;
                         } else if(buildingVolume > 700 && buildingVolume <= 800){
                             objectHeating = 0.67;
                         } else if(buildingVolume > 800 && buildingVolume <= 900){
                             objectHeating = 0.66;
                         } else if(buildingVolume > 900 && buildingVolume <= 1000){
                             objectHeating = 0.65;
                         } else if(buildingVolume > 1000 && buildingVolume <= 1100 ){
                             objectHeating = 0.62;
                         } else if(buildingVolume > 1100 && buildingVolume <= 1200 ){
                             objectHeating = 0.6;
                         } else if(buildingVolume > 1200 && buildingVolume <= 1300 ){
                             objectHeating = 0.59;
                         } else if(buildingVolume > 1300 && buildingVolume <= 1400 ){
                             objectHeating = 0.58;
                         } else if(buildingVolume > 1400 && buildingVolume <= 1500 ){
                             objectHeating = 0.57;
                         } else if(buildingVolume > 1500 && buildingVolume <= 1700 ){
                             objectHeating = 0.55;
                         } else if(buildingVolume > 1700 && buildingVolume <= 2000 ){
                             objectHeating = 0.53;
                         } else if(buildingVolume > 2000 && buildingVolume <= 2500 ){
                             objectHeating = 0.52;
                         } else if(buildingVolume > 2500 && buildingVolume <= 3000 ){
                             objectHeating = 0.5;
                         } else if(buildingVolume > 3000 && buildingVolume <= 3500 ){
                             objectHeating = 0.48;
                         } else if(buildingVolume > 3500 && buildingVolume <= 4000 ){
                             objectHeating = 0.47;
                         } else if(buildingVolume > 4000 && buildingVolume <= 4500 ){
                             objectHeating = 0.46;
                         } else if(buildingVolume > 4500 && buildingVolume <= 5000 ){
                             objectHeating = 0.45;
                         } else if(buildingVolume > 5000 && buildingVolume <= 6000 ){
                             objectHeating = 0.43;
                         } else if(buildingVolume > 6000 && buildingVolume <= 7000 ){
                             objectHeating = 0.42;
                         } else if(buildingVolume > 7000 && buildingVolume <= 8000 ){
                             objectHeating = 0.41;
                         } else if(buildingVolume > 8000 && buildingVolume <= 9000 ){
                             objectHeating = 0.4;
                         } else if(buildingVolume > 9000 && buildingVolume <= 10000 ){
                             objectHeating = 0.39;
                         } else if(buildingVolume > 10000 && buildingVolume <= 11000 ){
                             objectHeating = 0.38;
                         } else if(buildingVolume > 11000 && buildingVolume <= 12000 ){
                             objectHeating = 0.38;
                         } else if(buildingVolume > 12000 && buildingVolume <= 13000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 13000 && buildingVolume <= 14000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 14000 && buildingVolume <= 15000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 15000 && buildingVolume <= 20000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 20000 && buildingVolume <= 25000 ){
                             objectHeating = 0.37;
                         } else if(buildingVolume > 25000 && buildingVolume <= 30000 ){
                             objectHeating = 0.36;
                         } else if(buildingVolume > 30000 && buildingVolume <= 35000 ){
                             objectHeating = 0.35;
                         } else if(buildingVolume > 35000 && buildingVolume <= 40000 ){
                             objectHeating = 0.35;
                         } else if(buildingVolume > 40000 && buildingVolume <= 45000 ){
                             objectHeating = 0.34;
                         } else if(buildingVolume > 45000 && buildingVolume <= 50000 ){
                             objectHeating = 0.34;
                         } else if(buildingVolume > 50000){ 
                             objectHeating = 1.3 / Math.pow(buildingVolume, 1/8);
                             objectHeating = parseFloat(objectHeating.toFixed(2));
                         }

                    } else if(window.OvObjectType.type === 3) {

                        // objectHeating = 1.6 / Math.sqrt(buildingVolume);
                        // alert(123);
                        objectHeating = 1.6 / Math.pow(buildingVolume, 1/6);
                        objectHeating = objectHeating.toFixed(2);

                    } else if(window.OvObjectType.type === 4) {

                        // objectHeating = 1.3 / Math.sqrt(buildingVolume);
                        objectHeating = 1.3 / Math.pow(buildingVolume, 1/8);
                        objectHeating = objectHeating.toFixed(2);

                    } else {
                        $.each(window.OvObjectCalculation, function(index, obj){
                            if(object == null){
                                if((window.OvObjectCalculation.length - 1) <= index){ // Последний
                                    if(buildingVolume >= obj.building_volume){
                                        object = window.OvObjectCalculation[index++];
                                        objectHeating = object.heating;
                                        objectVentilation = object.ventilation;
                                    }
                                } else {
                                    if(buildingVolume <= obj.building_volume){
                                        object = window.OvObjectCalculation[index];
                                        objectHeating = object.heating;
                                        objectVentilation = object.ventilation;
                                    }
                                }
                            }
                        });
                    }

                    $('#objectsovform-specific_thermal_characteristic_heating').val(objectHeating);
                    $('#objectsovform-specific_thermal_characteristic_ventilation').val(objectVentilation);
    }
});

function getAllOVValue(){
    var heatingValue = $('#objectsovform-heating_load').val();
    var ventilationValue = $('#objectsovform-ventilation_load').val();

    var countAfterPoint = 0;

    var heatingValueCount = heatingValue.split('.');
    var ventilationValueCount = ventilationValue.split('.');

    if(heatingValueCount.length > 1){
        heatingValueCount = heatingValueCount[1].length;
    } else {
        heatingValueCount = 0;
    }

    if(ventilationValueCount.length > 1){
        ventilationValueCount = ventilationValueCount[1].length;
    } else {
        ventilationValueCount = 0;
    }

    if(heatingValueCount > ventilationValueCount){
        countAfterPoint = heatingValueCount;
    } else {
        countAfterPoint = ventilationValueCount;
    }
 
    var allValue = (parseFloat(heatingValue) + parseFloat(ventilationValue)).toFixed(countAfterPoint);

    if(allValue != null && isNaN(allValue) == false){
        $('#ov_all').text(allValue);
    }
}

getAllOVValue();


$('#objectsovform-heating_load, #objectsovform-ventilation_load').change(getAllOVValue);

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>