<?php

use app\models\Objects;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Calculation */
?>
<div class="calculation-update">
        <?php
        if ($type_object == Objects::TYPE_OV) {
            echo $this->render('_form-object-ov', [
                'model' => $model,
                'isNewRecord' => false,
            ]);
        }
        if ($type_object == Objects::TYPE_GVS) {
            echo $this->render('_form-object-gvs', [
                'model' => $model,
                'isNewRecord' => false,
            ]);
        }
        if ($type_object == Objects::TYPE_TECH) {
            echo $this->render('_form-object-tech', [
                'model' => $model,
                'isNewRecord' => false,
            ]);
        }
        ?>
</div>
