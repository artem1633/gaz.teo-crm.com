<?php

use app\models\City;
use app\models\Objects;
use app\models\User;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use app\models\ObjectsOv;
use app\models\ObjectsGvs;
use app\models\ObjectsTech;
use yii\widgets\Pjax;
use app\helpers\MathNumber;

/* @var $this yii\web\View */
/* @var $model app\models\Equipment */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel app\models\ObjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile('/js/calculation.js');
CrudAsset::register($this);


$object = Objects::find()->where(['calculation_id' => $model->calculation_id, 'number_group' => $model->number_group])->one();

if($object != null){

if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
    $model->type_object = 'Котельное';
} else {
    $model->type_object = 'Технологическое';
}

}


$model->status = 'Корректно';


// if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
    $model->specific_consumption = $model->formula->getO9();
// }

if($model->power_v > 0){
    $model->coefficient_load = $model->formula->getO5();

	if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
	    $model->consumption_e = round($model->formula->getO11(), 2);
	    $model->consumption_o = round($model->formula->getO12(), 3);
	} else {
	    $model->consumption_e = round($model->formula->getO11(), 1);
	    $model->consumption_o = round($model->formula->getO12(), 3);
	}

	if($model->pover_common == null){
		$model->pover_common = $model->formula->getO8();
	}

} else {
    if($object->type_object == Objects::TYPE_TECH){
        $objectTech = ObjectsTech::find()->where(['objects_id' => $object->id])->one();
        // $model->power_v = $objectTech->formula->getTehQh1();
        $model->power_v = $model->formula->getO6();

        $model->coefficient_load = $model->formula->getO5();

        // $model->pover_g = $objectTech->formula->getTehQh();
        $model->pover_g = $model->formula->getO6() / 1163;
        $model->pover_common = $model->formula->getO8();
        $model->consumption_o = round($model->formula->getO12(), 3);
    }
}

    if($object->type_object == Objects::TYPE_OV){
        $objectOv = ObjectsOv::find()->where(['objects_id' => $object->id])->one();
        // $model->power_v = $objectOv->formula->getOvQomax1() + $objectOv->formula->getOvQvmax1();
        // $model->pover_g = $model->power_v / 1164;
        $model->pover_g = round($model->power_v / 1163, 5);
        // if($model->pover_g > 0){
            $model->pover_common = $model->formula->getO8();
        // }
    }
    if($object->type_object == Objects::TYPE_GVS){
        $objectGvs = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();
        // $model->power_v = $objectGvs->formula->getGvsQhm1();
        // $model->pover_g = $model->power_v / 1164;
        $model->pover_g = round($model->power_v / 1163, 5);
        // if($model->pover_g > 0){
            $model->pover_common = $model->formula->getO8();
        // }
    }

$sumOvQomax = 0;
$sumOvQomax1 = 0;
$sumOvQvmax = 0;
$sumOvQvmax1 = 0;
$sumOvQo = 0;
$sumOvQv = 0;
$sumOvGncho = 0;
$sumOvGnchv = 0;
$sumOvGno = 0;
$sumOvGnv = 0;
$sumOvGuto = 0;
$sumOvGutv = 0;

$sumGvsQhm = 0;
$sumGvsQhm1 = 0;
$sumGvsQh = 0;
$sumGvsGnch = 0;
$sumGvsGn = 0;
$sumGvsGut = 0;

$sumTehQh = 0;
$sumTehQh1 = 0;
$sumTehQn = 0;
$sumTehQhd = 0;
$sumTehGn = 0;
$sumTehGut = 0;


$objects = Objects::find()->where(['number_group' => $model->number_group, 'calculation_id' => $model->calculation_id])->all();

$obj = null;

foreach ($objects as $object)
{
    $obj = $object;
    if($object->type_object == Objects::TYPE_OV){
        $object = ObjectsOv::findOne(['objects_id' => $object->id]);

        if($object){
            $sumOvQomax += $object->formula->getOvQomax();
            $sumOvQomax1 += $object->formula->getOvQomax1();
            $sumOvQvmax += $object->formula->getOvQvmax();
            $sumOvQvmax1 += $object->formula->getOvQvmax1();
            $sumOvQo += $object->formula->getQo();
            $sumOvQv += $object->formula->getQv();
            $sumOvGncho += $object->formula->getOvGncho();
            $sumOvGnchv += $object->formula->getOvGnchv();
            $sumOvGno += $object->formula->getOvGno();
            $sumOvGnv += $object->formula->getOvGnv();
            $sumOvGuto += $object->formula->getOvGuto();
            $sumOvGutv += $object->formula->getOvGutv();
        }

    } else if($object->type_object == Objects::TYPE_GVS){
        $object = ObjectsGvs::findOne(['objects_id' => $object->id]);

        if($object){
            $sumGvsQhm += $object->formula->getGvsQhm();
            $sumGvsQhm1 += $object->formula->getGvsQhm1();
            $sumGvsQh += $object->formula->getGvsQh();
            $sumGvsGnch += $object->formula->getGvsGnch();
            $sumGvsGn += $object->formula->getGvsGn();
            $sumGvsGut += $object->formula->getGvsGut();
        }

    } else if($object->type_object == Objects::TYPE_TECH){
        $object = ObjectsTech::findOne(['objects_id' => $object->id]);

        if($object){
            $sumTehQh += $object->formula->getTehQh();
            $sumTehQh1 += $object->formula->getTehQh1();
            $sumTehQn += $object->formula->getTehQn();
            $sumTehQhd += $object->formula->getTehQhd();
            $sumTehGn += $object->formula->getTehGn();
            $sumTehGut += $object->formula->getTehGut();
        }
    }
}


$allQhmax = $sumOvQomax + $sumOvQvmax + $sumGvsQhm + $sumTehQh;
$allQhmax1 = $sumOvQomax1 + $sumOvQvmax1 + $sumGvsQhm1 + $sumTehQh1;
$allQh = $sumOvQo + $sumOvQv + $sumGvsQh + $sumTehQn;
$allQh1 = ($sumOvQo * 4.1868) + ($sumOvQv * 4.1868) + ($sumGvsQh * 4.1868) + ($sumTehQn * 4.1868);
$allGnch = $sumOvGncho + $sumOvGnchv + $sumGvsGnch + $sumTehQhd;
$allGn = $sumOvGno + $sumOvGnv + $sumGvsGn + $sumTehGn;
$allGut = $sumOvGuto + $sumOvGutv + $sumGvsGut + $sumTehGut;



// $sumOvQomax = round($sumOvQomax, 4);
// $sumOvQomax1 = round($sumOvQomax1, 4);
// $sumGvsQhm = round($sumGvsQhm, 5);
// $sumTehQh = round($sumTehQh, 4);
// $allQhmax = round($allQhmax, 4);

// $sumOvQomax1 = round($sumOvQomax1, 4);
// $sumOvQvmax1 = round($sumOvQvmax1, 4);
// $sumGvsQhm1 = round($sumGvsQhm1);
// $sumTehQh1 = round($sumTehQh1, 2);
// $allQhmax1 = round($allQhmax1, 1);


// $sumOvQo = round($sumOvQo, 3);
// $sumOvQv = round($sumOvQv, 3);
// $sumGvsQh = round($sumGvsQh, 2);
// // $sumTehQh1 = round($sumTehQh1, 1);


// $sumOvQoM = round($sumOvQo * 4.1868, 2);
// $sumOvQvM = round($sumOvQv * 4.1868, 2);
// $sumGvsQhM = round($sumGvsQh * 4.1868, 2);
// $sumTehQnM = round($sumTehQn * 4.1868, 2);


// $sumOvGncho = round($sumOvGncho, 2);
// $sumOvGnchv = round($sumOvGnchv, 2);
// $sumGvsGnch = round($sumGvsGnch, 2);
// $sumTehQhd = round($sumTehQhd, 3);


// $sumOvGno = round($sumOvGno, 2);
// $sumOvGnv = round($sumOvGnv, 2);
// $sumGvsGn = round($sumGvsGn, 2);
// $sumTehGn = round($sumTehGn, 2);


// $sumOvGuto = round($sumOvGuto, 2);
// $sumOvGutv = round($sumOvGutv, 2);
// $sumGvsGut = round($sumGvsGut, 2);
// $sumTehGut = round($sumTehGut, 2);


// $allQhmax = round($allQhmax, 4);
// $allQhmax1 = round($allQhmax1, 1);
// $allQh = round($allQh, 3);
// $allQh1 = round($allQh1, 2);
// $allGnch = round($allGnch, 2);
// $allGn = round($allGn, 2);
// $allGut = round($allGut, 1);


$sumOvQomax = MathNumber::round($sumOvQomax);
$sumOvQomax1 = MathNumber::round($sumOvQomax1);
$sumGvsQhm = MathNumber::round($sumGvsQhm);
$sumTehQh = MathNumber::round($sumTehQh);
$allQhmax = MathNumber::round($allQhmax);

$sumOvQomax1 = MathNumber::round($sumOvQomax1);
$sumOvQvmax1 = MathNumber::round($sumOvQvmax1);
$sumGvsQhm1 = MathNumber::round($sumGvsQhm1);
$sumTehQh1 = MathNumber::round($sumTehQh1);
$allQhmax1 = MathNumber::round($allQhmax1);


$sumOvQo = MathNumber::round($sumOvQo);
// $sumOvQv = MathNumber::round($sumOvQv);
$sumOvQv = MathNumber::round($sumOvQv);
$sumGvsQh = MathNumber::round($sumGvsQh);


$sumOvQoM = MathNumber::round($sumOvQo * 4.1868);
$sumOvQvM = MathNumber::round($sumOvQv * 4.1868);
$sumGvsQhM = MathNumber::round($sumGvsQh * 4.1868);
$sumTehQnM = MathNumber::round($sumTehQn * 4.1868);


$sumOvGncho = MathNumber::round($sumOvGncho);
$sumOvGnchv = MathNumber::round($sumOvGnchv);
$sumGvsGnch = MathNumber::round($sumGvsGnch);
$sumTehQhd = MathNumber::round($sumTehQhd);


$sumOvGno = MathNumber::round($sumOvGno);
$sumOvGnv = MathNumber::round($sumOvGnv);
$sumGvsGn = MathNumber::round($sumGvsGn);
$sumTehGn = MathNumber::round($sumTehGn);


$sumOvGuto = MathNumber::round($sumOvGuto);
$sumOvGutv = MathNumber::round($sumOvGutv);
$sumGvsGut = MathNumber::round($sumGvsGut);
$sumTehGut = MathNumber::round($sumTehGut);


$allQhmax = MathNumber::round($allQhmax);
$allQhmax1 = MathNumber::round($allQhmax1);
$allQh = MathNumber::round($allQh);
$allQh1 = MathNumber::round($allQh1);
$allGnch = MathNumber::round($allGnch);
$allGn = MathNumber::round($allGn);
$allGut = MathNumber::round($allGut);

$allQh1 = $sumOvQoM + $sumOvQvM + $sumGvsQhM + $sumTehQnM;


if(in_array($object->objects->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
	if($model->power_v == 0 || $model->power_v == null || $model->coefficient_load > 1){
		$model->status = 'Ошибка';
	}
} else {
	if($model->consumption_e == 0 || $model->consumption_e == null || $model->coefficient_load > 1){
		$model->status = 'Ошибка';
	}
}

// if($model->coefficient_load != null){
    // if($model->coefficient_load < 0 || $model->coefficient_load > 1){
        // $model->status = 'Ошибка';
    // }   
// }

$model->power_v = MathNumber::round($model->power_v);
$model->pover_g = MathNumber::round($model->pover_g);

?>


<div class="calculation-form">
    <?php $form = ActiveForm::begin([
        'id' => 'calculation-form',
        'fieldConfig' => ['options' => ['class' => 'form-group-sm']],
    ]); ?>
    <div class="panel panel-success ">
        <div class="panel-heading">
            <h4 class="panel-title">Группа <?= $model->number_group ?></h4>
        </div>
        <div class="panel-body">
            <table class="table equipment-table">
                <thead>
                <tr>
                    <th>Показатель</th>
                    <th>Отопление</th>
                    <th>Вентиляция</th>
                    <th>ГВС</th>
                    <th>Технолог</th>
                    <th>ИТОГО</th>
                    <th>Ед.изм.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td rowspan="2">Часовой расход тепла</td>
                    <td data-value='sum-ov-qomax'><?= $sumOvQomax ?></td>
                    <td data-value='sum-ov-qvmax'><?= $sumOvQvmax ?></td>
                    <td data-value='sum-gvs-qhm'><?= $sumGvsQhm ?></td>
                    <td data-value='sum-teh-qh'><?= $sumTehQh ?></td>
                    <td data-value='all-qhmax'><?= $allQhmax ?></td>
                    <td>Гкал/час</td>
                </tr>
                <tr>
                    <td data-value="sum-ov-qomax1"><?= $sumOvQomax1 ?></td>
                    <td data-value="sum-ov-qvmax1"><?= $sumOvQvmax1 ?></td>
                    <td data-value="sum-gvs-qhm1"><?= $sumGvsQhm1 ?></td>
                    <td data-value="sum-teh-qh1"><?= $sumTehQh1 ?></td>
                    <td data-value="all-qhmax1"><?= $allQhmax1 ?></td>
                    <td>кВт</td>
                </tr>
                <tr>
                    <td rowspan="2">Годовой расход тепла</td>
                    <td data-value="sum-ov-qo"> <?= $sumOvQo ?> </td>
                    <td data-value="sum-ov-qv"><?= $sumOvQv ?></td>
                    <td data-value="sum-gvs-qh"><?= $sumGvsQh ?></td>
                    <td data-value="sum-teh-qn"><?= $sumTehQn ?></td>
                    <td data-value="all-qh"><?= $allQh ?></td>
                    <td>Гкал/год</td>
                </tr>
                <tr>
                    <td data-value="sum-ov-qo-m"><?= $sumOvQoM ?></td>
                    <td data-value="sum-ov-qv-m"><?= $sumOvQvM ?></td>
                    <td data-value="sum-gvs-qh-m"><?= $sumGvsQhM ?></td>
                    <td data-value="sum-teh-qn-m"><?= $sumTehQnM ?></td>
                    <td data-value="all-qh1"><?= $allQh1 ?></td>
                    <td>ГДж/год</td>
                </tr>
                <tr>
                    <td>Часовой расход пр.газа</td>
                    <td data-value="sum-ov-gncho"> <?= $sumOvGncho ?> </td>
                    <td data-value="sum-ov-gnchv"> <?= $sumOvGnchv ?> </td>
                    <td data-value="sum-gvs-gnch"><?= $sumGvsGnch ?></td>
                    <td data-value="sum-teh-qhd"><?= $sumTehQhd ?></td>
                    <td data-value="all-gnch"><?= $allGnch ?></td>
                    <td>нм<sup>3</sup>/ч</td>
                </tr>
                <tr>
                    <td>Годовой расход пр.газа</td>
                    <td data-value="sum-ov-gno"> <?= $sumOvGno ?> </td>
                    <td data-value="sum-ov-gnv"> <?= $sumOvGnv ?> </td>
                    <td data-value="sum-gvs-gn"><?= $sumGvsGn ?></td>
                    <td data-value="sum-teh-gn"><?= $sumTehGn ?></td>
                    <td data-value="all-gn"><?= $allGn ?></td>
                    <td>тыс.нм<sup>3</sup>/г</td>
                </tr>
                <tr>
                    <td>Годовой расход усл.топ</td>
                    <td data-velue="sum-ov-guto"> <?= $sumOvGuto ?> </td>
                    <td data-velue="sum-ov-gutv"> <?= $sumOvGutv ?> </td>
                    <td data-velue="sum-gvs-gut"><?= $sumGvsGut ?></td>
                    <td data-velue="sum-teh-gut"><?= $sumTehGut ?></td>
                    <td data-velue="all-gut"><?= $allGut ?></td>
                    <td>тут.год</td>
                </tr>
                </tbody>

            </table>
        </div>
    </div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">Присоединенное оборудование</h4>
        </div>
        <?php

        if(in_array($obj->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
        	$consumptionEDisabled = true;
        	$consumptionODisabled = true;
        	$coefficientLoadDisabled = true;
        	$specificConsumptionDisabled = true;
        	$poverCommonDisabled = true;
        	$poverGDisabled = false;
        	$powerVDisabled = false;
        } else if($obj->type_object == Objects::TYPE_TECH) {
        	$consumptionEDisabled = false;
        	$consumptionODisabled = true;
        	$coefficientLoadDisabled = true;
        	$specificConsumptionDisabled = false;
        	$poverCommonDisabled = true;
        	$poverGDisabled = true;
        	$powerVDisabled = true;
            if($model->coefficient_load == null){
                $model->coefficient_load = 1;
            }
            if($model->consumption_e == null){
                $model->consumption_e = 0;
            }
        }

        ?>
        <div class="panel-body">
            <?= $form->field($model, 'name_equipment')->textInput(['maxlength' => true]) ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'KPD')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'power_v')->textInput(['disabled' => $powerVDisabled]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'type_object')->textInput(['disabled'=>true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'coefficient_unambiguity')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'pover_g')->textInput(['disabled' => $poverGDisabled]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'consumption_e')->textInput(['disabled' => $consumptionEDisabled]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'count_equipment')->textInput() ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'pover_common')->textInput(['disabled' => $poverCommonDisabled]) ?>
                </div>
                <div class="col-md-2">
                    <p data-role="pover_common" style="font-size: 13px; display: inline-block; margin-top: 26px;"> <?=round(($model->formula->getO8_1()), 5)?> Гкал/ч</p>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'consumption_o')->textInput(['disabled' => $consumptionODisabled]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">

                    <?= $form->field($model, 'coefficient_load')->textInput(['disabled' => $coefficientLoadDisabled]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'specific_consumption')->textInput(['disabled' => $specificConsumptionDisabled]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->textInput(['disabled'=>true, 'style' => $model->status == 'Ошибка' ? 'color: red; font-weight: bold;' : '']) ?>
                </div>
            </div>


            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>


        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>


<?php

$script = <<< JS

$('#calculation-form input, #calculation-form select').change(function(){
    var form = $('#calculation-form');

    $.ajax({
        type: 'POST',
        url: '/calculation/calculate-equipment?id={$model->id}',
        data: form.serialize()
    }).done(function(data){

        var model = data.model;
        var formulas = data.formulas;

        $('#equipment-kpd').val(model.KPD);
        $('#equipment-coefficient_unambiguity').val(model.coefficient_unambiguity);
        $('#equipment-count_equipment').val(model.count_equipment);
        $('#equipment-coefficient_load').val(model.coefficient_load);
        $('#equipment-power_v').val(model.power_v);
        $('#equipment-pover_g').val(model.pover_g);
        $('#equipment-pover_common').val(model.pover_common);
        $('#equipment-specific_consumption').val(model.specific_consumption);
        $('#equipment-consumption_e').val(model.consumption_e);
        $('#equipment-consumption_o').val(model.consumption_o);
        $('#equipment-status').val(model.status);
        // $('[data-role="pover_common"]').text('/ '+(Math.round(((model.pover_g * model.count_equipment) + Number.EPSILON) * 1000) / 1000)+' Гкал/ч');
        $('[data-role="pover_common"]').text('/ '+formulas.O8_1+' Гкал/ч');


        $('[data-value="sum-ov-qomax"]').text(formulas.sumOvQomax);
        $('[data-value="sum-ov-qvmax"]').text(formulas.sumOvQvmax);
        $('[data-value="sum-gvs-qhm"]').text(formulas.sumGvsQhm);
        $('[data-value="sum-teh-qh"]').text(formulas.sumTehQh);
        $('[data-value="all-qhmax"]').text(formulas.allQhmax);

        $('[data-value="sum-ov-qomax1"]').text(formulas.sumOvQomax1);
        $('[data-value="sum-ov-qvmax1"]').text(formulas.sumOvQvmax1);
        $('[data-value="sum-gvs-qhm1"]').text(formulas.sumGvsQhm1);
        $('[data-value="sum-teh-qh1"]').text(formulas.sumTehQh1);
        $('[data-value="all-qhmax1"]').text(formulas.allQhmax1);

        $('[data-value="sum-ov-qo"]').text(formulas.sumOvQo);
        $('[data-value="sum-ov-qv"]').text(formulas.sumOvQv);
        $('[data-value="sum-gvs-qh"]').text(formulas.sumGvsQh);
        $('[data-value="sum-tech-qn"]').text(formulas.sumTehQn);
        $('[data-value="all-qh"]').text(formulas.allQh);

        $('[data-value="sum-ov-qo-m"]').text(formulas.sumOvQoM);
        $('[data-value="sum-ov-qv-m"]').text(formulas.sumOvQvM);
        $('[data-value="sum-gvs-qh-m"]').text(formulas.sumGvsQhM);
        $('[data-value="sum-tech-qn-m"]').text(formulas.sumTehQnM);
        $('[data-value="all-qh1"]').text(formulas.allQh1);

        $('[data-value="sum-ov-gncho"]').text(formulas.sumOvGncho);
        $('[data-value="sum-ov-gnchv"]').text(formulas.sumOvGnchv);
        $('[data-value="sum-gvs-gnch"]').text(formulas.sumGvsGnch);
        $('[data-value="sum-teh-qhd"]').text(formulas.sumTehQhd);
        $('[data-value="all-gnch"]').text(formulas.allGnch);

        $('[data-value="sum-ov-guto"]').text(formulas.sumOvGuto);
        $('[data-value="sum-ov-gutv"]').text(formulas.sumOvGutv);
        $('[data-value="sum-gvs-gut"]').text(formulas.sumGvsGut);
        $('[data-value="sum-tech-gut"]').text(formulas.sumTehGut);
        $('[data-value="all-gut"]').text(formulas.allGut);

        $('#equipment-coefficient_load').val(formulas.coefficientLoad);
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>