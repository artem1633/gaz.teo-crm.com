<?php

use app\models\ObjectsGvsType;
use app\models\ObjectsOvType;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Objects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objects-form">

    <?php $form = ActiveForm::begin([
     ]); ?>

    <?= $form->field($model, 'name', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count_object', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'purpose_system', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->dropDownList(ObjectsGvsType::getTypes(),['onChange'=>'systemGvsTypeChange()', 'prompt' => 'Выберите тип']) ?>

    <?= $form->field($model, 'water_consumption_rate', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_per_day', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count_work_day', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'calculation_regarding_quantity', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count_consumers', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'heat_loads_known')->checkbox(['onChange'=>'gvsLoadsKnowChange()']) ?>

    <?= $form->field($model, 'heating_load', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true,'disabled'=> $model->heat_loads_known ? false : true,'onChange'=>'gvsPowerChange()']) ?>

    <?= $form->field($model, 'ventilation_load', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true,'disabled'=> $model->heat_loads_known ? false : true,'onChange'=>'gvsPowerChange()']) ?>

    <div class="row">
        <div class="col-md-7">
            <label>
                Общая нагрузка сиcтемы ГВС q, Гкал/час
            </label>
        </div>
        <div class="col-md-5" id="gvs_all">

        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<?php

$script = <<< JS

function getAllGvsValue(){
    var heatingValue = parseFloat($('#objectsgvsform-heating_load').val());
    var ventilationValue = parseFloat($('#objectsgvsform-ventilation_load').val());

    var allValue = heatingValue + ventilationValue;

    if(allValue != null && isNaN(allValue) == false){
        $('#gvs_all').text(allValue);
    }
}

getAllGvsValue();


$('#objectsgvsform-heating_load, #objectsgvsform-ventilation_load').change(getAllGvsValue);

JS;


$this->registerJs($script, \yii\web\View::POS_READY);


?>

