<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Objects;
use app\models\ObjectsTech;
use app\helpers\MathNumber;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number_group',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type_object',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name_equipment',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'KPD',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'coefficient_unambiguity',
    // ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'count_equipment',
     // 'content' => function($model){
        // return array_sum(ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $model->calculation_id, 'number_group' => $model->number_group])->all(), 'count_object'));
     // }
     ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'coefficient_load',
    // ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'power_v',
     'value' => function($model){
        $output = [];
        $object = Objects::find()->where(['calculation_id' => $model->calculation_id, 'number_group' => $model->number_group])->one();
        // if($object->type_object == Objects::TYPE_TECH){
            $objectTech = ObjectsTech::find()->where(['objects_id' => $object->id])->one();
            $model->power_v = $model->formula->getO6();
            $model->pover_g = $model->formula->getO6() / 1163;
            $model->pover_common = $model->power_v * $model->count_equipment;

            $powerV = MathNumber::round($model->power_v);

            $output[] = "{$powerV} кВт";
        // }

        return implode(', ', $output);
     }
     ],
//     [
//     'class'=>'\kartik\grid\DataColumn',
//     'attribute'=>'pover_g',
//     ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'specific_consumption',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'consumption_e',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'consumption_o',
    // ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'status',
     'content' => function($model){

            $object = Objects::find()->where(['calculation_id' => $model->calculation_id, 'number_group' => $model->number_group])->one();

            // if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
            //     if($model->power_v == 0 || $model->power_v == null || $model->coefficient_load > 1){
            //         return '<b class="text-danger">Ошибка</b>';
            //     }
            // } else {
            //     if($model->consumption_e == 0 || $model->consumption_e == null || $model->coefficient_load > 1){
            //         return '<b class="text-danger">Ошибка</b>';
            //     }
            // }

            $model->coefficient_load = $model->formula->getO5();

            if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
                if($model->power_v == 0 || $model->power_v == null || $model->coefficient_load > 1){
                    return '<b class="text-danger">Ошибка</b>';
                }
            } else {
                if($model->consumption_e == 0 || $model->consumption_e == null || $model->coefficient_load > 1){
                    return '<b class="text-danger">Ошибка</b>';
                }
            }

            return 'Корректно';
     }
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action.'-equipment','id'=>$key]);
        },
        'template' => '{update}',
        'buttons' => [
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            },
            // 'delete' => function ($url, $model) {
            //     return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
            //         'role' => 'modal-remote', 'title' => 'Удалить',
            //         'data-confirm' => false, 'data-method' => false,// for overide yii data api
            //         'data-request-method' => 'post',
            //         'data-confirm-title' => 'Вы уверены?',
            //         'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
            //     ]);
            // },
        ],
    ],
];