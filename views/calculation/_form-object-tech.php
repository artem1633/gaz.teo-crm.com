<?php

use app\models\ObjectsOvType;
use app\models\ObjectsTechType;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Objects */
/* @var $form yii\widgets\ActiveForm */
/* @var $isNewRecord boolean */

if($model->purpose_system != null){
    $purposeSystem = $model->purpose_system;
    $purposeSystem = ObjectsTechType::findOne($purposeSystem);

    if($purposeSystem != null){
        $purposeSystemIsRound = $purposeSystem->is_random;
    }

    $purposeSystemIsRound = false;
} else {
    $purposeSystemIsRound = false;
}

$model->count_object = 1;

?>

<div class="objects-form">

    <?php $form = ActiveForm::begin([
     ]); ?>

    <?= $form->field($model, 'name', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count_object', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'purpose_system', ['template' => "<div class='row'> <div class='col-md-4' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-8'>{input}</div></div>\n{error}",
    ])->dropDownList(ObjectsTechType::getTypes(),['onChange'=>'systemTechTypeChange()', 'prompt' => 'Выберите тип']) ?>

    <?= Html::beginTag('div', [
            'id' => 'destination-system-wrapper',
            'style' => $purposeSystemIsRound ? '' : 'display: none',
        ]) ?>

        <?= $form->field($model, 'destination_system', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
        ])->textInput(['maxlength' => true]) ?>

    <?= Html::endTag('div') ?>

    <?= $form->field($model, 'consumption_thermal_units', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'consumption_equivalent_fuel', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?php 
    // echo $form->field($model, 'thermal_1_7', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    // ])->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'day_per_week', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'norm_document', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount', ['template' => "<div class='row'> <div class='col-md-7' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-5'>{input}</div></div>\n{error}",
    ])->textInput(['maxlength' => true]) ->label(strval(($model->amount_name ? $model->amount_name : "Количество")),['id'=>'label-objectstechform-amount'])?>

    <?= $form->field($model, 'amount_name')->hiddenInput()->label(false)?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton( 'Update', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php
// if($isNewRecord){
    $script = <<<JS
// systemTechTypeChange();

$('#objectstechform-consumption_thermal_units').change(function(){
    var purposeSystem = $('#objectstechform-purpose_system').val();

    if(purposeSystem == 16){
        var consumptionEquivalentFuel = ($(this).val() / 7).toFixed(2);
        $('#objectstechform-consumption_equivalent_fuel').val(consumptionEquivalentFuel);
    }
});
JS;
    $this->registerJs($script);
// }
?>


