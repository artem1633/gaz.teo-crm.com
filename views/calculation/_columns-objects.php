<?php

use app\models\Objects;
use app\models\ObjectsGvs;
use app\models\ObjectsOv;
use app\models\ObjectsTech;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'count_object',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'status',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type_object',
        'filter' => Objects::getTypesObjects(),
        'value' => function ($model) {
            return isset(Objects::getTypesObjects()[$model->type_object]) ? Objects::getTypesObjects()[$model->type_object] : 'Не определен';
        }
    ],
    [

        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'number_group',
        'editableOptions'=> [
            'preHeader' => '',
            'header' => 'Выбор группы',
            'formOptions' => ['action' => ['/calculation/group-one-object']],
            'pluginEvents' => [
                'editableSuccess' => "function(val, form, data, jqXHR){
                    $.pjax.reload({container: '#crud-datatable-object-pjax', async: false});
                    $('#crud-datatable-object-pjax').removeClass('kv-grid-loading');
                    $.pjax.reload({container: '#crud-datatable-equipment-pjax', async: false});
                    $('#crud-datatable-equipment-pjax').removeClass('kv-grid-loading');
                    $.pjax.reload('#global-pjax-container');
                }",
            ],
            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
            'data' => [
                '0' => 'Нет',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
            ],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'power_v',
        'label' => 'Мощность, кВт',
        'content' => function($model){

            if($model->number_group == null){

                $sumOvQomax1 = 0;
                $sumOvQvmax1 = 0;
                $sumGvsQhm1 = 0;
                $sumTehQh1 = 0;

                    if($model->type_object == Objects::TYPE_OV){
                        $model = ObjectsOv::findOne(['objects_id' => $model->id]);

                        if($model){
                            $sumOvQomax1 += $model->formula->getOvQomax1();
                            $sumOvQvmax1 += $model->formula->getOvQvmax1();
                        }

                    } else if($model->type_object == Objects::TYPE_GVS){
                        $model = ObjectsGvs::findOne(['objects_id' => $model->id]);

                        if($model){
                            $sumGvsQhm1 += $model->formula->getGvsQhm1();
                        }

                    } else if($model->type_object == Objects::TYPE_TECH){
                        $model = ObjectsTech::findOne(['objects_id' => $model->id]);

                        if($model){
                            $sumTehQh1 += $model->formula->getTehQh1();
                        }

                    }

                $allQhmax1 = $sumOvQomax1 + $sumOvQvmax1 + $sumGvsQhm1 + $sumTehQh1;

                return round($allQhmax1, 2);
            }

            // $objects = Objects::find()->where(['number_group' => $model->number_group, 'calculation_id' => $model->calculation_id])->all();
            $objects = [Objects::findOne($model->id)];

            $sumOvQomax1 = 0;
            $sumOvQvmax1 = 0;
            $sumGvsQhm1 = 0;
            $sumTehQh1 = 0;

            foreach ($objects as $object){
                    if($object->type_object == Objects::TYPE_OV){
                        $object = ObjectsOv::findOne(['objects_id' => $object->id]);

                        if($object){
                            $sumOvQomax1 += $object->formula->getOvQomax1();
                            $sumOvQvmax1 += $object->formula->getOvQvmax1();
                        }

                    } else if($object->type_object == Objects::TYPE_GVS){
                        $object = ObjectsGvs::findOne(['objects_id' => $object->id]);

                        if($object){
                            $sumGvsQhm1 += $object->formula->getGvsQhm1();
                        }

                    } else if($object->type_object == Objects::TYPE_TECH){
                        $object = ObjectsTech::findOne(['objects_id' => $object->id]);

                        if($object){
                            $sumTehQh1 += $object->formula->getTehQh1();
                        }

                    }
            }
            $allQhmax1 = $sumOvQomax1 + $sumOvQvmax1 + $sumGvsQhm1 + $sumTehQh1;

            return round($allQhmax1, 2);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action.'-object', 'id' => $key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote', 'title' => 'Удалить',
                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role' => 'modal-remote', 'title' => 'Изменить',
                        'data-confirm' => false, 'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                    ]) . "&nbsp;";
            }
        ],
    ],

];