<?php

use app\models\Region;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin([
        'fieldConfig' => ['options' => ['class' => 'form-group-sm']],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'region_id')->dropDownList(Region::getCitiesName()) ?>
        </div>
    </div>

    <?= $form->field($model, 'temperature_average',
        ['template' => "<div class='row'> <div class='col-md-8' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
        ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'temperature_calculated',
        ['template' => "<div class='row'> <div class='col-md-8' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
        ])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'winder_speed',
        ['template' => "<div class='row'> <div class='col-md-8' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-4'>{input}</div></div>\n{error}",
        ])->textInput(['maxlength' => true]) ?>

    <h4><strong>Средняя температура</strong></h4>
    <hr style="margin: 0px 0px 5px 0px">
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'temp_average1',
            ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
            ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average2',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average3',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average4',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'temp_average5',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average6',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average7',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average8',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'temp_average9',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average10',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average11',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'temp_average12',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
    </div>
    <h4><strong>Продолжительность отопительного периода</strong></h4>
    <hr style="margin: 0px 0px 5px 0px">
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'duration_op1',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op2',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op3',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op3',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'duration_op1',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op2',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op3',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op3',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'duration_op4',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op5',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op6',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'duration_op3',
                ['template' => "<div class='row'> <div class='col-md-6' style='padding-top:8px ;: '>{label}{hint}</div><div class='col-md-6'>{input}</div></div>\n{error}",
                ])->textInput(['maxlength' => true]) ?></div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
