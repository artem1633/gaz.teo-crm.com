<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\widgets\MaskedInput;
use unclead\multipleinput\MultipleInput;
use app\models\ObjectsOvTypeCharacteristic;
use app\models\ObjectsOvType;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsOvType */
/* @var $form yii\widgets\ActiveForm */

$lastId = null;

if($model->isNewRecord == false){
    $model->charecteristics = ObjectsOvTypeCharacteristic::find()->where(['objects_ov_type_id' => $model->id])->asArray()->all();

    if(count($model->charecteristics) > 0){
        $lastId = $model->charecteristics[count($model->charecteristics)-1]['id'];
    }

    // foreach ($charecteristics as $charecteristic) {
        // $model->charecteristic[] = [
            // ''
        // ];
    // }
}

?>

<div class="objects-ov-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'specific_thermal_characteristic_heating')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'decimal',
            'groupSeparator' => '.',
        ],
    ]) ?>

    <?= $form->field($model, 'specific_thermal_characteristic_ventilation')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'decimal',
            'groupSeparator' => '.',
        ],
    ]) ?>

    <?= $form->field($model, 'type')->dropDownList(ObjectsOvType::typeLabels(), ['prompt' => 'Выберите тип']) ?>

    <?= $form->field($model, 'external_temperature')->textInput() ?>

    <?= $form->field($model, 'charecteristics')->widget(MultipleInput::className(), [
        'columns' => [
            [
                'name' => 'comment',
                'type' => 'static',
                'value' => function($model) use($lastId){
                    if($lastId == $model['id']){
                        return 'После';
                    } else {
                        return 'До';
                    }
                },
            ],
            [
                'name' => 'building_volume',
                'title' => 'Строительный объем',
            ],
            [
                'name' => 'heating',
                'title' => 'Отопление',
            ],
            [
                'name' => 'ventilation',
                'title' => 'Вентиляция',
            ],
        ],
    ])->label(false) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
