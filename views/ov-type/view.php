<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsOvType */
?>
<div class="objects-ov-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'specific_thermal_characteristic_heating',
            'specific_thermal_characteristic_ventilation',
        ],
    ]) ?>

</div>
