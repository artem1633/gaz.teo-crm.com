<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsOvType */
?>
<div class="objects-ov-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
