<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsTechType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objects-tech-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'consumption_thermal_units')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'decimal',
            'groupSeparator' => '.',
        ],
    ]) ?>

    <?= $form->field($model, 'consumption_equivalent_fuel')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'decimal',
            'groupSeparator' => '.',
        ],
    ]) ?>

    <?= $form->field($model, 'day_per_week')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'integer',
            'groupSeparator' => '.',
        ],
    ]) ?>

    <?= $form->field($model, 'norm_document')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'one_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_random')->checkbox() ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
