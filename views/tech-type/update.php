<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsTechType */
?>
<div class="objects-tech-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
