<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsTechType */
?>
<div class="objects-tech-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'consumption_thermal_units',
            'consumption_equivalent_fuel',
            'day_per_week',
            'norm_document',
            'amount_name',
            'amount',
        ],
    ]) ?>

</div>
