<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ObjectsGvsType */

?>
<div class="objects-gvs-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
