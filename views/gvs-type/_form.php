<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsGvsType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objects-gvs-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'water_consumption_rate')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'decimal',
            'groupSeparator' => '.',
            'removeMaskOnSubmit' => true
        ],
    ]) ?>

    <?= $form->field($model, 'calculation_regarding_quantity')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
