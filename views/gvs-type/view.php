<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectsGvsType */
?>
<div class="objects-gvs-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'water_consumption_rate',
            'calculation_regarding_quantity',
        ],
    ]) ?>

</div>
