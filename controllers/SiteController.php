<?php

namespace app\controllers;

use app\helpers\ImportCity;
use app\models\forms\ResetPasswordForm;
use app\models\Objects;
use app\models\ObjectsTech;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;
use app\models\Calculation;
use app\models\ObjectsOv;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        ImportCity::import();
    }

    public function actionTest()
    {
        // $email = new \SendGrid\Mail\Mail();
        // $email->setFrom("qist@qist.com", "Example User");
        // $email->setSubject("Sending with SendGrid is Fun");
        // $email->addTo("qist27000@gmail.com", "Example User");
        // $email->addContent(
        //     "text/html", "Тестирование сообщения"
        // );
        // $sendgrid = new \SendGrid('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');
        // try {
        //     $response = $sendgrid->send($email);
        //     // print $response->statusCode() . "\n";
        //     print_r($response->headers());
        //     // print $response->body() . "\n";
        // } catch (Exception $e) {
        //     echo 'Caught exception: '. $e->getMessage() ."\n";
        // }

        // $tehPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => 18, 'type_object' => Objects::TYPE_TECH])->all(), 'id');

        // /** @var $tehObjects ObjectsTech[] */
        // $tehObjects = ObjectsTech::find()->where(['objects_id' => $tehPks])->all();

        // var_dump($tehObjects);

        // $calculation = Calculation::findOne(32);
        // $object = ObjectsOv::findOne(['objects_id' => 54]);

        //     $k6 = $calculation->formula->getK6();
        //     $k3 = $calculation->formula->getK3();

        //     $ovKip = round($object->formula->getOvKip(), 2);

        //     $ovQvmax = $k6 * $object->formula->getOv4() * $object->formula->getOv6() * ($object->formula->getOv8() - $k3) * (1 + $ovKip) * 0.000001;


        // $x = $k6 * $object->formula->getOv4() * $object->formula->getOv6() * ($object->formula->getOv8() - $k3) * (1 + $ovKip) * 0.000001;

        // $x = $x * $object->formula->getOv2();

        // $x = floor($object->formula->getOvQvmax()*pow(10,4))/pow(10,4);

        // echo $x;

        $tehPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => 32, 'type_object' => Objects::TYPE_TECH])->all(), 'id');

        /** @var $tehObjects ObjectsTech[] */
        $tehObjects = ObjectsTech::find()->where(['objects_id' => $tehPks])->all();

        var_dump($tehPks);
        var_dump(count($tehObjects));
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
