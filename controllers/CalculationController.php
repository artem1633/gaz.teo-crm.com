<?php

namespace app\controllers;

use app\models\City;
use app\models\Equipment;
use app\models\EquipmentSearch;
use app\models\forms\ObjectsGvsForm;
use app\models\forms\ObjectsTechForm;
use app\models\Objects;
use app\models\ObjectsGvs;
use app\models\ObjectsGvsType;
use app\models\ObjectsOv;
use app\models\forms\ObjectsOvForm;
use app\models\ObjectsOvType;
use app\models\ObjectsSearch;
use app\models\ObjectsTech;
use app\models\ObjectsTechType;
use app\models\Region;
use app\models\ObjectsOvTypeCharacteristic;
use kartik\grid\EditableColumnAction;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Yii;
use app\models\Calculation;
use app\models\CalculationSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\helpers\MathNumber;

  
/**
 * CalculationController implements the CRUD actions for Calculation model.
 */
class CalculationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Calculation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalculationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTest()
    {
        echo MathNumber::round(0.00007864)."<br>";
    }


    /**
     * Displays a single Calculation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "расчеты #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Displays a single Calculation model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewFormula($id)
    {
        $objectsPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_OV])->all(), 'id');

        /** @var ObjectsOv[] $ovObjects */
        $ovObjects = ObjectsOv::find()->where(['objects_id' => $objectsPks])->all();

//        foreach ($ovObjects as $object){
//            echo "Qomax: ".$object->formula->ovQomax."<br>";
//            echo "Qomax1: ".$object->formula->ovQomax1."<br>";
//            echo "Qvmax: ".$object->formula->ovQvmax."<br>";
//            echo "Qvmax1: ".$object->formula->ovQvmax1."<br>";
//            echo "Gncho: ".$object->formula->ovGncho."<br>";
//            echo "Gnchv: ".$object->formula->ovGnchv."<br>";
//            echo "Gnch: ".$object->formula->ovGnch."<br>";
//            echo "<br><br><br>";
//        }
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Calculation();
        $model->duration_op1=31;
        $model->duration_op2=28;
        $model->duration_op3=31;
        $model->duration_op4=30;
        $model->duration_op5=0;
        $model->duration_op6=0;
        $model->duration_op7=0;
        $model->duration_op8=0;
        $model->duration_op9=0;
        $model->duration_op10=31;
        $model->duration_op11=30;
        $model->duration_op12=31;
        $model->c_alfa=1;
        $model->user_id = Yii::$app->user->id;
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Расчет успешно создан');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
        	$model->type_oil = 'Природный газ';
        	$model->year_start_consume = date('Y');
        	$model->calorific_value = 8000;
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    public function actionGenerate($id)
    {
        $model = $this->findModel($id);

        $headerFontStyle = [
        	'font' => [
        		'size' => 14,
        	],
        ];
        $headerBoldFontStyle = [
        	'font' => [
        		'bold' => true,
        		'size' => 14,
        	],
        ];
        $headerFontSize10Style = [
        	'font' => [
        		'size' => 10,
        	],
        ];
        $headerBoldFontSize10Style = [
        	'font' => [
        		'bold' => true,
        		'size' => 10,
        	],
        ];


        $spreadsheet = new Spreadsheet();

        $spreadsheet->getDefaultStyle()->applyFromArray([
        	'font' => [
        		'name' => 'Times Roman',
        	],
        ]);

        $sheet = $spreadsheet->getActiveSheet();
        // $spreadsheet->getActiveSheet()->getPageSetup()
    // ->setOrientation(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->getPageSetup()
    ->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
        $sheet->mergeCells('A2:I2');
        $sheet->mergeCells('A15:I16');
        $sheet->getStyle('B2:J2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->mergeCells('A4:I4');
        $sheet->mergeCells('A47:I47');
        // $sheet->mergeCells('E12:J12');
        $sheet->getStyle('A47:I47')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('B4:J5')->getFont()->setSize(14);
        $sheet->getStyle('E12:J12')->getFont()->setSize(12);
        $sheet->getStyle('B7:F7')->getFont()->setSize(11);
        $sheet->getStyle('B8:F8')->getFont()->setSize(11);
        $sheet->getStyle('B9:F9')->getFont()->setSize(11);  
        $sheet->setCellValue('A2', 'Российская Федерация');
        $sheet->setCellValue('A4', $model->employer_organization);
        $sheet->setCellValue('A15', 'Технико-экономический расчет потребности в тепле и топливе');
        $sheet->setCellValue('A29', "Заказчик");
        $sheet->setCellValue('A31', "Объект");
        $sheet->setCellValue('A33', "Адрес");
        $sheet->setCellValue('C29', "{$model->customer_name}");
        $sheet->setCellValue('C31', "{$model->object_name}");
        $sheet->setCellValue('C33', "{$model->object_address}");
        $sheet->setCellValue('A47', "{$model->employer_position}___________{$model->employer_name}");

        $sheet->getStyle('A2')->applyFromArray($headerFontStyle);
        $sheet->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A4')->applyFromArray($headerBoldFontStyle);
        $sheet->getStyle('A4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A15')->applyFromArray([
	        	'font' => [
	        		'bold' => true,
	        		'size' => 18
	        	],
        	]);
        $sheet->getStyle('A15')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A29')->applyFromArray($headerBoldFontStyle);
        $sheet->getStyle('A31')->applyFromArray($headerBoldFontStyle);
        $sheet->getStyle('A33')->applyFromArray($headerBoldFontStyle);
        $sheet->getStyle('C29')->applyFromArray($headerFontStyle);
        $sheet->getStyle('C31')->applyFromArray($headerFontStyle);
        $sheet->getStyle('C33')->applyFromArray($headerFontStyle);
        $sheet->getStyle('A47')->applyFromArray($headerFontStyle);


        $secondSheet = $spreadsheet->createSheet();
        $secondSheet->setTitle('Общие требования к оформлению');
        $secondSheet->mergeCells('A2:J2');
        $secondSheet->getStyle('A2:J2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $secondSheet->getStyle('A2:J2')->getFont()->setBold(2);
        $secondSheet->setCellValue('B2', 'Общие требования к оформлению');
        $secondSheet->mergeCells('A3:K6');
        $secondSheet->getStyle('A3:K6')->getAlignment()->setWrapText(true);
        $secondSheet->setCellValue('A3', 'На странице с содержанием должна быть рамка формы 2, на всех последующих – формы 2а.
Размеры рамок нормируются стандартами. Рамки должны иметь отступы сверху, снизу и справа по 5мм, слева – 20мм.');

        $secondSheet->mergeCells('A8:I8');
        $secondSheet->mergeCells('A9:E9');
        $secondSheet->mergeCells('A10:E10');
        $secondSheet->mergeCells('A11:E11');
        $secondSheet->mergeCells('A12:E12');
        $secondSheet->mergeCells('A13:E13');
        $secondSheet->mergeCells('A14:E14');
        $secondSheet->mergeCells('A15:E15');

        $secondSheet->setCellValue('A8', 'По форме 2а – на автомате заполняем только поле Лист – порядковый номер листа.');
        $secondSheet->setCellValue('A9', 'По форме 2:');
        $secondSheet->setCellValue('A10', "Поле 1 – {$model->object_name}");
        $secondSheet->setCellValue('A11', 'Поле 2 – пустое');
        $secondSheet->setCellValue('A12', "Поле 7 – {$model->customer_name}");
        $secondSheet->setCellValue('A13', "Поле 8 только на уровне «разработал» - {$model->employer_name}");
        $secondSheet->setCellValue('A14', 'Пол 11 – равно 2');
        $secondSheet->setCellValue('A15', 'Поле 12 – общее количество листов');


        $thirdSheet = $spreadsheet->createSheet();
        $thirdSheet->setTitle('Содержание расчета');
        $thirdSheet->mergeCells('A2:I2');
        $thirdSheet->getStyle('A2:I2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $thirdSheet->getStyle('A2:I2')->getFont()->setBold(2);
        $thirdSheet->getStyle('A2:I2')->applyFromArray([
	        	'font' => [
	        		'bold' => true,
	        		'size' => 11
	        	],
        	]);
        $thirdSheet->setCellValue('A2', 'Содержание расчета');


        $bordersStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $thirdSheet->getStyle('A4:I4')->getFont()->setBold(2);
        $thirdSheet->getStyle('A4:I4')->getBorders();
        $thirdSheet->getStyle('A4:I4')->applyFromArray($bordersStyle);
        $thirdSheet->setCellValue('A4', '№п/п');

        $thirdSheet->mergeCells('B4:H4');
        $thirdSheet->setCellValue('B4', 'Наименование раздела');
        $thirdSheet->getStyle('B4:H4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $thirdSheet->getStyle('A4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $thirdSheet->getStyle('I4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $thirdSheet->getStyle('A4')->applyFromArray($headerBoldFontSize10Style);
        $thirdSheet->getStyle('B4')->applyFromArray($headerBoldFontSize10Style);
        $thirdSheet->getStyle('I4')->applyFromArray($headerBoldFontSize10Style);

        $thirdSheet->setCellValue('I4', 'Стр.');


        $thirdData = [
            ['Содержание расчета.', '2'],
            ['Общие данные расчета потребности в тепловой энергии, условном и натуральном топливе.', '3'],
        ];


        $objects = Objects::find()->where(['calculation_id' => $id])->all();

        foreach ($objects as $object) {
            $thirdData[] = [$object->name, ''];
        }

        $thirdData = ArrayHelper::merge($thirdData, [['Приложение 1 – Котельное оборудование', ''],
            ['Приложение 2 – Технологическое оборудование', ''],
            ['Приложение 3 – Расчет помесячного распределения потребности в топливе', ''],
            ['Заявление по форме утв. приложением к Порядку оформления решений об установлении видов топлива (Приказ Минэкономразвития РФ, Минэнерго РФ и ОАО "Газпром" от 15 октября 2002 г. N 333/358/101)', 'на 4 листах']
        ]);

        $offset = 5;
        for ($i = 0; $i < count($thirdData); $i++) {
            $v = $offset + $i;
            $data = $thirdData[$i];

            $thirdSheet->getStyle("A{$v}:I{$v}")->applyFromArray($bordersStyle);

			$thirdSheet->getStyle("A{$v}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
			$thirdSheet->getStyle("A{$v}")->applyFromArray($headerFontSize10Style);
            $thirdSheet->mergeCells("B{$v}:H{$v}");
            // $thirdSheet->getStyle("B{$v}")->getAlignment()->setWrapText(true);
            $thirdSheet->getStyle("B{$v}")->applyFromArray($headerFontSize10Style);
            $thirdSheet->getStyle("B{$v}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $thirdSheet->setCellValue("A{$v}", $i + 1);
            $thirdSheet->setCellValue("B{$v}", $data[0]);
            $thirdSheet->setCellValue("I{$v}", $data[1]);
            $thirdSheet->getStyle("I{$v}")->applyFromArray($headerFontSize10Style);
            $thirdSheet->getStyle("I{$v}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        }


        $fourSheet = $spreadsheet->createSheet();

        $fourSheet->getColumnDimension('A')->setWidth(6.5);
        $fourSheet->getColumnDimension('B')->setWidth(6.5);
        $fourSheet->getColumnDimension('C')->setWidth(6.5);
        $fourSheet->getColumnDimension('D')->setWidth(6.5);
        $fourSheet->getColumnDimension('E')->setWidth(6.5);
        $fourSheet->getColumnDimension('E')->setWidth(6.5);
        $fourSheet->getColumnDimension('F')->setWidth(6.5);
        $fourSheet->getColumnDimension('G')->setWidth(6.5);
        $fourSheet->getColumnDimension('H')->setWidth(6.5);
        $fourSheet->getColumnDimension('I')->setWidth(6.5);
        $fourSheet->getColumnDimension('J')->setWidth(6.5);
        $fourSheet->getColumnDimension('K')->setWidth(6.5);
        $fourSheet->getColumnDimension('L')->setWidth(6.5);
        $fourSheet->getColumnDimension('M')->setWidth(6.5);
        $fourSheet->getColumnDimension('N')->setWidth(6.5);

        $fourSheet->getStyle('A1:Z100')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);

        $fourSheet->setTitle('Общие данные');
        $fourSheet->mergeCells('A2:N2');
        $fourSheet->getStyle('A2:N2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('A2:N2')->getFont()->setBold(2);
        $fourSheet->getStyle('A2:N2')->applyFromArray([
        	'font' => [
        		'size' => 11,
        	],
        ]);
        $fourSheet->setCellValue('A2', 'Общие данные расчета потребности в тепловой энергии, условном и натуральном топливе');

        $fourSheet->mergeCells('A4:C4');
        $fourSheet->mergeCells('A5:C5');
        $fourSheet->mergeCells('A6:C6');
        // $fourSheet->mergeCells('B8:S14');
        $fourSheet->mergeCells('C15:I15');
        $fourSheet->getStyle('C15:I15')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('C15:I15')->getFont()->setBold(2);
        $fourSheet->getStyle("B8:J13")->getAlignment()->setWrapText(true);

        $region = Region::findOne($model->region_id);

        $fourSheet->setCellValue('A4', "Объект: {$model->object_name}");
        $fourSheet->setCellValue('A5', "По адресу: {$model->object_address}");
        $fourSheet->setCellValue('A6', "Заказчик: {$model->customer_name}");
        $fourSheet->setCellValue('A7', "Вид топлива: {$model->customer_name}");
        $fourSheet->setCellValue('A8', "Теплотворная способность топлива: {$model->customer_name}");
        $fourSheet->setCellValue('A9', "Регион строительства: {$model->customer_name}");
        $fourSheet->setCellValue('A10', "Продолжительность отопительного сезона: {$model->customer_name}");
        $fourSheet->setCellValue('A11', "Расчетная температура наружного воздуха: {$model->customer_name}");
        $fourSheet->setCellValue('A12', "Средняя температура наружного воздуха: {$model->customer_name}");
        $fourSheet->setCellValue('A13', "Расчетная скорость ветра: {$model->customer_name}");
        $fourSheet->setCellValue('A14', "Поправочный коэффициент a(альфа): {$model->customer_name}");
        $fourSheet->setCellValue('A16', "Расчет выполнен в соответствии с МДК 4-05.2004 'Методика определения потребности в топливе, электрической энергии и воде при производстве и передаче тепловой энергии и теплоносителей в системах коммунального теплоснабжения', СП 42-101-2003 'Общие положения по проектированию и строительству газораспределительных систем из металлических и полиэтиленовых труб, на основании климатических данных, согласно СП 131.13330.2012 'Строительная климатология'

Вид топлива: {$model->type_oil}
Теплотворная способность топлива: Qн = {$model->calorific_value} ккал/н.м3.

Регион строительства: {$region->name}.
Продолжительность отопительного сезона: n = (K5) суток.
Расчетная температура наружного воздуха: t0 = tv = {$model->temperature_calculated} °C. 
Средняя температура наружного воздуха: tот = {$model->temperature_average} °C. 
Расчетная скорость ветра: w0 = {$model->winder_speed} [м/с]. 
Поправочный коэффициент a(альфа) = {$model->c_alfa}.  
");
        $fourSheet->mergeCells('A16:N16');

        $fourSheet->setCellValue('A18', 'Результаты расчетов сведены в таблицу');
        $fourSheet->mergeCells('A18:N18');


        $fourSheet->mergeCells('A19:B19');
        $fourSheet->mergeCells('C19:D19');
        $fourSheet->mergeCells('E19:F19');
        $fourSheet->mergeCells('G19:H19');
        $fourSheet->mergeCells('I19:J19');
        $fourSheet->mergeCells('K19:L19');
        $fourSheet->mergeCells('M19:N19');
        $fourSheet->getStyle('A19:B19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('A19:B19')->getFont()->setBold(2);
        $fourSheet->getStyle('C19:D19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('C19:D19')->getFont()->setBold(2);
        $fourSheet->getStyle('E19:F19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('E19:F19')->getFont()->setBold(2);
        $fourSheet->getStyle('G19:H19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('G19:H19')->getFont()->setBold(2);
        $fourSheet->getStyle('I19:J19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('I19:J19')->getFont()->setBold(2);
        $fourSheet->getStyle('K19:L19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('K19:L19')->getFont()->setBold(2);
        $fourSheet->getStyle('M19:N19')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->getStyle('M19:N19')->getFont()->setBold(2);
        $fourSheet->setCellValue('A19', 'Показатель');
        $fourSheet->setCellValue('C19', 'Отопление');
        $fourSheet->setCellValue('E19', 'Вентиляция');
        $fourSheet->setCellValue('G19', 'ГВС');
        $fourSheet->setCellValue('I19', 'Технолог. нужды');
        $fourSheet->setCellValue('K19', 'ИТОГО');
        $fourSheet->setCellValue('M19', 'Ед.изм.');
        $fourSheet->getStyle('A19:B19')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('C19:D19')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('E19:F19')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('G19:H19')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('I19:J19')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('K19:L19')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('M19:N19')->applyFromArray($bordersStyle);


        $fourSheet->mergeCells('A20:B21');
        $fourSheet->mergeCells('A22:B23');
        $fourSheet->mergeCells('A24:B25');
        $fourSheet->mergeCells('A26:B27');
        $fourSheet->mergeCells('A28:B29');

        $fourSheet->getStyle('A20:B21')->getAlignment()->setWrapText(true);

        $fourSheet->setCellValue('A20', 'Часовой расход тепла');
        $fourSheet->setCellValue('A22', 'Годовой расход тепла');
        $fourSheet->setCellValue('A24', 'Часовой расход топлива');
        $fourSheet->setCellValue('A26', 'Годовой расход топлива');
        $fourSheet->setCellValue('A28', 'Годовой расход топлива');

        $fourSheet->getStyle('A20:B22')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('A22:B23')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('A24:B25')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('A26:B27')->applyFromArray($bordersStyle);
        $fourSheet->getStyle('A28:B29')->applyFromArray($bordersStyle);


        $ovPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_OV])->all(), 'id');
        $ovObjects = ObjectsOv::find()->where(['objects_id' => $ovPks])->all();

        $gvsPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_GVS])->all(), 'id');
        $gvsObjects = ObjectsGvs::find()->where(['objects_id' => $gvsPks])->all();

        $tehPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_TECH])->all(), 'id');
        $tehObjects = ObjectsGvs::find()->where(['objects_id' => $tehPks])->all();

        $ovQomax = 0;
        $ovQomax1 = 0;
        $ovQo = 0;
        $ovGncho = 0;
        $ovGno = 0;
        $ovGuto = 0;
//        $ov 

        $ovQvmax = 0;
        $ovQvmax1 = 0;
        $ovQv = 0;
        $ovGnchv = 0;
        $ovGnv = 0;
        $ovGutv = 0;

        $gvsQhm = 0;
        $gvsQhm1 = 0;
        $gvsQh = 0;
        $gvsGnch = 0;
        $gvsGn = 0;
        $gvsGut = 0;

        $tehQh = 0;
        $tehQh1 = 0;
        $tehQn = 0;
        $tehQhd = 0;
        $tehGn = 0;
        $tehGut = 0;


        $k5 = $model->duration_op11;

        foreach ($ovObjects as $obj) {
            $ovQomax += $obj->formula->getOvQomax();
            $ovQomax1 += $obj->formula->getOvQomax1();
            $ovQo += $obj->formula->getQo();
            if ($obj->heating_load != 0) {
                $ovGncho += $obj->formula->getOvGncho();
                $ovGno += $obj->formula->getOvGno();
                $ovGuto += $obj->formula->getOvGuto();
            }

            $ovQvmax += $obj->formula->getOvQvmax();
            $ovQvmax1 += $obj->formula->getOvQvmax1();
            $ovQv += $obj->formula->getQv();

            if ($obj->ventilation_load != 0) {
                $ovGnchv += $obj->formula->getOvGnchv();
                $ovGnv += $obj->formula->getOvGnv();
                $ovGutv += $obj->formula->getOvGutv();
            }
        }

        foreach ($gvsObjects as $obj) {
            $gvsQhm += $obj->formula->getGvsQhm();
            $gvsQhm1 += $obj->formula->getGvsQhm1();

            $gvsQhc = $gvsQhm * 1 * (55 - 15) / (55 - 5);
            $gvsN0 = $k5 / 7 * $obj->count_work_day * $obj->work_per_day;
            $gvsNs = (365 - $k5) / 7 * $obj->count_work_day * $obj->work_per_day;

            $gvsQh += $obj->formula->getGvsQh();

            if ($obj->ventilation_load != 0) {
                $gvsGnch += ($gvsQhm / $model->calorific_value * pow(10, 6)) / ($gvsQhm / $model->calorific_value * pow(10, 6));
                $gvsGn += ($gvsQhm / $model->calorific_value * pow(10, 6)) / ($gvsQh / $model->calorific_value * pow(10, 3));
                $gvsGut += ($gvsQhm / 7000 * pow(10, 3)) / ($gvsQh / 7000 * pow(10, 3));
            }
        }

        foreach ($tehObjects as $obj) {

            $tehQhd += $obj->formula->getTehQhd(); // TODO: O3 не найден (пока 1), КПД не найден (пока 100), ЗАК5 не найден (пока 1)

            $tehQh += $obj->formula->getTehQh(); //TODO: неправльный расчет 011 (пока 1)
            $tehQh1 += $obj->formula->getTehQh1();

            $tehQn += $obj->amount * $obj->consumption_thermal_units * pow(10, -6);

            $tehGn += ($tehQn / $model->calorific_value * pow(10, 3)) / ($obj->amount / 1000 / 8000 * pow(10, 3)) / 100; // TODO: КПД не найден (пока 100)

            $tehGut += ($tehQn / 7000 * pow(10, 3)) / ($obj->amount / 1000 / 7000 * pow(10, 3)) / 100; // TODO: КПД не найден (пока 100)
        }




        // NEW TABLE CODE START
        $sumOvQomax = 0;
        $sumOvQomax1 = 0;
        $sumOvQvmax = 0;
        $sumOvQvmax1 = 0;
        $sumOvQo = 0;
        $sumOvQv = 0;
        $sumOvGncho = 0;
        $sumOvGnchv = 0;
        $sumOvGno = 0;
        $sumOvGnv = 0;
        $sumOvGuto = 0;
        $sumOvGutv = 0;

        $sumGvs5 = 0;
        $sumGvs6 = 0;
        $sumGvsQhm = 0;
        $sumGvsQhm1 = 0;
        $sumGvsQh = 0;
        $sumGvsGnch = 0;
        $sumGvsGn = 0;
        $sumGvsGut = 0;

        $sumTehQh = 0;
        $sumTehQh1 = 0;
        $sumTehQn = 0;
        $sumTehQhd = 0;
        $sumTehGn = 0;
        $sumTehGut = 0;


        $objects = Objects::find()->where(['calculation_id' => $model->id])->all();

        foreach ($objects as $object)
        {
            if($object->type_object == Objects::TYPE_OV){
                $object = ObjectsOv::findOne(['objects_id' => $object->id]);

                if($object){
                    $sumOvQomax += $object->formula->getOvQomax();
                    $sumOvQomax1 += $object->formula->getOvQomax1();
                    $sumOvQvmax += $object->formula->getOvQvmax();
                    $sumOvQvmax1 += $object->formula->getOvQvmax1();
                    $sumOvQo += $object->formula->getQo();
                    $sumOvQv += $object->formula->getQv();
                    $sumOvGncho += $object->formula->getOvGncho();
                    $sumOvGnchv += $object->formula->getOvGnchv();
                    $sumOvGno += $object->formula->getOvGno();
                    $sumOvGnv += $object->formula->getOvGnv();
                    $sumOvGuto += $object->formula->getOvGuto();
                    $sumOvGutv += $object->formula->getOvGutv();
                }

            } else if($object->type_object == Objects::TYPE_GVS){
                $object = ObjectsGvs::findOne(['objects_id' => $object->id]);

                if($object){
                	$sumGvs5 += $object->formula->getGvs5();
                	$sumGvs6 += $object->formula->getGvs6();
                    $sumGvsQhm += $object->formula->getGvsQhm();
                    $sumGvsQhm1 += $object->formula->getGvsQhm1();
                    $sumGvsQh += $object->formula->getGvsQh();
                    $sumGvsGnch += $object->formula->getGvsGnch();
                    $sumGvsGn += $object->formula->getGvsGn();
                    $sumGvsGut += $object->formula->getGvsGut();
                }

            } else if($object->type_object == Objects::TYPE_TECH){
                $object = ObjectsTech::findOne(['objects_id' => $object->id]);

                if($object){
                    $sumTehQh += $object->formula->getTehQh();
                    $sumTehQh1 += $object->formula->getTehQh1();
                    $sumTehQn += $object->formula->getTehQn();
                    $sumTehQhd += $object->formula->getTehQhd();
                    $sumTehGn += $object->formula->getTehGn();
                    $sumTehGut += $object->formula->getTehGut();
                }

            }
        }


        $allQhmax = $sumOvQomax + $sumOvQvmax + $sumGvsQhm + $sumTehQh;
        $allQhmax1 = $sumOvQomax1 + $sumOvQvmax1 + $sumGvsQhm1 + $sumTehQh1;
        $allQh = $sumOvQo + $sumOvQv + $sumGvsQh + $sumTehQn;
        $allQh1 = ($sumOvQo * 4.1868) + ($sumOvQv * 4.1868) + ($sumGvsQh * 4.1868) + ($sumTehQn * 4.1868);
        $allGnch = $sumOvGncho + $sumOvGnchv + $sumGvsGnch + $sumTehQhd;
        $allGn = $sumOvGno + $sumOvGnv + $sumGvsGn + $sumTehGn;
        $allGut = $sumOvGuto + $sumOvGutv + $sumGvsGut + $sumTehGut;

        // NEW TABLE CODE END

        $fourSheet->mergeCells("C20:D20");
        $fourSheet->getStyle("C20:D20")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("C20", $sumOvQomax);

        $fourSheet->mergeCells("C21:D21");
        $fourSheet->getStyle("C21:D21")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("C21", $sumOvQomax1);

        $fourSheet->mergeCells("C22:D22");
        $fourSheet->getStyle("C22:D22")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("C22", $sumOvQo);

        $fourSheet->mergeCells("C23:D23");
        $fourSheet->getStyle("C23:D23")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("C23", $sumOvQo * 4.1868);


        $fourSheet->mergeCells("C24:D25");
        $fourSheet->mergeCells("C26:D27");
        $fourSheet->mergeCells("C28:D29");
        $fourSheet->getStyle("C24:D25")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("C26:D27")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("C28:D29")->applyFromArray($bordersStyle);

        $fourSheet->setCellValue('C24', $sumOvGncho);
        $fourSheet->setCellValue('C26', $sumOvGno);
        $fourSheet->setCellValue('C28', $sumOvGuto);


//        for ($i = 17; $i <= 20; $i++){
//            $fourSheet->mergeCells("E{$i}:F{$i}");
//            $fourSheet->getStyle("E{$i}:F{$i}")->applyFromArray($bordersStyle);
//            $fourSheet->setCellValue("E{$i}", "∑OV_Qomax");
//        }

        $fourSheet->mergeCells("E20:F20");
        $fourSheet->getStyle("E20:F20")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("E20", $sumOvQvmax);

        $fourSheet->mergeCells("E21:F21");
        $fourSheet->getStyle("E21:F21")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("E21", $sumOvQvmax1);

        $fourSheet->mergeCells("E22:F22");
        $fourSheet->getStyle("E22:F22")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("E22", $sumOvQv);

        $fourSheet->mergeCells("E23:F23");
        $fourSheet->getStyle("E23:F23")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("E23", $sumOvQv * 4.1868);


        $fourSheet->mergeCells("E24:F25");
        $fourSheet->mergeCells("E26:F27");
        $fourSheet->mergeCells("E28:F29");
        $fourSheet->getStyle("E24:F25")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("E26:F27")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("E28:F29")->applyFromArray($bordersStyle);

        $fourSheet->setCellValue('E24', $sumOvGnchv);
        $fourSheet->setCellValue('E26', $sumOvGnv);
        $fourSheet->setCellValue('E28', $sumOvGutv);


//        for ($i = 17; $i <= 20; $i++){
//            $fourSheet->mergeCells("G{$i}:H{$i}");
//            $fourSheet->getStyle("G{$i}:H{$i}")->applyFromArray($bordersStyle);
//            $fourSheet->setCellValue("G{$i}", "∑OV_Qomax");
//        }

        $fourSheet->mergeCells("G20:H20");
        $fourSheet->getStyle("G20:H20")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("G20", $sumGvsQhm);

        $fourSheet->mergeCells("G21:H21");
        $fourSheet->getStyle("G21:H21")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("G21", $sumGvsQhm1);

        $fourSheet->mergeCells("G22:H22");
        $fourSheet->getStyle("G22:H22")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("G22", $sumGvsQh);

        $fourSheet->mergeCells("G23:H23");
        $fourSheet->getStyle("G23:H23")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("G23", $sumGvsQh * 4.1868);


        $fourSheet->mergeCells("G24:H25");
        $fourSheet->mergeCells("G26:H27");
        $fourSheet->mergeCells("G28:H29");
        $fourSheet->getStyle("G24:H25")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("G26:H27")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("G28:H29")->applyFromArray($bordersStyle);

        $fourSheet->setCellValue('G24', $sumGvsGnch);
        $fourSheet->setCellValue('G26', $sumGvsGn);
        $fourSheet->setCellValue('G28', $sumGvsGut);


//        for ($i = 17; $i <= 20; $i++){
//            $fourSheet->mergeCells("I{$i}:J{$i}");
//            $fourSheet->getStyle("I{$i}:J{$i}")->applyFromArray($bordersStyle);
//            $fourSheet->setCellValue("I{$i}", "∑OV_Qomax");
//        }

//        $tehQh = 0;
//        $tehQh1 = 0;
//        $tehQn = 0;
//        $tehQhd = 0;
//        $tehGn = 0;
//        $tehGut = 0;

        $fourSheet->mergeCells("I20:J20");
        $fourSheet->getStyle("I20:J20")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("I20", $sumTehQh);

        $fourSheet->mergeCells("I21:J21");
        $fourSheet->getStyle("I21:J21")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("I21", $sumTehQh1);

        $fourSheet->mergeCells("I22:J22");
        $fourSheet->getStyle("I22:J22")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("I22", $sumTehQn);

        $fourSheet->mergeCells("I23:J23");
        $fourSheet->getStyle("I23:J23")->applyFromArray($bordersStyle);
        $fourSheet->setCellValue("I23", $sumTehQn * 4.1868);

        $fourSheet->mergeCells("I24:J25");
        $fourSheet->mergeCells("I26:J27");
        $fourSheet->mergeCells("I28:J29");
        $fourSheet->getStyle("I24:J25")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("I26:J27")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("I28:J29")->applyFromArray($bordersStyle);

        $fourSheet->setCellValue('I24', $sumTehQhd);
        $fourSheet->setCellValue('I26', $sumTehGn);
        $fourSheet->setCellValue('I28', $sumTehGut);


        // for ($i = 17; $i <= 20; $i++) {
            // $fourSheet->mergeCells("K{$i}:L{$i}");
            // $fourSheet->getStyle("K{$i}:L{$i}")->applyFromArray($bordersStyle);
            // $fourSheet->setCellValue("K{$i}", "∑OV_Qomax");
        // }

        $fourSheet->setCellValue("K20", $allQhmax);
        $fourSheet->setCellValue("K21", $allQhmax1);
        $fourSheet->setCellValue("K22", $allQh);
        $fourSheet->setCellValue("K23", $allQh1);

        $fourSheet->mergeCells("K20:L20");
        $fourSheet->mergeCells("K21:L21");
        $fourSheet->mergeCells("K22:L22");
        $fourSheet->mergeCells("K23:L23");
        $fourSheet->getStyle("K20:L20")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("K21:L21")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("K22:L22")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("K23:L23")->applyFromArray($bordersStyle);

        $fourSheet->mergeCells("K24:L25");
        $fourSheet->mergeCells("K26:L27");
        $fourSheet->mergeCells("K28:L29");
        $fourSheet->getStyle("K24:L25")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("K26:L27")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("K28:L29")->applyFromArray($bordersStyle);

        $fourSheet->setCellValue('K24', $allGnch);
        $fourSheet->setCellValue('K26', $allGn);
        $fourSheet->setCellValue('K28', $allGut);


        // for ($i = 17; $i <= 20; $i++) {
            // $fourSheet->mergeCells("M{$i}:N{$i}");
            // $fourSheet->getStyle("M{$i}:N{$i}")->applyFromArray($bordersStyle);
        // }

        $fourSheet->setCellValue('M20', 'Гкал/час');
        $fourSheet->setCellValue('M21', 'кВт');
        $fourSheet->setCellValue('M22', 'Гкал/год');
        $fourSheet->setCellValue('M23', 'ГДж/год');

        $fourSheet->mergeCells("M24:N25");
        $fourSheet->mergeCells("M26:N27");
        $fourSheet->mergeCells("M28:N29");
        $fourSheet->getStyle("M24:N25")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("M26:N27")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("M28:N29")->applyFromArray($bordersStyle);

        $fourSheet->setCellValue('M24', 'нм3/ч');
        $fourSheet->setCellValue('M26', 'тыс.нм3/г');
        $fourSheet->setCellValue('M28', 'тут.год');

        $fourSheet->mergeCells("M20:N20");
        $fourSheet->mergeCells("M21:N21");
        $fourSheet->mergeCells("M22:N22");
        $fourSheet->mergeCells("M23:N23");
        $fourSheet->getStyle("M20:N20")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("M21:N21")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("M22:N22")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("M23:N23")->applyFromArray($bordersStyle);

        $fourSheet->mergeCells('A31:N31');
        $fourSheet->setCellValue('A31', 'Для объекта выполнены отдельные расчеты:');

        $totalFourData = 0;
        $fourData = [];
        foreach ($objects as $object) {
            $typeName = Objects::getTypesObjects()[$object->type_object];

            $gut = 0;

            if($object->type_object == Objects::TYPE_OV){
            	$obj = ObjectsOv::find()->where(['objects_id' => $object->id])->one();

            	$gut = $obj->formula->getOvGut();
            } else if($object->type_object == Objects::TYPE_GVS){
            	$obj = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

            	$gut = $obj->formula->getGvsGut();
            } else if($object->type_object == Objects::TYPE_TECH){
            	$obj = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

            	$gut = $obj->formula->getTehGut();
            }

            $gut = Yii::$app->formatter->asDecimal($gut);

            $fourData[] = [$object->name, $typeName, $gut, 'X'];
        }



        $fourSheet->mergeCells('A33:E33');
        $fourSheet->setCellValue('A33', 'Название');
        $fourSheet->getStyle("A33:E33")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("A33:E33")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->mergeCells('F33:I33');
        $fourSheet->setCellValue('F33', 'Тип расчета');
        $fourSheet->getStyle("F33:I33")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("F33:I33")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->mergeCells('J33:L33');
        $fourSheet->setCellValue('J33', 'Расх.у.т. [тут/год]');
        $fourSheet->getStyle("J33:L33")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("J33:L33")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fourSheet->mergeCells('M33:N33');
        $fourSheet->setCellValue('M33', 'Лист');
        $fourSheet->getStyle("M33:N33")->applyFromArray($bordersStyle);
        $fourSheet->getStyle("M33:N33")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);


        $offset = 34;
        for ($i = 0; $i < count($fourData); $i++) {
            $v = $offset + $i;
            $data = $fourData[$i];

            $fourSheet->getStyle("A{$v}:E{$v}")->applyFromArray($bordersStyle);
            $fourSheet->mergeCells("A{$v}:E{$v}");
            $fourSheet->getStyle("F{$v}:I{$v}")->applyFromArray($bordersStyle);
            $fourSheet->mergeCells("F{$v}:I{$v}");
            $fourSheet->getStyle("J{$v}:L{$v}")->applyFromArray($bordersStyle);
            $fourSheet->mergeCells("J{$v}:L{$v}");
            $fourSheet->getStyle("M{$v}:N{$v}")->applyFromArray($bordersStyle);
            $fourSheet->mergeCells("M{$v}:N{$v}");


            $fourSheet->setCellValue("A{$v}", $data[0]);
            $fourSheet->setCellValue("F{$v}", $data[1]);
            $fourSheet->setCellValue("J{$v}", $data[2]);
            $fourSheet->setCellValue("M{$v}", $data[3]);
        }

        $fourSheet->getRowDimension('16')->setRowHeight('70');
        $fourSheet->getStyle('A16')->getAlignment()->applyFromArray([
        	'horizontal' => Alignment::HORIZONTAL_LEFT,
        	'vertical' => Alignment::VERTICAL_TOP,
        ]);
        $fourSheet->getStyle('A16')->getAlignment()->setWrapText(true);

        // $fourSheet->mergeCells('A39:N45');
        // $fourSheet->getStyle('A39:N45')->getAlignment()->setWrapText(true);

        $equipmentsText = Equipment::find()->where(['calculation_id' => $model->id])->all();

        $offsetEqs = count($fourData) - 4;

        $fourSheet->setCellValue("A".(39 + $offsetEqs), 'Для покрытия тепловой нагрузки предусматривается следующее оборудование:');
        // $fourSheet->mergeCells('A36:N36');

        for($i = 0; $i < count($equipmentsText); $i++){
        	$ipos = 39 + ($i + 1) + $offsetEqs;

        	$eq = $equipmentsText[$i];

        	$o2 = $eq->formula->getO2() * 100;
        	$eqText = "
{$eq->formula->getO1()}, мощность = {$eq->formula->getO6()}[кВт], КПД = {$o2}[%], количество = {$eq->formula->getO4()}[шт].";

        	$fourSheet->mergeCells("A{$ipos}:N{$ipos}");
        	$fourSheet->setCellValue("A{$ipos}", $eqText);
        }

        // foreach ($equipmentsText as $eq) {
        	// $o2 = $eq->formula->getO2() * 100;
        	// $eqText .= "
// {$eq->formula->getO1()}, мощность = {$eq->formula->getO6()}[кВт], КПД = {$o2}[%], количество = {$eq->formula->getO4()}[шт].";
        // }


        $fiveSheet = $spreadsheet->createSheet();
        $fiveSheet->setTitle('Расчет потребности');

        $fiveSheet->getPageSetup()->setOrientation(PageSetup::PAPERSIZE_A4);


        $ovObjects = ObjectsOv::find()->where(['objects_id' => $ovPks])
            ->andWhere(['or', ['>', 'specific_thermal_characteristic_ventilation', 0], ['>', 'ventilation_load', 0]])
            ->all();


        $counter = 0;

//         foreach ($ovObjects as $object) {
//             /** @var $object ObjectsOv */

//             $str = '';

//             $ovQomax = round($object->formula->getOvQomax(), 4);
//             $ovQomax1 = round($object->formula->getOvQomax1(), 4);
//             $ovQo = $object->formula->getQo();
//             if ($object->heating_load != 0) {
//                 $ovGncho = $object->formula->getOvGncho();
//                 $ovGno = $object->formula->getOvGno();
//                 $ovGuto = $object->formula->getOvGuto();
//             }
// //
//             $ovQvmax = $object->formula->getOvQvmax();
//             $ovQvmax1 = $object->formula->getOvQvmax1();
//             $ovQv = round($object->formula->getQv(), 2);
// //
//             if ($object->ventilation_load != 0) {
//                 $ovGnchv = $object->formula->getOvGnchv();
//                 $ovGnv += $object->formula->getOvGnv();
//                 $ovGutv += $object->formula->getOvGutv();
//             }

//             $str .= "Расчет потребности в тепловой энергии
// на нужды отопления и вентиляции, для объекта:
// {$object->objects->name}";

//             if ($object->heat_loads_known == 1) {

//             	$heatingLoadKvt = $object->heating_load * 1163;

//             	if($object->heating_load > 0){
//                 $str .= "
// Тепловые нагрузки отопления здания приняты по проектным данным.
// Тепловая нагрузка отопления по данным проекта:
// Qo.max = {$object->heating_load} [Гкал/ч] ({$heatingLoadKvt} [кВт])
//                 ";
//             	}

//                 if ($object->objects->count_object > 1) {
//                 	if($ovQomax > 0){
//                     $str .= "
// Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт. 
// Тепловая нагрузка отопления группы составит: 
                    
// Qo.max = {$object->heating_load} х {$object->objects->count_object} = {$ovQomax} [Гкал/ч] ({$ovQomax1} [кВт])
                    
//                     ";
//                 	}
//                 }

//                 $ventilationLoadKvt = $object->ventilation_load * 1163;

//                 if($object->ventilation_load > 0) {
//                 	if($object->ventilation_load > 0){
//                 $str .= "
// Тепловая нагрузка вентиляции по данным проекта:
// Qv.max = {$object->ventilation_load} [Гкал/ч] ({$ventilationLoadKvt} [кВт])
//                 ";
//                 	}
//                 }

//                 if($object->objects->count_object > 1){
//                 	if($ovQvmax > 0){
//                     $str .= "
// Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт.
// Тепловая нагрузка вентиляции группы составит: 
// Qv.max = {$object->ventilation_load} х {$object->objects->count_object} = {$ovQvmax} ([Гкал/ч] ({$ovQvmax1} [кВт])
//                     ";
//                 	}
//                 }

//             } else {
            	
//                 $buildType = ArrayHelper::getValue(ObjectsOvType::getTypes(), $object->building_type);

// //                $ovKip = 0.01 * sqrt((2*9.8*$object->objects->power_v*(1-(273 + $model->temperature_calculated)/(273+$object->internal_temperature)) + pow($model->winder_speed, 2)));
//                 $winnerSpeedPow2X = pow($model->winder_speed, 2);

//                 $ovKip = round($object->formula->getOvKip(), 3);

//                 $temperatureCalculated = MathNumber::handleOne($model->temperature_calculated);

//                 $str .= "
// Проектные тепловые нагрузки отопления и вентиляции отсутствуют, поэтому расчетная часовая тепловая нагрузка отопления отдельного здания и годовая потребность в тепловой энергии определена по укрупненным показателям, в соответствии с МДК 4-05.2004.

// Тип здания: Административные здания, конторы {$buildType}.
// Строительный объем здания: V={$object->building_volume}[м3].
// Свободная высота здания: L={$object->building_height}[м].
// Внутренняя температура: tj={$object->internal_temperature}°С.
// Удельная отопительная характеристика здания: qо={$object->specific_thermal_characteristic_heating}[ккал/м3ч °С].
// Удельная вентиляционная характеристика здания: qv={$object->specific_thermal_characteristic_ventilation}[ккал/м3ч °С].

// Расчетный коэффициент инфильтрации Kи.р определяется по формуле 3.3 прил.3:
// Kи.p. = 0.01*√[2*gL(1-(273+t0)/(273+t1))+w*w]
// Kи.р.= 0,01x√[2x9,8x{$object->formula->getOv7()}x(1-(273+{$temperatureCalculated}/(273+{$object->internal_temperature}))+{$winnerSpeedPow2X}] = {$ovKip} 
//                 ";




//                 if ($object->objects->count_object > 1) {
// //                     $str .= "
                        
// // Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт. 
// // Тепловая нагрузка отопления группы составит: 

// // Qo.max = {$ovQomax}

                    
// //                     ";
//                 }

//                 $k6 = $object->objects->calculation->formula->getK6();
//                 $k3 = MathNumber::handleOne($object->objects->calculation->formula->getK3());
//                 $k5 = $object->objects->calculation->formula->getK5();
//                 $k2 = MathNumber::handleOne($object->objects->calculation->formula->getK2());

//                 // $ovQvmax = round($object->formula->getOvQvmax(), 4);
//                 $ovQvmax = round($object->formula->getOvQvmax(), 4);  

//                 $ovKip = round($object->formula->getOvKip(), 3);

//                 $ovQomaxOnce = round($object->formula->getOvQomax(false), 4);
//                 $ovQvmaxOnce = round($object->formula->getOvQvmax(false), 4);

//                 $ovQomax1Once = round($object->formula->getOvQomax1(false), 4);
//                 $ovQvmax1Once = round($object->formula->getOvQvmax1(false), 4);

//                 $str .= "
// Тепловая нагрузка отопления определяется по формуле 3.2 прил.3:
// Qo.max = {$k6} x {$object->formula->getOv4()} x {$object->formula->getOv5()} x ({$object->formula->getOv8()} – {$k3}) x (1 + {$ovKip}) x 0.000001 = {$ovQomaxOnce} [Гкал/ч] ({$ovQomax1Once} [кВт])

// Тепловая нагрузка вентиляции определяется по формуле 3.2a прил.3:
// Qv.max = {$k6} x {$object->formula->getOv4()} x {$object->formula->getOv6()} x ({$object->formula->getOv8()} – {$k3}) x 0.000001 = {$ovQvmaxOnce} [Гкал/ч] ({$ovQvmax1Once} [кВт])

//                 ";

//                 if ($object->objects->count_object > 1) {
//                     $str .= "
// Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт.";

// 					if($ovQomax > 0){
// 					$str .= "
// Тепловая нагрузка отопления группы составит:
// Qo.max = {$ovQomaxOnce} x {$object->objects->count_object} = {$ovQomax} [Гкал/ч] ({$ovQomax1} [кВт])";
// 					}

// 					if($ovQvmax > 0){
// 					$str .= "

// Тепловая нагрузка вентиляции группы составит:
// Qv.max = {$ovQvmaxOnce} x {$object->objects->count_object} = {$ovQvmax} [Гкал/ч] ({$ovQvmax1} [кВт])";
// 					}
//                 }

//             }

//             $ovQo = round($object->formula->getQo(), 3);

//             $equipment = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $object->objects->number_group])->one();
//             if($equipment){
//                 $o2 = $equipment->formula->getO2();
//             } else {
//                 $o2 = 1;
//             }

//             $o2 = $o2 * 100;

//             $k3 = MathNumber::handleOne($object->objects->calculation->formula->getK3());
//             $k2 = MathNumber::handleOne($object->objects->calculation->formula->getK2());


//             if($ovQo > 0){
//             $str .= "          
// Количество тепловой энергии, Гкал, необходимой для отопления зданий на отопительный период в целом, определяется по формуле (16):
                
// Qo = Qo.max x 24 x (tj – tот) x n)/(tj – t0)
// Qo = ({$ovQomax} x 24 x ({$object->formula->getOv8()} – {$k2}) x {$k5})/({$object->formula->getOv8()} – {$k3}) = {$ovQo} [Гкал/год]";
//             }

//             if($ovQv > 0){
// 			$str .= "

// Количество тепловой энергии, Гкал, необходимой для вентиляции зданий на отопительный период в целом, определяется по формуле (18):
                
// Qv = (Qv.max x 24 x (tj – tот) x n)/(tj – t0)
// Qv = ({$ovQvmax} x 24 x ({$object->formula->getOv8()} – {$k2}) x {$k5})/({$object->formula->getOv8()} – {$k3}) = {$ovQv} [Гкал/год]";
//             }
            
//             if($equipment != null){
//             $str .= "
// Для покрытия тепловой нагрузки данного объекта выбрано следующее оборудование:
// {$equipment->formula->getO1()} с КПД = {$o2}[%]. 
            
// Примечание: характеристики данного оборудования с учетом всех подключенных систем приведены в таблице Котельное оборудование Приложения 1.
//             ";
//             }

//             if ($object->objects->count_object > 1) {
//                 $str .= "
// Расчет годовой потребности в тепле, уловном и натуральном топливе, приведенный ниже,
// будет производиться для группы зданий в целом, на основании общих суммарных тепловых нагрузок.
//                 ";
//             }

//             $ovGnch = $ovGncho + $ovGnchv;

//             $ovQo = $object->formula->getQo();
//             $ovQv = ($object->ventilation_load * 24 * ($object->internal_temperature - $model->temperature_average) * $model->duration_op11) / ($object->internal_temperature - $model->temperature_calculated);

//             $ovGn = $ovGno + $ovGnv;


//             $ovGut = $ovGuto + $ovGutv;

//             $equipment = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $object->objects->number_group])->one();
//             if($equipment){
//                 $o2 = $equipment->formula->getO2();
//             } else {
//                 $o2 = 1;
//             }

//             $o2 = $o2 * 100;

//             $ovGncho = round($object->formula->getOvGncho(), 3);
//             $ovGnchv = round($object->formula->getOvGnchv(), 2);
//             $ovGnch = round($object->formula->getOvGnch(), 2);

//             $ovQvmax = round($ovQvmax, 4);

//             $ovGno = round($object->formula->getOvGno(), 2);
//             $ovGnv = round($object->formula->getOvGnv(), 2);
//             $ovGn = round($object->formula->getOvGn(), 2);

//             $ovGuto = round($object->formula->getOvGuto(), 2);
//             $ovGutv = round($object->formula->getOvGutv(), 2);
//             $ovGut = round($object->formula->getOvGut(), 2);

//             $ovQo = round($object->formula->getQo(), 3);
//             $ovQv = round($object->formula->getQv(), 1);

//             $str .= "
            
// Определение часового расхода топлива:";

// 			if($ovGncho > 0){
// 			$str .= "
// Отопление: 
// Gнчо =  (Qo.max / Qн х 1000000) / КПД = {$ovQomax} / {$model->formula->getZak6()} x 1000000 / {$o2} = {$ovGncho} [нм3/ч]";
// 			}

// 			if($ovGnchv > 0){
// 			$str .= "
// Вентиляция:  
// Gнчв = (Qv.max / Qн х 1000000) / КПД = {$ovQvmax} / {$model->formula->getZak6()} x 1000000 / {$o2} [%] = {$ovGnchv} [нм3/ч]";
// 			}

// 			if($ovGncho > 0 && $ovGnchv > 0){
// 				$str .= "
// Итого: 
// Gнч = (Gнчо + Gнчв) = {$ovGncho} + {$ovGnchv} = {$ovGnch} [нм3/ч]";   
// 			} else {
// // 				$str .= "
// // Итого: 
// // Gнч = {$ovGnch} [нм3/ч]";   
// 			}     

// 			$str .= "
// Определение расчетного годового расхода топлива:";

// 			if($ovGno > 0){
// 			$str .= "
// Отопление:
// Gно = ({$ovQo} / {$model->formula->getZak6()} х 1000) / КПД = ({$ovQo} / {$model->formula->getZak6()} x 1000) / {$o2} = {$ovGno}[тыс.нм3/год]";
// 			}

// 			if($ovGnv > 0){
// 			$str .= "
// Вентиляция: 
// Gнв = ({$ovQv} / {$model->formula->getZak6()} х 1000) / КПД = ({$ovQv} / {$model->formula->getZak6()} x 1000) / {$o2} = {$ovGnv} [тыс.нм3/год]";
// 			}

// 			if($ovGno > 0 && $ovGnv > 0){
// 			$str .= "    
// Итого: 
// Gн = Gно + Gнв = {$ovGno} + {$ovGnv} = {$ovGn} [тыс.нм3/год]";       
// 			} else {
// // 			$str .= "    
// // Итого: 
// // Gн = {$ovGn} [тыс.нм3/год]";       
// 			}            

// 			$str .= "
// Определение расчетного годового расхода условного топлива,
// при его нормативной теплотворной способности Qут=7000 [ккал/кг]:";

// 			if($ovGuto > 0){
// 			$str .= "
// Отопление: 
// Gуто = ({$ovQo} / 7000 х 1000) / КПД = ({$ovQo} / 7000 x 1000) / {$o2} [%] = {$ovGuto} [тут/год]";
// 			}

// 			if($ovGutv > 0){
// 			$str .= "            
// Вентиляция: 
// Gутв = ({$ovQv} / 7000 х 1000) / КПД = ({$ovQv} / 7000 x 1000) / {$o2} [%] = {$ovGutv} [тут/год]";
// 			}

// 			if($ovGuto > 0 && $ovGutv > 0){
// 			$str .= "               
// Итого: 
// Gут = Gуто + Gутв = {$ovGuto} + {$ovGutv} = {$ovGut} [тут/год]";
// 			} else {
// // 			$str .= "               
// // Итого: 
// // Gут = {$ovGut} [тут/год]";
// 			}


//             $Apos = 1 + (71 * $counter);
//             $Bpos = 70 + (71 * $counter);

//             $fiveSheet->mergeCells("A{$Apos}:Z{$Bpos}");
//             $fiveSheet->getStyle("A{$Apos}:M{$Bpos}")->getAlignment()->setWrapText(true);
//             $fiveSheet->setCellValue("A${Apos}", $str);

//             $counter++;
//         }

        $ovObjects = ObjectsOv::find()->where(['objects_id' => $ovPks])
            ->andWhere(['or', ['specific_thermal_characteristic_ventilation' => 0], ['ventilation_load' => 0]])
            ->all();


        foreach ($ovObjects as $object) {
            /** @var $object ObjectsOv */

            $str = '';

            $ovQomax += $object->heating_load;
            $ovQomax1 += $object->heating_load * 1163;
//            $ovQo += ($obj->heating_load * 24 * ($obj->internal_temperature - $model->temperature_average) * $model->duration_op11)/($obj->internal_temperature-$model->temperature_calculated);
            if ($object->heating_load != 0) {
                $ovGncho = ($object->heating_load / $model->calorific_value * pow(10, 6)) / $object->heating_load / $model->calorific_value * pow(10, 6) / $object->objects->count_object;
                $ovGno = ($ovQo / $model->calorific_value * pow(10, 3)) / $object->objects->count_object;
                $ovGuto = ($ovQo / $model->calorific_value * pow(10, 3)) / ($ovQo / 7000 * pow(10, 3)) / $object->objects->count_object;
            }
//
            $ovQvmax = $object->ventilation_load;
            $ovQvmax1 = $object->ventilation_load;
            $ovQv = ($object->ventilation_load * 24 * ($object->internal_temperature - $model->temperature_average) * $model->duration_op11) / ($object->internal_temperature - $model->temperature_calculated);
//
            if ($object->ventilation_load != 0) {
                $ovGnchv += ($object->ventilation_load / $model->calorific_value * pow(10, 6)) / $object->heating_load / $model->calorific_value * pow(10, 6) / $object->objects->count_object;
                $ovGnv += ($ovQv / $model->calorific_value * pow(10, 3)) / ($ovQv / $model->calorific_value * pow(10, 3)) / $object->objects->count_object;
                $ovGutv += ($ovQv / $model->calorific_value * pow(10, 3)) / ($ovQv / 7000 * pow(10, 3)) / $object->objects->count_object;
            }

            $str .= "Расчет потребности в тепловой энергии
на нужды отопления и вентиляции, для объекта:
{$object->objects->name}";


            if ($object->heat_loads_known == 1) {

            	$heatingLoadKvt = $object->heating_load * 1163;
            	$ventilationLoadKvt = $object->ventilation_load * 1163;

            	if($object->heating_load > 0){
                $str .= "
Тепловые нагрузки отопления здания приняты по проектным данным.
Тепловая нагрузка отопления по данным проекта:
Qo.max = {$object->heating_load} [Гкал/ч] ({$heatingLoadKvt} [кВт])
                ";
            	}

                if ($object->objects->count_object > 1) {
                    $str .= "
Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт.";
                	if($ovQomax > 0){
					$str .= "
Тепловая нагрузка отопления группы составит: 
                    
Qo.max = {$object->heating_load} х {$object->objects->count_object} = {$ovQomax} [Гкал/ч] ({$ovQomax1} [кВт])
                    
                    ";
                	}
                }

                if($object->formula->getOv6() > 0 || $object->formula->getOv11() > 0){
                	if($object->ventilation_load > 0){
$str .= "
Тепловая нагрузка вентиляции по данным проекта:
Qv.max = {$object->ventilation_load} [Гкал/ч] ({$ventilationLoadKvt} [кВт])
";
                	}

                }

                if ($object->objects->count_object > 1) {
                	if($ovQvmax > 0){
                	$ovQvmax = $object->formula->getOvQvmax();
                	$ovQvmax1 = $object->formula->getOvQvmax1();
                    $str .= "
Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт.
Тепловая нагрузка вентиляции группы составит: 
Qv.max = {$object->ventilation_load} х {$object->objects->count_object} = {$ovQvmax} [Гкал/ч] ($ovQvmax1 [кВт])
                    ";
                	}
                }

            } else {

//                $ovKip = 0.01 * sqrt((2*9.8*$object->objects->power_v*(1-(273 + $model->temperature_calculated)/(273+$object->internal_temperature)) + pow($model->winder_speed, 2)));
                $winnerSpeedPow2X = pow($model->winder_speed, 2);

                $ovKip = round($object->formula->getOvKip(), 2);

                $temperatureCalculated = MathNumber::handleOne($model->temperature_calculated);


                $buildType = ArrayHelper::getValue(ObjectsOvType::getTypes(), $object->building_type);
                $str .= "
Проектные тепловые нагрузки отопления и вентиляции отсутствуют, поэтому расчетная часовая тепловая нагрузка отопления отдельного здания и годовая потребность в тепловой энергии определена по укрупненным показателям, в соответствии с МДК 4-05.2004.

Тип здания: Административные здания, конторы {$buildType}.
Строительный объем здания: V={$object->building_volume}[м3].
Свободная высота здания: L={$object->building_height}[м].
Внутренняя температура: tj={$object->internal_temperature}°С.
Удельная отопительная характеристика здания: qо={$object->specific_thermal_characteristic_heating}[ккал/м3ч °С].
Удельная вентиляционная характеристика здания: qv={$object->specific_thermal_characteristic_ventilation}[ккал/м3ч °С].";

				$str .= "
Расчетный коэффициент инфильтрации Kи.р определяется по формуле 3.3 прил.3:
Kи.p. = 0.01*√[2*gL(1-(273+t0)/(273+t1))+w*w]
Kи.р.= 0,01x√[2x9,8x{$object->formula->getOv7()}x(1-(273+{$temperatureCalculated}/(273+{$object->internal_temperature}))+{$winnerSpeedPow2X}] = {$ovKip} 

Тепловая нагрузка отопления определяется по формуле 3.2 прил.3:

Qo.max = {$ovQomax}
                ";


                if ($object->objects->count_object > 1) {
                	if($ovQomax > 0){
                    $str .= "
                        
Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт. 
Тепловая нагрузка отопления группы составит: 

Qo.max = {$ovQomax}";
                	}
                }


                $ovKip = $object->formula->getOvKip();

//                $ovQomax = $model->c_alfa * $object->building_volume * $object->specific_thermal_characteristic_heating * ($object->internal_temperature - $model->temperature_calculated) * (1 + $ovKip) * 10 - 6;
                $ovQomax = $object->formula->getOvQomax();

                $k6 = $object->objects->calculation->formula->getK6();
                $k3 = $object->objects->calculation->formula->getK3();
                $k5 = $object->objects->calculation->formula->getK5();
                $k2 = $object->objects->calculation->formula->getK2();

                $str .= "
Тепловая нагрузка отопления определяется по формуле 3.2 прил.3:
Qo.max = Qo.max = {$k6} x {$object->formula->getOv4()} x {$object->formula->getOv5()} x ({$object->formula->getOv8()} – {$k3}) x (1 + {$object->formula->getOvKip()}) x 0.000001= {$ovQomax} [Гкал/ч] ({$ovQomax1} [кВт])

                ";

                if ($object->objects->count_object > 1) {
                    $str .= "
Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт.
Тепловая нагрузка вентиляции группы составит: 
                        
{$ovQomax} Гкал/ч
{$ovQomax1} кВт
                    ";
                }

            }

            $equipment = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $object->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }

            $o2 = $o2 * 100;

            $k6 = $object->objects->calculation->formula->getK6();
            $k3 = $object->objects->calculation->formula->getK3();
            $k5 = $object->objects->calculation->formula->getK5();
            $k2 = $object->objects->calculation->formula->getK2();


            if($ovQo > 0){
            $str .= "          
Количество тепловой энергии, Гкал, необходимой для отопления зданий на отопительный период в целом, определяется по формуле (16):
                
Qo = Qo.max x 24 x (tj – tот) x n)/(tj – t0)
Qo = ({$ovQomax} x 24 x ({$object->formula->getOv8()} – {$k2}) x {$k5})/({$object->formula->getOv8()} – {$k3}) = {$ovQo} [Гкал/год]";   
            } 

			if($object->formula->getOv6() > 0 || $object->formula->getOv11() > 0){
				$str .= "
Количество тепловой энергии, Гкал, необходимой для вентиляции зданий на отопительный период в целом, определяется по формуле (18):
                
Qv = (Qv.max x 24 x (tj – tот) x n)/(tj – t0)
Qv = ({$ovQvmax} x 24 x ({$object->formula->getOv8()} – {$k2}) x {$k5})/({$object->formula->getOv8()} – {$k3}) = {$ovQv} [Гкал/год] 
";
			}
            
            

			$str .= "            
Для покрытия тепловой нагрузки данного объекта выбрано следующее оборудование:
{$equipment->formula->getO1()} с КПД = {$o2} [%]. 
            
Примечание: характеристики данного оборудования с учетом всех подключенных систем приведены в таблице Котельное оборудование Приложения 1.
            ";

            if ($object->objects->count_object > 1) {
                $str .= "
Расчет годовой потребности в тепле, уловном и натуральном топливе, приведенный ниже,
будет производиться для группы зданий в целом, на основании общих суммарных тепловых нагрузок.
                ";
            }


            $ovGnch = $ovGncho + $ovGnchv;

            $ovQo += ($object->heating_load * 24 * ($object->internal_temperature - $model->temperature_average) * $model->duration_op11) / ($object->internal_temperature - $model->temperature_calculated);
            $ovQv += ($object->ventilation_load * 24 * ($object->internal_temperature - $model->temperature_average) * $model->duration_op11) / ($object->internal_temperature - $model->temperature_calculated);

            $ovGn = $ovGno + $ovGnv;


            $ovGut = $ovGuto + $ovGutv;

            $str .= "
            
Определение часового расхода топлива:";

			if($ovGncho > 0){
			$str .= "
Отопление: 
Gнчо = ({$ovQomax} / {$model->calorific_value} х 1000000) / КПД = {$ovQomax} / {$model->calorific_value} x 1000000 / {$object->objects->count_object} [%] = {$ovGncho} [нм3/ч]";
			}

			if($ovGnchv > 0){
			$str .= " 
Вентиляция: 
Gнчв = ($ovQomax / {$model->calorific_value} х 1000000) / КПД = {$ovQomax} / {$model->calorific_value} x 1000000 / {$object->objects->count_object} [%] = {$ovGnchv} [нм3/ч]";
			}

			if($ovGncho > 0 && $ovGnchv > 0){
				$str .= "
Итого: 
Gнч = {$ovGncho} + {$ovGnchv} = {$ovGnch} [нм3/ч]";
			} else{
// 				$str .= "
// Итого: 
// Gнч = {$ovGnch} [нм3/ч]";
			}

			$str .= "

Определение расчетного годового расхода топлива:";
			
			if($ovGno > 0){
			$str .= "
Отопление:
Gно = (Qo / Qн х 1000) / КПД  = ({$ovQo} / {$model->calorific_value} x 1000) / {$object->objects->count_object} [%] = {$ovGno}[тыс.нм3/год]";
			}
			
			if($ovGnv > 0){
			$str .= "
Вентиляция: 
Gнв = (Qv / Qн х 1000) / КПД = ({$ovQv} / {$model->calorific_value} x 1000) / {$object->objects->count_object} [%] = {$ovGnv} [тыс.нм3/год]";
			}
			
			if($ovGnv > 0 && $ovGno > 0){
			$str .= "
Итого: 
Gн = Gно + Gнв = {$ovGno} + {$ovGnv} = {$ovGn} [тыс.нм3/год]";
			} else {
// 			$str .= "
// Итого: 
// Gн = {$ovGn} [тыс.нм3/год]";
			}

			$str .= "                                                

Определение расчетного годового расхода условного топлива,
при его нормативной теплотворной способности Qут=7000 [ккал/кг]:";

			if($ovGuto > 0){
			$str .= "
Отопление: 
Gуто = ({$ovQo} / 7000 х 100) / КПД = ({$ovQo} / 7000 x 100) / {$object->objects->count_object} [%] = {$ovGuto} [тут/год].";
			}

			if($ovGutv > 0){
			$str .= "               
Вентиляция: 
Gутв = ({$ovQv} / 7000 х 100) / КПД = ({$ovQv} / 7000 x 100) / {$object->objects->count_object} [%] = {$ovGutv} [тут/год]";
			}

			if($ovGuto > 0 && $ovGutv > 0){
			$str .= "          
Итого: 
Gут = Gуто + Gутв = {$ovGuto} + {$ovGutv} = {$ovGut} [тут/год]";
			} else {
// 			$str .= "          
// Итого: 
// Gут = {$ovGut} [тут/год]";	
			}


            $Apos = 1 + (8 * $counter);
            $headerTwoPos = $Apos + 1;
            $headerThreePos = $headerTwoPos + 1;
            $contentPos = $headerThreePos + 1;
            // $Bpos = 70 + (8 * $counter);

            $fiveSheet->getStyle('A1:A1000')->applyFromArray([
            	'font' => [
            		'size' => 10,
            	],
            ]);

            $header = 'Расчет потребности в тепловой энергии';
            $headerTwo = 'на нужды отопления и вентиляции, для объекта: ';
            $headerThree = $object->objects->name;

            $fiveSheet->getColumnDimension('A')->setWidth(120);
            // $fiveSheet->mergeCells("A{$contentPos}");
            $fiveSheet->getStyle("A{$contentPos}")->getAlignment()->setWrapText(true);

            $fiveSheet->getRowDimension($contentPos)->setRowHeight(700);
            $fiveSheet->getStyle("A${contentPos}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_LEFT,
            	'vertical' => Alignment::VERTICAL_TOP,
            ]);

            $fiveSheet->setCellValue("A${Apos}", $header);
            $fiveSheet->setCellValue("A${headerTwoPos}", $headerTwo);
            $fiveSheet->setCellValue("A${headerThreePos}", $headerThree);
            $fiveSheet->setCellValue("A${contentPos}", $str);

            $fiveSheet->getStyle("A{$Apos}:A{$headerThreePos}")->applyFromArray([
            	'font' => [
            		'size' => 11,
            		'bold' => true,
            	],
            ]);

            $fiveSheet->getStyle("A{$Apos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$headerTwoPos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$headerThreePos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$Apos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            // $fiveSheet->setCellValue();

            $counter++;
        }


        $tehPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_TECH])->all(), 'id');
        $tehObjects = ObjectsTech::find()->where(['objects_id' => $tehPks])->all();

        foreach($gvsObjects as $object)
        {
            /** @var $object ObjectsGvs */

            $str = '';


            $gvsQhm = round($object->formula->getGvsQhm(), 4);
            $gvsQhm1 = round($gvsQhm * 1163, 4);

            $gvsQhmOnce = round($object->formula->getGvsQhm(false), 4);
            $gvsQhm1Once = round($gvsQhmOnce * 1163, 4);

            $gvsQhc = round($object->formula->getGvsQhc(), 4);
            $gvsQhc1 = round($object->formula->getGvsQhc1(), 4);
            $gvsN0 = $k5 / 7 * $object->count_work_day * $object->work_per_day;
            $gvsNs = (365 - $k5) / 7 * $object->count_work_day * $object->work_per_day;

            $gvsQh = round($object->formula->getGvsQh(), 2);

            if ($object->ventilation_load != 0) {
                $gvsGnch = ($gvsQhm / $model->calorific_value * pow(10, 6)) / $object->objects->count_object;
                $gvsGn = ($gvsQh / $model->calorific_value * pow(10, 3)) / $object->objects->count_object;
                $gvsGut = ($gvsQh / 7000 * pow(10, 3)) / $object->objects->count_object;
            }

            $str .= "
Расчет потребности в тепловой энергии на нужды горячего водоснабжения, для объекта: {$object->objects->name}
                ";

            if($object->heat_loads_known == 1){

                $str .= "
Тепловая нагрузка горячего водоснабжения здания принята по проектным данным, годовая потребность в тепловой энергии определена в соответствии с МДК 4-05.2004.

Продолжительность работы системы ГВС в сутки T={$object->work_per_day}
Количество рабочих дней системы ГВС в неделю n={$object->count_work_day}
Тепловая нагрузка ГВС в отопительный период по данным проекта:
Qhm = {$gvsQhmOnce} [Гкал/ч] ({$gvsQhm1Once} [кВт])

                ";

                if($object->objects->count_object > 1){
                    $str .= "
Расчет производится для группы одинаковых зданий из {$object->formula->getGvs2()} шт. 
Тепловая нагрузка ГВС в отопительном периоде для группы составит: 
Qhm = {$gvsQhmOnce} x {$object->objects->count_object} = {$gvsQhm} [Гкал/ч] ({$gvsQhm1} [кВт])
                ";
                }
            } else {

                $purposeSystem = ArrayHelper::getValue(ObjectsGvsType::getTypes(), $object->purpose_system);

                $gvsQhm = round($object->formula->getGvsQhm(), 4);
                $gvsQhm1 = round($object->formula->getGvsQhm1(), 4);

                $str .= "
Проектные тепловые нагрузки горячего водоснабжения отсутствуют, поэтому расчетная часовая тепловая нагрузка горячего водоснабжения здания и годовая потребность в тепловой энергии определены в соответствии с МДК 4-05.2004 и нормами водопотребления по прил.3 СП 30.13330.2010
«ВНУТРЕННИЙ ВОДОПРОВОД И КАНАЛИЗАЦИЯ ЗДАНИЙ».
                
Тип здания: {$purposeSystem}
Норма затрат воды на горячее водоснабжение абонента, л/ед. измерения в сутки a={$object->water_consumption_rate}
Количество единиц измерения (человек в смену), отнесенное к суткам , N={$object->count_consumers}
Продолжительность работы системы ГВС в сутки T={$object->work_per_day}
Количество рабочих дней системы ГВС в неделю n={$object->count_work_day}
Температура водопроводной воды в отопительный период tc = 5°C
Температура водопроводной воды в неотопительный период tcs = 15°C
Тепловая нагрузка ГВС в отопительный период определяется по формуле 3.13 прил.3:
Qhm = (a х N х (55 - tc) х 0.000001)/T
Qhm = ({$object->water_consumption_rate} х {$object->count_consumers} х (55 - 5) х 0.000001)/{$object->formula->getGvs6()} = {$gvsQhmOnce} [Гкал/ч] ({$gvsQhm1Once}[кВт])

                ";

                if($object->objects->count_object > 1){

                    $str .= "
Расчет производится для группы одинаковых зданий из {$object->objects->count_object} шт. 
Тепловая нагрузка ГВС в отопительном периоде для группы составит: 
Qhm = {$gvsQhmOnce} x {$object->objects->count_object} = {$gvsQhm} [Гкал/ч] ({$gvsQhm1} [кВт])
                    ";
                
                }
            }

			$equipment = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $object->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }

            $o2 = $o2 * 100;

            $k5 = $model->formula->getK5();

            $gvsNO = round($object->formula->getGvsNO());
            $gvsNs = round($object->formula->getGvsNs());

            $str .= "
          
Тепловая нагрузка ГВС в неотопительный период определяется по формуле 3.13а прил.3:

Qhс=Qhm x b х (ths – tcs)/(th – tc)
Qhс={$gvsQhm} x 1 х (55-15)/(55-5) = {$gvsQhc} [Гкал/ч] ({$gvsQhc1} [кВт]).
            
Для определения годовой нагрузки определяется количество часов работы системы горячего водоснабжения в отопительный и неотопительный периоды года, в зависимости от общей продолжительности соответствующих периодов, количества рабочих дней в неделе, и продолжительности работы ГВС в сутки:
nо= {$k5} / 7 x {$object->formula->getGvs5()} x {$object->formula->getGvs6()} = {$gvsNO} часов в отопительном периоде.                              
ns= (365 - {$k5})/ 7 x {$object->formula->getGvs5()} x {$object->formula->getGvs6()} = {$gvsNs} часов в неотопительном периоде                 
Необходимое количество тепловой энергии на горячее водоснабжение на год, Гкал, определяется по формуле (19):
Qh= Qhm x no + Qhс x ns
Qh= {$gvsQhm} x {$gvsN0} + {$gvsQhc} x {$gvsNs} = {$gvsQh} [Гкал/год]                              
            
Для покрытия тепловой нагрузки данного объекта выбрано следующее оборудование:
{$equipment->formula->getO1()} с КПД = {$o2} [%]. 
            
Примечание: характеристики данного оборудования с учетом всех подключенных систем приведены в таблице Котельное оборудование Приложения 1.
            ";

            if($object->objects->count_object > 1){
                $str .= "
Расчет годовой потребности в тепле, уловном и натуральном топливе, приведенный ниже, будет производится для группы зданий в целом, на основании общих суммарных тепловых нагрузок.
                ";
            }

            $gvsGnch = round($object->formula->getGvsGnch(), 2);
            $gvsGn = round($object->formula->getGvsGn(), 2);
            $gvsGut = round($object->formula->getGvsGut(), 2);

			$equipment = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $object->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }

            $o2 = $o2 * 100;

            // $c = count($tehObjects);

            $str .= "
Определение часового расхода топлива:
Gнч = Qhm / Qн x 1000000) / КПД  = ({$gvsQhm} / {$model->calorific_value} x 1000000) / {$o2} [%] = {$gvsGnch} [нм3/ч]           
            
Определение расчетного годового расхода топлива:
Gн = (Qh / Qн x 1000000) / КПД = ({$gvsQh} / {$model->calorific_value} x 1000) / {$o2} = {$gvsGn} [тыс.нм3/год]            
            
Определение расчетного годового расхода условного топлива, при его нормативной теплотворной способности Qут=7000 [ккал/кг]:
Gут = (Qh / Qут x 1000) / КПД  = ({$gvsQh} / 7000 x 1000) / {$o2 } = {$gvsGut} [тут/год]               


            ";

            // $Apos = 1 + (71 * $counter);
            // $Bpos = 70 + (71 * $counter);

            // $fiveSheet->mergeCells("A{$Apos}:A{$Bpos}");
            // $fiveSheet->getStyle("A{$Apos}:A{$Bpos}")->getAlignment()->setWrapText(true);
            // $fiveSheet->setCellValue("A${Apos}", $str);

            $Apos = 1 + (8 * $counter);
            $headerTwoPos = $Apos + 1;
            $headerThreePos = $headerTwoPos + 1;
            $contentPos = $headerThreePos + 1;

            $fiveSheet->getStyle('A1:A1000')->applyFromArray([
                'font' => [
                    'size' => 10,
                ],
            ]);

            $header = 'Расчет потребности в тепловой энергии';
            $headerTwo = 'на нужды горячего водоснабжения, для объекта: ';
            $headerThree = $object->objects->name;

            $fiveSheet->getColumnDimension('A')->setWidth(120);
            // $fiveSheet->mergeCells("A{$contentPos}");
            $fiveSheet->getStyle("A{$contentPos}")->getAlignment()->setWrapText(true);

            $fiveSheet->getRowDimension($contentPos)->setRowHeight(700);
            $fiveSheet->getStyle("A${contentPos}")->getAlignment()->applyFromArray([
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_TOP,
            ]);

            $fiveSheet->setCellValue("A${Apos}", $header);
            $fiveSheet->setCellValue("A${headerTwoPos}", $headerTwo);
            $fiveSheet->setCellValue("A${headerThreePos}", $headerThree);
            $fiveSheet->setCellValue("A${contentPos}", $str);

            $fiveSheet->getStyle("A{$Apos}:A{$headerThreePos}")->applyFromArray([
                'font' => [
                    'size' => 11,
                    'bold' => true,
                ],
            ]);

            $fiveSheet->getStyle("A{$Apos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$headerTwoPos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$headerThreePos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$Apos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $counter++;
        }

        foreach ($tehObjects as $object)
        {
            /** @var $object ObjectsTech */

            $str = '';

            $str .= "
Расчет потребности в тепловой энергии,
условном и натуральном топливе, на технологические нужды:
{$object->objects->name}
            ";

            $purposeSystem = ArrayHelper::getValue(ObjectsTechType::getTypes(), $object->purpose_system);

            $O7 = $object->objects->power_v / 1164;
            // $O11 = $O7 / 1000000 / $object->objects->count_object; // TODO: не правильный расчет

            $equipment = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $object->objects->number_group])->one();
            if($equipment){
                $o2 = $equipment->formula->getO2();
            } else {
                $o2 = 1;
            }

            $o2 = $o2 * 100;


            if($equipment != null){
            	$O11 = round($equipment->formula->getO11(), 4);
            	$O3 = round($equipment->formula->getO3(), 1);
            } else {
            	$O11 = 0;
            	$O3 = 0;
            }


            $tehQhd = round($object->formula->getTehQhd(), 3);
            $tehQh = round($object->formula->getTehQh(), 4);
            $tehQh1 = round($object->formula->getTehQh1(), 4);
            $tehGn = round($object->formula->getTehGn(), 2);
            $tehGut = round($object->formula->getTehGut(), 2);
            $tehQn = round($object->formula->getTehQn(), 2);

			$zak6 = $model->formula->getZak6();

            $type = ObjectsTechType::findOne($object->purpose_system);


            if($equipment != null){
$str .= "
            
Расчет потребности в тепловой энергии, условном и натуральном топливе, на осуществление технологического процесса выполнен в соответствии с СП 42-101-2003 Общие положения по проектированию и строительству газораспределительных систем из металлических и полиэтиленовых труб.
            
Технологический процесс: {$purposeSystem}
{$type->amount_name} = {$object->amount}
Норма расхода условного топлива {$type->one_name} составляет: {$object->formula->getTeh5()} [кг], что при расчете в тепловых единицах эквивалентно: {$object->consumption_thermal_units} [тыс.ккал].
Нормативный расход условного топлива (или тепла) на единицу продукции) принят в соответствии с: СП 42-101-2003(Приложение А).


Для данного объекта выбрано следующее оборудование:
{$equipment->name_equipment}, количество, n={$equipment->count_equipment} [шт].
Номинальный расход газа прибором q={$tehQhd} [нм3/ч].
Коэффициент одновременности Ksim={$equipment->formula->getO3()}, КПД={$o2} [%]
            
Примечание: характеристики данного оборудования с учетом всех подключенных систем приведены в таблице Технологическое оборудование Приложения 2.
            
Часовой расхода газа группой приборов определяется по формуле (2):


Qhd = q x Ksim x n
Qhd = {$O11} x {$O3} x {$equipment->formula->getO4()} = {$tehQhd}  [нм3/ч]                                            
            
Определение часового расхода топлива:
Qн={$zak6} [ккал/н.м3]
Qчас = (Qhd / Qн x 1000000) / КПД = {$tehQhd} х {$zak6} х 0.000001 x {$o2}= {$tehQh} [Гкал/час] ( {$tehQh1} [кВт]).   
            
Для определения годовой потребности группы приборов в природном газе, определяется годовая потребность всех потребителей в тепловой энергии, как произведение нормативного расхода теплоты (1 тыс.ккал на 1 обед) на количество потребителей {$object->formula->getTeh8()}, что составит:
Qн = {$object->formula->getTeh8()} x {$object->formula->getTeh4()} x 0.001 = {$tehQn} [Гкал/год]
         
            
Определение расчетного годового расхода топлива, при теплотворной способности Qн={$zak6} [ккал/н.м3]:
Gн = (Qг / Qн x 1900) / КПД = ({$tehQn} / {$model->formula->getZak6()} x 1000) / {$o2} [%] = {$tehGn} [тыс.нм3/год]
            
Определение расчетного годового расхода условного топлива, при его нормативной теплотворной способности Qут=7000 [ккал/кг]:
Gут = (Qг / Qут x 1000) / КПД = ({$tehQn} / 7000 x 1000) / {$o2} [%] = {$tehGut} [тут/год]

            ";
            }


            $Apos = 1 + (71 * $counter);
            $Bpos = 70 + (71 * $counter);

            $Apos = 1 + (8 * $counter);
            $headerTwoPos = $Apos + 1;
            $headerThreePos = $headerTwoPos + 1;
            $contentPos = $headerThreePos + 1;
            // $Bpos = 70 + (8 * $counter);

            $fiveSheet->getStyle('A1:A1000')->applyFromArray([
            	'font' => [
            		'size' => 10,
            	],
            ]);

            $header = 'Расчет потребности в тепловой энергии';
            $headerTwo = 'условном и натуральном топливе, на технологические нужды:';
            $headerThree = $object->objects->name;

            $fiveSheet->getColumnDimension('A')->setWidth(120);
            // $fiveSheet->mergeCells("A{$contentPos}");
            $fiveSheet->getStyle("A{$contentPos}")->getAlignment()->setWrapText(true);

            $fiveSheet->getRowDimension($contentPos)->setRowHeight(700);
            $fiveSheet->getStyle("A${contentPos}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_LEFT,
            	'vertical' => Alignment::VERTICAL_TOP,
            ]);

            $fiveSheet->setCellValue("A${Apos}", $header);
            $fiveSheet->setCellValue("A${headerTwoPos}", $headerTwo);
            $fiveSheet->setCellValue("A${headerThreePos}", $headerThree);
            $fiveSheet->setCellValue("A${contentPos}", $str);

            $fiveSheet->getStyle("A{$Apos}:A{$headerThreePos}")->applyFromArray([
            	'font' => [
            		'size' => 10,
            		'bold' => true,
            	],
            ]);

            $fiveSheet->getStyle("A{$Apos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$headerTwoPos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$headerThreePos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fiveSheet->getStyle("A{$Apos}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            // $fiveSheet->mergeCells("A{$Apos}:Z{$Bpos}");
            // $fiveSheet->getStyle("A{$Apos}:M{$Bpos}")->getAlignment()->setWrapText(true);
            $fiveSheet->setCellValue("A{$contentPos}", $str);

            $counter++;
        }




        $sevenSheet = $spreadsheet->createSheet();
        $sevenSheet->setTitle('Приложение 1');
        $sevenSheet->setCellValue('A1', '- Приложение 1 -');
        $sevenSheet->getStyle('A1:U1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sevenSheet->setCellValue('A2', 'Котельное оборудование');
        $sevenSheet->getStyle('A2:U2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sevenSheet->mergeCells('A1:U1');
        $sevenSheet->mergeCells('A2:U2');

        $sixData = [
//            ['1', 'КС-16 (О1)', '0,01376 (О7) (16кВт) (О6)', '90 (О2)', '1 (О4)', '1 (О3)', '0,01376 (О8) (16кВт)', '1,91 (О11)', '0,0132 (Ob_Qhmax) (15,4кВт) (Ob_Qhmax1)', '0,96 (О5)'],
//            ['2', 'ВПГ-18', '0,01548 (18кВт)', '90', '1', '1', '0,01548 (18кВт)', '2,15', '0,01375 (16кВт)', '0,89'],
        ];

        $ovObjects = ObjectsOv::find()->where(['objects_id' => $ovPks])
            ->andWhere(['or', ['>', 'specific_thermal_characteristic_ventilation', 0], ['>', 'ventilation_load', 0]])
            ->all();

        $counter = 1;
        foreach ($ovObjects as $object)
        {
            /** @var $object ObjectsOv */

            $o8 = $object->objects->power_g * $object->objects->count_object * $object->objects->power_v * $object->objects->count_object;

            $o11 = $object->objects->power_v/$model->calorific_value * 1000000 / $object->objects->count_object;

            $ovQomax = $object->heating_load;
            $ovQomax1 = $object->heating_load * 1163;

//            $o5 =

            // $sixData[] = [
                // $counter, $object->objects->name, $object->objects->power_v, 90, $object->objects->count_object, 1, $o8, $o11, '(OB_MAX)', ''
            // ];

            $counter++;
        }


        $objGroups = ArrayHelper::getColumn(Objects::find()->where(['type_object' => [Objects::TYPE_OV, Objects::TYPE_GVS], 'calculation_id' => $model->id])->all(), 'number_group');

        $equipments = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $objGroups])->all();

        $counter = 1;
        foreach ($equipments as $eq) {

            $eqPoverGKvt = round($eq->pover_g * 1164);
            $obQhmaxKvt = round($eq->formula->getObQhmax() * 1164);

        	$sixData[] = [
        		$counter, $eq->formula->getO1(), [round($eq->pover_g, 5), " {$eq->power_v} кВт"], ($eq->formula->getO2() * 100), $eq->formula->getO4(), $eq->formula->getO3(),
        		[round($eq->pover_g, 5), "$eqPoverGKvt кВт"], round($eq->formula->getO11(), 2),
        		[round($eq->formula->getObQhmax(), 5), "{$obQhmaxKvt} кВт"], $eq->formula->getO5()
        	];
        	$counter++;
        }

        $counterOffset = $counter;


        $sixMergedColumns = [
            'A4:B4' => '№ п/п',
            'C4:E4' => 'Название',
            'F4:G4' => 'Ед. мощн. Гкал/час (кВт)',
            'H4:I4' => 'КПД %',
            'J4:K4' => 'Кол-во',
            'L4:M4' => 'К.одн.',
            'N4:O4' => 'Сум. мощн. Гкал/час (кВт)',
            'P4:Q4' => 'Расх. топл. нм3/ч',
            'R4:S4' => 'Присоед. мощн. Гкал/час (кВт)',
            'T4:U4' => 'Коэф. загр.'
        ];

        $sevenSheet->getRowDimension('4')->setRowHeight('55');

        foreach ($sixMergedColumns as $coords => $title){
            $coord = explode(':', $coords)[0];
            $sevenSheet->mergeCells($coords);
            $sevenSheet->getStyle($coords)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sevenSheet->getStyle($coord)->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle($coords)->getFont()->setBold(2);
            $sevenSheet->getStyle($coords)->applyFromArray($bordersStyle);
            $sevenSheet->setCellValue($coord, $title);

            $sevenSheet->getStyle("A4:U4")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
        }

        $offset = 3;
        for ($i = 0; $i < count($sixData); $i++)
        {
        	$j = $i + 1;
            $v = $offset + ($j * 2);
            $v1 = $v + 1;
            $data = $sixData[$i];

            $sevenSheet->getStyle("A{$v}:B{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("A{$v}:B{$v1}");
            $sevenSheet->getStyle("C{$v}:E{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("C{$v}:E{$v1}");
            $sevenSheet->getStyle("F{$v}:G{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("F{$v}:G{$v}");
            $sevenSheet->getStyle("F{$v1}:G{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("F{$v1}:G{$v1}");
            $sevenSheet->getStyle("H{$v}:I{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("H{$v}:I{$v1}");
            $sevenSheet->getStyle("J{$v}:K{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("J{$v}:K{$v1}");
            $sevenSheet->getStyle("L{$v}:M{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("L{$v}:M{$v1}");
            $sevenSheet->getStyle("N{$v}:O{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("N{$v}:O{$v}");
            $sevenSheet->getStyle("N{$v1}:O{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("N{$v1}:O{$v1}");
            $sevenSheet->getStyle("P{$v}:Q{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("P{$v}:Q{$v1}");
            $sevenSheet->getStyle("R{$v}:S{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("R{$v}:S{$v}");
            $sevenSheet->getStyle("R{$v1}:S{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("R{$v1}:S{$v1}");
            $sevenSheet->getStyle("T{$v}:U{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("T{$v}:U{$v1}");

            $sevenSheet->getStyle("A{$v}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sevenSheet->getStyle("F{$v}:U{$v}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sevenSheet->getStyle("C{$v}")->getAlignment()->applyFromArray([
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);


            $sevenSheet->setCellValue("A{$v}", $data[0]);
            $sevenSheet->setCellValue("C{$v}", $data[1]);
            $sevenSheet->setCellValue("F{$v}", $data[2][0]);
            $sevenSheet->setCellValue("F{$v1}", $data[2][1]);
            $sevenSheet->setCellValue("H{$v}", $data[3]);
            $sevenSheet->setCellValue("J{$v}", $data[4]);
            $sevenSheet->setCellValue("L{$v}", $data[5]);
            $sevenSheet->setCellValue("N{$v}", $data[6][0]);
            $sevenSheet->setCellValue("N{$v1}", $data[6][1]);
            $sevenSheet->setCellValue("P{$v}", $data[7]);
            $sevenSheet->setCellValue("R{$v}", $data[8][0]);
            $sevenSheet->setCellValue("R{$v1}", $data[8][1]);
            $sevenSheet->setCellValue("T{$v}", $data[9]);
        }

        $offsetCell = 9 + (count($sixData) * 2);
        $counter = 2 + (count($sixData) * 2);
        $counterOffset = $counter;

        $sevenSheet->mergeCells('A4:B4');
        $sevenSheet->getStyle('A4:B4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $sevenSheet->mergeCells("A{$offsetCell}:U{$offsetCell}");
        $sevenSheet->getStyle("A{$offsetCell}:U{$offsetCell}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sevenSheet->setCellValue("A{$offsetCell}", 'Потребители тепла (от котельной)');


        $sevenSheet->getColumnDimension('A')->setWidth(3);
        $sevenSheet->getColumnDimension('B')->setWidth(2);
        $sevenSheet->getColumnDimension('C')->setWidth(8);
        $sevenSheet->getColumnDimension('D')->setWidth(8);
        $sevenSheet->getColumnDimension('E')->setWidth(8);
        $sevenSheet->getColumnDimension('F')->setWidth(5);
        $sevenSheet->getColumnDimension('G')->setWidth(5);
        $sevenSheet->getColumnDimension('H')->setWidth(5);
        $sevenSheet->getColumnDimension('I')->setWidth(5);
        $sevenSheet->getColumnDimension('J')->setWidth(2);
        $sevenSheet->getColumnDimension('K')->setWidth(2);
        $sevenSheet->getColumnDimension('L')->setWidth(2);
        $sevenSheet->getColumnDimension('M')->setWidth(2);
        $sevenSheet->getColumnDimension('N')->setWidth(5);
        $sevenSheet->getColumnDimension('O')->setWidth(5);
        $sevenSheet->getColumnDimension('P')->setWidth(5);
        $sevenSheet->getColumnDimension('Q')->setWidth(5);
        $sevenSheet->getColumnDimension('R')->setWidth(5);
        $sevenSheet->getColumnDimension('S')->setWidth(5);
        $sevenSheet->getColumnDimension('T')->setWidth(3);
        $sevenSheet->getColumnDimension('U')->setWidth(3);

        $sixData = [
            ['1', 'Приготовление обеда (Teh3) (261 (365х(Teh6)/7) дней в году)', '7820 (Teh8) (Количество обедов в год) (Teh8.1)', '0,143 (Teh5) (на 1 обед) (Teh4_1)', '1 (Teh4) (на 1 обед) (Teh4_1)', '1,12 (Teh_Gut)', '7,82 (Teh_Qn) (0,03) (Teh_Qn/(365хTeh6/7))'],
            ['2', 'Выпечка формового хлеба (313 дней в году)', '360 (Годовое производство хлеба, тонн)', '85,714 (на 1 тонну)', '600 (на 1 тонну)', '30,86', '216 (0,6901)'],
        ];

//        foreach ($ovObjects as $object)
//        {
//            /** @var $object ObjectsOv */
//
//            $o8 = $object->objects->power_g * $object->objects->count_object * $object->objects->power_v * $object->objects->count_object;
//
//            $o11 = $object->objects->power_v/$model->calorific_value * 1000000 / $object->objects->count_object;
//
//            $ovQomax = $object->heating_load;
//            $ovQomax1 = $object->heating_load * 1163;
//
////            $o5 =
//
//            $sixData[] = [
//                $counter, $object->objects->name, $object->objects->power_v, 90, $object->objects->count_object, 1, $o8, $o11, '(OB_MAX)', ''
//            ];
//
//            $counter++;
//        }

//        $sixMergedColumns = [
//            'A9:B9' => '№ п/п',
//            'C9:E9' => 'Вид использования (продукция), суток в году',
//            'F9:G9' => 'Годовой выпуск (указать единицу измерения)',
//            'H9:I9' => 'Удельный расход топлива [кгу.т.] на единицу продукции',
//            'J9:K9' => 'то же, в тепловых единицах [тыс.ккал](ГДж)',
//            'L9:M9' => 'Годовая потребность в условном топливе [тут/год]',
//            'N9:O9' => 'то же, в тепловых единицах [Гкал/год]/(Гкал/сут)',
//        ];
//
//        foreach ($sixMergedColumns as $coords => $title){
//            $coord = explode(':', $coords)[0];
//            $sevenSheet->mergeCells($coords);
//            $sevenSheet->getStyle($coords)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
//            $sevenSheet->getStyle($coords)->getFont()->setBold(2);
//            $sevenSheet->getStyle($coords)->applyFromArray($bordersStyle);
//            $sevenSheet->setCellValue($coord, $title);
//        }


        $sevenSheet->mergeCells("A".(9+$counter).":B".(10+$counter)."");
        $sevenSheet->setCellValue("A".(9+$counter)."", '№п/п');
        $sevenSheet->getStyle("A".(9+$counter).":B".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("A".(9+$counter).":B".(10+$counter)."")->applyFromArray($bordersStyle);
        $sevenSheet->mergeCells("C".(9+$counter).":F".(10+$counter)."");
        $sevenSheet->setCellValue("C".(9+$counter)."", 'Потребители тепла');
        $sevenSheet->getStyle("C".(9+$counter).":F".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("C".(9+$counter).":F".(10+$counter)."")->applyFromArray($bordersStyle);
        $sevenSheet->mergeCells("G".(9+$counter).":R".(9+$counter)."");
        $sevenSheet->setCellValue("G".(9+$counter)."", 'Максимальные тепловые нагрузки (Гкал/ч)');
        $sevenSheet->getStyle("G".(9+$counter).":R".(9+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("G".(9+$counter).":R".(9+$counter)."")->applyFromArray($bordersStyle);
        $sevenSheet->mergeCells("G".(10+$counter).":J".(10+$counter)."");
        $sevenSheet->setCellValue("G".(10+$counter)."", 'Отопление');
        $sevenSheet->getStyle("G".(10+$counter).":J".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("G".(10+$counter).":J".(10+$counter)."")->applyFromArray($bordersStyle); // z
        $sevenSheet->mergeCells("K".(10+$counter).":N".(10+$counter)."");
        $sevenSheet->setCellValue("K".(10+$counter)."", 'Вентиляция');
        $sevenSheet->getStyle("K".(10+$counter).":N".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("K".(10+$counter).":N".(10+$counter)."")->applyFromArray($bordersStyle);
        $sevenSheet->mergeCells("O".(10+$counter).":P".(10+$counter)."");
        $sevenSheet->setCellValue("O".(10+$counter)."", 'Горячее водоснабжение');
        $sevenSheet->getStyle("O".(10+$counter).":P".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("O".(10+$counter).":P".(10+$counter)."")->applyFromArray($bordersStyle);
        $sevenSheet->mergeCells("Q".(10+$counter).":R".(10+$counter)."");
        $sevenSheet->setCellValue("Q".(10+$counter)."", 'Технология');
        $sevenSheet->getStyle("Q".(10+$counter).":R".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("Q".(10+$counter).":R".(10+$counter)."")->applyFromArray($bordersStyle);
        $sevenSheet->mergeCells("S".(9+$counter).":U".(10+$counter)."");
        $sevenSheet->setCellValue("S".(9+$counter)."", 'Итого');
        $sevenSheet->getStyle("S".(9+$counter).":U".(10+$counter)."")->getFont()->setBold(2);
        $sevenSheet->getStyle("S".(9+$counter).":U".(10+$counter)."")->applyFromArray($bordersStyle);

        $sevenSheet->getStyle("O".(10+$counter).":P".(10+$counter))->getAlignment()->setWrapText(true);
        $sevenSheet->getStyle("Q".(10+$counter).":R".(10+$counter))->getAlignment()->setWrapText(true);

        $sevenSheet->getStyle("A".(9 + $counter).":U".(10 + $counter))->getAlignment()->applyFromArray([
        	'horizontal' => Alignment::HORIZONTAL_CENTER,
        	'vertical' => Alignment::VERTICAL_CENTER,
        ]);
        $sevenSheet->getRowDimension((10 + $counter))->setRowHeight('35');

        $sevenData = [
//            ['1', 'КС-16 (О1)', '0,01376 (О7) (16кВт) (О6)', '90 (О2)', '1 (О4)', '1 (О3)', '0,01376 (О8) (16кВт)'],
//            ['2', 'ВПГ-18', '0,01548 (18кВт)', '90', '1', '1', '0,01548 (18кВт)'],
        ];


        // $objects = Objects::find()->where(['calculation_id' => $id])->all();

        $objGroups = ArrayHelper::getColumn(Objects::find()->where(['type_object' => [Objects::TYPE_OV, Objects::TYPE_GVS], 'calculation_id' => $model->id])->all(), 'number_group');

        $equipments = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $objGroups])->all();

        $counter = 1;
        foreach ($equipments as $eq) {
            $objects = Objects::find()->where(['calculation_id' => $id, 'number_group' => $eq->number_group])->all();

            $ovQoMax = 0;
            $ovQvMax = 0;
            $gvsQhm = 0;
            $tehQh = 0;

            $names = [];


            foreach ($objects as $object) {
                $names[] = $object->name;

                if($object->type_object == Objects::TYPE_OV)
                {
                    $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();


                    $ovQoMax += $object->formula->getOvQomax();
                    $ovQvMax += $object->formula->getOvQvmax();

                } else if($object->type_object == Objects::TYPE_GVS)
                {
                    $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

                    $name = $object->formula->getGvs1();

                    $gvsQhm += $object->formula->getGvsQhm();

                } else if($object->type_object == Objects::TYPE_TECH)
                {
                    $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                    $name = $object->formula->getTeh1();

                    $tehQh += $object->formula->getTehQh();
                }

            }

            $ovQoMax = round($ovQoMax, 3);
            $ovQoMaxKvt = $ovQoMax * 1163;

            $ovQvMax = round($ovQvMax, 4);
            $ovQvMaxKvt = $ovQvMax * 1163;

            $gvsQhm = round($gvsQhm, 5);
            $gvsQhmKvt = $gvsQhm * 1163;

            $obQhmax = round($eq->formula->getObQhmax(), 5);
            $obQhmaxKvt = round($obQhmax * 1163, 1);

            $tehQhKvt = $tehQh * 1163;

            $sevenData[] = [
                $counter, implode('; ', $names), [$ovQoMax, "{$ovQoMaxKvt} кВт"], [$ovQvMax, "{$ovQvMaxKvt} кВт"], [$gvsQhm, "{$gvsQhmKvt} кВт"], [$tehQh, "{$tehQhKvt} кВт"],
                [$obQhmax, "{$obQhmaxKvt} кВт"]
            ];

            $counter++;
        }
            // VarDumper::dump($sevenData, 10, true);

            // exit;


   //      $counter = 1;
   //      foreach ($objects as $object) {

   //      	$name = null;

			// if($object->type_object == Objects::TYPE_OV)
   //          {
   //              $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();

   //              $name = $object->formula->getO1();

   //          } else if($object->type_object == Objects::TYPE_GVS)
   //          {
   //              $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

   //              $name = $object->formula->getGvs1();

   //          } else if($object->type_object == Objects::TYPE_TECH)
   //          {
   //              $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

			// 	$name = $object->formula->getTeh1();
   //          }


   //      	$sevenData[] = [
   //      		$counter, $name, 
   //      	];
   //      	$counter++;
   //      }




        $offset = 9 + $counterOffset;
        for ($i = 0; $i < count($sevenData); $i++)
        {
        	$j = $i + 1;
            $v = $offset + ($j * 2);
            $v1 = $v + 1;
            $data = $sevenData[$i];

            $sevenSheet->getStyle("A{$v}:B{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("A{$v}:B{$v1}");
            $sevenSheet->getStyle("A{$v}:B{$v1}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("C{$v}:F{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("C{$v}:F{$v1}");
            $sevenSheet->getStyle("C{$v}:F{$v1}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("G{$v}:J{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("G{$v}:J{$v}");
            $sevenSheet->getStyle("G{$v}:J{$v}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("G{$v1}:J{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("G{$v1}:J{$v1}");
            $sevenSheet->getStyle("G{$v1}:J{$v1}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("K{$v}:N{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("K{$v}:N{$v}");
            $sevenSheet->getStyle("K{$v}:N{$v}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("K{$v1}:N{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("K{$v1}:N{$v1}");
            $sevenSheet->getStyle("K{$v1}:N{$v1}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("O{$v}:P{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("O{$v}:P{$v}");
            $sevenSheet->getStyle("O{$v}:P{$v}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("O{$v1}:P{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("O{$v1}:P{$v1}");
            $sevenSheet->getStyle("O{$v1}:P{$v1}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("Q{$v}:R{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("Q{$v}:R{$v}");
            $sevenSheet->getStyle("Q{$v1}:R{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("Q{$v1}:R{$v1}");
            $sevenSheet->getStyle("Q{$v1}:R{$v1}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("S{$v}:U{$v}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("S{$v}:U{$v}");
            $sevenSheet->getStyle("S{$v}:U{$v}")->getAlignment()->setWrapText(true);
            $sevenSheet->getStyle("S{$v1}:U{$v1}")->applyFromArray($bordersStyle);
            $sevenSheet->mergeCells("S{$v1}:U{$v1}");
            $sevenSheet->getStyle("S{$v1}:U{$v1}")->getAlignment()->setWrapText(true);

            $sevenSheet->getStyle("A{$v}:B{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sevenSheet->getStyle("C{$v}:D{$v1}")->getAlignment()->applyFromArray([
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sevenSheet->getStyle("G{$v}:U{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);


            $sevenSheet->setCellValue("A{$v}", $data[0]);
            $sevenSheet->setCellValue("C{$v}", $data[1]);
            $sevenSheet->setCellValue("G{$v}", $data[2][0]);
            $sevenSheet->setCellValue("G{$v1}", $data[2][1]);
            $sevenSheet->setCellValue("K{$v}", $data[3][0]);
            $sevenSheet->setCellValue("K{$v1}", $data[3][1]);
            $sevenSheet->setCellValue("O{$v}", $data[4][0]);
            $sevenSheet->setCellValue("O{$v1}", $data[4][1]);
            $sevenSheet->setCellValue("Q{$v}", $data[5][0]);
            $sevenSheet->setCellValue("Q{$v1}", $data[5][1]);
            $sevenSheet->setCellValue("S{$v}", $data[6][0]);
            $sevenSheet->setCellValue("S{$v1}", $data[6][1]);
        }

        $sevenSheet->getStyle('A1:U1000')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);

        $sevenSheet->getStyle('A1')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);
        $sevenSheet->getStyle('A2')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);
        $sevenSheet->getStyle("A{$offsetCell}")->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $sixSheet = $spreadsheet->createSheet();

        $sixSheet->getColumnDimension('A')->setWidth(2);
        $sixSheet->getColumnDimension('B')->setWidth(2);
        $sixSheet->getColumnDimension('C')->setWidth(10);
        $sixSheet->getColumnDimension('D')->setWidth(10);
        $sixSheet->getColumnDimension('E')->setWidth(10);
        $sixSheet->getColumnDimension('F')->setWidth(5);
        $sixSheet->getColumnDimension('G')->setWidth(5);
        $sixSheet->getColumnDimension('H')->setWidth(5);
        $sixSheet->getColumnDimension('I')->setWidth(5);
        $sixSheet->getColumnDimension('J')->setWidth(2);
        $sixSheet->getColumnDimension('K')->setWidth(2);
        $sixSheet->getColumnDimension('L')->setWidth(2);
        $sixSheet->getColumnDimension('M')->setWidth(4);
        $sixSheet->getColumnDimension('N')->setWidth(5);
        $sixSheet->getColumnDimension('O')->setWidth(5);
        $sixSheet->getColumnDimension('P')->setWidth(5);
        $sixSheet->getColumnDimension('Q')->setWidth(4);
        $sixSheet->getColumnDimension('R')->setWidth(5);
        $sixSheet->getColumnDimension('S')->setWidth(4);

        $sixSheet->setTitle('Приложение 2');
        $sixSheet->setCellValue('A1', '- Приложение 2 -');
        $sixSheet->getStyle('A1:S1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sixSheet->setCellValue('A2', 'Технологическое оборудование');
        $sixSheet->getStyle('A2:S2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sixSheet->mergeCells('A1:S1');
        $sixSheet->mergeCells('A2:S2');

        $sixData = [
            // ['1', 'КС-16 (О1)', '0,01376 (О7) (16кВт) (О6)', '90 (О2)', '1 (О4)', '1 (О3)', '0,01376 (О8) (16кВт)', '1,91 (О11)', '0,0132 (Ob_Qhmax) (15,4кВт) (Ob_Qhmax1)', '0,96 (О5)'],
//            ['2', 'ВПГ-18', '0,01548 (18кВт)', '90', '1', '1', '0,01548 (18кВт)', '2,15', '0,01375 (16кВт)', '0,89'],
        ];

        $ovObjects = ObjectsOv::find()->where(['objects_id' => $ovPks])
//            ->andWhere(['or', ['>', 'specific_thermal_characteristic_ventilation', 0], ['>', 'ventilation_load', 0]])
            ->all();

        $tehPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_TECH])->all(), 'id');

        /** @var $tehObjects ObjectsTech[] */
        $tehObjects = ObjectsTech::find()->where(['objects_id' => $tehPks])->all();

//        $counter = 1;
//        foreach ($ovObjects as $object)
//        {
//            /** @var $object ObjectsOv */
//
//            $o8 = $object->objects->power_g * $object->objects->count_object * $object->objects->power_v * $object->objects->count_object;
//
//            $o11 = $object->objects->power_v/$model->calorific_value * 1000000 / $object->objects->count_object;
//
//            $ovQomax = $object->heating_load;
//            $ovQomax1 = $object->heating_load * 1163;
//
////            $sixData[] = [
////                $counter, $object->objects->name, "{$ovQomax} кВт {$ovQomax1}", "{$ovQvmax} кВт {$ovQvmax1}", $object->objects->count_object, $gvsQhm, $tehQh, 0, 0
////            ];
//
//            $counter++;
//        }

        // $counter = 1;
        // foreach ($tehObjects as $object){
            // $sixData[] = [
                // $counter, $object->objects->formula->getO1(), $object->formula->getTehQh(), $object->objects->formula->getO2(), $object->objects->formula->getO4(), $object->objects->formula->getO3(), $object->objects->formula->getO8(), $object->objects->formula->getO11(), $object->objects->formula->getO5()
            // ];
            // $counter++;
        // }

        $objGroups = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_TECH])->all(), 'number_group');

        $equipments = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $objGroups])->all();

        $counter = 1;
        foreach ($equipments as $eq) {

            $object = Objects::find()->where(['calculation_id' => $id, 'number_group' => $eq->number_group])->one();

            $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

            // var_dump($object);
            // exit;

            $tehQh = round($object->formula->getTehQh(), 4);
            $tehQhKvt = $tehQh * 1164;
            $eqO2 = ($eq->formula->getO2() * 100);
            $eqO2Kvt = $eqO2 * 1164;
            $o4 = $eq->formula->getO4();
            $o4Kvt = $o4 * 1164;
            $o3 = $eq->formula->getO3();
            $o3Kvt = $o3 * 1164;
            $poverG = $tehQh;
            $poverGKvt = $tehQh * 1164;
            $o5 = round($eq->formula->getO5(), 2);
            $o5Kvt = $o5 * 1164;

            $sixData[] = [
                $counter, $eq->name_equipment, [$tehQh, "{$tehQhKvt} кВт"], "{$eqO2}", "{$o4}", "{$o3}", [$poverG, "{$poverGKvt} кВт"], round($eq->formula->getO11(), 1), "{$o5}"
            ];

            $counter++;
        }


        $sixMergedColumns = [
            'A4:B4' => '№ п/п',
            'C4:E4' => 'Название',
            'F4:G4' => 'Ед. мощн. Гкал/час (кВт)',
            'H4:I4' => 'КПД %%',
            'J4:K4' => 'Кол-во',
            'L4:M4' => 'К.одн.',
            'N4:O4' => 'Сум. мощн. Гкал/час (кВт)',
            'P4:Q4' => 'Расх. топл. нм3/ч',
            'R4:S4' => 'Коэф. загр.'
        ];

        foreach ($sixMergedColumns as $coords => $title){
            $coord = explode(':', $coords)[0];
            $sixSheet->mergeCells($coords);
            $sixSheet->getStyle($coords)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sixSheet->getStyle($coords)->getFont()->setBold(2);
            $sixSheet->getStyle($coords)->applyFromArray($bordersStyle);
            $sixSheet->setCellValue($coord, $title);
        }

 
        $offset = 3;
        for ($i = 0; $i < count($sixData); $i++)
        {
        	$j = $i + 1;
            $v = $offset + ($j * 2);
            $v1 = $v + 1;
            $data = $sixData[$i];

            $sixSheet->getStyle("A{$v}:B{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("A{$v}:B{$v1}");
            $sixSheet->getStyle("C{$v}:E{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("C{$v}:E{$v1}");
            $sixSheet->getStyle("F{$v}:G{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("F{$v}:G{$v}");
            $sixSheet->getStyle("F{$v1}:G{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("F{$v1}:G{$v1}");
            $sixSheet->getStyle("H{$v}:I{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("H{$v}:I{$v}");
            $sixSheet->getStyle("H{$v1}:I{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("H{$v1}:I{$v1}");
            $sixSheet->getStyle("J{$v}:K{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("J{$v}:K{$v}");
            $sixSheet->getStyle("J{$v1}:K{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("J{$v1}:K{$v1}");
            $sixSheet->getStyle("L{$v}:M{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("L{$v}:M{$v}");
            $sixSheet->getStyle("L{$v1}:M{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("L{$v1}:M{$v1}");
            $sixSheet->getStyle("N{$v}:O{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("N{$v}:O{$v}");
            $sixSheet->getStyle("N{$v1}:O{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("N{$v1}:O{$v1}");
            $sixSheet->getStyle("P{$v}:Q{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("P{$v}:Q{$v1}");
            $sixSheet->getStyle("R{$v}:S{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("R{$v}:S{$v1}");

			$sixSheet->getStyle("A{$v}:B{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sixSheet->getStyle("C{$v}:E{$v1}")->getAlignment()->applyFromArray([
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sixSheet->getStyle("F{$v}:S{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);

            $sixSheet->setCellValue("A{$v}", $data[0]);
            $sixSheet->setCellValue("C{$v}", $data[1]);
            $sixSheet->setCellValue("F{$v}", $data[2][0]);
            $sixSheet->setCellValue("F{$v1}", $data[2][1]);
            $sixSheet->setCellValue("H{$v}", $data[3]);
            $sixSheet->setCellValue("H{$v1}", $data[3]);
            $sixSheet->setCellValue("J{$v}", $data[4]);
            $sixSheet->setCellValue("J{$v1}", $data[4]);
            $sixSheet->setCellValue("L{$v}", $data[5]);
            $sixSheet->setCellValue("L{$v1}", $data[5]);
            $sixSheet->setCellValue("N{$v}", $data[6][0]);
            $sixSheet->setCellValue("N{$v1}", $data[6][1]);
            $sixSheet->setCellValue("P{$v}", $data[7]);
            $sixSheet->setCellValue("R{$v}", $data[8]);
        }

        $sixSheet->mergeCells('A4:B4');
        $sixSheet->getStyle('A4:B4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $sixSheet->getRowDimension('4')->setRowHeight('50');
        $sixSheet->getStyle("A4:S4")->getAlignment()->setWrapText(true);
        $sixSheet->getStyle("A4:S4")->getAlignment()->applyFromArray([
            'horizontal' => Alignment::HORIZONTAL_CENTER,
            'vertical' => Alignment::VERTICAL_CENTER,
        ]);

        $sixSheetSecondHeaderOffset = 7 + (count($sixData) * 2);

        $sixSheet->mergeCells("A{$sixSheetSecondHeaderOffset}:S{$sixSheetSecondHeaderOffset}");
        $sixSheet->getStyle("A{$sixSheetSecondHeaderOffset}:A{$sixSheetSecondHeaderOffset}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sixSheet->setCellValue("A{$sixSheetSecondHeaderOffset}", 'Потребители топлива на технологические нужды');

        $sixData = [
//            ['1', 'Приготовление обеда (Teh3) (261 (365х(Teh6)/7) дней в году)', '7820 (Teh8) (Количество обедов в год) (Teh8.1)', '0,143 (Teh5) (на 1 обед) (Teh4_1)', '1 (Teh4) (на 1 обед) (Teh4_1)', '1,12 (Teh_Gut)', '7,82 (Teh_Qn) (0,03) (Teh_Qn/(365хTeh6/7))'],
//            ['2', 'Выпечка формового хлеба (313 дней в году)', '360 (Годовое производство хлеба, тонн)', '85,714 (на 1 тонну)', '600 (на 1 тонну)', '30,86', '216 (0,6901)'],
        ];

        $sixSheet->getRowDimension((3+$sixSheetSecondHeaderOffset))->setRowHeight('70');
        $sixSheet->getStyle('A'.(2+$sixSheetSecondHeaderOffset).':S'.(3+$sixSheetSecondHeaderOffset))->getAlignment()->setWrapText(true);
        $sixSheet->getStyle('A'.(2+$sixSheetSecondHeaderOffset).':S'.(3+$sixSheetSecondHeaderOffset))->getAlignment()->applyFromArray([
        	'horizontal' => Alignment::HORIZONTAL_CENTER,
        	'vertical' => Alignment::VERTICAL_CENTER,
        ]);

        $sixMergedColumns = [
            'A'.(2+$sixSheetSecondHeaderOffset).':B'.(3+$sixSheetSecondHeaderOffset) => '№ п/п',
            'C'.(2+$sixSheetSecondHeaderOffset).':E'.(3+$sixSheetSecondHeaderOffset) => 'Вид использования (продукция), суток в году',
            'F'.(2+$sixSheetSecondHeaderOffset).':G'.(3+$sixSheetSecondHeaderOffset) => 'Годовой выпуск (указать единицу измерения)',
            'H'.(2+$sixSheetSecondHeaderOffset).':L'.(3+$sixSheetSecondHeaderOffset) => 'Удельный расход топлива [кгу.т.] на единицу продукции',
            'M'.(2+$sixSheetSecondHeaderOffset).':N'.(3+$sixSheetSecondHeaderOffset) => 'то же, в тепловых единицах [тыс.ккал](ГДж)',
            'O'.(2+$sixSheetSecondHeaderOffset).':P'.(3+$sixSheetSecondHeaderOffset) => 'Годовая потребность в условном топливе [тут/год]',
            'Q'.(2+$sixSheetSecondHeaderOffset).':S'.(3+$sixSheetSecondHeaderOffset) => 'то же, в тепловых единицах [Гкал/год]/(Гкал/сут)',
        ];

        foreach ($sixMergedColumns as $coords => $title){
            $coord = explode(':', $coords)[0];
            $sixSheet->mergeCells($coords);
            $sixSheet->getStyle($coords)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sixSheet->getStyle($coords)->getFont()->setBold(2);
            $sixSheet->getStyle($coords)->applyFromArray($bordersStyle);
            $sixSheet->setCellValue($coord, $title);
        }

        $tehPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $model->id, 'type_object' => Objects::TYPE_TECH])->all(), 'id');

        /** @var $tehObjects ObjectsTech[] */
        $tehObjects = ObjectsTech::find()->where(['objects_id' => $tehPks])->all();

        $sumTehQh = 0;
        $sumTehQh1 = 0;

        $sumTehGn = 0;

        foreach ($tehObjects as $object)
        {
            $sumTehQh += $object->formula->getTehQh();
            $sumTehQh1 += $object->formula->getTehQh1();
            $sumTehGn += $object->formula->getTehGn();
        }

        $gvsPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $model->id, 'type_object' => Objects::TYPE_GVS])->all(), 'id');

        /** @var $gvsObjects ObjectsGvs[] */
        $gvsObjects = ObjectsGvs::find()->where(['objects_id' => $gvsPks])->all();

        $sumGvsQhm = 0;
        $sumGvsQhm1 = 0;

        $sumGvsGn = 0;

        $sumGvsQh = 0;

        foreach ($gvsObjects as $object)
        {
            $sumGvsQhm += $object->formula->getGvsQhm();
            $sumGvsQhm1 += $object->formula->getGvsQhm1();

            $sumGvsGn += $object->formula->getGvsGn();

            $sumGvsQh += $object->formula->getGvsQh();
        }

        $ovPks = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $model->id, 'type_object' => Objects::TYPE_OV])->all(), 'id');

        /** @var $ovObjects ObjectsOv[] */
        $ovObjects = ObjectsOv::find()->where(['objects_id' => $ovPks])->all();

        $ovQomax = 0;
        $ovQomax1 = 0;

        $ovQvmax = 0;
        $ovQvmax1 = 0;

        $sumOvGno = 0;
        $sumOvGnv = 0;

        $sumOvQo = 0;
        $sumOvQv = 0;

        foreach ($ovObjects as $object)
        {
            $ovQomax += $object->formula->getOvQomax();
            $ovQomax1 += $object->formula->getOvQomax1();

            $ovQvmax += $object->formula->getOvQvmax();
            $ovQvmax1 += $object->formula->getOvQvmax1();

            $sumOvGno += $object->formula->getOvGno();
            $sumOvGnv += $object->formula->getOvGnv();

            $sumOvQo += $object->formula->getQo();
            $sumOvQv += $object->formula->getQv();
        }

        $objectsNames = implode(', ', ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $model->id])->all(), 'name'));



        $sixData = [
            // ['1', $objectsNames, $ovQomax, $ovQvmax, $sumGvsQhm, $sumTehQh, '7,82 (Teh_Qn) (0,03) (Teh_Qn/(365хTeh6/7))'],
//            ['2', 'Выпечка формового хлеба (313 дней в году)', '360 (Годовое производство хлеба, тонн)', '85,714 (на 1 тонну)', '600 (на 1 тонну)', '30,86', '216 (0,6901)'],
        ];

        $counter = 1;
        foreach ($tehObjects as $object) {
            // $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

            $tehQn = $object->formula->getTehQn();
            $tehQnMul = round($object->formula->getTehQn() / (365 * $object->formula->getTeh6() / 7), 4);

            $teh3Mul = round(365 * $object->formula->getTeh6() / 7);

            $sixData[] = [
                $counter, [$object->formula->getTeh3(), "($teh3Mul дней в году)"], [$object->formula->getTeh8(), "(Количество обедов в год)"],
                [$object->formula->getTeh5(), "(на 1 обед)"], [$object->consumption_thermal_units, "(на 1 обед)"], round($object->formula->getTehGut(), 2),
                [$tehQn, $tehQnMul]
            ];
            $counter++;
        }


        $offset = 2;
        for ($i = 0; $i < count($sixData); $i++)
        {
        	$j = $i + 1;
            $v = $offset + ($j * 2) + $sixSheetSecondHeaderOffset;
            $v1 = $v + 1;
            $data = $sixData[$i];

            $sixSheet->getStyle("A{$v}:B{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("A{$v}:B{$v1}");
            $sixSheet->getStyle("A{$v}:B{$v1}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("C{$v}:E{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("C{$v}:E{$v}");
            $sixSheet->getStyle("C{$v}:E{$v}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("C{$v1}:E{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("C{$v1}:E{$v1}");
            $sixSheet->getStyle("C{$v1}:E{$v1}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("F{$v}:G{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("F{$v}:G{$v}");
            $sixSheet->getStyle("F{$v}:G{$v}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("F{$v1}:G{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("F{$v1}:G{$v1}");
            $sixSheet->getStyle("F{$v1}:G{$v1}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("H{$v}:L{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("H{$v}:L{$v}");
            $sixSheet->getStyle("H{$v}:L{$v}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("H{$v1}:L{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("H{$v1}:L{$v1}");
            $sixSheet->getStyle("H{$v1}:L{$v1}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("M{$v}:N{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("M{$v}:N{$v}");
            $sixSheet->getStyle("M{$v}:N{$v}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("M{$v1}:N{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("M{$v1}:N{$v1}");
            $sixSheet->getStyle("M{$v1}:N{$v1}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("O{$v}:P{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("O{$v}:P{$v1}");
            $sixSheet->getStyle("O{$v}:P{$v1}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("Q{$v}:S{$v}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("Q{$v}:S{$v}");
            $sixSheet->getStyle("Q{$v}:S{$v}")->getAlignment()->setWrapText(true);
            $sixSheet->getStyle("Q{$v1}:S{$v1}")->applyFromArray($bordersStyle);
            $sixSheet->mergeCells("Q{$v1}:S{$v1}");
            $sixSheet->getStyle("Q{$v1}:S{$v1}")->getAlignment()->setWrapText(true);

			$sixSheet->getStyle("A{$v}:B{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sixSheet->getStyle("C{$v}:E{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $sixSheet->getStyle("F{$v}:S{$v1}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);

            $sixSheet->getRowDimension($v)->setRowHeight('40');
            $sixSheet->getRowDimension($v1)->setRowHeight('40');

            $sixSheet->setCellValue("A{$v}", $data[0]);
            $sixSheet->setCellValue("C{$v}", $data[1][0]);
            $sixSheet->setCellValue("C{$v1}", $data[1][1]);
            $sixSheet->setCellValue("F{$v}", $data[2][0]);
            $sixSheet->setCellValue("F{$v1}", $data[2][1]);
            $sixSheet->setCellValue("H{$v}", $data[3][0]);
            $sixSheet->setCellValue("H{$v1}", $data[3][1]);
            $sixSheet->setCellValue("M{$v}", $data[4][0]);
            $sixSheet->setCellValue("M{$v1}", $data[4][1]);
            $sixSheet->setCellValue("O{$v}", $data[5]);
            $sixSheet->setCellValue("Q{$v}", $data[6][0]);
            $sixSheet->setCellValue("Q{$v1}", $data[6][1]);
        }


        $sixSheet->getStyle('A1:S1000')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);
        $sixSheet->getStyle('A1')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);
        $sixSheet->getStyle('A2')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);
        $sixSheet->getStyle("A{$sixSheetSecondHeaderOffset}")->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $tSheet = $spreadsheet->createSheet();

        $tSheet->getColumnDimension('A')->setWidth(6.6);
        $tSheet->getColumnDimension('B')->setWidth(6.6);
        $tSheet->getColumnDimension('C')->setWidth(6.6);
        $tSheet->getColumnDimension('D')->setWidth(6.6);
        $tSheet->getColumnDimension('E')->setWidth(6.6);
        $tSheet->getColumnDimension('F')->setWidth(6.6);
        $tSheet->getColumnDimension('G')->setWidth(6.6);
        $tSheet->getColumnDimension('H')->setWidth(6.6);
        $tSheet->getColumnDimension('I')->setWidth(6.6);
        $tSheet->getColumnDimension('J')->setWidth(6.6);
        $tSheet->getColumnDimension('K')->setWidth(6.6);
        $tSheet->getColumnDimension('L')->setWidth(6.6);
        $tSheet->getColumnDimension('M')->setWidth(6.6);
        $tSheet->getColumnDimension('N')->setWidth(6.6);

        $tSheet->getStyle('A1:S1000')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);
        $tSheet->getStyle('A1')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);
        $tSheet->getStyle('A2')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);
        $tSheet->setTitle('Приложение 3');
        $tSheet->setCellValue('A1', '- Приложение 3 -');
        $tSheet->getStyle('A1:N1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $tSheet->setCellValue('A2', 'Расчет помесячного распределения потребности в топливе');
        $tSheet->getStyle('A2:N2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $tSheet->mergeCells('A1:N1');
        $tSheet->mergeCells('A2:N2');

        // $column3Sum = $model->duration_op1 + $model->duration_op2 + $model->duration_op3 + $model->duration_op4 + $model->duration_op5 + $model->duration_op6 + $model->duration_op7 + $model->duration_op8 + $model->duration_op9 + $model->duration_op10 + $model->duration_op11 + $model->duration_op12;

        $column3Sum = $model->formula->getK5();

        $monthDaysCount = [
        	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        ];

        // $column4_1 = $model->duration_op1;
        // $column4_2 = $model->duration_op2;
        // $column4_3 = $model->duration_op3;
        // $column4_4 = $model->duration_op4;
        // $column4_5 = $model->duration_op5;
        // $column4_6 = $model->duration_op6;
        // $column4_7 = $model->duration_op7;
        // $column4_8 = $model->duration_op8;
        // $column4_9 = $model->duration_op9;
        // $column4_10 = $model->duration_op10;
        // $column4_11 = $model->duration_op11;
        // $column4_12 = $model->duration_op12;

        $column4_1 = $monthDaysCount[0] - $model->duration_op1;
        $column4_2 = $monthDaysCount[1] - $model->duration_op2;
        $column4_3 = $monthDaysCount[2] - $model->duration_op3;
        $column4_4 = $monthDaysCount[3] - $model->duration_op4;
        $column4_5 = $monthDaysCount[4] - $model->duration_op5;
        $column4_6 = $monthDaysCount[5] - $model->duration_op6;
        $column4_7 = $monthDaysCount[6] - $model->duration_op7;
        $column4_8 = $monthDaysCount[7] - $model->duration_op8;
        $column4_9 = $monthDaysCount[8] - $model->duration_op9;
        $column4_10 = $monthDaysCount[9] - $model->duration_op10;
        $column4_11 = $monthDaysCount[10] - $model->duration_op11;
        $column4_12 = $monthDaysCount[11] - $model->duration_op12;


        // $column4Sum = $column4_1 + $column4_2 + $column4_3 + $column4_4 + $column4_5 + $column4_6 + $column4_7 + $column4_8 + $column4_9 + $column4_10 + $column4_11 + $column4_12;
        $column4Sum = 365 - $column3Sum;


        $column5_1 = $model->duration_op1 * (18 - $model->temp_average1);
        $column5_2 = $model->duration_op2 * (18 - $model->temp_average2);
        $column5_3 = $model->duration_op3 * (18 - $model->temp_average3);
        $column5_4 = $model->duration_op4 * (18 - $model->temp_average4);
        $column5_5 = $model->duration_op5 * (18 - $model->temp_average5);
        $column5_6 = $model->duration_op6 * (18 - $model->temp_average6);
        $column5_7 = $model->duration_op7 * (18 - $model->temp_average7);
        $column5_8 = $model->duration_op8 * (18 - $model->temp_average8);
        $column5_9 = $model->duration_op9 * (18 - $model->temp_average9);
        $column5_10 = $model->duration_op10 * (18 - $model->temp_average10);
        // $column5_10 = 126;
        $column5_11 = $model->duration_op11 * (18 - $model->temp_average11);
        $column5_12 = $model->duration_op12 * (18 - $model->temp_average12);

        $column5Sum = $column5_1 + $column5_2 + $column5_3 + $column5_4 + $column5_5 + $column5_6 + $column5_7 + $column5_8 + $column5_9 + $column5_10 + $column5_11 + $column5_12;

        $column6_1 = $model->duration_op1 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_2 = $model->duration_op2 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_3 = $model->duration_op3 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_4 = $model->duration_op4 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_5 = $model->duration_op5 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_6 = $model->duration_op6 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_7 = $model->duration_op7 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_8 = $model->duration_op8 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_9 = $model->duration_op9 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_10 = $model->duration_op10 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_11 = $model->duration_op11 * 50 + 1 * 40; // TODO: 1 — КОЛ 4
        $column6_12 = $model->duration_op12 * 50 + 1 * 40; // TODO: 1 — КОЛ 4

        $column6Sum = $column6_1 + $column6_2 + $column6_3 + $column6_4 + $column6_5 + $column6_6 + $column6_7 + $column6_8 + $column6_9 + $column6_10 + $column6_11 + $column6_12;

        $column7_1 = round($column5_1 / $column5Sum, 2);
        $column7_2 = round($column5_2 / $column5Sum, 2);
        $column7_3 = round($column5_3 / $column5Sum, 2);
        $column7_4 = round($column5_4 / $column5Sum, 2);
        $column7_5 = round($column5_5 / $column5Sum, 2);
        $column7_6 = round($column5_6 / $column5Sum, 2);
        $column7_7 = round($column5_7 / $column5Sum, 2);
        $column7_8 = round($column5_8 / $column5Sum, 2);
        $column7_9 = round($column5_9 / $column5Sum, 2);
        $column7_10 = round($column5_10 / $column5Sum, 2);
        $column7_11 = round($column5_11 / $column5Sum, 2);
        $column7_12 = round($column5_12 / $column5Sum, 2);


        $column8_1 = round($column6_1 / $column6Sum, 2);
        $column8_2 = round($column6_2 / $column6Sum, 2);
        $column8_3 = round($column6_3 / $column6Sum, 2);
        $column8_4 = round($column6_4 / $column6Sum, 2);
        $column8_5 = round($column6_5 / $column6Sum, 2);
        $column8_6 = round($column6_6 / $column6Sum, 2);
        $column8_7 = round($column6_7 / $column6Sum, 2);
        $column8_8 = round($column6_8 / $column6Sum, 2);
        $column8_9 = round($column6_9 / $column6Sum, 2);
        $column8_10 = round($column6_10 / $column6Sum, 2);
        $column8_11 = round($column6_11 / $column6Sum, 2);
        $column8_12 = round($column6_12 / $column6Sum, 2);

        $column9_1 = round(($model->duration_op1 + $column4_1)/365, 2);
        $column9_2 = round(($model->duration_op2 + $column4_2)/365, 2);
        $column9_3 = round(($model->duration_op3 + $column4_3)/365, 2);
        $column9_4 = round(($model->duration_op4 + $column4_4)/365, 2);
        $column9_5 = round(($model->duration_op5 + $column4_5)/365, 2);
        $column9_6 = round(($model->duration_op6 + $column4_6)/365, 2);
        $column9_7 = round(($model->duration_op7 + $column4_7)/365, 2);
        $column9_8 = round(($model->duration_op8 + $column4_8)/365, 2);
        $column9_9 = round(($model->duration_op9 + $column4_9)/365, 2);
        $column9_10 = round(($model->duration_op10 + $column4_10)/365, 2);
        $column9_11 = round(($model->duration_op11 + $column4_11)/365, 2);
        $column9_12 = round(($model->duration_op12 + $column4_12)/365, 2);


        $column10Sum = round($sumOvGno + $sumOvGnv, 2);

        $column10_1 = round($column10Sum * $column7_1, 2);
        $column10_2 = round($column10Sum * $column7_2, 2);
        $column10_3 = round($column10Sum * $column7_3, 2);
        $column10_4 = round($column10Sum * $column7_4, 2);
        $column10_5 = round($column10Sum * $column7_5, 2);
        $column10_6 = round($column10Sum * $column7_6, 2);
        $column10_7 = round($column10Sum * $column7_7, 2);
        $column10_8 = round($column10Sum * $column7_8, 2);
        $column10_9 = round($column10Sum * $column7_9, 2);
        $column10_10 = round($column10Sum * $column7_10, 2);
        $column10_11 = round($column10Sum * $column7_11, 2);
        $column10_12 = round($column10Sum * $column7_12, 2);


        $column11_1 = round($sumGvsGn * $column8_1, 2);
        $column11_2 = round($sumGvsGn * $column8_2, 2);
        $column11_3 = round($sumGvsGn * $column8_3, 2);
        $column11_4 = round($sumGvsGn * $column8_4, 2);
        $column11_5 = round($sumGvsGn * $column8_5, 2);
        $column11_6 = round($sumGvsGn * $column8_6, 2);
        $column11_7 = round($sumGvsGn * $column8_7, 2);
        $column11_8 = round($sumGvsGn * $column8_8, 2);
        $column11_9 = round($sumGvsGn * $column8_9, 2);
        $column11_10 = round($sumGvsGn * $column8_10, 2);
        $column11_11 = round($sumGvsGn * $column8_11, 2);
        $column11_12 = round($sumGvsGn * $column8_12, 2);

        $column12_1 = round($sumTehGn * $column9_1, 2);
        $column12_2 = round($sumTehGn * $column9_2, 2);
        $column12_3 = round($sumTehGn * $column9_3, 2);
        $column12_4 = round($sumTehGn * $column9_4, 2);
        $column12_5 = round($sumTehGn * $column9_5, 2);
        $column12_6 = round($sumTehGn * $column9_6, 2);
        $column12_7 = round($sumTehGn * $column9_7, 2);
        $column12_8 = round($sumTehGn * $column9_8, 2);
        $column12_9 = round($sumTehGn * $column9_9, 2);
        $column12_10 = round($sumTehGn * $column9_10, 2);
        $column12_11 = round($sumTehGn * $column9_11, 2);
        $column12_12 = round($sumTehGn * $column9_12, 2);

        $column13Sum = round($sumOvGno + $sumOvGnv + $sumGvsGn + $sumTehGn, 2);

        $column13_1 = round($column9_1 + $column10_1 + $column11_1 + $column12_1, 2);
        $column13_2 = round($column9_2 + $column10_2 + $column11_2 + $column12_2, 2);
        $column13_3 = round($column9_3 + $column10_3 + $column11_3 + $column12_3, 2);
        $column13_4 = round($column9_4 + $column10_4 + $column11_4 + $column12_4, 2);
        $column13_5 = round($column9_5 + $column10_5 + $column11_5 + $column12_5, 2);
        $column13_6 = round($column9_6 + $column10_6 + $column11_6 + $column12_6, 2);
        $column13_7 = round($column9_7 + $column10_7 + $column11_7 + $column12_7, 2);
        $column13_8 = round($column9_8 + $column10_8 + $column11_8 + $column12_8, 2);
        $column13_9 = round($column9_9 + $column10_9 + $column11_9 + $column12_9, 2);
        $column13_10 = round($column9_10 + $column10_10 + $column11_10 + $column12_10, 2);
        $column13_11 = round($column9_11 + $column10_11 + $column11_11 + $column12_11, 2);
        $column13_12 = round($column9_12 + $column10_12 + $column11_12 + $column12_12, 2);

        $column14_1 = round($column13_1 / $column13Sum * 100, 2).' %';
        $column14_2 = round($column13_2 / $column13Sum * 100, 2).' %';
        $column14_3 = round($column13_3 / $column13Sum * 100, 2).' %';
        $column14_4 = round($column13_4 / $column13Sum * 100, 2).' %';
        $column14_5 = round($column13_5 / $column13Sum * 100, 2).' %';
        $column14_6 = round($column13_6 / $column13Sum * 100, 2).' %';
        $column14_7 = round($column13_7 / $column13Sum * 100, 2).' %';
        $column14_8 = round($column13_8 / $column13Sum * 100, 2).' %';
        $column14_9 = round($column13_9 / $column13Sum * 100, 2).' %';
        $column14_10 = round($column13_10 / $column13Sum * 100, 2).' %';
        $column14_11 = round($column13_11 / $column13Sum * 100, 2).' %'; 

        $column14_12 = round($column13_12 / $column13Sum * 100, 2).' %';

        $tData = [
            ['Месяц', 'Средняя температура воздуха, tн, °С', 'Продолжительность отопительного периода, nо, сут', 'Продол житель ность меж отопи тель ного пери ода, nм, сут', 'Градусо сутки отопи тельно го периода, при средней темпера туре внутри отапли ваемых помещений', 'Градусо сутки периода ГВС, в холод ный период tхв=5°С, в теплый tхв=15°С', 'Приведен ный коэф фициент ОВ /проп/', 'Приведен ный коэф фициент ГВС /проп/', 'Приведен ный коэф фициент для ТО /проп/', 'Расход природного газа на нужды отопления, тыс.нм3', 'Расход природ ного газа на нужды ГВС, тыс.нм3', 'Расход природ ного газа для работы газовых плит, тыс.нм3', 'Общая потреб ность в природ ном газе в целом, тыс.нм3', 'Доля от годового потреб ления в целом %'],
        	['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14'],
            ['1', $model->temp_average1, $model->duration_op1, $column4_1, $column5_1, $column6_1, $column7_1, $column8_1, $column9_1, $column10_1, $column11_1, $column12_1, $column13_1, $column14_1],
            ['2', $model->temp_average2, $model->duration_op2, $column4_2, $column5_2, $column6_2, $column7_2, $column8_2, $column9_2, $column10_2, $column11_2, $column12_2, $column13_2, $column14_2],
            ['3', $model->temp_average3, $model->duration_op3, $column4_3, $column5_3, $column6_3, $column7_3, $column8_3, $column9_3, $column10_3, $column11_3, $column12_3, $column13_3, $column14_3],
            ['4', $model->temp_average4, $model->duration_op4, $column4_4, $column5_4, $column6_4, $column7_4, $column8_4, $column9_4, $column10_4, $column11_4, $column12_4, $column13_4, $column14_4],
            ['5', $model->temp_average5, $model->duration_op5, $column4_5, $column5_5, $column6_5, $column7_5, $column8_5, $column9_5, $column10_5, $column11_5, $column12_5, $column13_5, $column14_5],
            ['6', $model->temp_average6, $model->duration_op6, $column4_6, $column5_6, $column6_6, $column7_6, $column8_6, $column9_6, $column10_6, $column11_6, $column12_6, $column13_6, $column14_6],
            ['7', $model->temp_average7, $model->duration_op7, $column4_7, $column5_7, $column6_7, $column7_7, $column8_7, $column9_7, $column10_7, $column11_7, $column12_7, $column13_7, $column14_7],
            ['8', $model->temp_average8, $model->duration_op8, $column4_8, $column5_8, $column6_8, $column7_8, $column8_8, $column9_8, $column10_8, $column11_8, $column12_8, $column13_8, $column14_8],
            ['9', $model->temp_average9, $model->duration_op9, $column4_9, $column5_9, $column6_9, $column7_9, $column8_9, $column9_9, $column10_9, $column11_9, $column12_9, $column13_9, $column14_9],
            ['10', $model->temp_average10, $model->duration_op10, $column4_10, $column5_10, $column6_10, $column7_10, $column8_10, $column9_10, $column10_10, $column11_10, $column12_10, $column13_10, $column14_10],
            ['11', $model->temp_average11, $model->duration_op11, $column4_11, $column5_11, $column6_11, $column7_11, $column8_11, $column9_11, $column10_11, $column11_11, $column12_11, $column13_11, $column14_11],
            ['12', $model->temp_average12, $model->duration_op12, $column4_12, $column5_12, $column6_12, $column7_12, $column8_12, $column9_12, $column10_12, $column11_12, $column12_12, $column13_12, $column14_12],
            // ['ГОД', 0.9, $column3Sum, $column4Sum, $column5Sum, $column6Sum, 1, 1, 1, $column10Sum, $sumGvsGn, $sumTehGn, $column13Sum, '100%'],
            ['ГОД', 0.9, $column3Sum, $column4Sum, $column5Sum, $column6Sum, 1, 1, 1, $column10Sum, $sumGvsGn, $sumTehGn, $column13Sum, '100%'],
        ];


        $offset = 2;
        $j = 0;
        for ($i = 0; $i < count($tData); $i++) {
            // $j++;
            $j++;
            if($i == 0){
            	$j++;
            }
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $tData[$i];

            $tSheet->getStyle("A{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("A{$v}")->getAlignment()->setWrapText(true);;
            $tSheet->setCellValue("A{$v}", $data[0]);

            $tSheet->getStyle("B{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("B{$v}")->getAlignment()->setWrapText(true);;
            $tSheet->setCellValue("B{$v}", $data[1]);

            $tSheet->getStyle("C{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("C{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("C{$v}", $data[2]);

            $tSheet->getStyle("D{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("D{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("D{$v}", $data[3]);

            $tSheet->getStyle("E{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("E{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("E{$v}", $data[4]);

            $tSheet->getStyle("F{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("F{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("F{$v}", $data[5]);

            $tSheet->getStyle("G{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("G{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("G{$v}", $data[6]);

            $tSheet->getStyle("H{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("H{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("H{$v}", $data[7]);

            $tSheet->getStyle("I{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("I{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("I{$v}", $data[8]);

            $tSheet->getStyle("J{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("J{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("J{$v}", $data[9]);

            $tSheet->getStyle("K{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("K{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("K{$v}", $data[10]);

            $tSheet->getStyle("L{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("L{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("L{$v}", $data[11]);

            $tSheet->getStyle("M{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("M{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("M{$v}", $data[12]);

            $tSheet->getStyle("N{$v}")->applyFromArray($bordersStyle);
            $tSheet->getStyle("N{$v}")->getAlignment()->setWrapText(true);
            $tSheet->setCellValue("N{$v}", $data[13]);

            $tSheet->getStyle("A{$v}:N{$v}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_CENTER,
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);

            if($i == (count($tData)-1)){
            	$tSheet->getStyle("A{$v}:N{$v}")->applyFromArray([
	            	'font' => [
	            		'bold' => true,
	            	],
            	]);
            }
        }

        $eightSheet = $spreadsheet->createSheet();
        $eightSheet->setTitle('Общие вопросы');
        $eightSheet->setCellValue('A6', '1. Общие вопросы');
        $eightSheet->mergeCells('A6:B6');
        $eightSheet->getStyle('A6:B6')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $eightSheet->getColumnDimension('A')->setWidth(50);
        $eightSheet->getColumnDimension('B')->setWidth(40);

        $eightSheet->getStyle('A1:B100')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);

        $eightSheet->getStyle('A6')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $eightSheet->setCellValue('B1', 'Заявление по форме утв. приложением к Порядку оформления решений об установлении видов топлива (Приказ Минэкономразвития РФ, Минэнерго РФ и ОАО "Газпром" от 15 октября 2002 г. N 333/358/101)');
        $eightSheet->getStyle('B1')->getAlignment()->setWrapText(true);

        $zak1 = $model->formula->getZak1();
        $zak2 = $model->formula->getZak2();
        $zak4 = $model->formula->getZak4();
        $zak5 = $model->formula->getZak5();
        $zak7 = $model->formula->getZak7();

        $allGutResult = $allGut / 1000;


        $eightData = [
            ["Предприятие (котельная) и его местонахождение (республика, область, населенный пункт)", [$zak1, $zak2, $zak5]],
            ["Готовность предприятия к использованию топливноэнергетических(ресурсов)(действующее, реконструируемое, строящееся, проектируемое", ''],
            ['Документы согласования (дата, номер, наименование организации) об использовании природного газа', ''],
            ['Заключение добывающих (производящих) уголь, торф, сланец и дрова предприятий, объединений, ассоциаций, концернов', ''],
            ['На основании какого документа проектируется строится, расширяется, реконструируется предприятие, организация', 'теплотехнический расчет выполнен онлайн сервисом https://gigacalorie.ru'],
            ['Вид и количество (тыс. т у.т.) используемого в настоящее время топлива и на основании какого документа (дата, номер, установленный расход), для твердого топлива указать его месторождение', ''],
            ["Вид запрашиваемого топлива, общий годовой расход (тыс. т у.т.) и год начала потребления", [$zak5, $allGutResult, $zak7]],
            ['Год выхода предприятия, организации на проектную мощность, общий годовой расход (тыс. т у.т.) в этом году', ''],
        ];

        $offset = 7;
        $j = 0;
        $plusCoef = 0;
        for ($i = 0; $i < count($eightData); $i++)
        {
            $j++;
            $v = $offset + $j + $plusCoef;
            $v2 = ($offset + $j) + 6;
            $data = $eightData[$i];

            if($i == 0){
            	$v3 = $v + 2;
            	$plusCoef = 2;
            	$eightSheet->mergeCells("A{$v}:A{$v3}");
            	$eightSheet->getStyle("A{$v}:A{$v3}")->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("A{$v}:A{$v3}")->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("A{$v}", $data[0]);
            } else if($i == 6) {
            	$v3 = $v + 2;
            	$plusCoef = 4;
            	$eightSheet->mergeCells("A{$v}:A{$v3}");
            	$eightSheet->getStyle("A{$v}:A{$v3}")->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("A{$v}:A{$v3}")->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("A{$v}", $data[0]);
            } else {
            	$eightSheet->getStyle("A{$v}")->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("A{$v}")->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("A{$v}", $data[0]);
            }

            if($i == 0){
            	$eightSheet->getStyle("B{$v}")->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B{$v}")->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B{$v}", $data[1][0]);

            	$eightSheet->getStyle("B".($v + 1))->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B".($v + 1))->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B".($v + 1), $data[1][1]);

            	$eightSheet->getStyle("B".($v + 2))->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B".($v + 2))->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B".($v + 2), $data[1][2]);
            } else if($i == 6) {
            	$eightSheet->getStyle("B{$v}")->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B{$v}")->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B{$v}", $data[1][0]);

            	$eightSheet->getStyle("B".($v + 1))->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B".($v + 1))->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B".($v + 1), $data[1][1]);

            	$eightSheet->getStyle("B".($v + 2))->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B".($v + 2))->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B".($v + 2), $data[1][2]);
            } else {
            	$eightSheet->getStyle("B{$v}")->applyFromArray($bordersStyle);
            	$eightSheet->getStyle("B{$v}")->getAlignment()->setWrapText(true);
            	$eightSheet->setCellValue("B{$v}", $data[1]);
            }

            $eightSheet->getStyle("A{$v}")->getAlignment()->applyFromArray([
            	'vertical' => Alignment::VERTICAL_CENTER,
            ]);
            $eightSheet->getStyle("B{$v}")->getAlignment()->applyFromArray([
            	'horizontal' => Alignment::HORIZONTAL_LEFT,
            ]);
        }


        $tenSheet = $spreadsheet->createSheet();

        $tenSheet->getStyle('A1:R1000')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);

        $tenSheet->getStyle('A1')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $tenSheet->getStyle('A2')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $tenSheet->getColumnDimension('A')->setWidth(8);
        $tenSheet->getColumnDimension('B')->setWidth(8);
        $tenSheet->getColumnDimension('C')->setWidth(5);
        $tenSheet->getColumnDimension('D')->setWidth(5);
        $tenSheet->getColumnDimension('E')->setWidth(5);
        $tenSheet->getColumnDimension('F')->setWidth(5);
        $tenSheet->getColumnDimension('G')->setWidth(5);
        $tenSheet->getColumnDimension('H')->setWidth(5);
        $tenSheet->getColumnDimension('I')->setWidth(5);
        $tenSheet->getColumnDimension('J')->setWidth(5);
        $tenSheet->getColumnDimension('K')->setWidth(5);
        $tenSheet->getColumnDimension('L')->setWidth(5);
        $tenSheet->getColumnDimension('M')->setWidth(5);
        $tenSheet->getColumnDimension('N')->setWidth(5);
        $tenSheet->getColumnDimension('O')->setWidth(4);
        $tenSheet->getColumnDimension('P')->setWidth(4);
        $tenSheet->getColumnDimension('Q')->setWidth(4);
        $tenSheet->getColumnDimension('R')->setWidth(4);

        $tenSheet->getRowDimension('8')->setRowHeight('45');

        $tenSheet->getStyle("A5:R8")->getAlignment()->applyFromArray([
        	'horizontal' => Alignment::HORIZONTAL_CENTER,
        	'vertical' => Alignment::VERTICAL_CENTER,
        ]);

        $tenSheet->setTitle('Котельные установки и ТЭЦ');
        $tenSheet->setCellValue('A1', '2. Котельные установки и ТЭЦ');
        $tenSheet->mergeCells('A1:R1');
        $tenSheet->getStyle('A1:R1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $tenSheet->setCellValue('A2', 'а) потребность в теплоэнергии');
        $tenSheet->mergeCells('A2:R2');
        $tenSheet->getStyle('A2:R2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $tenSheet->mergeCells('A5:B8');
        $tenSheet->getStyle('A5:B8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("A5:B8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('A5', 'На какие нужды');

        $tenSheet->mergeCells('C5:F6');
        $tenSheet->getStyle('C5:F6')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("C5:F6")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('C5', 'Присоединенная максимальная(тепловая) нагрузка (Гкал/час)');

        $tenSheet->mergeCells('C7:D8');
        $tenSheet->getStyle('C7:D8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("C7:D8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('C7', 'существующая');

        $tenSheet->mergeCells('E7:F8');
        $tenSheet->getStyle('E7:F8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("E7:F8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('E7', 'проектиру емая (включая существ)');

        $tenSheet->mergeCells('G5:H8');
        $tenSheet->getStyle('G5:H8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("G5:H8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('G5', 'Количество часов работы в году');

        $tenSheet->mergeCells('I5:L6');
        $tenSheet->getStyle('I5:L6')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("I5:L6")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('I5', 'Годовая потребность в тепле (тыс. Гкал)');

        $tenSheet->mergeCells('I7:J8');
        $tenSheet->getStyle('I7:J8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("I7:J8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('I7', 'Существующая');

        $tenSheet->mergeCells('K7:L8');
        $tenSheet->getStyle('K7:L8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("K7:L8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('K7', 'Проектируемая (включая существ)');

        $tenSheet->mergeCells('M5:R6');
        $tenSheet->getStyle('M5:R6')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("M5:R6")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('M5', 'Покрытие потребности в тепле (тыс. Гкал/год)');

        $tenSheet->mergeCells('M7:N8');
        $tenSheet->getStyle('M7:N8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("M7:N8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('M7', 'Котельная (ТЭЦ)');

        // $tenSheet->mergeCells('K7:K8');
        // $tenSheet->getStyle('K7:K8')->applyFromArray($bordersStyle);
        // $tenSheet->getStyle("K7:K8")->getAlignment()->setWrapText(true);
        // $tenSheet->setCellValue('K7', 'Котельная (ТЭЦ)');

        $tenSheet->mergeCells('O7:P8');
        $tenSheet->getStyle('O7:P8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("O7:P8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('O7', 'Вторичные энерго ресурсы');

        $tenSheet->mergeCells('Q7:R8');
        $tenSheet->getStyle('Q7:R8')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("Q7:R8")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('Q7', 'За счет других источников');

        $tenSheet->mergeCells('A9:B9');
        $tenSheet->getStyle('A9:B9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("A9:B9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('A9', '1');
        $tenSheet->mergeCells('C9:D9');
        $tenSheet->getStyle('C9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("C9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('C9', '2');
        $tenSheet->mergeCells('E9:F9');
        $tenSheet->getStyle('E9:F9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("E9:F9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('E9', '3');
        $tenSheet->mergeCells('G9:H9');
        $tenSheet->getStyle('G9:H9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("G9:H9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('G9', '4');
        $tenSheet->mergeCells('I9:J9');
        $tenSheet->getStyle('I9:J9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("I9:J9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('I9', '5');
        $tenSheet->mergeCells('K9:L9');
        $tenSheet->getStyle('K9:L9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("K9:L9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('K9', '6');
        $tenSheet->mergeCells('M9:N9');
        $tenSheet->getStyle('M9:N9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("M9:N9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('M9', '7');
        $tenSheet->mergeCells('O9:P9');
        $tenSheet->getStyle('O9:P9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("O9:P9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('O9', '8');
        $tenSheet->mergeCells('Q9:R9');
        $tenSheet->getStyle('Q9:R9')->applyFromArray($bordersStyle);
        $tenSheet->getStyle("Q9:R9")->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue('Q9', '9');

        $ovQoMul = $sumOvQo * 4.1868;
        $ovQvMul = round($sumOvQv, 1) * 4.1868;

        $sumGvsQhMul = round($sumGvsQh, 2) * 4.1868;

        $thirdSum = $ovQomax + $ovQvmax + $sumGvsQhm;
        $thirdSumKvt = $thirdSum * 4.1868;
        $sixSum = $sumOvQo + $sumOvQv + $sumGvsQh;
        $sixSumGdz = $sixSum * 4.1868;
        $sevenSum = ($column5Sum + $column6Sum) * 3;
        $sevenSumGdz = $sevenSum * 4.1868;

        $ovQomax = round($ovQomax, 3);
        $ovQomax1 = round($ovQomax1, 1);

        $ovQvmax = round($ovQvmax, 4);
        $ovQvmax1 = round($ovQvmax1, 1);


        $sumOvQo = round($sumOvQo, 3);
        $ovQoMul = round($ovQoMul, 2);

        $sumGvsQhm = round($sumGvsQhm, 5);


        // foreach ($objects as $) {
        	# code...
        // }

        $sumGvsQh = round($sumGvsQh, 2);
        $sumGvsQhMul = round($sumGvsQhMul, 2);

        $sumGvsQhm1 = round($sumGvsQhm1);

        $sumOvQv = round($sumOvQv, 1);
        $ovQvMul = round($ovQvMul, 2);

        $thirdGvsMulti = round(365 * $sumGvs5 / 7  * $sumGvs6 / 24);

        $k5 = $model->formula->getK5();


        // $tenData = [
        //     ['Отопление', '', "{$ovQomax} ({$ovQomax1} кВт)", ($k5 * 24), '', "{$sumOvQo} ({$ovQoMul} ГДж)", round($column5Sum + $column6Sum, 3), '', ''],
        //     ['Вентиляция', '', "{$ovQvmax} ({$ovQvmax1} кВт)", ($k5 * 24), '', "{$sumOvQv} ({$ovQvMul} ГДж)", round($column5Sum + $column6Sum, 3), '', ''],
        //     ['Горячее водоснабжение', '', "{$sumGvsQhm} ({$sumGvsQhm1} кВт)", $thirdGvsMulti, '', "({$sumGvsQh} ГДж) {$sumGvsQhMul}", $column5Sum + $column6Sum, '', ''],
        //     ['Технологические нужды (производственные нужды)', '', '0 (0 кВт)', '0', '', '0 (0 ГДж)', '0 (0 ГДж)', '', ''],
        //     ['Собственные нужды котельной (ТЭЦ)', '', '',    '', '', '', '', '', ''],
        //     ['Потери в тепловых сетях', '', '', '', '', '', '', '', ''],
        //     ['ИТОГО', '', $thirdSum, '', '', $sixSum, $sevenSum, '', ''],
        // ];

        

        $tenData = [
            ['Отопление', '', [$ovQomax, "{$ovQomax1} кВт"], ($k5 * 24), ['', ''], [$sumOvQo, "{$ovQoMul} ГДж"], [$sumOvQo, "{$ovQoMul} ГДж"], '', ''],
            ['Вентиляция', '', [$ovQvmax, "{$ovQvmax1} кВт"], ($k5 * 24), ['', ''], [$sumOvQv, "{$ovQvMul} ГДж"], [$sumOvQv, "{$ovQvMul} ГДж"], '', ''],
            ['Горячее водоснабжение', '', [$sumGvsQhm, "{$sumGvsQhm1} кВт"], $thirdGvsMulti, ['', ''], [$sumGvsQh, "{$sumGvsQhMul} ГДж"], [$sumGvsQh, "{$sumGvsQhMul} ГДж"], '', ''],
            ['Технологические нужды (производственные нужды)', '', [0, "0 кВт"], '0', ['', ''], [0, "0 ГДж"], [0, "0 ГДж"], '', ''],
            ['Собственные нужды котельной (ТЭЦ)', '', ['', ''], '', ['', ''], ['', ''], ['', ''], '', ''],
            ['Потери в тепловых сетях', '', ['', ''], '', ['', ''], ['', ''], ['', ''], '', ''],
            ['ИТОГО', '', [$thirdSum, "{$thirdSumKvt} кВт"], '', ['', ''], [$sixSum, "{$sixSumGdz} ГДж"], [$sixSum, "{$sixSumGdz} ГДж"], '', ''],
        ];

        $offset = 8;
        $j = 0;
        for ($i = 0; $i < count($tenData); $i++)
        {
            $j++;
            $j++;
            // $j++;
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $tenData[$i];

            $tenSheet->mergeCells("A{$v}:B{$v2}");
            $tenSheet->getStyle("A{$v}:B{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("A{$v}", $data[0]);

            $tenSheet->mergeCells("C{$v}:D{$v2}");
            $tenSheet->getStyle("C{$v}:D{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("C{$v}:D{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("C{$v}", $data[1]);

            $tenSheet->mergeCells("E{$v}:F{$v}");
            $tenSheet->getStyle("E{$v}:F{$v}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("E{$v}:F{$v}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("E{$v}", $data[2][0]);

            $tenSheet->mergeCells("E{$v2}:F{$v2}");
            $tenSheet->getStyle("E{$v2}:F{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("E{$v2}:F{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("E{$v2}", $data[2][1]);

            $tenSheet->mergeCells("G{$v}:H{$v2}");
            $tenSheet->getStyle("G{$v}:H{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("G{$v}:H{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("G{$v}", $data[3]);

            $tenSheet->mergeCells("I{$v}:J{$v}");
            $tenSheet->getStyle("I{$v}:J{$v}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("I{$v}:J{$v}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("I{$v}", $data[4][0]);

            $tenSheet->mergeCells("I{$v2}:J{$v2}");
            $tenSheet->getStyle("I{$v2}:J{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("I{$v2}:J{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("I{$v2}", $data[4][1]);

            $tenSheet->mergeCells("K{$v}:L{$v}");
            $tenSheet->getStyle("K{$v}:L{$v}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("K{$v}:L{$v}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("K{$v}", $data[5][0]);

            $tenSheet->mergeCells("K{$v2}:L{$v2}");
            $tenSheet->getStyle("K{$v2}:L{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("K{$v2}:L{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("K{$v2}", $data[5][1]);

            $tenSheet->mergeCells("M{$v}:N{$v}");
            $tenSheet->getStyle("M{$v}:N{$v}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("M{$v}:N{$v}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("M{$v}", $data[6][0]);

            $tenSheet->mergeCells("M{$v2}:N{$v2}");
            $tenSheet->getStyle("M{$v2}:N{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("M{$v2}:N{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("M{$v2}", $data[6][1]);

            $tenSheet->mergeCells("O{$v}:P{$v2}");
            $tenSheet->getStyle("O{$v}:P{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("O{$v}:P{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("O{$v}", $data[7]);

            $tenSheet->mergeCells("Q{$v}:R{$v2}");
            $tenSheet->getStyle("Q{$v}:R{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("Q{$v}:R{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("Q{$v}", $data[8]);

	        $tenSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->applyFromArray([
	        	'vertical' => Alignment::VERTICAL_CENTER,
	        ]);

	        $tenSheet->getStyle("C{$v}:R{$v2}")->getAlignment()->applyFromArray([
	        	'horizontal' => Alignment::HORIZONTAL_CENTER,
	        	'vertical' => Alignment::VERTICAL_CENTER,
	        ]);
        }

        $tenOffset = 9 + (count($tenData) * 2) + 4;
        $tenOffsetHeader = $tenOffset - 1;

        $tenSheet->setCellValue("A{$tenOffsetHeader}", 'б) состав и характеристика оборудования котельных, вид и годовой расход топлива');
        $tenSheet->mergeCells("A{$tenOffsetHeader}:R{$tenOffsetHeader}");
        $tenSheet->getStyle("A{$tenOffsetHeader}:R{$tenOffsetHeader}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);


        $tenSheet->getStyle('A'.$tenOffsetHeader)->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $tenSheet->getStyle("A".($tenOffset + 1).":R".($tenOffset + 4))->getAlignment()->applyFromArray([
	    	'horizontal' => Alignment::HORIZONTAL_CENTER,
	    	'vertical' => Alignment::VERTICAL_CENTER,
	    ]);

        $tenSheet->mergeCells("A".($tenOffset + 1).":B".($tenOffset + 2));
        $tenSheet->getStyle("A".($tenOffset + 1).":B".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("A".($tenOffset + 1).":B".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("A".($tenOffset + 1), 'Тип котлов (по группам)');

        $tenSheet->mergeCells("C".($tenOffset + 1).":D".($tenOffset + 2));
        $tenSheet->getStyle("C".($tenOffset + 1).":D".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("C".($tenOffset + 1).":D".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("C".($tenOffset + 1), 'Количество');

        $tenSheet->mergeCells("E".($tenOffset + 1).":F".($tenOffset + 2));
        $tenSheet->getStyle("E".($tenOffset + 1).":F".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("E".($tenOffset + 1).":F".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("E".($tenOffset + 1), 'Общая мощн. (Гкал/час)');

        $tenSheet->mergeCells("G".($tenOffset + 1).":L".($tenOffset + 1));
        $tenSheet->getStyle("G".($tenOffset + 1).":L".($tenOffset + 1))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("G".($tenOffset + 1).":L".($tenOffset + 1))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("G".($tenOffset + 1), 'Используемое топливо');

        $tenSheet->mergeCells("G".($tenOffset + 2).":H".($tenOffset + 2));
        $tenSheet->getStyle("G".($tenOffset + 2).":H".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("G".($tenOffset + 2).":H".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("G".($tenOffset + 2), 'Вид основного (резервогого)');

        $tenSheet->mergeCells("I".($tenOffset + 2).":J".($tenOffset + 2));
        $tenSheet->getStyle("I".($tenOffset + 2).":J".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("I".($tenOffset + 2).":J".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("I".($tenOffset + 2), 'Удельный расход кгу.т./Гкал');

        $tenSheet->mergeCells("K".($tenOffset + 2).":L".($tenOffset + 2));
        $tenSheet->getStyle("K".($tenOffset + 2).":L".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("K".($tenOffset + 2).":L".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("K".($tenOffset + 2), 'годовой расход тыс.т.у.');

        $tenSheet->mergeCells("M".($tenOffset + 1).":R".($tenOffset + 1));
        $tenSheet->getStyle("M".($tenOffset + 1).":R".($tenOffset + 1))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("M".($tenOffset + 1).":R".($tenOffset + 1))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("M".($tenOffset + 1), 'Запрашиваемое топливо');

        // $tenSheet->mergeCells("F38:G38");
        // $tenSheet->getStyle("F38:G38")->applyFromArray($bordersStyle);
        // $tenSheet->getStyle("F38:G38")->getAlignment()->setWrapText(true);
        // $tenSheet->setCellValue("F38", 'Вид основного (резервогого)');

        $tenSheet->mergeCells("M".($tenOffset + 2).":N".($tenOffset + 2));
        $tenSheet->getStyle("M".($tenOffset + 2).":N".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("M".($tenOffset + 2).":N".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("M".($tenOffset + 2), 'Вид основного (резервогого)');

        $tenSheet->mergeCells("O".($tenOffset + 2).":P".($tenOffset + 2));
        $tenSheet->getStyle("O".($tenOffset + 2).":P".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("O".($tenOffset + 2).":P".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("O".($tenOffset + 2), 'Удельный расход (кгу.т./Гкал)');

        $tenSheet->mergeCells("Q".($tenOffset + 2).":R".($tenOffset + 2));
        $tenSheet->getStyle("Q".($tenOffset + 2).":R".($tenOffset + 2))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("Q".($tenOffset + 2).":R".($tenOffset + 2))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("Q".($tenOffset + 2), 'Годовой расход тыс.т.у.т');

        $tenSheet->mergeCells("A".($tenOffset + 3).":B".($tenOffset + 3));
        $tenSheet->getStyle("A".($tenOffset + 3).":B".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("A".($tenOffset + 3).":B".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("A".($tenOffset + 3), '1');

		$tenSheet->mergeCells("C".($tenOffset + 3).":D".($tenOffset + 3));
        $tenSheet->getStyle("C".($tenOffset + 3).":D".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("C".($tenOffset + 3).":D".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("C".($tenOffset + 3), '2');

        $tenSheet->mergeCells("E".($tenOffset + 3).":F".($tenOffset + 3));
        $tenSheet->getStyle("E".($tenOffset + 3).":F".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("E".($tenOffset + 3).":F".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("E".($tenOffset + 3), '3');

        $tenSheet->mergeCells("G".($tenOffset + 3).":H".($tenOffset + 3));
        $tenSheet->getStyle("G".($tenOffset + 3).":H".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("G".($tenOffset + 3).":H".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("G".($tenOffset + 3), '4');

        $tenSheet->mergeCells("I".($tenOffset + 3).":J".($tenOffset + 3));
        $tenSheet->getStyle("I".($tenOffset + 3).":J".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("I".($tenOffset + 3).":J".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("I".($tenOffset + 3), '5');

        $tenSheet->mergeCells("K".($tenOffset + 3).":L".($tenOffset + 3));
        $tenSheet->getStyle("K".($tenOffset + 3).":L".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("K".($tenOffset + 3).":L".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("K".($tenOffset + 3), '6');

        $tenSheet->mergeCells("M".($tenOffset + 3).":N".($tenOffset + 3));
        $tenSheet->getStyle("M".($tenOffset + 3).":N".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("M".($tenOffset + 3).":N".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("M".($tenOffset + 3), '7');

        $tenSheet->mergeCells("O".($tenOffset + 3).":P".($tenOffset + 3));
        $tenSheet->getStyle("O".($tenOffset + 3).":P".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("O".($tenOffset + 3).":P".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("O".($tenOffset + 3), '8');

        $tenSheet->mergeCells("Q".($tenOffset + 3).":R".($tenOffset + 3));
        $tenSheet->getStyle("Q".($tenOffset + 3).":R".($tenOffset + 3))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("Q".($tenOffset + 3).":R".($tenOffset + 3))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("Q".($tenOffset + 3), '9');

        $tenSheet->mergeCells("A".($tenOffset + 4).":R".($tenOffset + 4));
        $tenSheet->getStyle("A".($tenOffset + 4).":R".($tenOffset + 4))->applyFromArray($bordersStyle);
        $tenSheet->getStyle("A".($tenOffset + 4).":R".($tenOffset + 4))->getAlignment()->setWrapText(true);
        $tenSheet->setCellValue("A".($tenOffset + 4), 'Устанавливаемые');

        $tenData = [
//            ['КС-16(О1) (Потр.№ 1)', '1 (О4)', '0,01376 (О8) (16 кВт)', '', '', '', 'газ ГОСТ 5542-87 (Zak5)', '158,7(О9)', '0,0039 (Ob_Gut/1000)'],
//            ['ВПГ-18 (Потр.№ 2)', '1 (О4)', '0,01376 (О8) (16 кВт)', '', '', '', 'газ ГОСТ 5542-87 (Zak5)', '158,7(О9)', '0,0039 (Ob_Gut/1000)'],
//            ['Действующие', '1 (О4)', '0,01376 (О8) (16 кВт)', '', '', '', 'газ ГОСТ 5542-87 (Zak5)', '158,7(О9)', '0,0039 (Ob_Gut/1000)'],
//            ['из них демонтируемые', '1 (О4)', '0,01376 (О8) (16 кВт)', '', '', '', 'газ ГОСТ 5542-87 (Zak5)', '158,7(О9)', '0,0039 (Ob_Gut/1000)'],
//            ['Резервные', '1 (О4)', '0,01376 (О8) (16 кВт)', '', '', '', 'газ ГОСТ 5542-87 (Zak5)', '158,7(О9)', '0,0039 (Ob_Gut/1000)'],
        ];

        /** @var $objects Objects[] */
        // $objects = Objects::find()->where(['calculation_id' => $model->id])->all();

        $objGroups = ArrayHelper::getColumn(Objects::find()->where(['type_object' => [Objects::TYPE_OV, Objects::TYPE_GVS], 'calculation_id' => $model->id])->all(), 'number_group');

        $equipments = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $objGroups])->all();

        $counter = 1;
        foreach($equipments as $equipment){

        	$poverG = $equipment->pover_g;
        	$poverGKvt = $poverG * 1163;

        	$o1 = $equipment->formula->getO1();

            $tenData[] = [
                "{$o1} (Потр.№{$counter})", $equipment->formula->getO4(), [$poverG, "$equipment->power_v кВт"], '', '', '', $model->formula->getZak5(), $equipment->formula->getO9(), $equipment->formula->getObGut(),
            ];

            $counter++;
        }

        $tenData = ArrayHelper::merge($tenData, [
        	['', '', ['', ''], '', '', '', '', '', ''],
        	['', '', ['', ''], '', '', '', '', '', ''],
        	['Действующие', '', ['', ''], '', '', '', '', '', ''],
        	['Из них демонтируемые', '', ['', ''], '', '', '', '', '', ''],
        	['Резервные', '', ['', ''], '', '', '', '', '', ''],
        ]);


        $offset = $tenOffset + 3;
        $j = 0;
        for ($i = 0; $i < count($tenData); $i++)
        {
            $j++;
            $j++;
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $tenData[$i];

            $tenSheet->mergeCells("A{$v}:B{$v2}");
            $tenSheet->getStyle("A{$v}:B{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("A{$v}", $data[0]);

            $tenSheet->mergeCells("C{$v}:D{$v2}");
            $tenSheet->getStyle("C{$v}:D{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("C{$v}:D{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("C{$v}", $data[1]);

            $tenSheet->mergeCells("E{$v}:F{$v}");
            $tenSheet->getStyle("E{$v}:F{$v}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("E{$v}:F{$v}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("E{$v}", $data[2][0]);

            $tenSheet->mergeCells("E{$v2}:F{$v2}");
            $tenSheet->getStyle("E{$v2}:F{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("E{$v2}:F{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("E{$v2}", $data[2][1]);

            $tenSheet->mergeCells("G{$v}:H{$v2}");
            $tenSheet->getStyle("G{$v}:H{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("G{$v}:H{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("G{$v}", $data[3]);

            $tenSheet->mergeCells("I{$v}:J{$v2}");
            $tenSheet->getStyle("I{$v}:J{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("I{$v}:J{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("I{$v}", $data[4]);

            $tenSheet->mergeCells("K{$v}:L{$v2}");
            $tenSheet->getStyle("K{$v}:L{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("K{$v}:L{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("K{$v}", $data[5]);

            $tenSheet->mergeCells("M{$v}:N{$v2}");
            $tenSheet->getStyle("M{$v}:N{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("M{$v}:N{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("M{$v}", $data[6]);

            $tenSheet->mergeCells("O{$v}:P{$v2}");
            $tenSheet->getStyle("O{$v}:P{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("O{$v}:P{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("O{$v}", $data[7]);

            $tenSheet->mergeCells("Q{$v}:R{$v2}");
            $tenSheet->getStyle("Q{$v}:R{$v2}")->applyFromArray($bordersStyle);
            $tenSheet->getStyle("Q{$v}:R{$v2}")->getAlignment()->setWrapText(true);
            $tenSheet->setCellValue("Q{$v}", $data[8]);

        	$tenSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->applyFromArray([
	    		// 'horizontal' => Alignment::HORIZONTAL_CENTER,
	    		'vertical' => Alignment::VERTICAL_CENTER,
	    	]);

        	$tenSheet->getStyle("C{$v}:R{$v2}")->getAlignment()->applyFromArray([
	    		'horizontal' => Alignment::HORIZONTAL_CENTER,
	    		'vertical' => Alignment::VERTICAL_CENTER,
	    	]);
        }


        $elevenSheet = $spreadsheet->createSheet();

        $elevenSheet->getColumnDimension('A')->setWidth(6.5);
        $elevenSheet->getColumnDimension('B')->setWidth(6.5);
        $elevenSheet->getColumnDimension('C')->setWidth(6.5);
        $elevenSheet->getColumnDimension('D')->setWidth(6.5);
        $elevenSheet->getColumnDimension('E')->setWidth(6.5);
        $elevenSheet->getColumnDimension('E')->setWidth(6.5);
        $elevenSheet->getColumnDimension('F')->setWidth(6.5);
        $elevenSheet->getColumnDimension('G')->setWidth(6.5);
        $elevenSheet->getColumnDimension('H')->setWidth(6.5);
        $elevenSheet->getColumnDimension('I')->setWidth(6.5);
        $elevenSheet->getColumnDimension('J')->setWidth(6.5);
        $elevenSheet->getColumnDimension('K')->setWidth(6.5);
        $elevenSheet->getColumnDimension('L')->setWidth(6.5);
        $elevenSheet->getColumnDimension('M')->setWidth(6.5);
        $elevenSheet->getColumnDimension('N')->setWidth(6.5);

        $elevenSheet->setTitle('Потребители тепла');
        $elevenSheet->setCellValue('A1', '3. Котельные установки и ТЭЦ');
        $elevenSheet->mergeCells('A1:N1');
        $elevenSheet->getStyle('A1:N1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $elevenSheet->getStyle('A1')->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $elevenSheet->mergeCells('A3:B4');
        $elevenSheet->setCellValue('A3', '№п/п');
        $elevenSheet->getStyle('A3:B4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('A3:B4')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('C3:D4');
        $elevenSheet->setCellValue('C3', 'Потребители тепла');
        $elevenSheet->getStyle('C3:D4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('C3:D4')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('E3:J3');
        $elevenSheet->setCellValue('E3', 'Максимальные тепловые нагрузки (Гкал/ч)');
        $elevenSheet->getStyle('E3:J3')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('E3:J3')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('E4:F4');
        $elevenSheet->setCellValue('E4', 'Отопление');
        $elevenSheet->getStyle('E4:F4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('E4:F4')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('G4:H4');
        $elevenSheet->setCellValue('G4', 'Вентиляция');
        $elevenSheet->getStyle('G4:H4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('G4:H4')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('I4:J4');
        $elevenSheet->setCellValue('I4', 'Горячее водоснабжение');
        $elevenSheet->getStyle('I4:J4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('I4:J4')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('K3:L4');
        $elevenSheet->setCellValue('K3', 'Технология');
        $elevenSheet->getStyle('K3:L4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('K3:L4')->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells('M3:N4');
        $elevenSheet->setCellValue('M3', 'Итого');
        $elevenSheet->getStyle('M3:N4')->applyFromArray($bordersStyle);
        $elevenSheet->getStyle('M3:N4')->getAlignment()->setWrapText(true);

        $elevenSheet->getStyle("A3:N4")->getAlignment()->applyFromArray([
	    	'horizontal' => Alignment::HORIZONTAL_CENTER,
	    	'vertical' => Alignment::VERTICAL_CENTER,
	    ]);

        $elevenData = [
            // ['1', $objectsNames, $ovQomax, $ovQvmax, $sumGvsQhm, $sumTehQh, '7,82 (Teh_Qn) (0,03) (Teh_Qn/(365хTeh6/7))'],
//            ['2', 'здание пекарни;(OV1/GVS1/Teh1)', '0,011(∑OV_Qomax) (12,8 кВт) (∑OV_Qomax1)', '0,0022(∑OV_Qvmax) (2,6 кВт) (∑OV_Qvmax1)', '0 (∑GVS_Qhm) (0 кВт) (∑GVS_Qhm1)', '0 (∑Teh_Qh) (0 кВт) (∑Teh_Qh1)', '0,0132 (Ob_Qhmax) (15,4кВт) (Ob_Qhmax1)'],
//            ['3', 'здание пекарни;(OV1/GVS1/Teh1)', '0,011(∑OV_Qomax) (12,8 кВт) (∑OV_Qomax1)', '0,0022(∑OV_Qvmax) (2,6 кВт) (∑OV_Qvmax1)', '0 (∑GVS_Qhm) (0 кВт) (∑GVS_Qhm1)', '0 (∑Teh_Qh) (0 кВт) (∑Teh_Qh1)', '0,0132 (Ob_Qhmax) (15,4кВт) (Ob_Qhmax1)'],
//            ['4', 'здание пекарни;(OV1/GVS1/Teh1)', '0,011(∑OV_Qomax) (12,8 кВт) (∑OV_Qomax1)', '0,0022(∑OV_Qvmax) (2,6 кВт) (∑OV_Qvmax1)', '0 (∑GVS_Qhm) (0 кВт) (∑GVS_Qhm1)', '0 (∑Teh_Qh) (0 кВт) (∑Teh_Qh1)', '0,0132 (Ob_Qhmax) (15,4кВт) (Ob_Qhmax1)'],
        ];

        $objGroups = ArrayHelper::getColumn(Objects::find()->where(['type_object' => [Objects::TYPE_OV, Objects::TYPE_GVS], 'calculation_id' => $model->id])->all(), 'number_group');

        $equipments = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $objGroups])->all();

        $counter = 1;
        foreach ($equipments as $eq) {
            $objects = Objects::find()->where(['calculation_id' => $id, 'number_group' => $eq->number_group])->all();

            $ovQoMax = 0;
            $ovQvMax = 0;
            $gvsQhm = 0;
            $tehQh = 0;

            $names = [];

            foreach ($objects as $object) {
                $names[] = $object->name;

                if($object->type_object == Objects::TYPE_OV)
                {
                    $object = ObjectsOv::find()->where(['objects_id' => $object->id])->one();


                    $ovQoMax += $object->formula->getOvQomax();
                    $ovQvMax += $object->formula->getOvQvmax();

                } else if($object->type_object == Objects::TYPE_GVS)
                {
                    $object = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();

                    $name = $object->formula->getGvs1();

                    $gvsQhm += $object->formula->getGvsQhm();

                } else if($object->type_object == Objects::TYPE_TECH)
                {
                    $object = ObjectsTech::find()->where(['objects_id' => $object->id])->one();

                    $name = $object->formula->getTeh1();

                    $tehQh += $object->formula->getTehQh();
                }

            }

            $ovQomax = round($ovQoMax, 3);
            $ovQomaxKvt = $ovQomax * 1164;

            $ovQvmax = round($ovQvMax, 4);
            $ovQvmaxKvt = $ovQvmax * 1164;

            $gvsQhm = round($gvsQhm, 5);
            $gvsQhmKvt = $gvsQhm * 1164;

            $tehQhKvt = $tehQh * 1164;

            $obQhmax = round($eq->formula->getObQhmax(), 5);
            $obQhmaxKvt = $obQhmax * 1164;


            $elevenData[] = [
                $counter, implode('; ', $names), [$ovQomax, "{$ovQomaxKvt} кВт"], [$ovQvmax, "{$ovQvmaxKvt} кВт"], [$gvsQhm, "{$gvsQhmKvt} кВт"], [$tehQh, "{$tehQhKvt} кВт"], "{$obQhmaxKvt} кВт"
            ];

            $counter++;
        }


        // VarDumper::dump($elevenData[0][1], 10, true);
        // exit;

        $offset = 3;
        $j = 0;
        for ($i = 0; $i < count($elevenData); $i++)
        {
            $j++;
            $j++;
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $elevenData[$i];

            // var_dump($data[1]);
            // exit;

            $elevenSheet->mergeCells("A{$v}:B{$v2}");
            $elevenSheet->getStyle("A{$v}:B{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("A{$v}", $data[0]);

            $elevenSheet->mergeCells("C{$v}:D{$v2}");
            $elevenSheet->getStyle("C{$v}:D{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("C{$v}:D{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("C{$v}", $data[1]);

            $elevenSheet->mergeCells("E{$v}:F{$v}");
            $elevenSheet->getStyle("E{$v}:F{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("E{$v}:F{$v}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("E{$v}", $data[2][0]);

            $elevenSheet->mergeCells("E{$v2}:F{$v2}");
            $elevenSheet->getStyle("E{$v2}:F{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("E{$v2}:F{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("E{$v2}", $data[2][1]);

            // $elevenSheet->mergeCells("B{$v}:C{$v2}");
            // $elevenSheet->getStyle("B{$v}:C{$v2}")->applyFromArray($bordersStyle);
            // $elevenSheet->getStyle("B{$v}:C{$v2}")->getAlignment()->setWrapText(true);
            // $elevenSheet->setCellValue("B{$v}", $data[3]);

            $elevenSheet->mergeCells("G{$v}:H{$v}");
            $elevenSheet->getStyle("G{$v}:H{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("G{$v}:H{$v}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("G{$v}", $data[3][0]);

            $elevenSheet->mergeCells("G{$v2}:H{$v2}");
            $elevenSheet->getStyle("G{$v2}:H{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("G{$v2}:H{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("G{$v2}", $data[3][1]);

            $elevenSheet->mergeCells("I{$v}:J{$v}");
            $elevenSheet->getStyle("I{$v}:J{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("I{$v}:J{$v}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("I{$v}", $data[4][0]);

            $elevenSheet->mergeCells("I{$v2}:J{$v2}");
            $elevenSheet->getStyle("I{$v2}:J{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("I{$v2}:J{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("I{$v2}", $data[4][1]);

            $elevenSheet->mergeCells("K{$v}:L{$v}");
            $elevenSheet->getStyle("K{$v}:L{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("K{$v}:L{$v}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("K{$v}", $data[5][0]);

            $elevenSheet->mergeCells("K{$v2}:L{$v2}");
            $elevenSheet->getStyle("K{$v2}:L{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("K{$v2}:L{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("K{$v2}", $data[5][1]);

            $elevenSheet->mergeCells("M{$v}:N{$v2}");
            $elevenSheet->getStyle("M{$v}:N{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("M{$v}:N{$v2}")->getAlignment()->setWrapText(true);
            $elevenSheet->setCellValue("M{$v}", $data[6]);

           	$elevenSheet->getStyle("A{$v}:N{$v2}")->getAlignment()->applyFromArray([
		    	'horizontal' => Alignment::HORIZONTAL_CENTER,
		    	'vertical' => Alignment::VERTICAL_CENTER,
		    ]);
        }

        $offsetHeader = 7 + (count($elevenData) * 2);
        $elevenOffset = $offsetHeader + 2;

        $elevenSheet->setCellValue("A{$offsetHeader}", '4. Потребность в тепле на производственные нужды');
        $elevenSheet->mergeCells("A{$offsetHeader}:N{$offsetHeader}");
        $elevenSheet->getStyle("A{$offsetHeader}:N{$offsetHeader}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $elevenSheet->getStyle('A'.$offsetHeader)->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $elevenSheet->mergeCells("A{$elevenOffset}:B".($elevenOffset + 1));
        $elevenSheet->setCellValue("A{$elevenOffset}", "№п/п");
        $elevenSheet->getStyle("A{$elevenOffset}:B".($elevenOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("A{$elevenOffset}:B".($elevenOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("C{$elevenOffset}:D".($elevenOffset + 1));
        $elevenSheet->setCellValue("C{$elevenOffset}", "Потребители тепла");
        $elevenSheet->getStyle("C{$elevenOffset}:D".($elevenOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("C{$elevenOffset}:D".($elevenOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("E{$elevenOffset}:F".($elevenOffset + 1));
        $elevenSheet->setCellValue("E{$elevenOffset}", "Наименование продукции");
        $elevenSheet->getStyle("E{$elevenOffset}:F".($elevenOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("E{$elevenOffset}:F".($elevenOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("G{$elevenOffset}:H".($elevenOffset + 1));
        $elevenSheet->setCellValue("G{$elevenOffset}", "Годовое количество продукции");
        $elevenSheet->getStyle("G{$elevenOffset}:H".($elevenOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("G{$elevenOffset}:H".($elevenOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("I{$elevenOffset}:K".($elevenOffset + 1));
        $elevenSheet->setCellValue("I{$elevenOffset}", "Удельный расход тепла на ед. продукции (Гкал)");
        $elevenSheet->getStyle("I{$elevenOffset}:K".($elevenOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("I{$elevenOffset}:K".($elevenOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("L{$elevenOffset}:N".($elevenOffset + 1));
        $elevenSheet->setCellValue("L{$elevenOffset}", "Годовое потебление тепла (тысГкал)");
        $elevenSheet->getStyle("L{$elevenOffset}:N".($elevenOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("L{$elevenOffset}:N".($elevenOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->getStyle("A{$elevenOffset}:N".($elevenOffset + 1))->getAlignment()->applyFromArray([
			'horizontal' => Alignment::HORIZONTAL_CENTER,
			'vertical' => Alignment::VERTICAL_CENTER,
		]);

        $elevenData = [
            ['1', '2', '3', '4', '5', '6'],
            ['', '', '', '', '', ''],
            ['', '', '', '', '', ''],
            ['', '', '', '', '', ''],
        ];

        // $offset = 18;
        $j = 0;
        $lastPageOffset = 0;
        for ($i = 0; $i < count($elevenData); $i++)
        {
            $j++;
            $v = 1 + $elevenOffset + $j;
            $v2 = ($elevenOffset + $j) + 1;
            $data = $elevenData[$i];

            $lastPageOffset = $v;

            $elevenSheet->mergeCells("A{$v}:B{$v}");
            $elevenSheet->getStyle("A{$v}:B{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("A{$v}:B{$v}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("A{$v}", $data[0]);

            $elevenSheet->mergeCells("C{$v}:D{$v}");
            $elevenSheet->getStyle("C{$v}:D{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("C{$v}:D{$v}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("C{$v}", $data[1]);

            $elevenSheet->mergeCells("E{$v}:F{$v}");
            $elevenSheet->getStyle("E{$v}:F{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("E{$v}:F{$v}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("E{$v}", $data[2]);

            $elevenSheet->mergeCells("G{$v}:H{$v}");
            $elevenSheet->getStyle("G{$v}:H{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("G{$v}:H{$v}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("G{$v}", $data[3]);

            $elevenSheet->mergeCells("I{$v}:K{$v}");
            $elevenSheet->getStyle("I{$v}:K{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("I{$v}:K{$v}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("I{$v}", $data[4]);

            $elevenSheet->mergeCells("L{$v}:N{$v}");
            $elevenSheet->getStyle("L{$v}:N{$v}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("L{$v}:N{$v}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("L{$v}", $data[5]);

	        $elevenSheet->getStyle("A{$v}:N{$v}")->getAlignment()->applyFromArray([
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
			]);
        }

        $elevenSecondHeaderOffset = $lastPageOffset + 3;
        $elevenSecondHeaderOffset2 = $elevenSecondHeaderOffset + 2;

        $elevenSheet->setCellValue("A{$elevenSecondHeaderOffset}", "5. Технологические топливопотребляющие установки");
        $elevenSheet->mergeCells("A{$elevenSecondHeaderOffset}:N{$elevenSecondHeaderOffset}");
        $elevenSheet->getStyle("A{$elevenSecondHeaderOffset}:N{$elevenSecondHeaderOffset}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $elevenSheet->getStyle("A{$elevenSecondHeaderOffset}:N{$elevenSecondHeaderOffset}")->getAlignment()->setWrapText(true);

        $elevenSheet->setCellValue("A{$elevenSecondHeaderOffset2}", "а) мощность предприятия по выпуску основных видов продукции");
        $elevenSheet->mergeCells("A{$elevenSecondHeaderOffset2}:N{$elevenSecondHeaderOffset2}");
        $elevenSheet->getStyle("A{$elevenSecondHeaderOffset2}:N{$elevenSecondHeaderOffset2}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $elevenSheet->getStyle("A{$elevenSecondHeaderOffset2}:N{$elevenSecondHeaderOffset2}")->getAlignment()->setWrapText(true);

        $elevenSheet->getStyle('A'.$elevenSecondHeaderOffset)->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $elevenSheet->getStyle('A'.$elevenSecondHeaderOffset2)->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);


        $elevenDataOffset = $elevenSecondHeaderOffset2 + 1;

        $elevenSheet->mergeCells("A{$elevenDataOffset}:D".($elevenDataOffset + 1));
        $elevenSheet->setCellValue("A{$elevenDataOffset}", "Вид продукции");
        $elevenSheet->getStyle("A{$elevenDataOffset}:D".($elevenDataOffset + 1))->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("A{$elevenDataOffset}:D".($elevenDataOffset + 1))->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("E{$elevenDataOffset}:I{$elevenDataOffset}");
        $elevenSheet->setCellValue("E{$elevenDataOffset}", "Годовой выпуск (указать единицу измерения)");
        $elevenSheet->getStyle("E{$elevenDataOffset}:I{$elevenDataOffset}")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("E{$elevenDataOffset}:I{$elevenDataOffset}")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("E".($elevenDataOffset + 1).":F".($elevenDataOffset + 1)."");
        $elevenSheet->setCellValue("E".($elevenDataOffset + 1)."", "Существующий");
        $elevenSheet->getStyle("E".($elevenDataOffset + 1).":F".($elevenDataOffset + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("E".($elevenDataOffset + 1).":F".($elevenDataOffset + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("G".($elevenDataOffset + 1).":I".($elevenDataOffset + 1)."");
        $elevenSheet->setCellValue("G".($elevenDataOffset + 1)."", "Проектируемый");
        $elevenSheet->getStyle("G".($elevenDataOffset + 1).":I".($elevenDataOffset + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("G".($elevenDataOffset + 1).":I".($elevenDataOffset + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("J{$elevenDataOffset}:N{$elevenDataOffset}");
        $elevenSheet->setCellValue("J{$elevenDataOffset}", "Удельный расход топлива (кгу.т./единицу продукции)");
        $elevenSheet->getStyle("J{$elevenDataOffset}:N{$elevenDataOffset}")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("J{$elevenDataOffset}:N{$elevenDataOffset}")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("J".($elevenDataOffset + 1).":L".($elevenDataOffset + 1)."");
        $elevenSheet->setCellValue("J".($elevenDataOffset + 1)."", "Существующий");
        $elevenSheet->getStyle("J".($elevenDataOffset + 1).":L".($elevenDataOffset + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("J".($elevenDataOffset + 1).":L".($elevenDataOffset + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("M".($elevenDataOffset + 1).":N".($elevenDataOffset + 1)."");
        $elevenSheet->setCellValue("M".($elevenDataOffset + 1)."", "Проектируемый");
        $elevenSheet->getStyle("M".($elevenDataOffset + 1).":N".($elevenDataOffset + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("M".($elevenDataOffset + 1).":N".($elevenDataOffset + 1)."")->getAlignment()->setWrapText(true);

        $elevenData = [
            ['1', '2', '3', '4', '5'],
        ];


        foreach ($tehObjects as $object){

            $techType = ObjectsTechType::find()->where(['id' => $object->purpose_system])->one();

            if($techType){
                $amountName = $techType->amount_name;
            } else {
                $amountName = null;
            }


            $elevenData[] = [ 
                $object->formula->getTeh3().' ('.$object->formula->getTeh1().')', '', $object->formula->getTeh8().' ('.$amountName.')', '', $object->formula->getTeh5(),
            ];
        }

        $offset = $elevenSecondHeaderOffset2 + 1;
        $j = 0; 
        for ($i = 0; $i < count($elevenData); $i++)
        {
            $j++;
            $j++;
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $elevenData[$i];

            $lastPageOffset = $v2;

            $elevenSheet->mergeCells("A{$v}:D{$v2}");
            $elevenSheet->getStyle("A{$v}:D{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("A{$v}:D{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("A{$v}", $data[0]);

            $elevenSheet->mergeCells("E{$v}:F{$v2}");
            $elevenSheet->getStyle("E{$v}:F{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("E{$v}:F{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("E{$v}", $data[1]);

            $elevenSheet->mergeCells("G{$v}:I{$v2}");
            $elevenSheet->getStyle("G{$v}:I{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("G{$v}:I{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("G{$v}", $data[2]);

            $elevenSheet->mergeCells("J{$v}:L{$v2}");
            $elevenSheet->getStyle("J{$v}:L{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("J{$v}:L{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("J{$v}", $data[3]);

            $elevenSheet->mergeCells("M{$v}:N{$v2}");
            $elevenSheet->getStyle("M{$v}:N{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("M{$v}:N{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("M{$v}", $data[4]);

	        $elevenSheet->getStyle("A{$v}:N{$v2}")->getAlignment()->applyFromArray([
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
			]);
        }

        $elevenOffsetHeader3 = $lastPageOffset + 3; 

        $elevenSheet->setCellValue("A{$elevenOffsetHeader3}", "б) состав и характеристика технологического оборудования, вид и годовой расход топлива");
        $elevenSheet->mergeCells("A{$elevenOffsetHeader3}:N{$elevenOffsetHeader3}");
        $elevenSheet->getStyle("A{$elevenOffsetHeader3}:N{$elevenOffsetHeader3}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $elevenSheet->getStyle("A{$elevenOffsetHeader3}:N{$elevenOffsetHeader3}")->getAlignment()->setWrapText(true);

        $elevenSheet->getStyle('A'.$elevenOffsetHeader3)->applyFromArray([
        	'font' => [
        		'size' => 11,
        		'bold' => true,
        	],
        ]);

        $elevenOffsetData3 = $elevenOffsetHeader3 + 2;

        $elevenSheet->mergeCells("A{$elevenOffsetData3}:B".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("A{$elevenOffsetData3}", "Тип технологического оборудования");
        $elevenSheet->getStyle("A{$elevenOffsetData3}:B".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("A{$elevenOffsetData3}:B".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("C{$elevenOffsetData3}:D".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("C{$elevenOffsetData3}", "Количество");
        $elevenSheet->getStyle("C{$elevenOffsetData3}:D".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("C{$elevenOffsetData3}:D".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("C{$elevenOffsetData3}:D".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("C{$elevenOffsetData3}", "Количество");
        $elevenSheet->getStyle("C{$elevenOffsetData3}:D".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("C{$elevenOffsetData3}:D".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("E{$elevenOffsetData3}:F".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("E{$elevenOffsetData3}", "Мощность (единичная)");
        $elevenSheet->getStyle("E{$elevenOffsetData3}:F".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("E{$elevenOffsetData3}:F".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("G{$elevenOffsetData3}:J{$elevenOffsetData3}");
        $elevenSheet->setCellValue("G{$elevenOffsetData3}", "Используемое топливо");
        $elevenSheet->getStyle("G{$elevenOffsetData3}:J{$elevenOffsetData3}")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("G{$elevenOffsetData3}:J{$elevenOffsetData3}")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("G".($elevenOffsetData3 + 1).":H".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("G".($elevenOffsetData3 + 1)."", "вид");
        $elevenSheet->getStyle("G".($elevenOffsetData3 + 1).":H".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("G".($elevenOffsetData3 + 1).":H".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("I".($elevenOffsetData3 + 1).":J".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("I".($elevenOffsetData3 + 1)."", "годовой расход (отчетный) тыс.т.у.т.");
        $elevenSheet->getStyle("I".($elevenOffsetData3 + 1).":J".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("I".($elevenOffsetData3 + 1).":J".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("K{$elevenOffsetData3}:N{$elevenOffsetData3}");
        $elevenSheet->setCellValue("K{$elevenOffsetData3}", "Запрашиваемое топливо");
        $elevenSheet->getStyle("K{$elevenOffsetData3}:N{$elevenOffsetData3}")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("K{$elevenOffsetData3}:N{$elevenOffsetData3}")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("K".($elevenOffsetData3 + 1).":L".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("K".($elevenOffsetData3 + 1)."", "вид");
        $elevenSheet->getStyle("K".($elevenOffsetData3 + 1).":L".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("K".($elevenOffsetData3 + 1).":L".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("K".($elevenOffsetData3 + 1).":L".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("K".($elevenOffsetData3 + 1)."", "вид");
        $elevenSheet->getStyle("K".($elevenOffsetData3 + 1).":L".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("K".($elevenOffsetData3 + 1).":L".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("M".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("M".($elevenOffsetData3 + 1)."", "годовой расход (отчетный) тыс.т.у.т.");
        $elevenSheet->getStyle("M".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("M".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

        $elevenSheet->mergeCells("M".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."");
        $elevenSheet->setCellValue("M".($elevenOffsetData3 + 1)."", "годовой расход (отчетный) тыс.т.у.т.");
        $elevenSheet->getStyle("M".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."")->applyFromArray($bordersStyle);
        $elevenSheet->getStyle("M".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."")->getAlignment()->setWrapText(true);

	    $elevenSheet->getStyle("A".($elevenOffsetData3 + 1).":N".($elevenOffsetData3 + 1)."")->getAlignment()->applyFromArray([
			'horizontal' => Alignment::HORIZONTAL_CENTER,
			'vertical' => Alignment::VERTICAL_CENTER,
		]);

        $elevenData = [
            ['1', '2', '3', '4', '5', '6', '7'],
        ];

        // foreach($objects as $object)
        // {
        //     $elevenData[] = [
        //         $object->formula->getO1()." ({$object->formula->getO8()})", $object->formula->getO4(), "{$object->formula->getO7()} ({$object->formula->getO6()})", '', '', $object->calculation->formula->getZak5(), '-1'
        //     ];
        // }


        $objGroups = ArrayHelper::getColumn(Objects::find()->where(['calculation_id' => $id, 'type_object' => Objects::TYPE_TECH])->all(), 'number_group');

        $equipments = Equipment::find()->where(['calculation_id' => $model->id, 'number_group' => $objGroups])->all();

        foreach ($equipments as $equipment) {

        	$o7 = round($equipment->formula->getO7(), 4);
        	$o7Kvt = $o7 * 1164;

            $elevenData[] = [
                $equipment->formula->getO1(), $equipment->formula->getO4(), [$o7, "{$o7Kvt} кВт"], '', '', $model->formula->getZak5(), $equipment->formula->getObGut(),
            ];
        }


        // VarDumper::dump($elevenData, 10, true);


        $offset = $elevenOffsetData3;
        $j = 0;
        for ($i = 0; $i < count($elevenData); $i++)
        {
            $j++;
            $j++;
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $elevenData[$i];

            $elevenSheet->mergeCells("A{$v}:B{$v2}");
            $elevenSheet->getStyle("A{$v}:B{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("A{$v}", $data[0]);

            $elevenSheet->mergeCells("C{$v}:D{$v2}");
            $elevenSheet->getStyle("C{$v}:D{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("C{$v}:D{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("C{$v}", $data[1]);

            if($i > 0){
            	$elevenSheet->mergeCells("E{$v}:F{$v}");
            	$elevenSheet->getStyle("E{$v}:F{$v}")->applyFromArray($bordersStyle);
            	$elevenSheet->getStyle("E{$v}:F{$v}")->getAlignment()->setWrapText(true);;
            	$elevenSheet->setCellValue("E{$v}", $data[2][0]);

            	$elevenSheet->mergeCells("E{$v2}:F{$v2}");
            	$elevenSheet->getStyle("E{$v2}:F{$v2}")->applyFromArray($bordersStyle);
            	$elevenSheet->getStyle("E{$v2}:F{$v2}")->getAlignment()->setWrapText(true);;
            	$elevenSheet->setCellValue("E{$v2}", $data[2][1]);
            } else {
            	$elevenSheet->mergeCells("E{$v}:F{$v2}");
            	$elevenSheet->getStyle("E{$v}:F{$v2}")->applyFromArray($bordersStyle);
            	$elevenSheet->getStyle("E{$v}:F{$v2}")->getAlignment()->setWrapText(true);;
            	$elevenSheet->setCellValue("E{$v}", $data[2]);
            }

            $elevenSheet->mergeCells("G{$v}:H{$v2}");
            $elevenSheet->getStyle("G{$v}:H{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("G{$v}:H{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("G{$v}", $data[3]);

            $elevenSheet->mergeCells("I{$v}:J{$v2}");
            $elevenSheet->getStyle("I{$v}:J{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("I{$v}:J{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("I{$v}", $data[4]);

            $elevenSheet->mergeCells("K{$v}:L{$v2}");
            $elevenSheet->getStyle("K{$v}:L{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("K{$v}:L{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("K{$v}", $data[5]);

            $elevenSheet->mergeCells("M{$v}:N{$v2}");
            $elevenSheet->getStyle("M{$v}:N{$v2}")->applyFromArray($bordersStyle);
            $elevenSheet->getStyle("M{$v}:N{$v2}")->getAlignment()->setWrapText(true);;
            $elevenSheet->setCellValue("M{$v}", $data[6]);

	        $elevenSheet->getStyle("A{$v}:N{$v2}")->getAlignment()->applyFromArray([
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
			]);
        }

        $twelveSheet = $spreadsheet->createSheet();

        $twelveSheet->getColumnDimension('A')->setWidth(5.5);
        $twelveSheet->getColumnDimension('B')->setWidth(5.5);
        $twelveSheet->getColumnDimension('C')->setWidth(5.5);
        $twelveSheet->getColumnDimension('D')->setWidth(5.5);
        $twelveSheet->getColumnDimension('E')->setWidth(5.5);
        $twelveSheet->getColumnDimension('F')->setWidth(5.5);
        $twelveSheet->getColumnDimension('G')->setWidth(5.5);
        $twelveSheet->getColumnDimension('H')->setWidth(5.5);
        $twelveSheet->getColumnDimension('I')->setWidth(5.5);
        $twelveSheet->getColumnDimension('J')->setWidth(5.5);
        $twelveSheet->getColumnDimension('K')->setWidth(5.5);
        $twelveSheet->getColumnDimension('L')->setWidth(5.5);
        $twelveSheet->getColumnDimension('M')->setWidth(5.5);
        $twelveSheet->getColumnDimension('N')->setWidth(5.5);
        $twelveSheet->getColumnDimension('O')->setWidth(5.5);
        $twelveSheet->getColumnDimension('P')->setWidth(5.5);

		$twelveSheet->getRowDimension('5')->setRowHeight('30');
		$twelveSheet->getRowDimension('6')->setRowHeight('40');

        $twelveSheet->setTitle('Вторичные ресурсов');
        $twelveSheet->setCellValue('A1', '6. Использование топливных и тепловых вторичных ресурсов');
        $twelveSheet->mergeCells('A1:P1');
        $twelveSheet->getStyle('A1:P1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $twelveSheet->getStyle('A1')->applyFromArray([
        	'font' => [
        		'size' => 10,
        		'bold' => true,
        	],
        ]);

        $twelveSheet->getStyle('A1:P20')->applyFromArray([
        	'font' => [
        		'size' => 10,
        	],
        ]);


        $twelveSheet->mergeCells('A4:H4');
        $twelveSheet->setCellValue('A4', 'Топливные вторичные ресурсы');
        $twelveSheet->getStyle('A4:H4')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('A4:H4')->getAlignment()->setWrapText(true);
        $twelveSheet->getStyle('A4:H4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $twelveSheet->mergeCells('I4:P4');
        $twelveSheet->setCellValue('I4', 'Тепловые вторичные ресурсы');
        $twelveSheet->getStyle('I4:P4')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('I4:P4')->getAlignment()->setWrapText(true);
        $twelveSheet->getStyle('I4:P4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $twelveSheet->mergeCells('A5:B6');
        $twelveSheet->setCellValue('A5', 'Вид, источник');
        $twelveSheet->getStyle('A5:B6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('A5:B6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('C5:D6');
        $twelveSheet->setCellValue('C5', 'Выход (тыс.т.у.т. в год)');
        $twelveSheet->getStyle('C5:D6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('C5:D6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('E5:H5');
        $twelveSheet->setCellValue('E5', 'Количество используемого, (тыс.т.у.т.)	вид, источник');
        $twelveSheet->getStyle('E5:H5')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('E5:H5')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('E6:F6');
        $twelveSheet->setCellValue('E6', 'Существующее');
        $twelveSheet->getStyle('E6:F6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('E6:F6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('G6:H6');
        $twelveSheet->setCellValue('G6', 'Проектируемое');
        $twelveSheet->getStyle('G6:H6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('G6:H6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('I5:J6');
        $twelveSheet->setCellValue('I5', 'Вид, источник');
        $twelveSheet->getStyle('I5:J6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('I5:J6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('I5:J6');
        $twelveSheet->setCellValue('I5', 'Вид, источник');
        $twelveSheet->getStyle('I5:J6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('I5:J6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('K5:L6');
        $twelveSheet->setCellValue('K5', 'Выход (тыс.т.у.т. в год)');
        $twelveSheet->getStyle('K5:L6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('K5:L6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('M5:P5');
        $twelveSheet->setCellValue('M5', 'Количество используемого, (тыс.т.у.т.)');
        $twelveSheet->getStyle('M5:P5')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('M5:P5')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('M6:N6');
        $twelveSheet->setCellValue('M6', 'Существующее');
        $twelveSheet->getStyle('M6:N6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('M6:N6')->getAlignment()->setWrapText(true);

        $twelveSheet->mergeCells('O6:P6');
        $twelveSheet->setCellValue('O6', 'Проектируемое');
        $twelveSheet->getStyle('O6:P6')->applyFromArray($bordersStyle);
        $twelveSheet->getStyle('O6:P6')->getAlignment()->setWrapText(true);

        $twelveData = [
            [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        ];

        $offset = 5;
        $j = 0;
        for ($i = 0; $i < count($twelveData); $i++)
        {
            $j++;
            $j++;
            $v = $offset + $j;
            $v2 = ($offset + $j) + 1;
            $data = $twelveData[$i];


            $twelveSheet->mergeCells("A{$v}:B{$v2}");
            $twelveSheet->getStyle("A{$v}:B{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("A{$v}:B{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("A{$v}", $data[0]);

            $twelveSheet->mergeCells("C{$v}:D{$v2}");
            $twelveSheet->getStyle("C{$v}:D{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("C{$v}:D{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("C{$v}", $data[1]);

            $twelveSheet->mergeCells("E{$v}:F{$v2}");
            $twelveSheet->getStyle("E{$v}:F{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("E{$v}:F{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("E{$v}", $data[2]);

            $twelveSheet->mergeCells("G{$v}:H{$v2}");
            $twelveSheet->getStyle("G{$v}:H{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("G{$v}:H{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("G{$v}", $data[3]);

            $twelveSheet->mergeCells("I{$v}:J{$v2}");
            $twelveSheet->getStyle("I{$v}:J{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("I{$v}:J{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("I{$v}", $data[4]);

            $twelveSheet->mergeCells("K{$v}:L{$v2}");
            $twelveSheet->getStyle("K{$v}:L{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("K{$v}:L{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("K{$v}", $data[5]);

            $twelveSheet->mergeCells("M{$v}:N{$v2}");
            $twelveSheet->getStyle("M{$v}:N{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("M{$v}:N{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("M{$v}", $data[6]);

            $twelveSheet->mergeCells("O{$v}:P{$v2}");
            $twelveSheet->getStyle("O{$v}:P{$v2}")->applyFromArray($bordersStyle);
            $twelveSheet->getStyle("O{$v}:P{$v2}")->getAlignment()->setWrapText(true);;
            $twelveSheet->setCellValue("O{$v}", $data[7]);
        }

        $twelveSheet->mergeCells('I19:P20');
        $twelveSheet->setCellValue('I19', 'Руководитель предприятия ____________________ (подпись)');
        $twelveSheet->getStyle('I19:P20')->getAlignment()->setWrapText(true);
        $twelveSheet->getStyle('I19:P20')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        $twelveSheet->getStyle('I19:P20')->getFont()->setSize(12);

        $spreadsheet->getActiveSheet()->getPageSetup()
    ->setOrientation(PageSetup::PAPERSIZE_A4);

        $writer = new Xlsx($spreadsheet);
        $writer->save('test1.xlsx');

        Yii::$app->response->sendFile('test1.xlsx', "Расчет №{$model->id}.xlsx");
    }

    public function actionCreateObject($calculation_id, $type_object = Objects::TYPE_OV)
    {
        $request = Yii::$app->request;

        if ($type_object == Objects::TYPE_OV) {
            $model = new ObjectsOvForm();
            $model->count_object = 1;
            $model->internal_temperature = 18;
        }
        if ($type_object == Objects::TYPE_GVS) {
            $model = new ObjectsGvsForm();
            $model->count_object = 1;
        }
        if ($type_object == Objects::TYPE_TECH) {
            $model = new ObjectsTechForm();
            $model->count_object = 1;
        }
        $model->calculation_id = $calculation_id;
        $model->type_object = $type_object;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить объект",
                    'content' => $this->renderAjax('create-object', [
                        'model' => $model,
                        'type_object' => $type_object,

                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->save();
                return [
                    'forceReload' => '#crud-datatable-object-pjax',
                    'forceClose' => true,
                ];
            } else {
                return [
                    'title' => "Добавить объект",
                    'content' => $this->renderAjax('create-object', [
                        'model' => $model,
                        'type_object' => $type_object,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                $model->validate();
                //$model->save();
                var_dump($model->errors);
                //return $this->redirect(['view', 'id' => $model->id]);
            } else {

                return $this->render('create-object', [
                    'model' => $model,
                    'type_object' => $type_object,
                ]);
            }
        }

    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $searchModelObjects = new ObjectsSearch();
        $searchModelObjects->calculation_id = $id;
        $dataProviderObjects = $searchModelObjects->search(Yii::$app->request->queryParams);
        $searchModelEquipments = new EquipmentSearch();
        $searchModelEquipments->calculation_id = $id;
        $dataProviderEquipments = $searchModelEquipments->search(Yii::$app->request->queryParams);


        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Изменения сохранены');

            return $this->render('update', [
                'model' => $model,
                'searchModelObjects' => $searchModelObjects,
                'dataProviderObjects' => $dataProviderObjects,
                'searchModelEquipments' => $searchModelEquipments,
                'dataProviderEquipments' => $dataProviderEquipments,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'searchModelObjects' => $searchModelObjects,
                'dataProviderObjects' => $dataProviderObjects,
                'searchModelEquipments' => $searchModelEquipments,
                'dataProviderEquipments' => $dataProviderEquipments,
            ]);
        }
    }

    public function actionUpdateObject($id)
    {
        $request = Yii::$app->request;
        $type_object = Objects::findOne($id)->type_object;
        if ($type_object == Objects::TYPE_OV) {
            $model = new ObjectsOvForm();
            $title = 'Отопление и вентиляция';
        }
        if ($type_object == Objects::TYPE_GVS) {
            $model = new ObjectsGvsForm();
            $title = 'Горячее водоснабжение';
        }
        if ($type_object == Objects::TYPE_TECH) {
            $model = new ObjectsTechForm();
            $title = 'Технология';
        }
        $model->loadData($id);
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => $title,
                    'content' => $this->renderAjax('update-object', [
                        'model' => $model,
                        'type_object' => $type_object,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->save($id);
                return ['forceClose' => true, 'forceReload' => '#global-pjax-container'];
            } else {
                return [
                    'title' => $title,
                    'content' => $this->renderAjax('update-object', [
                        'model' => $model,
                        'type_object' => $type_object,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update-object', [
                    'model' => $model,
                    'type_object' => $type_object,
                ]);
            }
        }
    }

    public function actionUpdateEquipment($id)
    {
        $request = Yii::$app->request;
        $model= Equipment::findOne($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить оборудование #" . $id,
                    'content' => $this->renderAjax('_form-equipment', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return ['forceClose' => true, 'forceReload' => '#global-pjax-container'];
            } else {
                return [
                    'title' => "Изменить оборудование #" . $id,
                    'content' => $this->renderAjax('_form-equipment', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-equipment', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionCalculateEquipment($id)
    {
        $request = Yii::$app->request;
        $model= Equipment::findOne($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load($request->post()) && $model->save()) {

            $object = Objects::find()->where(['calculation_id' => $model->calculation_id, 'number_group' => $model->number_group])->one();

            if($object != null){

            if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
                $model->type_object = 'Котельное';
            } else {
                $model->type_object = 'Технологическое';
            }

            }


            $model->status = 'Корректно';

            $model->specific_consumption = $model->formula->getO9();


            if($model->power_v > 0){
                $model->coefficient_load = $model->formula->getO5();

                if(in_array($object->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
                    $model->consumption_e = round($model->formula->getO11(), 2);
                    $model->consumption_o = round($model->formula->getO12(), 3);
                } else {
                    $model->consumption_e = round($model->formula->getO11(), 1);
                    $model->consumption_o = round($model->formula->getO12(), 3);
                }

                if($model->pover_common == null){
                    $model->pover_common = $model->formula->getO8();
                }

            } else {
                if($object->type_object == Objects::TYPE_TECH){
                    $objectTech = ObjectsTech::find()->where(['objects_id' => $object->id])->one();
                    // $model->power_v = $objectTech->formula->getTehQh1();
                    $model->power_v = $model->formula->getO6();
                    // $model->pover_g = $objectTech->formula->getTehQh();
                    $model->pover_g = $model->formula->getO6() / 1163;
                    $model->pover_common = $model->formula->getO8();
                    $model->consumption_o = round($model->formula->getO12(), 3);
                }
            }

                if($object->type_object == Objects::TYPE_OV){
                    $objectOv = ObjectsOv::find()->where(['objects_id' => $object->id])->one();
                    // $model->power_v = $objectOv->formula->getOvQomax1() + $objectOv->formula->getOvQvmax1();
                    // $model->pover_g = $model->power_v / 1164;
                    $model->pover_g = round($model->power_v / 1163, 5);
                    // if($model->pover_g > 0){
                        $model->pover_common = $model->formula->getO8();
                    // }
                }
                if($object->type_object == Objects::TYPE_GVS){
                    $objectGvs = ObjectsGvs::find()->where(['objects_id' => $object->id])->one();
                    // $model->power_v = $objectGvs->formula->getGvsQhm1();
                    // $model->pover_g = $model->power_v / 1164;
                    $model->pover_g = round($model->power_v / 1163, 5);
                    // if($model->pover_g > 0){
                        $model->pover_common = $model->formula->getO8();
                    // }
                }


            if($model->coefficient_load != null){
                if($model->coefficient_load < 0 || $model->coefficient_load > 1){
                    $model->status = 'Ошибка';
                }   
            }

            $sumOvQomax = 0;
            $sumOvQomax1 = 0;
            $sumOvQvmax = 0;
            $sumOvQvmax1 = 0;
            $sumOvQo = 0;
            $sumOvQv = 0;
            $sumOvGncho = 0;
            $sumOvGnchv = 0;
            $sumOvGno = 0;
            $sumOvGnv = 0;
            $sumOvGuto = 0;
            $sumOvGutv = 0;

            $sumGvsQhm = 0;
            $sumGvsQhm1 = 0;
            $sumGvsQh = 0;
            $sumGvsGnch = 0;
            $sumGvsGn = 0;
            $sumGvsGut = 0;

            $sumTehQh = 0;
            $sumTehQh1 = 0;
            $sumTehQn = 0;
            $sumTehQhd = 0;
            $sumTehGn = 0;
            $sumTehGut = 0;


$objects = Objects::find()->where(['number_group' => $model->number_group, 'calculation_id' => $model->calculation_id])->all();

$obj = null;

foreach ($objects as $object)
{
    $obj = $object;
    if($object->type_object == Objects::TYPE_OV){
        $object = ObjectsOv::findOne(['objects_id' => $object->id]);

        if($object){
            $sumOvQomax += $object->formula->getOvQomax();
            $sumOvQomax1 += $object->formula->getOvQomax1();
            $sumOvQvmax += $object->formula->getOvQvmax();
            $sumOvQvmax1 += $object->formula->getOvQvmax1();
            $sumOvQo += $object->formula->getQo();
            $sumOvQv += $object->formula->getQv();
            $sumOvGncho += $object->formula->getOvGncho();
            $sumOvGnchv += $object->formula->getOvGnchv();
            $sumOvGno += $object->formula->getOvGno();
            $sumOvGnv += $object->formula->getOvGnv();
            $sumOvGuto += $object->formula->getOvGuto();
            $sumOvGutv += $object->formula->getOvGutv();
        }

    } else if($object->type_object == Objects::TYPE_GVS){
        $object = ObjectsGvs::findOne(['objects_id' => $object->id]);

        if($object){
            $sumGvsQhm += $object->formula->getGvsQhm();
            $sumGvsQhm1 += $object->formula->getGvsQhm1();
            $sumGvsQh += $object->formula->getGvsQh();
            $sumGvsGnch += $object->formula->getGvsGnch();
            $sumGvsGn += $object->formula->getGvsGn();
            $sumGvsGut += $object->formula->getGvsGut();
        }

    } else if($object->type_object == Objects::TYPE_TECH){
        $object = ObjectsTech::findOne(['objects_id' => $object->id]);

        if($object){
            $sumTehQh += $object->formula->getTehQh();
            $sumTehQh1 += $object->formula->getTehQh1();
            $sumTehQn += $object->formula->getTehQn();
            $sumTehQhd += $object->formula->getTehQhd();
            $sumTehGn += $object->formula->getTehGn();
            $sumTehGut += $object->formula->getTehGut();
        }
    }
}


$allQhmax = $sumOvQomax + $sumOvQvmax + $sumGvsQhm + $sumTehQh;
$allQhmax1 = $sumOvQomax1 + $sumOvQvmax1 + $sumGvsQhm1 + $sumTehQh1;
$allQh = $sumOvQo + $sumOvQv + $sumGvsQh + $sumTehQn;
$allQh1 = ($sumOvQo * 4.1868) + ($sumOvQv * 4.1868) + ($sumGvsQh * 4.1868) + ($sumTehQn * 4.1868);
$allGnch = $sumOvGncho + $sumOvGnchv + $sumGvsGnch + $sumTehQhd;
$allGn = $sumOvGno + $sumOvGnv + $sumGvsGn + $sumTehGn;
$allGut = $sumOvGuto + $sumOvGutv + $sumGvsGut + $sumTehGut;



            // $sumOvQomax = round($sumOvQomax, 4);
            // $sumOvQomax1 = round($sumOvQomax1, 4);
            // $sumGvsQhm = round($sumGvsQhm, 5);
            // $sumTehQh = round($sumTehQh, 4);
            // $allQhmax = round($allQhmax, 4);

            // $sumOvQomax1 = round($sumOvQomax1, 4);
            // $sumOvQvmax1 = round($sumOvQvmax1, 4);
            // $sumGvsQhm1 = round($sumGvsQhm1);
            // $sumTehQh1 = round($sumTehQh1, 2);
            // $allQhmax1 = round($allQhmax1, 1);


            // $sumOvQo = round($sumOvQo, 3);
            // $sumOvQv = round($sumOvQv, 3);
            // $sumGvsQh = round($sumGvsQh, 2);
            // // $sumTehQh1 = round($sumTehQh1, 1);


            // $sumOvQoM = round($sumOvQo * 4.1868, 2);
            // $sumOvQvM = round($sumOvQv * 4.1868, 2);
            // $sumGvsQhM = round($sumGvsQh * 4.1868, 2);
            // $sumTehQnM = round($sumTehQn * 4.1868, 2);


            // $sumOvGncho = round($sumOvGncho, 2);
            // $sumOvGnchv = round($sumOvGnchv, 2);
            // $sumGvsGnch = round($sumGvsGnch, 2);
            // $sumTehQhd = round($sumTehQhd, 3);


            // $sumOvGno = round($sumOvGno, 2);
            // $sumOvGnv = round($sumOvGnv, 2);
            // $sumGvsGn = round($sumGvsGn, 2);
            // $sumTehGn = round($sumTehGn, 2);


            // $sumOvGuto = round($sumOvGuto, 2);
            // $sumOvGutv = round($sumOvGutv, 2);
            // $sumGvsGut = round($sumGvsGut, 2);
            // $sumTehGut = round($sumTehGut, 2);


            // $allQhmax = round($allQhmax, 4);
            // $allQhmax1 = round($allQhmax1, 1);
            // $allQh = round($allQh, 3);
            // $allQh1 = round($allQh1, 2);
            // $allGnch = round($allGnch, 2);
            // $allGn = round($allGn, 2);
            // $allGut = round($allGut, 1);

            $sumOvQomax = MathNumber::round($sumOvQomax);
            $sumOvQomax1 = MathNumber::round($sumOvQomax1);
            $sumGvsQhm = MathNumber::round($sumGvsQhm);
            $sumTehQh = MathNumber::round($sumTehQh);
            $allQhmax = MathNumber::round($allQhmax);

            $sumOvQomax1 = MathNumber::round($sumOvQomax1);
            $sumOvQvmax1 = MathNumber::round($sumOvQvmax1);
            $sumGvsQhm1 = MathNumber::round($sumGvsQhm1);
            $sumTehQh1 = MathNumber::round($sumTehQh1);
            $allQhmax1 = MathNumber::round($allQhmax1);


            $sumOvQo = MathNumber::round($sumOvQo);
            $sumOvQv = MathNumber::round($sumOvQv);
            $sumGvsQh = MathNumber::round($sumGvsQh);


            $sumOvQoM = MathNumber::round($sumOvQo * 4.1868);
            $sumOvQvM = MathNumber::round($sumOvQv * 4.1868);
            $sumGvsQhM = MathNumber::round($sumGvsQh * 4.1868);
            $sumTehQnM = MathNumber::round($sumTehQn * 4.1868);


            $sumOvGncho = MathNumber::round($sumOvGncho);
            $sumOvGnchv = MathNumber::round($sumOvGnchv);
            $sumGvsGnch = MathNumber::round($sumGvsGnch);
            $sumTehQhd = MathNumber::round($sumTehQhd);


            $sumOvGno = MathNumber::round($sumOvGno);
            $sumOvGnv = MathNumber::round($sumOvGnv);
            $sumGvsGn = MathNumber::round($sumGvsGn);
            $sumTehGn = MathNumber::round($sumTehGn);


            $sumOvGuto = MathNumber::round($sumOvGuto);
            $sumOvGutv = MathNumber::round($sumOvGutv);
            $sumGvsGut = MathNumber::round($sumGvsGut);
            $sumTehGut = MathNumber::round($sumTehGut);


            $allQhmax = MathNumber::round($allQhmax);
            $allQhmax1 = MathNumber::round($allQhmax1);
            $allQh = MathNumber::round($allQh);
            $allQh1 = MathNumber::round($allQh1);
            $allGnch = MathNumber::round($allGnch);
            $allGn = MathNumber::round($allGn);
            $allGut = MathNumber::round($allGut);

            $allQh1 = $sumOvQoM + $sumOvQvM + $sumGvsQhM + $sumTehQnM;

            $coefficientLoad = $model->formula->getO5();

            $model->power_v = MathNumber::round($model->power_v);
            $model->pover_g = MathNumber::round($model->pover_g);

            return ['model' => $model, 'formulas' => [
                'sumOvQomax' => $sumOvQomax,
                'sumOvQomax1' => $sumOvQomax1,
                'sumOvQvmax' => $sumOvQvmax,
                'sumOvQvmax1' => $sumOvQvmax1,
                'sumOvQo' => $sumOvQo,
                'sumOvQv' => $sumOvQv,
                'sumOvGncho' => $sumOvGncho,
                'sumOvGnchv' => $sumOvGnchv,
                'sumOvGno' => $sumOvGno,
                'sumOvGnv' => $sumOvGnv,
                'sumOvGuto' => $sumOvGuto,
                'sumOvGutv' => $sumOvGutv,
                'sumGvsQhm' => $sumGvsQhm,
                'sumGvsQhm1' => $sumGvsQhm1,
                'sumGvsQh' => $sumGvsQh,
                'sumGvsGnch' => $sumGvsGnch,
                'sumGvsGn' => $sumGvsGn,
                'sumGvsGut' => $sumGvsGut,
                'sumTehQh' => $sumTehQh,
                'sumTehQh1' => $sumTehQh1,
                'sumTehQn' => $sumTehQn,
                'sumTehQhd' => $sumTehQhd,
                'sumTehGn' => $sumTehGn,
                'sumTehGut' => $sumTehGut,
                'allQhmax' => $allQhmax,
                'allQhmax1' => $allQhmax1,
                'allQh' => $allQh,
                'allQh1' => $allQh1,
                'allGut' => $allGut,
                'allGnch' => $allGnch,
                'coefficientLoad' => $coefficientLoad,
                'O8_1' => $model->formula->getO8_1(),
            ]];
        } else {
            return [];
        }
    }

    public function actionDeleteEquipment($id)
    {
        $request = Yii::$app->request;
        $model = Equipment::findOne($id);

        if($model == null){
        	throw new NotFoundHttpException();
        }

        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-equipment-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-object-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteObject($id)
    {
        $request = Yii::$app->request;
        $ObjectsOv = ObjectsOv::findOne(['objects_id' => $id]);
        if ($ObjectsOv) {
            $ObjectsOv->delete();
        }
        $ObjectsGvs = ObjectsGvs::findOne(['objects_id' => $id]);
        if ($ObjectsGvs) {
            $ObjectsGvs->delete();
        }
        $ObjectsTech = ObjectsTech::findOne(['objects_id' => $id]);
        if ($ObjectsTech) {
            $ObjectsTech->delete();
        }
        $object = Objects::findOne(['id' => $id]);
        $object->delete();

        $old_group = $object->number_group;
	    $objects_old_group = Objects::find()->where(['number_group' => $old_group,'calculation_id'=>$object->calculation_id])->count();
	    if ($objects_old_group == 0) {
	        $equipment = Equipment::findOne(['number_group' => $old_group,'calculation_id'=>$object->calculation_id]);
	        if ($equipment) {
	            $equipment->delete();
	        }
	    }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            // return ['forceClose' => true, 'forceReload' => '#crud-datatable-object-pjax'];
            return ['forceClose' => true, 'forceReload' => '#global-pjax-container'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $ObjectsOv = ObjectsOv::findOne(['objects_id' => $pk]);
            if ($ObjectsOv) {
                $ObjectsOv->delete();
            }
            Objects::findOne(['id' => $pk])->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkDeleteCalculation()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $ObjectsOv = Calculation::findOne(['id' => $pk]);
            if ($ObjectsOv) {
                $ObjectsOv->delete();
            }
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkDeleteObject()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $ObjectsOv = ObjectsOv::findOne(['objects_id' => $pk]);
            if ($ObjectsOv) {
                $ObjectsOv->delete();
            }
            $ObjectsGvs = ObjectsGvs::findOne(['objects_id' => $pk]);
            if ($ObjectsGvs) {
                $ObjectsGvs->delete();
            }
            $ObjectsTech = ObjectsTech::findOne(['objects_id' => $pk]);
            if ($ObjectsTech) {
                $ObjectsTech->delete();
            }
            $object = Objects::findOne(['id' => $pk]);
            $object->delete();

            $old_group = $object->number_group;
	        $objects_old_group = Objects::find()->where(['number_group' => $old_group,'calculation_id'=>$object->calculation_id])->count();
	        if ($objects_old_group == 0) {
	            $equipment = Equipment::findOne(['number_group' => $old_group,'calculation_id'=>$object->calculation_id]);
	            if ($equipment) {
	                $equipment->delete();
	            }
	        }
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#global-pjax-container'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionGroupObject()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $pks = $request->post('pks'); // Array or selected records primary keys
        $group = $request->post('num_group');
        foreach ($pks as $pk) {
            $objects = Objects::findOne($pk);
            $old_group = $objects->number_group;
            $objects->number_group = $group;
            $objects->save();
            $objects_old_group = Objects::find()->where(['number_group' => $old_group,'calculation_id'=>$objects->calculation_id])->count();
            if ($objects_old_group == 0) {
                $equipment = Equipment::findOne(['number_group' => $old_group]);
                if ($equipment) {
                    $equipment->delete();
                }
            }
            $objects_new_group = Objects::find()->where(['number_group' => $group,'calculation_id'=>$objects->calculation_id])->count();
            if ($objects_new_group == 1 && $group != 0)  {
                $equipment = new Equipment();
                $equipment->calculation_id = $objects->calculation_id;
                $equipment->number_group = $group;
                $equipment->name_equipment = 'Новое оборудование';
                $equipment->save();
            }

        }
        return ['error' => serialize($request->post())];
    }

    public function actionGroupOneObject()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = $_POST['editableKey'];
        $group = array_shift($_POST['Objects'])['number_group'];

        Yii::warning("{$id} {$group}");

        $objects = Objects::findOne($id);

        $groupObjects = Objects::find()->where(['calculation_id' => $objects->calculation_id, 'number_group' => $group])->all();

        foreach ($groupObjects as $obj) {
        	if(in_array($obj->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
        		if($objects->type_object == Objects::TYPE_TECH){
        			return ['output' => '', 'message' => 'Технологический объект должен быть в отдельной группе'];
        		}
        	} else if($obj->type_object == Objects::TYPE_TECH){
        		if(in_array($objects->type_object, [Objects::TYPE_OV, Objects::TYPE_GVS])){
        			return ['output' => '', 'message' => 'Технологический объект должен быть в отдельной группе'];
        		} else if($objects->type_object == Objects::TYPE_TECH){
        			return ['output' => '', 'message' => 'Технологический объект должен быть в отдельной группе'];
        		}
        	}
        }


        $old_group = $objects->number_group;
        $objects->number_group = $group;
        $objects->save();
        $objects_old_group = Objects::find()->where(['number_group' => $old_group,'calculation_id'=>$objects->calculation_id])->count();
        if ($objects_old_group == 0) {
            $equipment = Equipment::findOne(['number_group' => $old_group,'calculation_id'=>$objects->calculation_id]);
            if ($equipment) {
                $equipment->delete();
            }
        }
        $objects_new_group = Objects::find()->where(['number_group' => $group,'calculation_id'=>$objects->calculation_id])->count();
        if ($objects_new_group == 1 && $group != 0)  {
            $equipment = new Equipment();
            $equipment->calculation_id = $objects->calculation_id;
            $equipment->number_group = $group;
            $equipment->name_equipment = 'Новое оборудование';
            $equipment->save();
        }

        return ['output' => $group, 'message' => ''];
    }

    public function actionGetClimat()
    {
        $id = (int )Yii::$app->request->get('id');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $climat = City::findOne($id);
        if ($climat) {
            return ['data' => $climat, 'error' => false];
        }
        return ['error' => true];
    }

    public function actionGetOvBuildingType()
    {
        $id = (int )Yii::$app->request->get('id');
        Yii::$app->response->format = Response::FORMAT_JSON;

        $object = ObjectsOvType::findOne($id);
        $data = ObjectsOvTypeCharacteristic::find()->where(['objects_ov_type_id' => $id])->all();
        
        if ($data) {
            return ['object' => $object, 'data' => $data, 'error' => false];
        }
        return ['error' => true];
    }

    public function actionGetGvsPurposeSystem()
    {
        $id = (int )Yii::$app->request->get('id');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = ObjectsGvsType::findOne($id);
        if ($data) {
            return ['data' => $data, 'error' => false];
        }
        return ['error' => true];
    }

    public function actionGetTechPurposeSystem()
    {
        $id = (int )Yii::$app->request->get('id');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = ObjectsTechType::findOne($id);
        if ($data) {
            return ['data' => $data, 'error' => false];
        }
        return ['error' => true];
    }

    protected function findModel($id)
    {
        if (($model = Calculation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
